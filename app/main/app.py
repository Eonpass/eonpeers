from flask import Blueprint, Flask
from flask_restplus import Api
from werkzeug.middleware.proxy_fix import ProxyFix

from app.main.config import config_by_name
from app.main.controller.company_controller import api as company_ns
from app.main.controller.auth_controller import api as auth_ns
from app.main.controller.user_controller import api as user_ns
from app.main.controller.product_controller import api as product_ns
from app.main.controller.piece_controller import api as piece_ns
from app.main.controller.location_controller import api as location_ns
from app.main.controller.transportsegment_controller import api as transportsegment_ns
from app.main.controller.shipment_controller import api as shipment_ns
from app.main.controller.event_controller import api as event_ns
from app.main.controller.quoterequest_controller import api as quoterequest_ns
from app.main.controller.offer_controller import api as offer_ns
from app.main.controller.booking_controller import api as booking_ns
from app.main.controller.waybill_controller import api as waybill_ns
from app.main.controller.dangerousgood_controller import api as dangerousgood_ns
from app.main.controller.customsinfo_controller import api as customsinfo_ns
from app.main.controller.disclosure_controller import api as disclosure_ns

from app.main.services import db, flask_bcrypt
from app.main.celery import init_celery
from app.main.service.user_service import create_admin_first_startup


def create_app(**kwargs):
    """ create the flask app, then initalise the services: db, bycrypt, celery; finally preapre the api blueprint"""
    config_name=kwargs.get("config_name")
    app = Flask(__name__)
    app.config.from_object(config_by_name[config_name])
    app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1, x_host=1, x_port=1)

    db.init_app(app)
    flask_bcrypt.init_app(app)
    if(kwargs.get("celery")):
      init_celery(kwargs.get("celery"), app)

    blueprint = Blueprint('api', __name__)
    api = Api(blueprint,
              title='EONPEERS',
              version='1.0',
              description='Eonpeers restplus web service with JWT')
    api.add_namespace(company_ns, path='/company')
    api.add_namespace(user_ns, path='/user')
    api.add_namespace(auth_ns)
    api.add_namespace(product_ns, path='/product')
    api.add_namespace(piece_ns, path='/piece')
    api.add_namespace(location_ns, path='/location')
    api.add_namespace(transportsegment_ns, path='/transportsegment')
    api.add_namespace(shipment_ns, path='/shipment')
    api.add_namespace(event_ns, path='/event')
    api.add_namespace(quoterequest_ns, path='/quoterequest')
    api.add_namespace(offer_ns, path='/offer')
    api.add_namespace(booking_ns, path='/booking')
    api.add_namespace(waybill_ns, path='/waybill')
    api.add_namespace(dangerousgood_ns, path='/dangerousgood')
    api.add_namespace(customsinfo_ns, path='/customsinfo')
    api.add_namespace(disclosure_ns, path='/disclosure')
    app.register_blueprint(blueprint)

    app.app_context().push()

    @app.before_first_request
    def before_first_request():
      create_admin_first_startup()



    return app
