import os

basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    DEBUG = False
    SECRET_KEY = os.getenv('SECRET_KEY', 'my_precious_secret_key')

class DevelopmentConfig(Config):
    ADMIN_EMAIL = 'admin@admin.com'
    ADMIN_USERNAME = 'admin'
    ADMIN_PASSWORD = 'password'
    KMI_TYPE = 'DEV'
    VERIFIER_TYPE = 'STD'
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'eonpeers_dev.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    CELERY_BROKER_URL = os.getenv('REDIS_URL', 'redis://localhost:6379')
    CELERY_RESULT_BACKEND = os.getenv('REDIS_URL', 'redis://localhost:6379')
    NOTARIZATION_URL = ''
    NOTARIZATION_USER = ''
    NOTARIZATION_PWD = ''
    LOCAL_FLASK_PORT = 5001
    VERIFY_SSL_CERTS = False


class TestingConfig(Config):
    ADMIN_EMAIL = 'admin@admin.com'
    ADMIN_USERNAME = 'admin'
    ADMIN_PASSWORD = 'password'
    KMI_TYPE = 'DEV'
    VERIFIER_TYPE = 'STD'
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'eonpeers_test.db')
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    CELERY_BROKER_URL = os.getenv('REDIS_URL', 'redis://localhost:6379')
    CELERY_RESULT_BACKEND = os.getenv('REDIS_URL', 'redis://localhost:6379')
    NOTARIZATION_URL = ''
    NOTARIZATION_USER = ''
    NOTARIZATION_PWD = ''
    LOCAL_FLASK_PORT = 5001
    VERIFY_SSL_CERTS = False



class ProductionConfig(Config):
    ADMIN_EMAIL = 'admin@admin.com'
    ADMIN_USERNAME = 'admin'
    ADMIN_PASSWORD = 'password'
    KMI_TYPE = 'PYKMIP'
    VERIFIER_TYPE = 'STD'
    DEBUG = False
    # uncomment the line below to use postgres
    # postgres_local_base = os.environ['DATABASE_URL']
    # SQLALCHEMY_DATABASE_URI = postgres_local_base
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    CELERY_BROKER_URL = 'redis://localhost:6379'
    CELERY_RESULT_BACKEND = 'redis://localhost:6379'
    NOTARIZATION_URL = ''
    NOTARIZATION_USER = ''
    NOTARIZATION_PWD = ''
    LOCAL_FLASK_PORT = 5001
    VERIFY_SSL_CERTS = True


config_by_name = dict(
    dev=DevelopmentConfig,
    test=TestingConfig,
    prod=ProductionConfig
)

key = Config.SECRET_KEY