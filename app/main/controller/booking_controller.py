from flask import request
from flask_restplus import Resource

from app.main.service.booking_service import (get_a_booking, get_all_bookings, save_new_booking, update_booking, export_booking, import_booking)
from app.main.util.dto import BookingDto
from app.main.util.decorator import admin_token_required, token_required
from app.main.util.eonerror import EonError

api = BookingDto.api
_booking = BookingDto.booking
_new_booking = BookingDto.new_booking
_signed_booking = BookingDto.signed_booking
_method_result = BookingDto.method_result

parser = api.parser()
parser.add_argument('Authorization', location='headers', help="Auth token from login")

@api.route('/')
class BookingList(Resource):
    @api.doc('List of bookings registered on this node')
    @api.expect(parser)
    @api.marshal_with(_booking, as_list=True)
    @api.response(400, 'Malformed URL.')
    @api.response(500, 'Internal Server Error.')
    @admin_token_required
    def get(self):
        """Returns bookings, known by this node, in a list, admin only"""
        try:
            return get_all_offers()
        except Exception as e:
            api.abort(500)

    @api.doc('Register a new booking')
    @api.expect(parser, _new_booking, validate=True)
    @api.response(201, 'Booking successfully created.')
    @api.response(400, 'Booking input data is invalid.')
    @api.response(409, 'Booking already exists.')
    @api.response(500, 'Internal Server Error.')
    @admin_token_required
    def post(self):
        """Register a new Booking"""
        data = request.json
        try:
            return save_new_booking(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>')
@api.param('public_id', 'The booking identifier')
class Booking(Resource):
    @api.doc('get a booking')
    @api.expect(parser)
    @api.marshal_with(_booking)
    @api.response(404, 'Booking not found.')
    @api.response(500, 'Internal Server Error.')
    @admin_token_required
    def get(self, public_id):
        """Get details for a single booking""" 
        try:
            return get_a_booking(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

    @api.doc('update a booking, it expects a JSON body with the quote request fields')
    @api.expect(parser)
    @api.response(200, 'Success.')
    @api.response(404, 'Booking not found.')
    @api.response(409, 'Duplicate Offer.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser, _booking)
    @admin_token_required
    def post(self, public_id):
        """Update a booking""" 
        data = request.json
        try:
            return update_booking(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/rpc/import')
class BookingImport(Resource):
    @api.doc('Import a booking from a peer')
    @api.expect(_signed_booking, validate=True)
    @api.response(201, 'Booking successfully created.')
    @api.response(400, 'Booking input data is invalid.')
    @api.response(409, 'Booking already exists.')
    @api.response(500, 'Internal Server Error.')
    #@token_required
    def post(self):
        """Import new booking from a peer"""
        data = request.json
        try:
            return import_booking(data=data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/rpc/export/<public_id>')
@api.param('public_id', 'The booking identifier')
class SendBooking(Resource):
    @api.doc('send booking to the destination (i.e. requesting) company')
    @api.expect(parser)
    @api.response(404, 'Booking not found.')
    @api.response(500, 'Internal Server Error.')
    @admin_token_required
    def post(self, public_id):
        """Send the booking to the destination peer, the one which required the offer""" 
        data = request.json
        try:
            return export_booking(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)
