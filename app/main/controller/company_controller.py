from flask import request
from flask_restplus import Resource

from app.main.service.company_service import (get_a_company, get_all_companies, save_new_company, get_node_owner,mark_company_as_node_owner, update_company, get_manufactured_products_of_a_company, get_branded_products_of_a_company, create_user_on_remote_node, update_local_key)
from app.main.util.dto import CompanyDto, ProductDto
from app.main.util.decorator import admin_token_required, token_required
from app.main.util.eonerror import EonError

api = CompanyDto.api
_company = CompanyDto.company
_new_company = CompanyDto.new_company
_node_owner_company = CompanyDto.node_owner_company
_product = ProductDto.product
_new_key = CompanyDto.new_key

parser = api.parser()
parser.add_argument('Authorization', location='headers', help="Auth token from login")

@api.route('/')
class CompanyList(Resource):
    @api.doc('List of companies registered on this node')
    @api.marshal_with(_company, as_list=True)
    @api.response(400, 'Malformed URL.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def get(self):
        """Returns companies, known by this node, in a list"""
        try:
            return get_all_companies()
        except Exception as e:
                api.abort(500)

    @api.doc('Register a new company')
    @api.expect(parser, _new_company, validate=True)
    @api.response(201, 'Company successfully created.')
    @api.response(400, 'Company input data is invalid.')
    @api.response(409, 'Company already exists.')
    @api.response(500, 'Internal Server Error.')
    @admin_token_required
    def post(self):
        """Register a new Company"""
        data = request.json
        try:
            return save_new_company(data=data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>')
@api.param('public_id', 'The company identifier, can be either the local id or the unique hash (hash of concatenated vat and fiscal country)')
class Company(Resource):
    @api.doc('get a company')
    @api.marshal_with(_company)
    @api.response(404, 'Company not found.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def get(self, public_id):
        """Get details for a single company""" 
        try:
            return get_a_company(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

    @api.doc('update a company, it expects a JSON body with the company fields')
    @api.response(200, 'Success.')
    @api.response(404, 'Company not found.')
    @api.response(409, 'Duplicate Company.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser, _company)
    @admin_token_required
    def post(self, public_id):
        """Update a company""" 
        data = request.json
        try:
            return update_company(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>/rpc/makenodeowner')
@api.param('public_id', 'The company identifier, local id')
class OwnerCompany(Resource):
    @api.doc('mark a company as own, this will make sure the public key is the one connected to the node')
    @api.response(404, 'Company not found.')
    @api.response(409, 'Node already has an owner.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def post(self, public_id):
        """Mark a company as own""" 
        data = request.json
        try:
            return mark_company_as_node_owner(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>/rpc/createuser')
@api.param('public_id', 'The company identifier, local id')
class OwnerCompany(Resource):
    @api.doc('Ask the remote node of this company to create a user')
    @api.response(404, 'Company not found.')
    @api.response(409, 'Node already has a user.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def post(self, public_id):
        """Ask a node to create a user for you""" 
        data = request.json
        try:
            return create_user_on_remote_node(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>/manufacturedproducts')
@api.param('public_id', 'The company identifier')
class ManufacturedProdsOfCompany(Resource):
    @api.doc('get products manufactured by a company')
    @api.marshal_with(_product, as_list=True)
    @api.response(404, 'Company not found.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def get(self, public_id):
        try:
            return get_manufactured_products_of_a_company(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>/brandedproducts')
@api.param('public_id', 'The company identifier')
class BrandedProdsOfCompany(Resource):
    @api.doc('get products which brand is owned by a company')
    @api.marshal_with(_product, as_list=True)
    @api.response(404, 'Company not found.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def get(self, public_id):
        try:
            return get_branded_products_of_a_company(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/node-owner')
class NodeOwnerCompany(Resource):
    @api.doc('get the company owning this node')
    @api.marshal_with(_node_owner_company)
    @api.response(500, 'Internal Server Error.')
    #public service, TODO: understand whether to hold a registry of latest IPs and rate limit them, or make this not public
    def get(self):
        """Get details of the company running this node""" 
        try:
            return get_node_owner()
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/rpc/updatelocalkey')
class OwnerCompany(Resource):
    @api.doc('DEMO ONLY - update the local key use by the node, nodes in production are not using local keys anyway')
    @api.response(200, 'ACK')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser, _new_key)
    @admin_token_required
    def post(self):
        """Update the local key of the node - used only in dev and testing""" 
        data = request.json
        try:
            return update_local_key(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)