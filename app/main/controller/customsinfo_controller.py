from flask import request
from flask_restplus import Resource

from app.main.service.customsinfo_service import (get_a_customs_info, save_new_customs_info, upsert_a_customs_info)
from app.main.util.dto import CustomsInfoDto
from app.main.util.decorator import admin_token_required, token_required
from app.main.util.eonerror import EonError

api = CustomsInfoDto.api
_customs_info = CustomsInfoDto.customs_info
_new_customs_info = CustomsInfoDto.new_customs_info

parser = api.parser()
parser.add_argument('Authorization', location='headers', help="Auth token from login")

@api.route('/')
class CustomsInfoList(Resource):

    @api.doc('Register a new Customs Info')
    @api.expect(parser, _new_customs_info, validate=True)
    @api.response(201, 'Customs Info successfully created.')
    @api.response(400, 'Customs Info input data is invalid.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def post(self):
        """Register a new Customs Info"""
        data = request.json
        try:
            return save_new_customs_info(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>')
@api.param('public_id', 'The Customs Info identifier public_id')
class Event(Resource):
    @api.doc('get an Customs Info')
    @api.marshal_with(_customs_info)
    @api.response(404, 'Customs Info not found.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def get(self, public_id):
        """Get details for a single Customs Info""" 
        try:
            return get_a_customs_info(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

    @api.doc('update a Customs Info, it expects a JSON body with the Customs Info fields')
    @api.response(200, 'Success.')
    @api.response(404, 'Customs Info not found.')
    @api.response(409, 'Duplicate or invalid data.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser, _customs_info)
    @admin_token_required
    def post(self, public_id):
        """Update a Customs Info""" 
        data = request.json
        try:
            return upsert_a_customs_info(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)