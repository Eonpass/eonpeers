from flask import request
from flask_restplus import Resource

from app.main.service.dangerousgood_service import (get_a_dangerous_good, save_new_dangerous_good, upsert_a_dangerous_good)
from app.main.util.dto import DangerousGoodDto
from app.main.util.decorator import admin_token_required, token_required
from app.main.util.eonerror import EonError

api = DangerousGoodDto.api
_dangerous_good = DangerousGoodDto.dangerous_good
_new_dangerous_good = DangerousGoodDto.new_dangerous_good

parser = api.parser()
parser.add_argument('Authorization', location='headers', help="Auth token from login")

@api.route('/')
class DangerousGoodList(Resource):

    @api.doc('Register a new Dangerous Good')
    @api.expect(parser, _new_dangerous_good, validate=True)
    @api.response(201, 'Dangerous Good successfully created.')
    @api.response(400, 'Dangerous Good input data is invalid.')
    @api.response(500, 'Internal Server Error.')
    @admin_token_required
    def post(self):
        """Register a new Dangerous Good"""
        data = request.json
        try:
            return save_new_dangerous_good(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>')
@api.param('public_id', 'The Dangerous Good identifier public_id')
class Event(Resource):
    @api.doc('get a DangerousGood')
    @api.marshal_with(_dangerous_good)
    @api.response(404, 'Dangerous Good not found.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def get(self, public_id):
        """Get details for a single DG""" 
        try:
            return get_a_dangerous_good(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

    @api.doc('update a Dangerous Good, it expects a JSON body with the DG fields')
    @api.response(200, 'Success.')
    @api.response(404, 'Dangerous Good not found.')
    @api.response(409, 'Duplicate or invalid data.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser, _dangerous_good)
    @admin_token_required
    def post(self, public_id):
        """Update a dangerous good""" 
        data = request.json
        try:
            return upsert_a_dangerous_good(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)