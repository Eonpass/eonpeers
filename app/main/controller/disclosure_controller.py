from flask import request
from flask_restplus import Resource

from app.main.service.disclosure_service import (save_new_disclosure, update_disclosure, get_a_disclosure)
from app.main.util.dto import DisclosureDto
from app.main.util.decorator import admin_token_required, token_required
from app.main.util.eonerror import EonError

api = DisclosureDto.api
_disclosure = DisclosureDto.disclosure
_new_disclosure = DisclosureDto.new_disclosure

parser = api.parser()
parser.add_argument('Authorization', location='headers', help="Auth token from login")

@api.route('/')
class DisclosureList(Resource):
    @api.doc('Register a new disclosure')
    @api.expect(parser, _new_disclosure, validate=True)
    @api.response(201, 'Disclosure successfully created.')
    @api.response(400, 'Disclosure input data is invalid.')
    @api.response(409, 'Disclosure already exists.')
    @api.response(500, 'Internal Server Error.')
    @admin_token_required
    def post(self):
        """Register a new Disclosure"""
        data = request.json
        try:
            return save_new_disclosure(data=data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<shipment_id>/<user_id>')
@api.param('shipment_id', 'The shipment identifier')
@api.param('user_id', 'Id of the user which required the disclosure')
class Disclosure(Resource):
    @api.doc('update a disclosure')
    @api.expect(parser, _disclosure, validate=True)
    @api.response(404, 'Disclosure not found.')
    @api.response(500, 'Internal Server Error.')
    @admin_token_required
    def post(self, shipment_id, user_id):
        """Update details for a disclosure""" 
        data = request.json
        try:
            return update_disclosure(data=data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

    @api.doc('get a disclosure')
    @api.expect(parser)
    @api.marshal_with(_disclosure)
    @api.response(404, 'Disclosure not found.')
    @api.response(500, 'Internal Server Error.')
    @admin_token_required
    def get(self, shipment_id, user_id):
        """Get details for a single disclosure""" 
        try:
            return get_a_disclosure(shipment_id, user_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)