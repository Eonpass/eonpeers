from flask import request
from flask_restplus import Resource

from app.main.service.event_service import (get_an_event, save_new_event)
from app.main.util.dto import EventDto
from app.main.util.decorator import admin_token_required, token_required
from app.main.util.eonerror import EonError

api = EventDto.api
_event = EventDto.event
_new_event = EventDto.new_event

parser = api.parser()
parser.add_argument('Authorization', location='headers', help="Auth token from login")

@api.route('/')
class EventList(Resource):

    @api.doc('Register a new event')
    @api.expect(parser, _new_event, validate=True)
    @api.response(201, 'Event successfully created.')
    @api.response(400, 'Event input data is invalid.')
    @api.response(500, 'Internal Server Error.')
    @admin_token_required
    def post(self):
        """Register a new Event"""
        data = request.json
        try:
            return save_new_event(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>')
@api.param('public_id', 'The event identifier public_id')
class Event(Resource):
    @api.doc('get an event')
    @api.marshal_with(_event)
    @api.response(404, 'Event not found.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def get(self, public_id):
        """Get details for a single piece""" 
        try:
            return get_an_event(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)