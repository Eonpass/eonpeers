from flask import request
from flask_restplus import Resource

from app.main.service.offer_service import (get_an_offer, get_all_offers, save_new_offer, update_offer, export_offer, import_offer, accept_offer, register_offer_acceptance)
from app.main.util.dto import OfferDto
from app.main.util.decorator import admin_token_required, token_required
from app.main.util.eonerror import EonError

api = OfferDto.api
_offer = OfferDto.offer
_new_offer = OfferDto.new_offer
_signed_full_offer = OfferDto.signed_full_offer
_method_result = OfferDto.method_result

parser = api.parser()
parser.add_argument('Authorization', location='headers', help="Auth token from login")

@api.route('/')
class OfferList(Resource):
    @api.doc('List of offers registered on this node')
    @api.marshal_with(_offer, as_list=True)
    @api.response(400, 'Malformed URL.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def get(self):
        """Returns offers, known by this node, in a list, admin only"""
        try:
            return get_all_offers()
        except Exception as e:
            api.abort(500)

    @api.doc('Register a new offer')
    @api.expect(parser, _new_offer, validate=True)
    @api.response(201, 'Offer successfully created.')
    @api.response(400, 'Offer input data is invalid.')
    @api.response(409, 'Offer already exists.')
    @api.response(500, 'Internal Server Error.')
    @admin_token_required
    def post(self):
        """Register a new Offer"""
        data = request.json
        try:
            return save_new_offer(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>')
@api.param('public_id', 'The offer identifier')
class Offer(Resource):
    @api.doc('get an offer')
    @api.marshal_with(_offer)
    @api.response(404, 'Offer not found.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def get(self, public_id):
        """Get details for a single offer""" 
        try:
            return get_an_offer(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

    @api.doc('update an offer, it expects a JSON body with the offer fields')
    @api.response(200, 'Success.')
    @api.response(404, 'Offer not found.')
    @api.response(409, 'Duplicate Offer.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser, _offer)
    @admin_token_required
    def post(self, public_id):
        """Update an offer""" 
        data = request.json
        try:
            return update_offer(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/rpc/import')
class OfferImport(Resource):
    @api.doc('Import an offer from a peer')
    @api.expect(parser) #, _signed_full_offer, validate=True) .. cannot validate numbers with flaskrestplus, subclass the parser or upgrade to flask 2
    @api.response(201, 'Offer successfully created.')
    @api.response(400, 'Offer input data is invalid.')
    @api.response(409, 'Offer already exists.')
    @api.response(500, 'Internal Server Error.')
    #@token_required #signatures are checked anyway, regardless of access tokens
    def post(self):
        """Import new Offer from a peer"""
        data = request.json
        try:
            return import_offer(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/rpc/export/<public_id>')
@api.param('public_id', 'The offer identifier')
class SendOffer(Resource):
    @api.doc('send offer to the destination (i.e. requesting) company')
    @api.response(404, 'Offer not found.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def post(self, public_id):
        """Send the offer to the destination peer, the one which required the offer""" 
        data = request.json
        try:
            return export_offer(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>/rpc/accept')
@api.param('public_id', 'The offer identifier')
class AcceptOffer(Resource):
    @api.doc('Accept the offer and notify the provider company')
    @api.response(404, 'Offer not found.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @api.marshal_with(_method_result)
    @admin_token_required
    def post(self, public_id):
        """Accept the offer and notify the provider company""" 
        data = request.json
        try:
            return accept_offer(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>/rpc/registeracceptance')
@api.param('public_id', 'The offer identifier')
class RegisterOfferAcceptance(Resource):
    @api.doc('Register that the requester approved the offer')
    @api.response(404, 'Offer not found.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @api.marshal_with(_method_result)
    #@admin_token_required #this call is executed in the p2p flow, double check that there's a signature check also here
    def post(self, public_id):
        """Register that the requester approved the offer""" 
        data = request.json
        try:
            return register_offer_acceptance(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)