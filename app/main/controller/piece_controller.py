from flask import request
from flask_restplus import Resource

from app.main.service.product_service import (get_a_product, get_all_products, save_new_product, update_product)
from app.main.service.piece_service import (get_a_piece, get_all_pieces, save_new_piece, upsert_piece, get_piece_dangerous_goods, get_piece_customs_info)
from app.main.util.dto import ProductDto
from app.main.util.dto import PieceDto
from app.main.util.dto import CustomsInfoDto
from app.main.util.dto import DangerousGoodDto
from app.main.util.decorator import admin_token_required, token_required
from app.main.util.eonerror import EonError

api = PieceDto.api
_piece = PieceDto.piece
_new_piece = PieceDto.new_piece
_upsert_piece = PieceDto.upsert_piece
_customs_info = CustomsInfoDto.customs_info
_dangerous_good = DangerousGoodDto.dangerous_good

parser = api.parser()
parser.add_argument('Authorization', location='headers', help="Auth token from login")

@api.route('/')
class PieceList(Resource):
    @api.doc('List of pieces registered on this node')
    @api.marshal_with(_piece, as_list=True)
    @api.response(400, 'Malformed URL.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def get(self):
        """Returns pieces, known by this node and visible to the logged user, in a list"""
        try:
            return get_all_pieces()
        except Exception as e:
                api.abort(500)

    @api.doc('Register a new piece')
    @api.expect(parser, _new_piece, validate=True)
    @api.response(201, 'Piece successfully created.')
    @api.response(400, 'Piece input data is invalid.')
    @api.response(409, 'Piece already exists.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def post(self):
        """Register a new Piece"""
        data = request.json
        try:
            return save_new_piece(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>')
@api.param('public_id', 'The piece identifier public_id or upid if different')
class Piece(Resource):
    @api.doc('get a piece')
    @api.marshal_with(_piece)
    @api.response(404, 'Piece not found.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def get(self, public_id):
        """Get details for a single piece""" 
        try:
            return get_a_piece(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

    @api.doc('update a piece, it expects a JSON body with the piece fields')
    @api.response(200, 'Success.')
    @api.response(404, 'Piece not found.')
    @api.response(409, 'Duplicate Piece.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser, _upsert_piece)
    @admin_token_required
    def post(self, public_id):
        """Update a piece""" 
        data = request.json
        try:
            return upsert_piece(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>/customsinfo')
@api.param('public_id', 'Customs Info connected to this piece')
class CustomsinfoPieceList(Resource):
    @api.doc('get all Customs Info of this piece')
    @api.marshal_with(_customs_info, as_list=True)
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def get(self, public_id):
        """Get customs info for a single piece""" 
        try:
            return get_piece_customs_info(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>/dangerousgoods')
@api.param('public_id', 'Dangerous Goods connected to this piece')
class DangerousgoodPieceList(Resource):
    @api.doc('get all Dangerous Goods of this piece')
    @api.marshal_with(_dangerous_good, as_list=True)
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def get(self, public_id):
        """Get dangerous goods for a single piece""" 
        try:
            return get_piece_dangerous_goods(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)
