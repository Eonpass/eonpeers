from flask import request
from flask_restplus import Resource

from app.main.service.product_service import (get_a_product, get_all_products, save_new_product, update_product)
from app.main.util.dto import ProductDto
from app.main.util.decorator import admin_token_required, token_required
from app.main.util.eonerror import EonError

api = ProductDto.api
_product = ProductDto.product
_new_product = ProductDto.new_product
_update_product = ProductDto.update_product

parser = api.parser()
parser.add_argument('Authorization', location='headers', help="Auth token from login")

@api.route('/')
class ProductList(Resource):
    @api.doc('List of products registered on this node')
    @api.marshal_with(_product, as_list=True)
    @api.response(400, 'Malformed URL.')
    @api.response(500, 'Internal Server Error.')
    @admin_token_required
    def get(self):
        """Returns products, known by this node, in a list"""
        try:
            return get_all_products()
        except Exception as e:
                api.abort(500)

    @api.doc('Register a new product')
    @api.expect(parser, _new_product, validate=True)
    @api.response(201, 'Product successfully created.')
    @api.response(400, 'Product input data is invalid.')
    @api.response(409, 'Product already exists.')
    @api.response(500, 'Internal Server Error.')
    @admin_token_required
    def post(self):
        """Register a new Product"""
        data = request.json
        try:
            return save_new_product(data=data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>')
@api.param('public_id', 'The product identifier, can be either the local id or the unique hash (hash of concatenated product identifier and product descirption)')
class Product(Resource):
    @api.doc('get a product')
    @api.marshal_with(_product)
    @api.response(404, 'Product not found.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def get(self, public_id):
        """Get details for a single product""" 
        try:
            return get_a_product(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

    @api.doc('update a product, it expects a JSON body with the product fields')
    @api.response(200, 'Success.')
    @api.response(404, 'Product not found.')
    @api.response(409, 'Duplicate Product.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser, _update_product)
    @admin_token_required
    def post(self, public_id):
        """Update a product""" 
        data = request.json
        try:
            return update_product(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

