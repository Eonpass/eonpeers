from flask import request
from flask_restplus import Resource

from app.main.service.quoterequest_service import (get_a_quote_request, get_all_quote_requests, import_quote_request, save_new_quote_request, update_quote_request, export_quote_request, prepare_quote_request_for_subcontractor_all_pieces)
from app.main.util.dto import QuoteRequestDto, TransportSegmentDto
from app.main.util.decorator import admin_token_required, token_required
from app.main.util.eonerror import EonError

api = QuoteRequestDto.api
_quote_request = QuoteRequestDto.quote_request
_new_quote_request = QuoteRequestDto.new_quote_request
_signed_full_quote_request = QuoteRequestDto.signed_full_quote_request
_new_transport_segment_for_subcontractor = TransportSegmentDto.new_transport_segment_for_subcontractor
_method_result = QuoteRequestDto.method_result

parser = api.parser()
parser.add_argument('Authorization', location='headers', help="Auth token from login")

@api.route('/')
class QuoteReuquestList(Resource):
    @api.doc('List of quote requests registered on this node')
    @api.marshal_with(_quote_request, as_list=True)
    @api.response(400, 'Malformed URL.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def get(self):
        """Returns quote requests, known by this node, in a list"""
        try:
            return get_all_quote_requests()
        except Exception as e:
            api.abort(500)

    @api.doc('Register a new quote request')
    @api.expect(parser, _new_quote_request, validate=True)
    @api.response(201, 'Quote Request successfully created.')
    @api.response(400, 'Quote Request input data is invalid.')
    @api.response(409, 'Quote Request already exists.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def post(self):
        """Register a new Quote Request"""
        data = request.json
        try:
            return save_new_quote_request(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>')
@api.param('public_id', 'The quote request identifier')
class QuoteRequest(Resource):
    @api.doc('get a quote request')
    @api.marshal_with(_quote_request)
    @api.response(404, 'Quote Request not found.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def get(self, public_id):
        """Get details for a single quote request""" 
        try:
            return get_a_quote_request(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

    @api.doc('update a quote request, it expects a JSON body with the quote request fields')
    @api.response(200, 'Success.')
    @api.response(404, 'Quote Request not found.')
    @api.response(409, 'Duplicate Quote Request.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser, _quote_request)
    @admin_token_required
    def post(self, public_id):
        """Update a quote request""" 
        data = request.json
        try:
            return update_quote_request(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)


@api.route('/rpc/import')
class QuoteRequestImport(Resource):
    @api.doc('Import a quote request from a peer')
    @api.expect(parser) #removed: _signed_full_quote_request, validate=True; flask validation requires empty fields to be "" and has problem with numbers..
    @api.response(201, 'Quote Request successfully created.')
    @api.response(400, 'Quote Request input data is invalid.')
    @api.response(409, 'Quote Request already exists.')
    @api.response(500, 'Internal Server Error.')
    #@token_required #NOTE: import checks signatures, noone can impersonate an import - token is only for rate-limiting/spam prevention
    def post(self):
        """Import new Quote Request from a peer"""
        data = request.json
        try:
            return import_quote_request(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/rpc/export/<public_id>')
@api.param('public_id', 'The quote request identifier')
class SendQuoteRequest(Resource):
    @api.doc('send quote request to the destination (i.e. offering) company')
    @api.response(404, 'Quote Request not found.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def post(self, public_id):
        """Send the quote request to the destination peer, the one which will provide the offer""" 
        data = request.json
        try:
            return export_quote_request(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/rpc/cloneforsubcontractor/<public_id>')
@api.param('public_id', 'The quote request identifier')
class SendQuoteRequest(Resource):
    @api.doc('Create a clone of the quote request with a different transport segment for a subcontractor')
    @api.response(404, 'Quote Request not found.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser, _new_transport_segment_for_subcontractor, validate=True)
    @admin_token_required
    def post(self, public_id):
        """Create a clone of the quote request to be sent to a subcontractor with a different transport segment""" 
        data = request.json
        try:
            return prepare_quote_request_for_subcontractor_all_pieces(data, public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

