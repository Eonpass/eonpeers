from flask import request
from flask_restplus import Resource

from app.main.service.shipment_service import (get_a_shipment, get_all_shipments, delete_shipment, update_shipment, save_new_shipment, get_shipment_events, request_notarisation, notarise, get_proof, get_timestamps)
from app.main.util.dto import ShipmentDto, MethodResultDto, EventDto
from app.main.util.decorator import admin_token_required, token_required, shipment_disclosure_required
from app.main.util.eonerror import EonError

from app.main.service.auth_helper import Auth

api = ShipmentDto.api
_shipment = ShipmentDto.shipment
_upd_shipment = ShipmentDto.upd_shipment
_new_shipment = ShipmentDto.new_shipment
_import_shipment = ShipmentDto.import_shipment
_method_result = MethodResultDto.method_result
_event = EventDto.event
_event_page = EventDto.paged_event_list

parser = api.parser()
parser.add_argument('Authorization', location='headers', help="Auth token from login")

@api.route('/')
class ShipmentList(Resource):
    @api.doc('List of shipments registered on this node')
    @api.marshal_with(_shipment, as_list=True)
    @api.response(400, 'Malformed URL.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @token_required
    def get(self):
        """Returns shipments, known by this node, in a list"""
        try:
            data, status = Auth.get_logged_in_user(request)
            token = data.get('data')
            return get_all_shipments(token)
        except Exception as e:
            api.abort(500)

    @api.doc('Register a new shipment')
    @api.expect(parser, _new_shipment, validate=True)
    @api.response(201, 'Shipment successfully created.')
    @api.response(400, 'Shipment input data is invalid.')
    @api.response(409, 'Shipment already exists.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def post(self):
        """Register a new Shipment"""
        data = request.json
        try:
            return save_new_shipment(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>')
@api.param('public_id', 'The shipment identifier')
class Shipment(Resource):
    @api.doc('get a shipment')
    @api.marshal_with(_shipment)
    @api.response(404, 'Shipment not found.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @shipment_disclosure_required
    def get(self, public_id):
        """Get details for a single shipment""" 
        try:
            return get_a_shipment(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

    @api.doc('update a shipment, it expects a JSON body with the shipment fields')
    @api.response(200, 'Success.')
    @api.response(404, 'Shipment not found.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser, _upd_shipment)
    @admin_token_required
    def post(self, public_id):
        """Update a shipment""" 
        data = request.json
        try:
            return update_shipment(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

    @api.doc('delete a shipment, if there are no Quote Requests attached to it, otherwise start delete from QR')
    @api.response(200, 'Success.')
    @api.response(404, 'Shipment not found.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def delete(self, public_id):
        """Delete a shipment""" 
        try:
            return delete_shipment(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>/events')
@api.param('public_id', 'Id of the shipment for which we get connected events')
class ShipmentEventList(Resource):
    @api.doc('get all events of this shipment')
    @api.doc(params={
    'page': {'description': 'desired page number, defaults to 1', 'in': 'query', 'type': 'int'},
    'page_size': {'description': 'desired number of records per page, defaults to 20', 'in': 'query', 'type': 'int'}
    })
    @api.marshal_with(_event_page)
    @api.response(404, 'Shipment not found.')
    @api.response(400, 'Wrong page arguments.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @shipment_disclosure_required
    def get(self, public_id):
        """Get events for a single shipment""" 
        try:
            return get_shipment_events(public_id, request.args)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)


@api.route('/<public_id>/timestamps')
@api.param('public_id', 'Id of the shipment for which we get the Blockchain proof')
class ShipmentProof(Resource):
    @api.doc('get the proof for this shipment')
    @api.response(404, 'Shipment not found.')
    @api.response(400, 'Wrong page arguments.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @shipment_disclosure_required
    def get(self, public_id):
        """Get the blockchain proofs for a shipment""" 
        try:
            return get_timestamps(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/rpc/requestnotarisation/<public_id>')
@api.param('public_id', 'The shipment identifier')
class RequestNotarisationShipment(Resource):
    @api.doc('Call the notarisation service and get a session')
    @api.response(404, 'Shipment not found.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def post(self, public_id):
        """Call the notarisation service and get a session to attach this item to""" 
        try:
            return request_notarisation(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/rpc/notarise/<public_id>')
@api.param('public_id', 'The shipment identifier')
class NotariseShipment(Resource):
    @api.doc('Call the notarisation service and lock the session, write to blockchain')
    @api.response(404, 'Shipment not found.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def post(self, public_id):
        """Call the notarisation service, lock the session and write to blockchain""" 
        try:
            return notarise(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)
