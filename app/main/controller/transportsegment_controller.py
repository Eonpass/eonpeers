from flask import request
from flask_restplus import Resource

from app.main.service.transportsegment_service import (get_a_transport_segment, save_new_transport_segment, upsert_a_transport_segment)
from app.main.util.dto import TransportSegmentDto
from app.main.util.decorator import admin_token_required, token_required
from app.main.util.eonerror import EonError

api = TransportSegmentDto.api
_transport_segment = TransportSegmentDto.transport_segment
_new_transport_segment = TransportSegmentDto.new_transport_segment

parser = api.parser()
parser.add_argument('Authorization', location='headers', help="Auth token from login")

@api.route('/')
class TransportSegmentList(Resource):
    @api.doc('Register a new transport segment')
    @api.expect(parser, _new_transport_segment, validate=True)
    @api.response(201, 'Transport Segment  successfully created.')
    @api.response(400, 'Transport Segment  input data is invalid.')
    @api.response(409, 'Transport Segment  already exists.')
    @api.response(500, 'Internal Server Error.')
    @admin_token_required
    def post(self):
        """Register a new Transport Segment """
        data = request.json
        try:
            return save_new_transport_segment(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)


@api.route('/<public_id>')
@api.param('public_id', 'The transport segment identifier')
class TransportSegment(Resource):
    @api.doc('get a transport segment')
    @api.expect(parser)
    @api.marshal_with(_transport_segment)
    @api.response(404, 'Transport Segment not found.')
    @api.response(500, 'Internal Server Error.')
    @admin_token_required
    def get(self, public_id):
        """Get details for a single transport segment""" 
        try:
            return get_a_transport_segment(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

    @api.doc('Update a transport segment, it expects a JSON body with the TS fields')
    @api.response(200, 'Success.')
    @api.response(404, 'Transport Segment not found.')
    @api.response(409, 'Duplicate Transport Segment.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser, _transport_segment)
    @admin_token_required
    def post(self, public_id):
        """Update a transport segment""" 
        data = request.json
        try:
            return upsert_a_transport_segment(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)