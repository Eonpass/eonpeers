from flask import request
from flask_restplus import Resource

from app.main.service.waybill_service import (get_a_waybill, get_all_waybills, save_new_waybill)
from app.main.util.dto import WaybillDto
from app.main.util.decorator import admin_token_required, token_required
from app.main.util.eonerror import EonError

api = WaybillDto.api
_waybill = WaybillDto.waybill
_new_waybill = WaybillDto.new_waybill

parser = api.parser()
parser.add_argument('Authorization', location='headers', help="Auth token from login")

@api.route('/')
class WaybillList(Resource):
    @api.doc('List of waybills registered on this node')
    @api.marshal_with(_waybill, as_list=True)
    @api.response(400, 'Malformed URL.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def get(self):
        """Returns waybills, known by this node, in a list"""
        try:
            return get_all_waybills()
        except Exception as e:
            api.abort(500)

    @api.doc('Register a new waybill')
    @api.expect(parser, _new_waybill, validate=True)
    @api.response(201, 'Waybill successfully created.')
    @api.response(400, 'Waybill input data is invalid.')
    @api.response(409, 'Waybill already exists.')
    @api.response(500, 'Internal Server Error.')
    @admin_token_required
    def post(self):
        """Register a new Waybill"""
        data = request.json
        try:
            return save_new_waybill(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)


@api.route('/<public_id>')
@api.param('public_id', 'The waybill local identifier')
class Waybill(Resource):
    @api.doc('get a waybill')
    @api.marshal_with(_waybill)
    @api.response(404, 'Waybill not found.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def get(self, public_id):
        """Get details for a single waybill""" 
        try:
            return get_a_waybill(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)


