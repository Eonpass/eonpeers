import datetime
import json

from app.main.model.company import Company
from app.main.services import db
from app.main.util.eonerror import EonError


def work_validate_company_response(response, public_id):
    """
    Once the remote node replies to the GET node-owner, use the data to update the company

    """
    last_method = "validate_company"
    response_time = str(datetime.datetime.now(datetime.timezone.utc))
    response_status = str(response.status_code)
    response_message = "OK, validate company payload received" if response.status_code==200 else "KO, something went wrong"
    remote_company = response.json()
    response_message = compare_new_company(public_id, remote_company)
    latest_log = response_time+"; "+last_method+"; "+response_status+"; "+response_message
    local_company = Company.query.filter_by(public_id=public_id).first()
    if local_company:
        local_company.latest_log = response_time+"; "+last_method+"; "+response_status+"; "+response_message
        _save_changes(local_company)
    return response_message

def work_validate_company_excpetion(e, public_id):
    """
    Once the remote node replies to the GET node-owner, use the expcetion to update the company status
    """
    last_method = "validate_company"
    response_time = str(datetime.datetime.now(datetime.timezone.utc))
    response_status = str(500)
    response_message = "HTTPConnection error" #there may be more details inside Exception
    local_company = Company.query.filter_by(public_id=public_id).first()
    if local_company:
        local_company.latest_log = response_time+"; "+last_method+"; "+response_status+"; "+response_message
        _save_changes(local_company)
    return response_message


def compare_new_company(local_public_id, remote_company):
    """
    Compare the two companies, check if we already have a duplicate

    this method is called by the task util "validate_new_company", it's detached in order to test the logics withou needing the P2P setup
    #BUILD NOTE: this function cannot import from company_service, because it would create a ciruclar reference

    Parameters
    ----------
    local_public_id: str
        the local id of the company 
    remote_company: dict
        the JSON returned from the remote system with the additional latest_log field

    Response
    --------
    operation_log: str
        a string describing what happened during the validation, this string is only useful for debug purposes and logging within docker (where celery runs on its own)

    Raises
    ------
    """
    operation_log = ""
    local_company = Company.query.filter_by(public_id=local_public_id).first()
    buffer_company = local_company
    #double check that we don't already have the incoming vat and country id, if so, update THAT company and destroy this one, 
    #notice that within "validate_new_company" the local company is fresh and shouldn't have any connecte objects
    local_company_dup = Company.query.filter_by(vat_number = remote_company["vat_number"], fiscal_country = remote_company["fiscal_country"]).first()
    if(local_company_dup and local_company_dup.public_id!=local_company.public_id):
        #we have a duplicate, destroy local_company and work on the dup which already had the correct fields
        buffer_company = local_company_dup
        #move related products to the dup
        if(len(local_company.manufactured_products)>0):
            for manufactured in local_company.manufactured_products:
                manufactured.manufacturer = buffer_company
        if(len(local_company.branded_products)>0):
            for manufactured in local_company.branded_products:
                manufactured.brand_owner = buffer_company

        db.session.delete(local_company)
        db.session.commit()
        operation_log+="dup company found and removed;"

    #buffer_company.public_id = remote_company["public_id"] #harmonize the public_id? If so, we should update all the relationships..?

    fields = ['name','vat_number','public_key', 'eori_number','fiscal_address','fiscal_city','fiscal_province','fiscal_country','fiscal_zip', 'latest_log']
    #TODO: discuss what is the desired behaviour in case of conflict, remote (since it's the owner) for now wins regardless
    for field in fields:
        if(getattr(buffer_company, field) == None and remote_company.get(field,None)):
            setattr(buffer_company, field, remote_company[field])
        if(getattr(buffer_company, field) != remote_company.get(field,None) and remote_company.get(field,None)):
            setattr(buffer_company, field, remote_company[field])

    _save_changes(buffer_company)
    operation_log+="company changes saved;"
    return operation_log


def _save_changes(data):
    db.session.add(data)
    db.session.commit()

