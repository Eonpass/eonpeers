import datetime
import json

from app.main.model.offer import Offer
from app.main.services import db
from app.main.util.eonerror import EonError


def work_offer_acceptance_response(response, public_id):
    """
    Once the remote node replies to an offer acceptance, use the data to update the values on the offer
    """
    last_method = "accept_offer"
    response_time = str(datetime.datetime.now(datetime.timezone.utc))
    response_status = str(response.status_code)
    response_message = "OK, Received by Offer Provider" if response.status_code==200 else "KO, something went wrong"
    local_offer = Offer.query.filter_by(public_id=public_id).first()
    if local_offer:
        local_offer.latest_log = response_time+"; "+last_method+"; "+response_status+"; "+response_message
        _save_changes(local_offer)
    return response_message

def work_offer_acceptance_excpetion(e, public_id):
    """
    Once the remote node replies to an offer acceptance, use the data to update the values on the offer
    """
    last_method = "accept_offer"
    response_time = str(datetime.datetime.now(datetime.timezone.utc))
    response_status = str(500)
    response_message = "HTTPConnection error" #there may be more details inside Exception
    local_offer = Offer.query.filter_by(public_id=public_id).first()
    if local_offer:
        local_offer.latest_log = response_time+"; "+last_method+"; "+response_status+"; "+response_message
        _save_changes(local_offer)
    return response_message    


def work_send_offer_response(response, public_id):
    """
    Save the correct execution of send quote request
    """
    last_method = "send_offer"
    response_time = str(datetime.datetime.now(datetime.timezone.utc))
    response_status = str(response.status_code)
    response_message = "OK, offer sent" if response.status_code==200 else "KO, something went wrong"
    local_offer = Offer.query.filter_by(public_id=public_id).first()
    if local_offer:
        local_offer.latest_log = response_time+"; "+last_method+"; "+response_status+"; "+response_message
    return response_message


def work_send_offer_excpetion(e, public_id):
    """
    Save the error execution of send offer
    """
    print(e)
    last_method = "send_offer"
    response_time = str(datetime.datetime.now(datetime.timezone.utc))
    response_status = str(500)
    response_message = "HTTPConnection error" #there may be more details inside Exception
    local_offer = Offer.query.filter_by(public_id=public_id).first()
    if local_offer:
        local_offer.latest_log = response_time+"; "+last_method+"; "+response_status+"; "+response_message
    return response_message



def _save_changes(data):
    db.session.add(data)
    db.session.commit()

