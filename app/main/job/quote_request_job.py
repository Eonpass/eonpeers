import datetime
import json

from app.main.model.quoterequest import QuoteRequest
from app.main.services import db
from app.main.util.eonerror import EonError


def work_send_quoterequest_response(response, public_id):
    """
    Save the correct execution of send quote request
    """
    last_method = "send_quote_request"
    response_time = str(datetime.datetime.now(datetime.timezone.utc))
    response_status = str(response.status_code)
    response_message = response.text
    local_qr = QuoteRequest.query.filter_by(public_id=public_id).first()
    if local_qr:
        local_qr.latest_log = response_time+"; "+last_method+"; "+response_status+"; "+response_message
    _save_changes(local_qr)
    return response.text


def work_send_quoterequest_excpetion(e, public_id):
    """
    Save the error execution of send quote request
    """
    last_method = "send_quote_request"
    response_time = str(datetime.datetime.now(datetime.timezone.utc))
    response_status = str(500)
    response_message = "HTTPConnection error" #there may be more details inside Exception
    local_qr = QuoteRequest.query.filter_by(public_id=public_id).first()
    if local_qr:
        local_qr.latest_log = response_time+"; "+last_method+"; "+response_status+"; "+response_message
    _save_changes(local_qr)
    return response_message



def _save_changes(data):
    db.session.add(data)
    db.session.commit()

