import datetime
import json

from app.main.model.waybill import Waybill
from app.main.services import db
from app.main.util.eonerror import EonError

def work_waybill_import_response(response, public_id):
    """
    Once a node starts the booking export, it will call the booking import from the target node, here we register the result
    """
    last_method = "waybill_export"
    response_time = str(datetime.datetime.now(datetime.timezone.utc))
    response_status = str(response.status_code)
    response_message = "OK, Imported by Requester" if response.status_code==200 else "KO, something went wrong"

    local_waybill = Waybill.query.filter_by(public_id=public_id).first()
    if local_waybill:
        local_waybill.latest_log = response_time+"; "+last_method+"; "+response_status+"; "+response_message
        _save_changes(local_waybill)

def work_waybill_import_exception(e, public_id):
    """
    Once a node starts the booking export, it will call the booking import from the target node, here we register the result in case of exception
    """
    last_method = "booking_export"
    response_time = str(datetime.datetime.now(datetime.timezone.utc))
    response_status = str(500)
    response_message = "HTTPConnection error" #there may be more details inside Exception
    local_waybill = Waybill.query.filter_by(public_id=public_id).first()
    if local_waybill:
        local_waybill.latest_log = response_time+"; "+last_method+"; "+response_status+"; "+response_message
        _save_changes(local_waybill)


def _save_changes(data):
    db.session.add(data)
    db.session.commit()

