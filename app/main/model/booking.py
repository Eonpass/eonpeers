import datetime
from sqlalchemy import Column
from sqlalchemy.orm  import relationship

from app.main.config import key
from app.main.services import db, flask_bcrypt



class Booking(db.Model):
    """ Booking Model for storing Booking related details """
    __tablename__ = "booking"

    public_id = db.Column(db.String(100), unique=True, primary_key=True,)
    created_on = db.Column(db.DateTime, nullable=False)
    price_value = db.Column(db.Numeric(precision=4, asdecimal=True), unique=False, nullable=False)
    price_um = db.Column(db.String(100), unique=False, nullable=False)
    offer_id =  db.Column(db.String(100), db.ForeignKey('offer.public_id'), unique=True, nullable=False)
    offer = relationship("Offer", foreign_keys=[offer_id])
    routing = db.Column(db.String(255), unique=False, nullable=True)
    security_status = db.Column(db.String(255), unique=False, nullable=True) 
    latest_log =  db.Column(db.String(100), unique=False, nullable=True)

    def __repr__(self):
        return "<Booking '{}', offer '{}', routing '{}'>".format(self.public_id, self.offer_id, self.routing)

    