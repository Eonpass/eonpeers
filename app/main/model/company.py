import datetime
from flask import jsonify

from sqlalchemy.orm  import relationship

from app.main.config import key
from app.main.services import db, flask_bcrypt
from app.main.util.hashutils import hu


class Company(db.Model):
    """ Company Model for storing company related details """
    __tablename__ = "company"

    public_id = db.Column(db.String(100), unique=True, primary_key=True,)
    name = db.Column(db.String(255), unique=True, nullable=False)
    vat_number = db.Column(db.String(255), unique=True, nullable=False)
    created_on = db.Column(db.DateTime, nullable=False)
    is_own = db.Column(db.Boolean, nullable=False, default=False)
    did = db.Column(db.String(1000), unique=False, nullable=True)
    base_url = db.Column(db.String(255))
    remote_username = db.Column(db.String(50))
    remote_email = db.Column(db.String(255))
    remote_password = db.Column(db.String(255)) #this should be encrypted
    eori_number = db.Column(db.String(255), unique=False, nullable=True)
    aeo_status = db.Column(db.String(255), unique=False, nullable=True)
    public_key = db.Column(db.String(255), unique=False, nullable=True)
    fiscal_address = db.Column(db.String(255), unique=False, nullable=True)
    fiscal_city = db.Column(db.String(255), unique=False, nullable=True)
    fiscal_province = db.Column(db.String(255), unique=False, nullable=True)
    fiscal_country = db.Column(db.String(255), unique=False, nullable=False)
    fiscal_zip = db.Column(db.String(255), unique=False, nullable=True)
    unique_hash = db.Column(db.String(255), unique=True, nullable=False, index=True)
    #signed_hash = db.Column(db.String(255), unique=True, nullable=True)
    manufactured_products = relationship("Product", primaryjoin="Company.public_id==Product.manufacturer_company_id", back_populates="manufacturer")
    branded_products = relationship("Product", primaryjoin="Company.public_id==Product.brand_owner_company_id", back_populates="brand_owner")
    requested_quotes = relationship("QuoteRequest", primaryjoin="Company.public_id==QuoteRequest.requester_company_id")
    supplied_quotes = relationship("QuoteRequest", primaryjoin="Company.public_id==QuoteRequest.supplier_company_id")
    users = relationship("User", cascade="all, delete-orphan")
    latest_log = db.Column(db.String(255), unique=False, nullable=True)
    __table_args__ = (db.UniqueConstraint('vat_number', 'fiscal_country', name='_company_vat_country_uc'),)

    @property
    def hash(self):
        """
        From the fields, compute the hash

        TODO: decide if this is useful, if not, remove, for now it appears only in test_company, all other service logics or dtos don't use it
        """
        #exclude created_on and eventual other fields containing info for this node only
        tokens = [self.aeo_status, self.base_url, self.eori_number, self.fiscal_address, self.fiscal_city, self.fiscal_country, self.fiscal_province, self.fiscal_zip, self.name, self.vat_number]
        str_token = map(self.prepare_token, tokens)
        payload = ";".join(str_token)
        
        current_hash = hu.digest(payload).hex()
        return current_hash

    def prepare_token(self,field):
        return field if field else ''

    def __repr__(self):
        return "<Company '{}', vat '{}'>".format(self.name, self.vat_number)

    