import datetime
from sqlalchemy import Column
from sqlalchemy.orm  import relationship

from app.main.config import key
from app.main.services import db, flask_bcrypt



class Contact(db.Model):
    """ Booking Model for storing Booking related details """
    __tablename__ = "contact"

    public_id = db.Column(db.String(100), unique=True, primary_key=True,)
    created_on = db.Column(db.DateTime, nullable=False)
    company_id =  db.Column(db.String(100), db.ForeignKey('company.public_id'), unique=False, nullable=True)
    company = relationship("Company", foreign_keys=[offer_id])
    first_name = db.Column(db.String(255), unique=False, nullable=True)
    last_name = db.Column(db.String(255), unique=False, nullable=True) 
    job_title = db.Column(db.String(255), unique=False, nullable=True) 
    phone = db.Column(db.String(255), unique=False, nullable=True) 
    email = db.Column(db.String(255), unique=False, nullable=True) 

    def __repr__(self):
        return "<Contact '{}', name '{}''{}', email '{}'>".format(self.public_id, self.first_name, self.last_name, self.email)

    