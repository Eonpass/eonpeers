import datetime
from sqlalchemy import Column
from sqlalchemy.orm  import relationship

from app.main.config import key
from app.main.services import db, flask_bcrypt


class CustomsInfo(db.Model):
    """ Customs Info Model for storing Customs related details """
    __tablename__ = "customs_info"

    public_id = db.Column(db.String(100), unique=True, primary_key=True,)
    created_on = db.Column(db.DateTime, nullable=False)
    content_code = db.Column(db.String(100), unique=False)
    country_code = db.Column(db.String(10), unique=False)
    information = db.Column(db.String(100), unique=False)
    subject_code = db.Column(db.String(100), unique=False)
    note = db.Column(db.String(100), unique=False)
    piece_id = db.Column(db.String(100), db.ForeignKey('piece.public_id'), unique=False, nullable=False)
    piece = relationship("Piece", foreign_keys=[piece_id])

    def __repr__(self):
        return "<Customs Info '{}', content code '{}', subject code '{}'>".format(self.public_id, self.content_code, self.subject_code)
