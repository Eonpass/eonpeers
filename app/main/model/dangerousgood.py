import datetime
from sqlalchemy import Column
from sqlalchemy.orm  import relationship

from app.main.config import key
from app.main.services import db, flask_bcrypt


class DangerousGood(db.Model):
    """ Dangerous Goods Model for storing DG related details """
    __tablename__ = "dangerous_good"

    public_id = db.Column(db.String(100), unique=True, primary_key=True,)
    created_on = db.Column(db.DateTime, nullable=False)
    class_code = db.Column(db.String(100), unique=False)
    net_quantity_value = db.Column(db.Numeric(precision=4, asdecimal=True))
    net_quantity_um = db.Column(db.String(100), unique=False)
    packing_group = db.Column(db.String(100), unique=False)
    packing_instruction = db.Column(db.String(100), unique=False)
    un_number = db.Column(db.String(100), unique=False)
    piece_id = db.Column(db.String(100), db.ForeignKey('piece.public_id'), unique=False, nullable=False)
    piece = relationship("Piece", foreign_keys=[piece_id])

    def __repr__(self):
        return "<Dungerous Good '{}', class code '{}', UN number '{}'>".format(self.public_id, self.content_code, self.subject_code)