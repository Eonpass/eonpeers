import datetime

from app.main.config import key
from app.main.services import db, flask_bcrypt
from app.main.util.hashutils import hu


class Disclosure(db.Model):
    """ Selective disclosure, which user can request view right on which shipment """
    __tablename__ = "disclosure"

    user_id = db.Column(db.String(100), db.ForeignKey('user.public_id'), primary_key = True)
    shipment_id = db.Column(db.String(100), db.ForeignKey('shipment.public_id'), primary_key = True)
    approved = db.Column(db.Boolean, unique=False, nullable=False, default=False)
    created_on = db.Column(db.DateTime, nullable=False)
    
    def __repr__(self):
        return "<disclosure shipment '{}', user '{}', approved '{}'>".format(self.shipment_id, self.user_id, self.approved)
