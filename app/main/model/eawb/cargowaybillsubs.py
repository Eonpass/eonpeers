#!/usr/bin/env python

#
# Generated Thu Nov 26 18:17:58 2020 by generateDS.py version 2.37.5.
# Python 3.7.3 (default, Jun  8 2019, 17:47:22)  [Clang 10.0.1 (clang-1001.0.46.4)]
#
# Command line options:
#   ('-o', 'cargowaybill.py')
#   ('-s', 'cargowaybillsubs.py')
#
# Command line arguments:
#   schema0.xsd
#
# Command line:
#   generateDS.py -o "cargowaybill.py" -s "cargowaybillsubs.py" schema0.xsd
#
# Current working directory (os.getcwd()):
#   generateDS-2.37.5
#

import os
import sys
from lxml import etree as etree_

import ??? as supermod

def parsexml_(infile, parser=None, **kwargs):
    if parser is None:
        # Use the lxml ElementTree compatible parser so that, e.g.,
        #   we ignore comments.
        parser = etree_.ETCompatXMLParser()
    try:
        if isinstance(infile, os.PathLike):
            infile = os.path.join(infile)
    except AttributeError:
        pass
    doc = etree_.parse(infile, parser=parser, **kwargs)
    return doc

def parsexmlstring_(instring, parser=None, **kwargs):
    if parser is None:
        # Use the lxml ElementTree compatible parser so that, e.g.,
        #   we ignore comments.
        try:
            parser = etree_.ETCompatXMLParser()
        except AttributeError:
            # fallback to xml.etree
            parser = etree_.XMLParser()
    element = etree_.fromstring(instring, parser=parser, **kwargs)
    return element

#
# Globals
#

ExternalEncoding = ''
SaveElementTreeNode = True

#
# Data representation classes
#


class CargoWaybillTypeElementSub(supermod.CargoWaybillTypeElement):
    def __init__(self, Waybill=None, originalMessage=None, state=None, senderRole=None, operationalFlight=None, origin=None, finalDestination=None, **kwargs_):
        super(CargoWaybillTypeElementSub, self).__init__(Waybill, originalMessage, state, senderRole, operationalFlight, origin, finalDestination,  **kwargs_)
supermod.CargoWaybillTypeElement.subclass = CargoWaybillTypeElementSub
# end class CargoWaybillTypeElementSub


class WaybillSub(supermod.Waybill):
    def __init__(self, MessageHeaderDocument=None, BusinessHeaderDocument=None, MasterConsignment=None, **kwargs_):
        super(WaybillSub, self).__init__(MessageHeaderDocument, BusinessHeaderDocument, MasterConsignment,  **kwargs_)
supermod.Waybill.subclass = WaybillSub
# end class WaybillSub


class SenderPartySub(supermod.SenderParty):
    def __init__(self, PrimaryID=None, **kwargs_):
        super(SenderPartySub, self).__init__(PrimaryID,  **kwargs_)
supermod.SenderParty.subclass = SenderPartySub
# end class SenderPartySub


class RecipientPartySub(supermod.RecipientParty):
    def __init__(self, PrimaryID=None, **kwargs_):
        super(RecipientPartySub, self).__init__(PrimaryID,  **kwargs_)
supermod.RecipientParty.subclass = RecipientPartySub
# end class RecipientPartySub


class IncludedHeaderNoteSub(supermod.IncludedHeaderNote):
    def __init__(self, ContentCode=None, Content=None, **kwargs_):
        super(IncludedHeaderNoteSub, self).__init__(ContentCode, Content,  **kwargs_)
supermod.IncludedHeaderNote.subclass = IncludedHeaderNoteSub
# end class IncludedHeaderNoteSub


class SignatoryConsignorAuthenticationSub(supermod.SignatoryConsignorAuthentication):
    def __init__(self, Signatory=None, **kwargs_):
        super(SignatoryConsignorAuthenticationSub, self).__init__(Signatory,  **kwargs_)
supermod.SignatoryConsignorAuthentication.subclass = SignatoryConsignorAuthenticationSub
# end class SignatoryConsignorAuthenticationSub


class SignatoryCarrierAuthenticationSub(supermod.SignatoryCarrierAuthentication):
    def __init__(self, ActualDateTime=None, Signatory=None, IssueAuthenticationLocation=None, **kwargs_):
        super(SignatoryCarrierAuthenticationSub, self).__init__(ActualDateTime, Signatory, IssueAuthenticationLocation,  **kwargs_)
supermod.SignatoryCarrierAuthentication.subclass = SignatoryCarrierAuthenticationSub
# end class SignatoryCarrierAuthenticationSub


class IncludedTareGrossWeightMeasureSub(supermod.IncludedTareGrossWeightMeasure):
    def __init__(self, unitCode=None, valueOf_=None, **kwargs_):
        super(IncludedTareGrossWeightMeasureSub, self).__init__(unitCode, valueOf_,  **kwargs_)
supermod.IncludedTareGrossWeightMeasure.subclass = IncludedTareGrossWeightMeasureSub
# end class IncludedTareGrossWeightMeasureSub


class ConsignorPartySub(supermod.ConsignorParty):
    def __init__(self, Name=None, PostalStructuredAddress=None, DefinedTradeContact=None, **kwargs_):
        super(ConsignorPartySub, self).__init__(Name, PostalStructuredAddress, DefinedTradeContact,  **kwargs_)
supermod.ConsignorParty.subclass = ConsignorPartySub
# end class ConsignorPartySub


class ConsigneePartySub(supermod.ConsigneeParty):
    def __init__(self, Name=None, PostalStructuredAddress=None, DefinedTradeContact=None, **kwargs_):
        super(ConsigneePartySub, self).__init__(Name, PostalStructuredAddress, DefinedTradeContact,  **kwargs_)
supermod.ConsigneeParty.subclass = ConsigneePartySub
# end class ConsigneePartySub


class FreightForwarderPartySub(supermod.FreightForwarderParty):
    def __init__(self, Name=None, CargoAgentID=None, FreightForwarderAddress=None, SpecifiedCargoAgentLocation=None, **kwargs_):
        super(FreightForwarderPartySub, self).__init__(Name, CargoAgentID, FreightForwarderAddress, SpecifiedCargoAgentLocation,  **kwargs_)
supermod.FreightForwarderParty.subclass = FreightForwarderPartySub
# end class FreightForwarderPartySub


class OriginLocationSub(supermod.OriginLocation):
    def __init__(self, ID=None, **kwargs_):
        super(OriginLocationSub, self).__init__(ID,  **kwargs_)
supermod.OriginLocation.subclass = OriginLocationSub
# end class OriginLocationSub


class FinalDestinationLocationSub(supermod.FinalDestinationLocation):
    def __init__(self, ID=None, **kwargs_):
        super(FinalDestinationLocationSub, self).__init__(ID,  **kwargs_)
supermod.FinalDestinationLocation.subclass = FinalDestinationLocationSub
# end class FinalDestinationLocationSub


class SpecifiedLogisticsTransportMovementSub(supermod.SpecifiedLogisticsTransportMovement):
    def __init__(self, StageCode=None, ID=None, SequenceNumeric=None, UsedLogisticsTransportMeans=None, ArrivalEvent=None, DepartureEvent=None, **kwargs_):
        super(SpecifiedLogisticsTransportMovementSub, self).__init__(StageCode, ID, SequenceNumeric, UsedLogisticsTransportMeans, ArrivalEvent, DepartureEvent,  **kwargs_)
supermod.SpecifiedLogisticsTransportMovement.subclass = SpecifiedLogisticsTransportMovementSub
# end class SpecifiedLogisticsTransportMovementSub


class HandlingSPHInstructionsSub(supermod.HandlingSPHInstructions):
    def __init__(self, DescriptionCode=None, **kwargs_):
        super(HandlingSPHInstructionsSub, self).__init__(DescriptionCode,  **kwargs_)
supermod.HandlingSPHInstructions.subclass = HandlingSPHInstructionsSub
# end class HandlingSPHInstructionsSub


class HandlingOSIInstructionsSub(supermod.HandlingOSIInstructions):
    def __init__(self, Description=None, **kwargs_):
        super(HandlingOSIInstructionsSub, self).__init__(Description,  **kwargs_)
supermod.HandlingOSIInstructions.subclass = HandlingOSIInstructionsSub
# end class HandlingOSIInstructionsSub


class IncludedAccountingNoteSub(supermod.IncludedAccountingNote):
    def __init__(self, ContentCode=None, Content=None, **kwargs_):
        super(IncludedAccountingNoteSub, self).__init__(ContentCode, Content,  **kwargs_)
supermod.IncludedAccountingNote.subclass = IncludedAccountingNoteSub
# end class IncludedAccountingNoteSub


class IncludedCustomsNoteSub(supermod.IncludedCustomsNote):
    def __init__(self, ContentCode=None, Content=None, SubjectCode=None, CountryID=None, **kwargs_):
        super(IncludedCustomsNoteSub, self).__init__(ContentCode, Content, SubjectCode, CountryID,  **kwargs_)
supermod.IncludedCustomsNote.subclass = IncludedCustomsNoteSub
# end class IncludedCustomsNoteSub


class AssociatedConsignmentCustomsProcedureSub(supermod.AssociatedConsignmentCustomsProcedure):
    def __init__(self, GoodsStatusCode=None, **kwargs_):
        super(AssociatedConsignmentCustomsProcedureSub, self).__init__(GoodsStatusCode,  **kwargs_)
supermod.AssociatedConsignmentCustomsProcedure.subclass = AssociatedConsignmentCustomsProcedureSub
# end class AssociatedConsignmentCustomsProcedureSub


class ApplicableOriginCurrencyExchangeSub(supermod.ApplicableOriginCurrencyExchange):
    def __init__(self, SourceCurrencyCode=None, **kwargs_):
        super(ApplicableOriginCurrencyExchangeSub, self).__init__(SourceCurrencyCode,  **kwargs_)
supermod.ApplicableOriginCurrencyExchange.subclass = ApplicableOriginCurrencyExchangeSub
# end class ApplicableOriginCurrencyExchangeSub


class ApplicableLogisticsServiceChargeSub(supermod.ApplicableLogisticsServiceCharge):
    def __init__(self, TransportPaymentMethodCode=None, **kwargs_):
        super(ApplicableLogisticsServiceChargeSub, self).__init__(TransportPaymentMethodCode,  **kwargs_)
supermod.ApplicableLogisticsServiceCharge.subclass = ApplicableLogisticsServiceChargeSub
# end class ApplicableLogisticsServiceChargeSub


class ApplicableLogisticsAllowanceChargeSub(supermod.ApplicableLogisticsAllowanceCharge):
    def __init__(self, ID=None, PrepaidIndicator=None, PartyTypeCode=None, ActualAmount=None, **kwargs_):
        super(ApplicableLogisticsAllowanceChargeSub, self).__init__(ID, PrepaidIndicator, PartyTypeCode, ActualAmount,  **kwargs_)
supermod.ApplicableLogisticsAllowanceCharge.subclass = ApplicableLogisticsAllowanceChargeSub
# end class ApplicableLogisticsAllowanceChargeSub


class ApplicableRatingSub(supermod.ApplicableRating):
    def __init__(self, TypeCode=None, TotalChargeAmount=None, IncludedMasterConsignmentItem=None, **kwargs_):
        super(ApplicableRatingSub, self).__init__(TypeCode, TotalChargeAmount, IncludedMasterConsignmentItem,  **kwargs_)
supermod.ApplicableRating.subclass = ApplicableRatingSub
# end class ApplicableRatingSub


class ApplicableTotalRatingSub(supermod.ApplicableTotalRating):
    def __init__(self, TypeCode=None, ApplicablePrepaidCollectMonetarySummation=None, **kwargs_):
        super(ApplicableTotalRatingSub, self).__init__(TypeCode, ApplicablePrepaidCollectMonetarySummation,  **kwargs_)
supermod.ApplicableTotalRating.subclass = ApplicableTotalRatingSub
# end class ApplicableTotalRatingSub


class originalMessageSub(supermod.originalMessage):
    def __init__(self, type_=None, **kwargs_):
        super(originalMessageSub, self).__init__(type_,  **kwargs_)
supermod.originalMessage.subclass = originalMessageSub
# end class originalMessageSub


class operationalFlightSub(supermod.operationalFlight):
    def __init__(self, carrierCode=None, flightNumber=None, **kwargs_):
        super(operationalFlightSub, self).__init__(carrierCode, flightNumber,  **kwargs_)
supermod.operationalFlight.subclass = operationalFlightSub
# end class operationalFlightSub


class originSub(supermod.origin):
    def __init__(self, country=None, **kwargs_):
        super(originSub, self).__init__(country,  **kwargs_)
supermod.origin.subclass = originSub
# end class originSub


class finalDestinationSub(supermod.finalDestination):
    def __init__(self, country=None, **kwargs_):
        super(finalDestinationSub, self).__init__(country,  **kwargs_)
supermod.finalDestination.subclass = finalDestinationSub
# end class finalDestinationSub


class MessageHeaderDocumentTypeSub(supermod.MessageHeaderDocumentType):
    def __init__(self, ID=None, Name=None, TypeCode=None, IssueDateTime=None, PurposeCode=None, VersionID=None, SenderParty=None, RecipientParty=None, **kwargs_):
        super(MessageHeaderDocumentTypeSub, self).__init__(ID, Name, TypeCode, IssueDateTime, PurposeCode, VersionID, SenderParty, RecipientParty,  **kwargs_)
supermod.MessageHeaderDocumentType.subclass = MessageHeaderDocumentTypeSub
# end class MessageHeaderDocumentTypeSub


class BusinessHeaderDocumentTypeSub(supermod.BusinessHeaderDocumentType):
    def __init__(self, ID=None, IncludedHeaderNote=None, SignatoryConsignorAuthentication=None, SignatoryCarrierAuthentication=None, **kwargs_):
        super(BusinessHeaderDocumentTypeSub, self).__init__(ID, IncludedHeaderNote, SignatoryConsignorAuthentication, SignatoryCarrierAuthentication,  **kwargs_)
supermod.BusinessHeaderDocumentType.subclass = BusinessHeaderDocumentTypeSub
# end class BusinessHeaderDocumentTypeSub


class MasterConsignmentTypeSub(supermod.MasterConsignmentType):
    def __init__(self, NilCarriageValueIndicator=None, NilCustomsValueIndicator=None, NilInsuranceValueIndicator=None, TotalChargePrepaidIndicator=None, TotalDisbursementPrepaidIndicator=None, IncludedTareGrossWeightMeasure=None, TotalPieceQuantity=None, ConsignorParty=None, ConsigneeParty=None, FreightForwarderParty=None, OriginLocation=None, FinalDestinationLocation=None, SpecifiedLogisticsTransportMovement=None, HandlingSPHInstructions=None, HandlingOSIInstructions=None, IncludedAccountingNote=None, IncludedCustomsNote=None, AssociatedConsignmentCustomsProcedure=None, ApplicableOriginCurrencyExchange=None, ApplicableLogisticsServiceCharge=None, ApplicableLogisticsAllowanceCharge=None, ApplicableRating=None, ApplicableTotalRating=None, **kwargs_):
        super(MasterConsignmentTypeSub, self).__init__(NilCarriageValueIndicator, NilCustomsValueIndicator, NilInsuranceValueIndicator, TotalChargePrepaidIndicator, TotalDisbursementPrepaidIndicator, IncludedTareGrossWeightMeasure, TotalPieceQuantity, ConsignorParty, ConsigneeParty, FreightForwarderParty, OriginLocation, FinalDestinationLocation, SpecifiedLogisticsTransportMovement, HandlingSPHInstructions, HandlingOSIInstructions, IncludedAccountingNote, IncludedCustomsNote, AssociatedConsignmentCustomsProcedure, ApplicableOriginCurrencyExchange, ApplicableLogisticsServiceCharge, ApplicableLogisticsAllowanceCharge, ApplicableRating, ApplicableTotalRating,  **kwargs_)
supermod.MasterConsignmentType.subclass = MasterConsignmentTypeSub
# end class MasterConsignmentTypeSub


class PrimaryIDTypeSub(supermod.PrimaryIDType):
    def __init__(self, schemeID=None, valueOf_=None, **kwargs_):
        super(PrimaryIDTypeSub, self).__init__(schemeID, valueOf_,  **kwargs_)
supermod.PrimaryIDType.subclass = PrimaryIDTypeSub
# end class PrimaryIDTypeSub


class PrimaryIDType1Sub(supermod.PrimaryIDType1):
    def __init__(self, schemeID=None, valueOf_=None, **kwargs_):
        super(PrimaryIDType1Sub, self).__init__(schemeID, valueOf_,  **kwargs_)
supermod.PrimaryIDType1.subclass = PrimaryIDType1Sub
# end class PrimaryIDType1Sub


class IssueAuthenticationLocationTypeSub(supermod.IssueAuthenticationLocationType):
    def __init__(self, Name=None, **kwargs_):
        super(IssueAuthenticationLocationTypeSub, self).__init__(Name,  **kwargs_)
supermod.IssueAuthenticationLocationType.subclass = IssueAuthenticationLocationTypeSub
# end class IssueAuthenticationLocationTypeSub


class PostalStructuredAddressTypeSub(supermod.PostalStructuredAddressType):
    def __init__(self, PostcodeCode=None, StreetName=None, CityName=None, CountryID=None, CountrySubDivisionName=None, **kwargs_):
        super(PostalStructuredAddressTypeSub, self).__init__(PostcodeCode, StreetName, CityName, CountryID, CountrySubDivisionName,  **kwargs_)
supermod.PostalStructuredAddressType.subclass = PostalStructuredAddressTypeSub
# end class PostalStructuredAddressTypeSub


class DefinedTradeContactTypeSub(supermod.DefinedTradeContactType):
    def __init__(self, DirectTelephoneCommunication=None, **kwargs_):
        super(DefinedTradeContactTypeSub, self).__init__(DirectTelephoneCommunication,  **kwargs_)
supermod.DefinedTradeContactType.subclass = DefinedTradeContactTypeSub
# end class DefinedTradeContactTypeSub


class DirectTelephoneCommunicationTypeSub(supermod.DirectTelephoneCommunicationType):
    def __init__(self, CompleteNumber=None, **kwargs_):
        super(DirectTelephoneCommunicationTypeSub, self).__init__(CompleteNumber,  **kwargs_)
supermod.DirectTelephoneCommunicationType.subclass = DirectTelephoneCommunicationTypeSub
# end class DirectTelephoneCommunicationTypeSub


class PostalStructuredAddressType2Sub(supermod.PostalStructuredAddressType2):
    def __init__(self, PostcodeCode=None, StreetName=None, CityName=None, CountryID=None, **kwargs_):
        super(PostalStructuredAddressType2Sub, self).__init__(PostcodeCode, StreetName, CityName, CountryID,  **kwargs_)
supermod.PostalStructuredAddressType2.subclass = PostalStructuredAddressType2Sub
# end class PostalStructuredAddressType2Sub


class DefinedTradeContactType3Sub(supermod.DefinedTradeContactType3):
    def __init__(self, DirectTelephoneCommunication=None, **kwargs_):
        super(DefinedTradeContactType3Sub, self).__init__(DirectTelephoneCommunication,  **kwargs_)
supermod.DefinedTradeContactType3.subclass = DefinedTradeContactType3Sub
# end class DefinedTradeContactType3Sub


class DirectTelephoneCommunicationType4Sub(supermod.DirectTelephoneCommunicationType4):
    def __init__(self, CompleteNumber=None, **kwargs_):
        super(DirectTelephoneCommunicationType4Sub, self).__init__(CompleteNumber,  **kwargs_)
supermod.DirectTelephoneCommunicationType4.subclass = DirectTelephoneCommunicationType4Sub
# end class DirectTelephoneCommunicationType4Sub


class FreightForwarderAddressTypeSub(supermod.FreightForwarderAddressType):
    def __init__(self, StreetName=None, CityName=None, **kwargs_):
        super(FreightForwarderAddressTypeSub, self).__init__(StreetName, CityName,  **kwargs_)
supermod.FreightForwarderAddressType.subclass = FreightForwarderAddressTypeSub
# end class FreightForwarderAddressTypeSub


class SpecifiedCargoAgentLocationTypeSub(supermod.SpecifiedCargoAgentLocationType):
    def __init__(self, ID=None, **kwargs_):
        super(SpecifiedCargoAgentLocationTypeSub, self).__init__(ID,  **kwargs_)
supermod.SpecifiedCargoAgentLocationType.subclass = SpecifiedCargoAgentLocationTypeSub
# end class SpecifiedCargoAgentLocationTypeSub


class UsedLogisticsTransportMeansTypeSub(supermod.UsedLogisticsTransportMeansType):
    def __init__(self, Name=None, **kwargs_):
        super(UsedLogisticsTransportMeansTypeSub, self).__init__(Name,  **kwargs_)
supermod.UsedLogisticsTransportMeansType.subclass = UsedLogisticsTransportMeansTypeSub
# end class UsedLogisticsTransportMeansTypeSub


class ArrivalEventTypeSub(supermod.ArrivalEventType):
    def __init__(self, OccurrenceArrivalLocation=None, **kwargs_):
        super(ArrivalEventTypeSub, self).__init__(OccurrenceArrivalLocation,  **kwargs_)
supermod.ArrivalEventType.subclass = ArrivalEventTypeSub
# end class ArrivalEventTypeSub


class OccurrenceArrivalLocationTypeSub(supermod.OccurrenceArrivalLocationType):
    def __init__(self, ID=None, **kwargs_):
        super(OccurrenceArrivalLocationTypeSub, self).__init__(ID,  **kwargs_)
supermod.OccurrenceArrivalLocationType.subclass = OccurrenceArrivalLocationTypeSub
# end class OccurrenceArrivalLocationTypeSub


class DepartureEventTypeSub(supermod.DepartureEventType):
    def __init__(self, ScheduledOccurrenceDateTime=None, OccurrenceDepartureLocation=None, **kwargs_):
        super(DepartureEventTypeSub, self).__init__(ScheduledOccurrenceDateTime, OccurrenceDepartureLocation,  **kwargs_)
supermod.DepartureEventType.subclass = DepartureEventTypeSub
# end class DepartureEventTypeSub


class OccurrenceDepartureLocationTypeSub(supermod.OccurrenceDepartureLocationType):
    def __init__(self, ID=None, **kwargs_):
        super(OccurrenceDepartureLocationTypeSub, self).__init__(ID,  **kwargs_)
supermod.OccurrenceDepartureLocationType.subclass = OccurrenceDepartureLocationTypeSub
# end class OccurrenceDepartureLocationTypeSub


class ActualAmountTypeSub(supermod.ActualAmountType):
    def __init__(self, currencyID=None, valueOf_=None, **kwargs_):
        super(ActualAmountTypeSub, self).__init__(currencyID, valueOf_,  **kwargs_)
supermod.ActualAmountType.subclass = ActualAmountTypeSub
# end class ActualAmountTypeSub


class TotalChargeAmountTypeSub(supermod.TotalChargeAmountType):
    def __init__(self, currencyID=None, valueOf_=None, **kwargs_):
        super(TotalChargeAmountTypeSub, self).__init__(currencyID, valueOf_,  **kwargs_)
supermod.TotalChargeAmountType.subclass = TotalChargeAmountTypeSub
# end class TotalChargeAmountTypeSub


class IncludedMasterConsignmentItemTypeSub(supermod.IncludedMasterConsignmentItemType):
    def __init__(self, SequenceNumeric=None, GrossWeightMeasure=None, PieceQuantity=None, NatureIdentificationTransportCargo=None, TransportLogisticsPackage=None, ApplicableFreightRateServiceCharge=None, **kwargs_):
        super(IncludedMasterConsignmentItemTypeSub, self).__init__(SequenceNumeric, GrossWeightMeasure, PieceQuantity, NatureIdentificationTransportCargo, TransportLogisticsPackage, ApplicableFreightRateServiceCharge,  **kwargs_)
supermod.IncludedMasterConsignmentItemType.subclass = IncludedMasterConsignmentItemTypeSub
# end class IncludedMasterConsignmentItemTypeSub


class GrossWeightMeasureTypeSub(supermod.GrossWeightMeasureType):
    def __init__(self, unitCode=None, valueOf_=None, **kwargs_):
        super(GrossWeightMeasureTypeSub, self).__init__(unitCode, valueOf_,  **kwargs_)
supermod.GrossWeightMeasureType.subclass = GrossWeightMeasureTypeSub
# end class GrossWeightMeasureTypeSub


class NatureIdentificationTransportCargoTypeSub(supermod.NatureIdentificationTransportCargoType):
    def __init__(self, Identification=None, **kwargs_):
        super(NatureIdentificationTransportCargoTypeSub, self).__init__(Identification,  **kwargs_)
supermod.NatureIdentificationTransportCargoType.subclass = NatureIdentificationTransportCargoTypeSub
# end class NatureIdentificationTransportCargoTypeSub


class TransportLogisticsPackageTypeSub(supermod.TransportLogisticsPackageType):
    def __init__(self, ItemQuantity=None, LinearSpatialDimension=None, **kwargs_):
        super(TransportLogisticsPackageTypeSub, self).__init__(ItemQuantity, LinearSpatialDimension,  **kwargs_)
supermod.TransportLogisticsPackageType.subclass = TransportLogisticsPackageTypeSub
# end class TransportLogisticsPackageTypeSub


class LinearSpatialDimensionTypeSub(supermod.LinearSpatialDimensionType):
    def __init__(self, WidthMeasure=None, LengthMeasure=None, HeightMeasure=None, **kwargs_):
        super(LinearSpatialDimensionTypeSub, self).__init__(WidthMeasure, LengthMeasure, HeightMeasure,  **kwargs_)
supermod.LinearSpatialDimensionType.subclass = LinearSpatialDimensionTypeSub
# end class LinearSpatialDimensionTypeSub


class WidthMeasureTypeSub(supermod.WidthMeasureType):
    def __init__(self, unitCode=None, valueOf_=None, **kwargs_):
        super(WidthMeasureTypeSub, self).__init__(unitCode, valueOf_,  **kwargs_)
supermod.WidthMeasureType.subclass = WidthMeasureTypeSub
# end class WidthMeasureTypeSub


class LengthMeasureTypeSub(supermod.LengthMeasureType):
    def __init__(self, unitCode=None, valueOf_=None, **kwargs_):
        super(LengthMeasureTypeSub, self).__init__(unitCode, valueOf_,  **kwargs_)
supermod.LengthMeasureType.subclass = LengthMeasureTypeSub
# end class LengthMeasureTypeSub


class HeightMeasureTypeSub(supermod.HeightMeasureType):
    def __init__(self, unitCode=None, valueOf_=None, **kwargs_):
        super(HeightMeasureTypeSub, self).__init__(unitCode, valueOf_,  **kwargs_)
supermod.HeightMeasureType.subclass = HeightMeasureTypeSub
# end class HeightMeasureTypeSub


class ApplicableFreightRateServiceChargeTypeSub(supermod.ApplicableFreightRateServiceChargeType):
    def __init__(self, CategoryCode=None, ChargeableWeightMeasure=None, AppliedRate=None, AppliedAmount=None, **kwargs_):
        super(ApplicableFreightRateServiceChargeTypeSub, self).__init__(CategoryCode, ChargeableWeightMeasure, AppliedRate, AppliedAmount,  **kwargs_)
supermod.ApplicableFreightRateServiceChargeType.subclass = ApplicableFreightRateServiceChargeTypeSub
# end class ApplicableFreightRateServiceChargeTypeSub


class ChargeableWeightMeasureTypeSub(supermod.ChargeableWeightMeasureType):
    def __init__(self, unitCode=None, valueOf_=None, **kwargs_):
        super(ChargeableWeightMeasureTypeSub, self).__init__(unitCode, valueOf_,  **kwargs_)
supermod.ChargeableWeightMeasureType.subclass = ChargeableWeightMeasureTypeSub
# end class ChargeableWeightMeasureTypeSub


class AppliedAmountTypeSub(supermod.AppliedAmountType):
    def __init__(self, currencyID=None, valueOf_=None, **kwargs_):
        super(AppliedAmountTypeSub, self).__init__(currencyID, valueOf_,  **kwargs_)
supermod.AppliedAmountType.subclass = AppliedAmountTypeSub
# end class AppliedAmountTypeSub


class ApplicablePrepaidCollectMonetarySummationTypeSub(supermod.ApplicablePrepaidCollectMonetarySummationType):
    def __init__(self, PrepaidIndicator=None, WeightChargeTotalAmount=None, CarrierTotalDuePayableAmount=None, GrandTotalAmount=None, **kwargs_):
        super(ApplicablePrepaidCollectMonetarySummationTypeSub, self).__init__(PrepaidIndicator, WeightChargeTotalAmount, CarrierTotalDuePayableAmount, GrandTotalAmount,  **kwargs_)
supermod.ApplicablePrepaidCollectMonetarySummationType.subclass = ApplicablePrepaidCollectMonetarySummationTypeSub
# end class ApplicablePrepaidCollectMonetarySummationTypeSub


class WeightChargeTotalAmountTypeSub(supermod.WeightChargeTotalAmountType):
    def __init__(self, currencyID=None, valueOf_=None, **kwargs_):
        super(WeightChargeTotalAmountTypeSub, self).__init__(currencyID, valueOf_,  **kwargs_)
supermod.WeightChargeTotalAmountType.subclass = WeightChargeTotalAmountTypeSub
# end class WeightChargeTotalAmountTypeSub


class CarrierTotalDuePayableAmountTypeSub(supermod.CarrierTotalDuePayableAmountType):
    def __init__(self, currencyID=None, valueOf_=None, **kwargs_):
        super(CarrierTotalDuePayableAmountTypeSub, self).__init__(currencyID, valueOf_,  **kwargs_)
supermod.CarrierTotalDuePayableAmountType.subclass = CarrierTotalDuePayableAmountTypeSub
# end class CarrierTotalDuePayableAmountTypeSub


class GrandTotalAmountTypeSub(supermod.GrandTotalAmountType):
    def __init__(self, currencyID=None, valueOf_=None, **kwargs_):
        super(GrandTotalAmountTypeSub, self).__init__(currencyID, valueOf_,  **kwargs_)
supermod.GrandTotalAmountType.subclass = GrandTotalAmountTypeSub
# end class GrandTotalAmountTypeSub


def get_root_tag(node):
    tag = supermod.Tag_pattern_.match(node.tag).groups()[-1]
    rootClass = None
    rootClass = supermod.GDSClassesMapping.get(tag)
    if rootClass is None and hasattr(supermod, tag):
        rootClass = getattr(supermod, tag)
    return tag, rootClass


def parse(inFilename, silence=False):
    parser = None
    doc = parsexml_(inFilename, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'CargoWaybillTypeElement'
        rootClass = supermod.CargoWaybillTypeElement
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        doc = None
        rootNode = None
    if not silence:
        sys.stdout.write('<?xml version="1.0" ?>\n')
        rootObj.export(
            sys.stdout, 0, name_=rootTag,
            namespacedef_='xmlns:ns4="http://www.af-klm.com/services/cargo/data-v1/xsd"',
            pretty_print=True)
    return rootObj


def parseEtree(inFilename, silence=False):
    parser = None
    doc = parsexml_(inFilename, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'CargoWaybillTypeElement'
        rootClass = supermod.CargoWaybillTypeElement
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    mapping = {}
    rootElement = rootObj.to_etree(None, name_=rootTag, mapping_=mapping)
    reverse_mapping = rootObj.gds_reverse_node_mapping(mapping)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        doc = None
        rootNode = None
    if not silence:
        content = etree_.tostring(
            rootElement, pretty_print=True,
            xml_declaration=True, encoding="utf-8")
        sys.stdout.write(content)
        sys.stdout.write('\n')
    return rootObj, rootElement, mapping, reverse_mapping


def parseString(inString, silence=False):
    if sys.version_info.major == 2:
        from StringIO import StringIO
    else:
        from io import BytesIO as StringIO
    parser = None
    rootNode= parsexmlstring_(inString, parser)
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'CargoWaybillTypeElement'
        rootClass = supermod.CargoWaybillTypeElement
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        rootNode = None
    if not silence:
        sys.stdout.write('<?xml version="1.0" ?>\n')
        rootObj.export(
            sys.stdout, 0, name_=rootTag,
            namespacedef_='xmlns:ns4="http://www.af-klm.com/services/cargo/data-v1/xsd"')
    return rootObj


def parseLiteral(inFilename, silence=False):
    parser = None
    doc = parsexml_(inFilename, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'CargoWaybillTypeElement'
        rootClass = supermod.CargoWaybillTypeElement
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        doc = None
        rootNode = None
    if not silence:
        sys.stdout.write('#from ??? import *\n\n')
        sys.stdout.write('import ??? as model_\n\n')
        sys.stdout.write('rootObj = model_.rootClass(\n')
        rootObj.exportLiteral(sys.stdout, 0, name_=rootTag)
        sys.stdout.write(')\n')
    return rootObj


USAGE_TEXT = """
Usage: python ???.py <infilename>
"""


def usage():
    print(USAGE_TEXT)
    sys.exit(1)


def main():
    args = sys.argv[1:]
    if len(args) != 1:
        usage()
    infilename = args[0]
    parse(infilename)


if __name__ == '__main__':
    #import pdb; pdb.set_trace()
    main()
