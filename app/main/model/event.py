import datetime
import json

from sqlalchemy import Column
from sqlalchemy.orm  import relationship

from app.main.config import key
from app.main.services import db, flask_bcrypt
from app.main.util.hashutils import HashUtils


class Event(db.Model):
    """ Piece Model for storing pieces descriptions, the central object of IATA One Record """
    __tablename__ = "event"

    public_id = db.Column(db.String(100), unique=True, primary_key = True)
    created_on = db.Column(db.DateTime, nullable=False)
    code = db.Column(db.String(255), unique=False, nullable=True)
    name = db.Column(db.String(255), unique=False, nullable=True)
    latitude = db.Column(db.DECIMAL(11,8), unique=False, nullable=True)
    longitude = db.Column(db.DECIMAL(11,8), unique=False, nullable=True)
    recording_datetime = db.Column(db.DateTime, nullable=False)
    sensor_reading_um = db.Column(db.String(255), unique=False, nullable=True)
    sensor_reading_value = db.Column(db.String(255), unique=False, nullable=True)
    type_indicator = db.Column(db.String(20), unique=False, nullable=False) #this could be an ENUM but different engines require different code, so for now we just use strings
    shipment_id = db.Column(db.String(100), db.ForeignKey('shipment.public_id'))
    shipment = relationship("Shipment", foreign_keys=[shipment_id])
    
    @property
    def hash(self):
        """
        From the fields, compute the hash
        """
        json_body = self.as_dict()
        #transform datetime in a string, otherwise can't serialize:
        json_body["recording_datetime"] = json_body["recording_datetime"].strftime('%Y-%m-%d %H:%M:%S.%f')
        payload = json.dumps(json_body, sort_keys=True)
        hu = HashUtils()
        current_hash = hu.digest(payload).hex()
        return current_hash

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def prepare_token(self,field):
        return field if field else ''
    
    def __repr__(self):
        return "<Event id '{}', shipment '{}', created_on '{}'>".format(self.public_id, self.shipment_id, self.created_on)
