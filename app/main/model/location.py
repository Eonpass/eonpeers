import datetime
from sqlalchemy import Column
from sqlalchemy.orm  import relationship
#from geoalchemy2 import Geometry

from app.main.config import key
from app.main.services import db, flask_bcrypt


class Location(db.Model):
    """ Location Model for storing location related details """
    __tablename__ = "location"

    public_id = db.Column(db.String(100), unique=True, primary_key=True,)
    created_on = db.Column(db.DateTime, nullable=False)
    name = db.Column(db.String(255), unique=True, nullable=False)
    location_code = db.Column(db.String(255), unique=False, nullable=False)
    location_type = db.Column(db.String(255), unique=False, nullable=False)
    #coordinates = db.Column(Geometry('POINT'))
    city_code = db.Column(db.String(255), unique=False, nullable=True)
    city_name = db.Column(db.String(255), unique=False, nullable=False)
    country = db.Column(db.String(255), unique=False, nullable=False)
    po_box = db.Column(db.String(255), unique=False, nullable=True)
    postal_code = db.Column(db.String(255), unique=False, nullable=False)
    region_code = db.Column(db.String(255), unique=False, nullable=True)
    region_name =  db.Column(db.String(255), unique=False, nullable=True)
    street = db.Column(db.String(255), unique=False, nullable=False)
    unique_hash = db.Column(db.String(255), unique=True, nullable=False, index=True)
    arriving_segments = relationship("TransportSegment", primaryjoin="Location.public_id==TransportSegment.arrival_location_id")
    departing_segments = relationship("TransportSegment", primaryjoin="Location.public_id==TransportSegment.departure_location_id")
    __table_args__ = (db.UniqueConstraint('location_code', 'location_type', 'street', 'postal_code', 'city_name', 'country', name='_location_uc'),)

    
    def __repr__(self):
        return "<Location '{}', data '{}', key '{}'>".format(self.name, self.location_data, self.location_key)

    