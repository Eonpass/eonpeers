import datetime
from sqlalchemy import Column
from sqlalchemy.orm  import relationship

from app.main.config import key
from app.main.services import db, flask_bcrypt



class Offer(db.Model):
    """ Offer Model for storing Offer related details """
    __tablename__ = "offer"

    public_id = db.Column(db.String(100), unique=True, primary_key=True,)
    created_on = db.Column(db.DateTime, nullable=False)
    allotment = db.Column(db.String(255), unique=False, nullable=True)
    dangerous_goods = db.Column(db.String(255), unique=False, nullable=True)
    price_value = db.Column(db.DECIMAL(10,4), unique=False, nullable=False)
    price_um = db.Column(db.String(100), unique=False, nullable=False)
    quote_request_id =  db.Column(db.String(100), db.ForeignKey('quote_request.public_id'))
    quote_request = relationship("QuoteRequest", foreign_keys=[quote_request_id])
    rating = db.Column(db.String(255), unique=False, nullable=True)
    routing = db.Column(db.String(255), unique=False, nullable=True)
    security_status = db.Column(db.String(255), unique=False, nullable=True) 
    #TODO: are these two relationships, shipment and TS, useful ON the offer too? Not for now..
    shipment_id = db.Column(db.String(100), db.ForeignKey('shipment.public_id'))
    shipment = relationship("Shipment",  foreign_keys=[shipment_id]) 
    transport_segment_id = db.Column(db.String(100), db.ForeignKey('transport_segment.public_id'))
    transport_segment = relationship("TransportSegment",  foreign_keys=[transport_segment_id])
    latest_log = db.Column(db.String(255), unique=False, nullable=True)


    #the same requesting company, toward the same offering company, for the same shipment on the same route can open only 1 quote request but the QR can have many offers:
    __table_args__ = (db.UniqueConstraint('quote_request_id', 'price_value',  'price_um',  'rating', name='_offer_uc'),)

    
    
    def __repr__(self):
        return "<Offer '{}', transport segmnent '{}', routing '{}'>".format(self.public_id, self.transport_segment_id, self.routing)

    