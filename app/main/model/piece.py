import datetime
from sqlalchemy import Column
from sqlalchemy.orm  import relationship

from app.main.config import key
from app.main.services import db, flask_bcrypt
from app.main.util.hashutils import hu

from app.main.model.productpiececomposition import ProductPieceComposition
from app.main.model.transportsegmentpiececomposition import TransportSegmentPieceComposition


class Piece(db.Model):
    """ Piece Model for storing pieces descriptions, the central object of IATA One Record """
    __tablename__ = "piece"

    public_id = db.Column(db.String(100), unique=True, primary_key = True)
    created_on = db.Column(db.DateTime, nullable=False)
    additional_security_info = db.Column(db.String(255), unique=False, nullable=True)
    coload = db.Column(db.Boolean, unique=False, nullable=True)
    parent_piece_id = db.Column(db.String(100), db.ForeignKey('piece.public_id'))
    goods_description = db.Column(db.String(1000), unique=False, nullable=False)
    gross_weight_value = db.Column(db.Numeric(precision=4, asdecimal=True), unique=False, nullable=False)
    gross_weight_um = db.Column(db.String(100), unique=False, nullable=False)
    load_type = db.Column(db.String(255), unique=False, nullable=True)
    slac = db.Column(db.Integer, unique=False, nullable=True)
    stackable = db.Column(db.Boolean, unique=False, nullable=True)
    shipment_id = db.Column(db.String(100), db.ForeignKey('shipment.public_id'))
    shipment = relationship("Shipment", foreign_keys=[shipment_id])
    turnable = db.Column(db.Boolean, unique=False, nullable=True)
    upid = db.Column(db.String(100), unique=True, nullable=True)
    volumetric_weight_conversion_factor_um = db.Column(db.String(100), unique=False, nullable=False)
    volumetric_weight_conversion_factor_value = db.Column(db.DECIMAL(10,4), unique=False, nullable=False)
    volumetric_weight_chargeable_weight_um = db.Column(db.String(100), unique=False, nullable=False)
    volumetric_weight_chargeable_weight_value = db.Column(db.DECIMAL(10,4), unique=False, nullable=False)
    height_um = db.Column(db.String(100), unique=False, nullable=True)
    height_value = db.Column(db.DECIMAL(10,4), unique=False, nullable=True)
    length_um = db.Column(db.String(100), unique=False, nullable=True)
    length_value = db.Column(db.DECIMAL(10,4), unique=False, nullable=True)
    width_um = db.Column(db.String(100), unique=False, nullable=True)
    width_value = db.Column(db.DECIMAL(10,4), unique=False, nullable=True)
    children_pieces = relationship("Piece",
        backref=db.backref('parent_piece', remote_side=[public_id])
    )
    products = relationship('Product', secondary = 'product_piece_composition', cascade = 'all, delete')
    transport_segments = relationship('TransportSegment', secondary = 'transport_segment_piece_composition', cascade = 'all, delete')
    customs_infos = relationship("CustomsInfo", primaryjoin="Piece.public_id==CustomsInfo.piece_id")
    dangerous_goods = relationship("DangerousGood", primaryjoin="Piece.public_id==DangerousGood.piece_id")
    #transport_segments = relationship("TransportSegment", primaryjoin="Piece.public_id==TransportSegment.piece_id")

    
    @property
    def hash(self):
        """
        From the fields, compute the hash
        """
        tokens = [self.public_id, self.created_on, self.additional_security_info, self.coload, self.containing_piece, self.goods_description, 
        self.load_type, self.slac, self.stackable, self.turnable, self.upid ]
        str_token = map(self.prepare_token, tokens)
        payload = ";".join(str_token)
        current_hash = hu.digest(payload).hex()
        return current_hash

    def prepare_token(self,field):
        return field if field else ''
    
    def __repr__(self):
        return "<Piece id '{}', description '{}', created_on '{}'>".format(self.public_id, self.goods_description, self.created_on)
