import datetime

from sqlalchemy import Column
from sqlalchemy.orm  import relationship

from app.main.config import key
from app.main.services import db, flask_bcrypt
from app.main.util.hashutils import hu


class Product(db.Model):
    """ Product Model for storing product descriptions """
    __tablename__ = "product"

    public_id = db.Column(db.String(100), unique=True, primary_key = True)
    created_on = db.Column(db.DateTime, nullable=False)
    brand_owner_company_id = db.Column(db.String(100), db.ForeignKey('company.public_id'))
    brand_owner = relationship("Company",  foreign_keys=[brand_owner_company_id], back_populates="branded_products") 
    commodity_code = db.Column(db.String(255), unique=False, nullable=False)
    commodity_description = db.Column(db.String(255), unique=False, nullable=False)
    commodity_name = db.Column(db.String(255), unique=False, nullable=True)
    commodity_type = db.Column(db.String(255), unique=False, nullable=False)
    manufacturer_company_id = db.Column(db.String(100), db.ForeignKey('company.public_id')) #companies like locations can be created in different nodes, when the owner appears, its public_id is changed and the change should reflect also here
    manufacturer = relationship("Company",  foreign_keys=[manufacturer_company_id], back_populates="manufactured_products") 
    product_description = db.Column(db.String(255), unique=False, nullable=True)
    product_identifier = db.Column(db.String(255), unique=True, nullable=False)
    un_number = db.Column(db.String(255), unique=False, nullable=True)
    unique_hash = db.Column(db.String(255), unique=True, nullable=False, index=True)
    pieces = relationship('Piece', secondary = 'product_piece_composition', cascade = 'all, delete')

    __table_args__ = (db.UniqueConstraint('product_identifier', 'product_description', name='_product_id_description_uc'),)
    
    @property
    def hash(self):
        """
        From the fields, compute the hash
        """
        tokens = [self.product_description, self.product_identifier,  ]
        str_token = map(self.prepare_token, tokens)
        payload = ";".join(str_token)
        current_hash = hu.digest(payload).hex()
        return current_hash

    def prepare_token(self,field):
        return field if field else ''
    
    def __repr__(self):
        return "<product identifier '{}', description '{}', created_on '{}'>".format(self.product_identifier, self.commodity_description, self.created_on)
