import datetime

from app.main.config import key
from app.main.services import db, flask_bcrypt


class ProductionOrder(db.Model):
    """ Production Order Model for storing waybill related details """
    __tablename__ = "productionorder"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    public_id = db.Column(db.String(100), unique=True, index=True)
    name = db.Column(db.String(100), unique=False)
    hash_id = db.Column(db.String(100), unique=True, index=True)
    signed_hash = db.Column(db.String(255))
    created_on = db.Column(db.DateTime, nullable=False)
    expcted_delivery_within = db.Column(db.DateTime, nullable=False)
    brand_company_id = db.Column(db.String(100), db.ForeignKey('company.public_id'), nullable=False)
    step_id = db.Column(db.String(100), db.ForeignKey('shipment.public_id'), nullable=False)
    serial_numbers = db.Column(db.Text(), nullable=False)
    product_name = db.Column(db.String(255))
    
    __table_args__ = (db.UniqueConstraint('name', 'step_id', name='_name_step_id_uc'),)


    @property
    def hash(self):
        """
        From the fields, compute the hash
        """
        tokens = [self.name, self.expcted_delivery_within.isoformat(), self.serial_numbers, self.brand_company_id]
        str_token = map(self.prepare_token, tokens)
        payload = ";".join(str_token)
        hu = HashUtils()
        current_hash = hu.digest(payload).hex()
        return current_hash

    def prepare_token(self,field):
        return field if field else ''

    def __repr__(self):
        return "<production order '{}' type '{}' for Shipment '{}' by company '{}'>".format(self.hash_id, self.waybill_type, self.shipment_id, self.company_id)