import datetime

from app.main.config import key
from app.main.services import db, flask_bcrypt
from app.main.util.hashutils import hu

from app.main.model.product import Product


class ProductPieceComposition(db.Model):
    """ Product Model for storing product descriptions """
    __tablename__ = "product_piece_composition"

    product_id = db.Column(db.String(100), db.ForeignKey('product.public_id'), primary_key = True)
    piece_id = db.Column(db.String(100), db.ForeignKey('piece.public_id'), primary_key = True)
    
    @property
    def hash(self):
        """
        From the fields, compute the hash
        """
        tokens = [self.product_id, self.piece_id ]
        str_token = map(self.prepare_token, tokens)
        payload = ";".join(str_token)
        current_hash = hu.digest(payload).hex()
        return current_hash

    def prepare_token(self,field):
        return field if field else ''
    
    def __repr__(self):
        return "<product '{}', piece '{}'>".format(self.product_id, self.piece_id)
