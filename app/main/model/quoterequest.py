import datetime
from sqlalchemy import Column
from sqlalchemy.orm  import relationship

from app.main.config import key
from app.main.services import db, flask_bcrypt




class QuoteRequest(db.Model):
    """ Quote Request Model for storing TS related details """
    __tablename__ = "quote_request"

    public_id = db.Column(db.String(100), unique=True, primary_key=True,)
    created_on = db.Column(db.DateTime, nullable=False)
    allotment = db.Column(db.String(255), unique=False, nullable=True)
    dangerous_goods = db.Column(db.String(255), unique=False, nullable=True)
    rating_preferences = db.Column(db.String(255), unique=False, nullable=True)
    requester_company_id = db.Column(db.String(100), db.ForeignKey('company.public_id'))
    requester = relationship("Company", foreign_keys=[requester_company_id])
    routing_preference = db.Column(db.String(255), unique=False, nullable=True)
    security_status = db.Column(db.String(255), unique=False, nullable=True) 
    shipment_id = db.Column(db.String(100), db.ForeignKey('shipment.public_id'))
    shipment = relationship("Shipment",  foreign_keys=[shipment_id]) 
    supplier_company_id = db.Column(db.String(100), db.ForeignKey('company.public_id'))
    supplier = relationship("Company", foreign_keys=[supplier_company_id])
    transport_segment_id = db.Column(db.String(100), db.ForeignKey('transport_segment.public_id'))
    transport_segment = relationship("TransportSegment",  foreign_keys=[transport_segment_id])
    units_preference = db.Column(db.String(255), unique=False, nullable=True)
    latest_log = db.Column(db.String(255), unique=False, nullable=True)
    notarisation_session_id = db.Column(db.String(255), unique=False, nullable=True)
    notarisation_item_id =  db.Column(db.String(255), unique=False, nullable=True)
    notarised_on = db.Column(db.DateTime, nullable=True)

    #the same requesting company, toward the same offering company, for the same shipment on the same route can open only 1 quote request
    __table_args__ = (db.UniqueConstraint('requester_company_id', 'shipment_id',  'supplier_company_id',  'transport_segment_id', name='_quote_request_uc'),)

    
    
    def __repr__(self):
        return "<Quote Request '{}', transport segmnent '{}', routing preference '{}', supplier '{}'>".format(self.public_id, self.transport_segment_id, self.routing_preference, self.supplier_company_id)

    