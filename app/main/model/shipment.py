import datetime
from sqlalchemy.orm  import relationship

from app.main.config import key
from app.main.services import db, flask_bcrypt
#from app.main.util.hashutils import HashUtils


class Shipment(db.Model):
    """ Shipment Model for storing location related details """
    __tablename__ = "shipment"

    public_id = db.Column(db.String(100), unique=True, primary_key=True,)
    created_on = db.Column(db.DateTime, nullable=False)
    height_um = db.Column(db.String(100), unique=False, nullable=True)
    height_value = db.Column(db.DECIMAL(10,4), unique=False, nullable=True)
    length_um = db.Column(db.String(100), unique=False, nullable=True)
    length_value = db.Column(db.DECIMAL(10,4), unique=False, nullable=True)
    volume_um = db.Column(db.String(100), unique=False, nullable=True)
    volume_value = db.Column(db.DECIMAL(10,4), unique=False, nullable=True)
    width_um = db.Column(db.String(100), unique=False, nullable=True)
    width_value = db.Column(db.DECIMAL(10,4), unique=False, nullable=True)
    external_reference = db.Column(db.String(255), unique=False, nullable=True)
    goods_description = db.Column(db.String(255), unique=False, nullable=True)
    insurance_details = db.Column(db.String(255), unique=False, nullable=True)
    total_gross_weight_um = db.Column(db.String(255), unique=False, nullable=False)
    total_gross_weight_value =  db.Column(db.DECIMAL(10,4), unique=False, nullable=False)
    total_slac = db.Column(db.Integer, unique=False, nullable=True)
    volumetric_weight_conversion_factor_um = db.Column(db.String(100), unique=False, nullable=False)
    volumetric_weight_conversion_factor_value = db.Column(db.DECIMAL(10,4), unique=False, nullable=False)
    volumetric_weight_chargeable_weight_um = db.Column(db.String(100), unique=False, nullable=False)
    volumetric_weight_chargeable_weight_value = db.Column(db.DECIMAL(10,4), unique=False, nullable=False)
    origin_hash = db.Column(db.String(100), unique=True, nullable=True) #shipment object with pieces and products
    signed_origin_hash = db.Column(db.String(255), unique=True, nullable=True)
    originator_pubkey = db.Column(db.String(255), unique=False, nullable=False) #if creating the public_id, then this is auto-populated with the local key, must be imported otherwise
    notarisation_session_id = db.Column(db.String(255), unique=False, nullable=True)
    notarisation_item_id =  db.Column(db.String(255), unique=False, nullable=True)
    notarised_on = db.Column(db.DateTime, nullable=True)
    pieces = relationship("Piece", primaryjoin="Shipment.public_id==Piece.shipment_id")
    disclosures = relationship("Disclosure", cascade="all, delete-orphan")
    timestamp = relationship("Timestamp", cascade="all, delete-orphan")
    
    def __repr__(self):
        return "<Id '{}', goods '{}', created_on '{}'>".format(self.public_id, self.goods_description, self.created_on)
