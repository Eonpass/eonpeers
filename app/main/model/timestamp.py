import datetime
from sqlalchemy.orm  import relationship

from app.main.config import key
from app.main.services import db, flask_bcrypt


class Timestamp(db.Model):
    """ Timestamp Model for storing blockchain timestamp related details """
    __tablename__ = "timestamp"

    public_id = db.Column(db.String(100), unique=True, primary_key=True,)
    created_on = db.Column(db.DateTime, nullable=False)
    shipment_id = db.Column(db.String(100), db.ForeignKey('shipment.public_id'))
    body = db.Column(db.Text, nullable=False)
    status = db.Column(db.String(10), nullable=False, default='new', index=True)
    notarisation_session_id = db.Column(db.String(255), unique=False, nullable=False)
    notarisation_item_id =  db.Column(db.String(255), unique=False, nullable=False)

    
    def __repr__(self):
        return "<Id '{}', shipment '{}', created_on '{}'>".format(self.public_id, self.shipment_id, self.created_on)
