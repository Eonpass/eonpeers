import datetime
from sqlalchemy import Column
from sqlalchemy.orm  import relationship

from app.main.config import key
from app.main.services import db, flask_bcrypt




class TransportSegment(db.Model):
    """ Transport Segment Model for storing TS related details """
    __tablename__ = "transport_segment"

    public_id = db.Column(db.String(100), unique=True, primary_key=True,)
    created_on = db.Column(db.DateTime, nullable=False)
    arrival_location_id = db.Column(db.String(100), db.ForeignKey('location.public_id'))
    arrival_location = relationship("Location",  foreign_keys=[arrival_location_id]) 
    co2_calculation_method = db.Column(db.String(255), unique=False, nullable=True)
    co2_emission_value = db.Column(db.DECIMAL(10,4), unique=False, nullable=True)
    co2_emission_unit = db.Column(db.String(10), unique=False, nullable=True)
    departure_location_id = db.Column(db.String(100), db.ForeignKey('location.public_id')) 
    departure_location = relationship("Location", foreign_keys=[departure_location_id])
    distance_calculated_value = db.Column(db.DECIMAL(10,4), unique=False, nullable=True)
    distance_calculated_unit = db.Column(db.String(10), unique=False, nullable=True)
    distance_measured_value = db.Column(db.DECIMAL(10,4), unique=False, nullable=True)
    distance_measured_unit = db.Column(db.String(10), unique=False, nullable=True)
    fuel_amount_calculated_value = db.Column(db.DECIMAL(10,4), unique=False, nullable=True)
    fuel_amount_calculated_unit = db.Column(db.String(10), unique=False, nullable=True)
    fuel_amount_measured_value = db.Column(db.DECIMAL(10,4), unique=False, nullable=True)
    fuel_amount_measured_unit = db.Column(db.String(10), unique=False, nullable=True)
    fuel_type = db.Column(db.String(100), unique=False, nullable=True)
    mode_code = db.Column(db.String(100), unique=False, nullable=True)
    movement_type = db.Column(db.String(100), unique=False, nullable=True)
    piece_id = db.Column(db.String(100), db.ForeignKey('piece.public_id'))
    piece = relationship("Piece", foreign_keys=[piece_id])
    seal = db.Column(db.String(100), unique=False, nullable=True)
    segment_level = db.Column(db.String(100), unique=False, nullable=True)
    transport_date = db.Column(db.DateTime, nullable=True)
    transport_identifier = db.Column(db.String(100), unique=False, nullable=True)
    pieces = relationship('Piece', secondary = 'transport_segment_piece_composition', cascade = 'all, delete')
    #unique contraint: the same piece cannot have 2 TS on the same A->B route for the same responsible company <--- add responsible company?
    
    
    def __repr__(self):
        return "<Transport Segment '{}', departure '{}', arrival '{}'>".format(self.public_id, self.departure_location_id, self.arrival_location_id)

    