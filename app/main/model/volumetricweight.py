import datetime

from app.main.config import key
from app.main.services import db, flask_bcrypt
from app.main.util.hashutils import HashUtils

""""
    TODO: remove, this is present in IATA One Record but doesn't make a lot of sense and adds risks of overwriting data.
    All ther relevant fields have been ported directly into Piece.

"""


class VolumetricWeight(db.Model):
    """ VolumetricWeight Model for storing volumetric descriptions """
    __tablename__ = "volumetric_weight"

    public_id = db.Column(db.String(100), unique=True, primary_key = True)
    conversion_factor_um = db.Column(db.String(100), unique=False, nullable=False)
    conversion_factor_value = db.Column(db.Decimal(10,4), unique=False, nullable=False)
    chargeable_weight_um = db.Column(db.String(100), unique=False, nullable=False)
    chargeable_weight_value = db.Column(db.Decimal(10,4), unique=False, nullable=False)
    piece_id = db.Column(db.String(100), db.ForeignKey('piece.public_id'))
    piece = relationship("Piece", back_populates="volumetric_weight")

    @property
    def hash(self):
        """
        From the fields, compute the hash
        """
        tokens = [self.public_id, self.conversion_factor_um, self.conversion_factor_value, self.chargeable_weight_um, self.chargeable_weight_value, self.piece_id]
        str_token = map(self.prepare_token, tokens)
        payload = ";".join(str_token)
        hu = HashUtils()
        current_hash = hu.digest(payload).hex()
        return current_hash

    def prepare_token(self,field):
        return field if field else ''
    
    def __repr__(self):
        return "<id '{}', conversion factor '{}''{}', weight value '{}''{}'>".format(self.public_id, self.conversion_factor_value, self.conversion_factor_um, self.chargeable_weight_value, self.chargeable_weight_um)
