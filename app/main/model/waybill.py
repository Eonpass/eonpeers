import datetime

from sqlalchemy.orm  import relationship

from app.main.config import key
from app.main.services import db, flask_bcrypt


class Waybill(db.Model):
    """ Waybill Model for storing waybill related details """
    __tablename__ = "waybill"

    public_id = db.Column(db.String(100), unique=True, primary_key=True,)
    name = db.Column(db.String(100), unique=False, nullable=False)
    export_reference = db.Column(db.String(100), unique=False)
    created_on = db.Column(db.DateTime, nullable=False)
    shipment_id = db.Column(db.String(100), db.ForeignKey('shipment.public_id'), nullable=False)
    shipment = relationship("Shipment", foreign_keys=[shipment_id])
    waybill_type = db.Column(db.String(255))
    waybill_file_type = db.Column(db.String(255))
    base64encodedfilebody = db.Column(db.Text(), nullable=True)
    latest_log =  db.Column(db.String(100), unique=False, nullable=True)
    __table_args__ = (db.UniqueConstraint('name', 'shipment_id', name='_name_shipment_id_uc'),)

    def __repr__(self):
        return "<waybill '{}' type '{}' for Shipment '{}'>".format(self.name, self.waybill_type, self.shipment_id)

    

