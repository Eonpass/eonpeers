import datetime
import uuid
import json

from flask_restplus import marshal

from app.main.model.booking import Booking
from app.main.model.offer import Offer 
from app.main.services import db
from app.main.util.eonerror import EonError
from app.main.util.keymanagementutils import kmc
from app.main.util.verifierutils import verifier
from app.main.util.hashutils import hu
from app.main.util.dto import BookingDto
from app.main.util.tasks import send_booking


def save_new_booking(*args, **kwargs):
    """
    Save a new Booking

    The Booking id is created by the creator node, other nodes will inherit it. This method is used by the creator node,
    a node which inherits the offer will use the import method (and the sender the export method)

    Parameters
    ----------
    args[0]: dict
        data, JSON payload received from the controller

    args[1]: str
        public_id, if this new Booking comes with an ID, it's inherited (i.e. "creator lead ID")

    Response
    --------
    [dict, int]
        object with the message, int is the http status code to be returned

    Raises
    ------
    EonError
        Error that contains a message and http code to be returned by the controller to the client

    """
    data = args[0]
    public_id = args[1] if (len(args)>1) else None

    if(data.get('offer_id', None)):
        offer = _get_an_offer(data["offer_id"])
        if not offer:
            raise EonError('Offer does not exist in this node.', 404)  
    else:
        #this should never happen due to DTO but still..
        raise EonError('Offer is mandatory.', 404)  

    try:
        now = datetime.datetime.utcnow()    
        new_booking = Booking(
            public_id=public_id if public_id else str(uuid.uuid4()),
            created_on=now,
            price_value=data.get('price_value', offer.price_value),
            price_um=data.get('price_um', offer.price_um),
            offer_id=data['offer_id'],
            routing=data.get('routing', None),
            security_status=data.get('security_status', None)
        )
        _save_changes(new_booking) 
        return _generate_creation_ok_message(new_booking)

    except Exception as e:
        print(e)
        db.session.rollback() 
        raise EonError('Another booking already exists with the given data (same offer).', 409)

def update_booking(data):
    """
    update a booking given its local identifier

    we expect to receive at least the mandatory fields public_id, if relation fields are empty relations are removed, if other fields are empty old values are preserved.
    This function is designed to be called by the node owner, not for direct P2P use

    """
    booking = Booking.query.filter_by(public_id=data['public_id']).first()
    if booking:
        if(data.get('offer_id', None)):
            offer = _get_an_offer(data["offer_id"])
            if not offer:
                raise EonError('Offer does not exist in this node.', 404)  
        else:
            #this should never happen due to DTO but still..
            raise EonError('Offer is mandatory.', 404)  

        booking.price_value = data.get('price_value', None) if data.get('price_value', None) else offer.price_value
        booking.price_um = data.get('price_um', None) if data.get('price_um', None) else offer.price_um
        booking.routing = data.get('routing', None) if data.get('routing') else offer.routing
        booking.security_status = data.get('security_status', None) if data.get('security_status', None) else offer.security_status

        #a booking cannot change offer!
        
        try:
            _save_changes(booking)
        except Exception as e:
            print(e)
            db.session.rollback() 
            raise EonError('Internal Server Error', 500) 

        return _generate_update_ok_message(booking)
    else:
        db.session.rollback() 
        raise EonError('Booking not found.', 404)

def upsert_booking(data):
    """
    upsert booking

    this is used during the import step, so that a booking can be updated with new information. A booking update is rejected if:
    - the public key is different from the one known for the supplier company (checked in the import step)
    """
    if(data["public_id"]):
        booking = Booking.query.filter_by(public_id=data['public_id']).first()
        if booking:
            return update_booking(data)
        else:
            return save_new_booking(data, data["public_id"])
    else:
        return save_new_booking(data)

def get_all_bookings():
    """
    TODO: only the node admin or the requester/supplier of an offer should be able to query for it
    """
    return Booking.query.filter_by().all()

def get_a_booking(public_id):
    """
    TODO: only the node admin or the requester/supplier of an offer should be able to query for it
    """
    return Booking.query.filter_by(public_id=public_id).first()

def export_booking(public_id):
    """
    check the booking, then get the node details of the requester company and send call its /booking/rpc/import method

    This method is called after receiving the "accept offer".
    
    Parameters
    ----------
    public_id: str
        the id of the booking to export, the suppllier_id is the target node (the sender is the current node and requester_id)

    Response
    --------
    dict
        body for the response message, a JSON with the payload and the signed payload

    Raises
    ------
    EonError
        Error that contains a specific message and http code to be returned by the controller to the client

    """
    booking = _check_booking(public_id)
    #prepare the data for the export:
    target_url = booking.offer.quote_request.requester.base_url
    export_payload = _prepare_booking_export_payload(booking)
    _task = send_booking.delay(target_url, export_payload)
    return export_payload

def _check_booking(public_id):
    """
    Check that a booking has the minimum related objects before sending it out
    
    An offer should have at least the quote request and the price

    Parameters
    ----------
    public_id: str
        the id of the booking to check

    Response
    --------
    Object
        Booking, the booking object if it passes every check

    Raises
    ------
    EonError
        Error that contains a specific message and http code to be returned by the controller to the client

    """
    booking = Booking.query.filter_by(public_id=public_id).first()  
    if not booking:
        raise EonError('Booking does not exist.', 404)

    if(booking.offer == None):
        raise EonError('Booking data is corrupt, missing offer', 409)  
    if(booking.offer.quote_request.supplier ==  None):
        raise EonError('Connected Quote Request missing supplier company.', 409)        
    if(booking.offer.quote_request.requester ==  None):
        raise EonError('Connected Quote Request missing requester company.', 409)
    if(booking.offer.quote_request.shipment ==  None):
        raise EonError('Connected Quote Request missing shipment.', 409)
    if(booking.offer.quote_request.transport_segment ==  None):
        raise EonError('Connected Quote Request missing transport segment.', 409)
    if(booking.offer.quote_request.transport_segment.arrival_location == None or booking.offer.quote_request.transport_segment.departure_location == None):
        raise EonError('Connected Transport segment is missing information.', 409)        
    if(len(booking.offer.quote_request.shipment.pieces)==0):
        raise EonError('Connected Shipment is missing attached pieces.', 409)     
    
    return booking

def _prepare_booking_export_payload(booking):
    #the booking has a simpler body as it expects all other objects to have been created by a previous quote request import
    #get the payload and sign it
    payload = marshal(booking,BookingDto.booking) 
    payload_to_sign = bytes.fromhex(hu.digest(json.dumps(payload, sort_keys=True)).hex())
    signed_payload_hash = kmc.sign_message(payload_to_sign)
    signed_payload_hash_hex = signed_payload_hash['signed'].hex()
    export_payload = {
        "payload":payload,
        "signed_hash":signed_payload_hash_hex,
        "supplier_company_seralised_public_key": kmc.get_serialized_pub_key().decode('utf-8')
    }
    return export_payload   

def import_booking(import_payload):
    """
    get the related quote, check that the supplier public key matches the payload public key, then create or update the booking
    """
    #find the quote and the supplier:
    offer =  _get_an_offer(import_payload["payload"]["offer_id"])
    if(offer.quote_request.supplier.public_key != import_payload["supplier_company_seralised_public_key"]):
        raise EonError("Public keys do no match, please align the keys first", 400)

    #TODO: check if this offer id doesn't exist already (could be someone trying to override the same id with a different payload)

    #check signatures and payloads:
    #signed = bytes from hex (signed_message)
    signed_bytes = bytes.fromhex(import_payload["signed_hash"])
    #message = compute the hash of payload
    message = bytes.fromhex(hu.digest(json.dumps(import_payload["payload"], sort_keys=True)).hex())
    #serialized public should already be fine
    try:
        #in the future different verifier types may require different args..
        #verifier_type = current_app.config['VERIFIER_TYPE']
        #if verifier_type == 'STD'
        signature_checks_out = verifier.verify_payload(signed_bytes, message, import_payload["supplier_company_seralised_public_key"])
        if(not signature_checks_out):
            raise EonError("Signed message is corrupt", 400)        
    except Exception as e:
        print(e)
        raise EonError("Signed message is corrupt", 400) 

    booking_res = upsert_booking(import_payload["payload"])

def _get_an_offer(public_id):
    """
    query the DB for an offer
    """
    return Offer.query.filter_by(public_id=public_id).first()

def _save_changes(data):
    db.session.add(data)
    db.session.commit()

def _generate_creation_ok_message(quote):
    try:
        response_object = {
            'status': 'success',
            'message': 'New booking created and currently being checked.',
            'public_id': quote.public_id
        }
        return response_object, 201
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401

def _generate_update_ok_message(quote):
    try:
        response_object = {
            'status': 'success',
            'message': 'Booking locally updated.',
            'public_id': quote.public_id
        }
        return response_object, 200
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401


