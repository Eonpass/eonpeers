import datetime
import uuid
import os
import base58
import json

from sqlalchemy import or_

from app.main.model.company import Company
from app.main.model.product import Product
from app.main.services import db, flask_bcrypt
from app.main.util.tasks import validate_new_company, create_user_for_company
from app.main.util.eonerror import EonError
from app.main.util.keymanagementutils import kmc
from app.main.util.hashutils import hu


def save_new_company(data):
    company = Company.query.filter_by(vat_number=data['vat_number'], fiscal_country=data['fiscal_country']).first()
    if not company:
        unique_hash = _generate_unique_hash_company(data)
        new_company = Company(
            public_id = str(uuid.uuid4()),
            name = data['name'],
            vat_number = data['vat_number'],
            created_on = datetime.datetime.utcnow(),
            is_own = False,
            base_url = data['base_url'],
            public_key = data.get('public_key', None),
            fiscal_address = data.get('fiscal_address', None),
            fiscal_city = data.get('fiscal_city', None),
            fiscal_province = data.get('fiscal_province', None),
            fiscal_country = data.get('fiscal_country', None),
            fiscal_zip =data.get('fiscal_zip', None),
            unique_hash=unique_hash
        )
        try:
            _save_changes(new_company) 
        except Excpetion as e:
            #unique contraints fails - custom logics
            db.session.rollback()
            raise EonError('Another company already exists with the given data.', 409)

        #GET data from the new base_url and check if it matches
        if(data['base_url']):
            _task = validate_new_company.delay(data['base_url'], new_company.public_id)

        return _generate_creation_ok_message(new_company)
    else:
       raise EonError('Another company already exists with the given data.', 409)


def update_company(data):
    company = Company.query.filter_by(vat_number=data['vat_number'], fiscal_country=data['fiscal_country']).first()
    if company:

        unique_hash = _generate_unique_hash_company(data)

        company.name = data.get('name', None) if data.get('name', None) else company.name
        company.vat_number = data.get('vat_number', None) if data.get('vat_number') else company.vat_number
        company.base_url = data.get('base_url', None) if data.get('base_url') else company.base_url
        company.public_key = data.get('public_key', None) if data.get('public_key') else company.public_key
        company.fiscal_address = data.get('fiscal_address', None) if data.get('fiscal_address') else company.fiscal_address
        company.fiscal_city = data.get('fiscal_city', None) if data.get('fiscal_city') else company.fiscal_city
        company.fiscal_province = data.get('fiscal_province', None) if data.get('fiscal_province') else company.fiscal_province
        company.fiscal_country = data.get('fiscal_country', None) if data.get('fiscal_country') else company.fiscal_country
        company.fiscal_zip = data.get('fiscal_zip', None) if data.get('fiscal_zip') else company.fiscal_zip
        company.unique_hash = unique_hash

        try:
            _save_changes(company) 
        except Excpetion as e:
            #unique contraints fails - custom logics
            db.session.rollback()
            raise EonError('Another company already exists with the given data.', 409)

        #GET data from the new base_url and check if it matches
        if(data.get('base_url', None)):
            _task = validate_new_company.delay(data['base_url'], company.public_id)

        return _generate_update_ok_message(company)
    else:
       raise EonError('Company does not exist.', 404)


def get_all_companies():
    return Company.query.filter_by().all()

def get_node_owner():
    """ 
    Gets the details of the company owning the node, 

    Passed to the DTO in the controller, only meaningful fields are put in the response.
    Notice that the key is retrieved via KMC, so it's returned in bytes and thus decoded.

    """
    owner = Company.query.filter_by(is_own=True).first()
    owner.public_key = kmc.get_serialized_pub_key().decode('utf-8')
    return owner

def mark_company_as_node_owner(public_id):
    """
    A node can have one company which describes its owner, so that other nodes get that information and store as reference to the node.
    ADMIN ONLY - the controllers requires an admin token

    There can be only 1 node owner per node, once set the details will have to be changed with update_company.
    What happens if someone registers a node with the company of someonelse? This should be addressed by the EUIPO
    which will hold a registry of public keys. Alternatively for now Eonpeers is a closed network.
    """
    already_admin = Company.query.filter_by(is_own=True).first()
    if(already_admin):
        raise EonError('This node already has an admin.', 409)

    company = Company.query.filter_by(public_id=public_id).first()
    if(not company):
        raise EonError('Company not found.', 404)

    company.public_key = kmc.get_serialized_pub_key().decode('utf-8')
    company.is_own=True
    _save_changes(company)
    return _generate_update_ok_message(company)

def get_a_company(public_id):
    """
    Return a company matching its local public_id or the unique_hash

    Different node will have different public_ids, but will agree on the unique_hash
    """
    #if this is a 32 characters string, it's an hash, if it has "-" it's an UUID4
    if not public_id:
        return None
    if("-" in public_id):
        return Company.query.filter_by(public_id=public_id).first()
    else:
        return Company.query.filter_by(unique_hash=public_id).first()
    
    #the old approach below could lead to linking the wrong company if two nodes generate the same id for different companies
    #return Company.query.filter(or_((Company.public_id==public_id),(Company.unique_hash==public_id))).first()

def get_branded_products_of_a_company(company_id):
    return Product.query.filter_by(brand_owner_company_id=company_id).all()

def get_manufactured_products_of_a_company(company_id):
    return Product.query.filter_by(manufacturer_company_id=company_id).all()

def create_user_on_remote_node(data, public_id):
    """
    Prepare the payload and send it to the remote node to create a user there connected to the company

    This function also saves locally the authorisation details to reach the remote node. Password should be encrypted to reduce toxic material in the db.
    TODO: possibly every field should be encrypted, research best practices and performance impact.

    Parameters
    ----------
    data: dict
        data, JSON payload received from the controller
            username: str
                username of the user on the remote node
            password: str
                password of the user on the remote node
            email:str
                email related to the remote user
    public_id: str
        id of the company for which we want to create the user on the remote node

    Response
    --------
    dict, 
        payload to send to the remote end

    Raises
    ------
    EonError
        Error that contains a message and http code to be returned by the controller to the client
    """
    company = Company.query.filter_by(public_id=public_id).first()
    #update company
    company.remote_username = data['username']
    company.remote_password = data['password'] #this should be encrypted
    company.remote_email = data['email']
    _save_changes(company)

    password_hash = flask_bcrypt.generate_password_hash(data['password']).decode('utf-8')
    signed_password_hash = kmc.sign_message(password_hash)
    signed_password_hash_hex = signed_password_hash['signed'].hex()
    payload = {
        'company_public_key':kmc.get_serialized_pub_key().decode('utf-8'), #will be used by the remote node to understand which company
        'username':data['username'], 
        'email':data['email'],
        'password_hash':password_hash,
        'signed_pwd_hash':signed_password_hash_hex #will be used to make sure this is indeed coming from the expected company owner
    }

    #send out the message with the task:
    if company.base_url:
        create_user_for_company(company.base_url, payload, public_id)
    else:
        raise EonError("Missing remote node URL", 409)

    return _generate_update_ok_message(company)

def update_local_key(data):
    """
    Update the local key on the node by overwriting the local pem file

    Notice that this is meaningful only in testing and dev environments, production should not use the DEV keymanagement type

    Parameters
    ----------
    data: dict
        data, JSON payload received from the controller, this is a JWK formatted key

    Response
    --------
    dict, 
        payload with ok message and the new pem econded public key

    Raises
    ------
    EonError
        Error that contains a message and http code to be returned by the controller to the client
    """
    pem = kmc.convert_jwk_to_pem(data)
    public_pem = kmc.overwrite_local_key(pem)
    admin_company = Company.query.filter_by(is_own=True).first()
    admin_company.public_key = kmc.get_serialized_pub_key().decode('utf-8')
    _save_changes(admin_company)
    return {"status":"success", "new_pem_pubkey":public_pem.decode('utf-8')}, 200

def update_local_did(data):
    """
    Update the local did on the node by overwriting the old value if any

    Parameters
    ----------
    data: dict
        data, JSON payload received from the controller, this is simply {"did":"did:ebsi:..."} or any other type of did

    Response
    --------
    dict, 
        payload with ok message and the new pem econded public key

    Raises
    ------
    EonError
        Error that contains a message and http code to be returned by the controller to the client
    """
    admin_company = Company.query.filter_by(is_own=True).first()
    #TODO: decide how to pass dids, JSON or JSONString?
    #in the db it is saved as JSONString
    admin_company.did = data.get('did', None);
    _save_changes(admin_company)
    return {"status":"success", "new did":admin_company.did}, 200

def create_local_did(data):
    """
    Create a new did for this node and link it to the admin company (node owner)
    This requires the local node to have been initialized with the node owner and its key

    Parameters
    ----------
    data: dict
        data, JSON payload received from the controller, this is simply {"did_type":"did:ebsi"} or "did:key"

    Response
    --------
    dict, 
        payload with ok message and the new pem econded public key

    Raises
    ------
    EonError
        Error that contains a message and http code to be returned by the controller to the client
    """
    admin_company = Company.query.filter_by(is_own=True).first()
    did_type = data.get('did_type', None);
    new_did = ""
    # Generate 17 random bytes
    random_bytes = os.urandom(17)
    # Prepend a '1' byte
    random_bytes = b'\x01' + random_bytes
    encoded = base58.b58encode(random_bytes).decode('utf-8')
    if(did_type and did_type == "did:key"):
        #16 random bytes as well?
        new_did = "did:key:z"+encoded
    else:
        new_did = "did:ebsi:z"+encoded
    #build the full did body with the local key;
    publicjwk = kmc.convert_localpem_to_publicjwk()
    publicjwk["kid"] = new_did+"#"+publicjwk["kid"] #EBSI convention
    full_did = {
        "@context":[
            "https://www.w3.org/ns/did/v1","https://w3id.org/security/suites/jws-2020/v1"
        ],
        "id":new_did,
        "verificationMethod":[{
            "id": publicjwk["kid"],
            "type": "JsonWebKey2020",
            "controller": new_did,
            "publicKeyJwk": publicjwk
        }],
        "authentication":[publicjwk["kid"]],
        "assertionMethod":[publicjwk["kid"]],
        "capabilityInvocation":[publicjwk["kid"]]
    }
    admin_company.did = json.dumps(full_did)
    _save_changes(admin_company)
    return {"status":"success", "new did":admin_company.did}, 200

def _save_changes(data):
    db.session.add(data)
    db.session.commit()

def _generate_unique_hash_company(data):
    """
    Return the alternative ID based on concatenation of UQ constraint.

    TODO: double check what happens when I change the public_id but not the unique_hash and vice versa. What if an import quote requests connects to the wrong company?

    """
    payload = data['vat_number']+data['fiscal_country']
    return hu.digest(payload).hex()

def _generate_creation_ok_message(company):
    try:
        response_object = {
            'status': 'success',
            'message': 'New company created and currently being checked.',
            'public_id': company.public_id
        }
        return response_object, 201
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401

def _generate_update_ok_message(company):
    try:
        response_object = {
            'status': 'success',
            'message': 'Company updated and currently being checked.',
            'public_id': company.public_id
        }
        return response_object, 201
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401
