import datetime
import uuid
import math

from app.main.model.customsinfo import CustomsInfo
from app.main.model.piece import Piece
from app.main.services import db
from app.main.util.eonerror import EonError
from app.main.util.keymanagementutils import kmc


def save_new_customs_info(*args, **kwargs):
    """
    Save a new Customs Info

    The CustomsInfo id is created by the creator node, other nodes will inherit it 

    Parameters
    ----------
    args[0]: dict
        data, JSON payload received from the controller

    args[1]: str
        public_id, if this new CI comes with an ID, it's inherited (i.e. "creator lead ID")

    Response
    --------

    Raises
    ------
    EonError
        Error that contains a message and http code to be returned by the controller to the client

    """
    data = args[0]
    public_id = args[1] if (len(args)>1) else None
    _check_piece(data)
    _check_customs_info(data)


    try:

        new_ci = CustomsInfo(
            public_id=public_id if public_id else str(uuid.uuid4()),
            created_on=datetime.datetime.utcnow(),
            content_code= data.get('content_code', None),
            country_code= data.get('country_code', None),
            information= data.get('information', None),
            subject_code= data.get('subject_code', None),
            note=data.get('note', None),
            piece_id=data.get('piece_id', None)
        )

        #CI has no Unique Contraints, there is no risk of 409 here:
        _save_changes(new_ci) 

        return _generate_creation_ok_message(new_ci)
    except Exception as e:
        print(e)
        raise EonError('Another CI already exists with the given data.', 409)

def get_a_customs_info(public_id):
    return CustomsInfo.query.filter_by(public_id=public_id).first()

def get_all_piece_customs_info(piece_id):
    """ 
    Get all the Customs Info, 

    
    Parameters
    ----------
    piece_id: str
        id of the piece to get customs info for

    Returns:
    --------
    [obj]
        array of ORM objects (will be marshalled by the controller)

    Raises
    ------
    EonError:   
        500: manipulation goes wrong, db errors, etc..
        400: when there is something strange in the args (e.g. cannot cast to int)
    """
    return CustomsInfo.query.filter_by(piece_id=piece_id).all()

def upsert_a_customs_info(data):
    #Update a CI if you find it, create it from scratch if missing

    #TODO: double check that I'm updateting a CI that is linked only to the shipment ID it weas imported from
    ci = CustomsInfo.query.filter_by(public_id = data['public_id']).first()
    if not ci:
        return save_new_customs_info(data, data['public_id'])
    else:
        _check_piece(data)
        _check_customs_info(data)
        ci.content_code=data.get('content_code', None)
        ci.country_code=data.get('country_code', None)
        ci.information=data.get('information', None)
        ci.subject_code=data.get('subject_code', None)
        ci.note=data.get('note', None)
        ci.piece_id=data['piece_id'] #must be there
        _save_changes(ci)
        return _generate_update_ok_message(ci)

def _save_changes(data):
    db.session.add(data)
    db.session.commit()

def _check_piece(data):
    """ 
    check if the related piece exists, if not, raise error
    """
    if(not data.get("piece_id", None)):
            raise EonError('Missing Piece Id', 404) 
    else:
        piece = Piece.query.filter_by(public_id=data['piece_id']).first()
        if not piece:
            raise EonError('Connected Piece does not exist in this node.', 404) 

def _check_customs_info(data):
    """
    at least one of the three codes should be present 
    """
    if(not data.get("content_code", None) and not data.get("country_code", None) and not data.get("subject_code", None)):
        raise EonError('Customs Info must have at least one fo the codes - country, content, subject.', 409) 


def _generate_creation_ok_message(ci):
    try:
        response_object = {
            'status': 'success',
            'message': 'New customs info created.',
            'public_id': ci.public_id
        }
        return response_object, 201
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401

def _generate_update_ok_message(ci):
    try:
        response_object = {
            'status': 'success',
            'message': 'Customs Info udpated.',
            'public_id': ci.public_id
        }
        return response_object, 200
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401