import datetime
import uuid
import math

from app.main.model.dangerousgood import DangerousGood
from app.main.model.piece import Piece
from app.main.services import db
from app.main.util.eonerror import EonError
from app.main.util.keymanagementutils import kmc


def save_new_dangerous_good(*args, **kwargs):
    """
    Save a new Dangerous Good

    The DG id is created by the creator node, other nodes will inherit it 

    Parameters
    ----------
    args[0]: dict
        data, JSON payload received from the controller

    args[1]: str
        public_id, if this new DG comes with an ID, it's inherited (i.e. "creator lead ID")

    Response
    --------

    Raises
    ------
    EonError
        Error that contains a message and http code to be returned by the controller to the client

    """
    data = args[0]
    public_id = args[1] if (len(args)>1) else None
    _check_piece(data)

    try:

        new_dg = DangerousGood(
            public_id=public_id if public_id else str(uuid.uuid4()),
            created_on=datetime.datetime.utcnow(),
            class_code= data.get('class_code', None),
            net_quantity_value= data.get('net_quantity_value', None),
            net_quantity_um= data.get('net_quantity_um', None),
            packing_group= data.get('packing_group', None),
            packing_instruction=data.get('packing_instruction', None),
            un_number=data.get('un_number', None),
            piece_id=data.get('piece_id', None)
        )

        #TODO: DG should have a Unique Contraint, at least to avoid duplicate entries
        _save_changes(new_dg) 

        return _generate_creation_ok_message(new_dg)
    except Exception as e:
        print(e)
        raise EonError('Another DG already exists with the given data.', 409)

def get_a_dangerous_good(public_id):
    return DangerousGood.query.filter_by(public_id=public_id).first()

def get_all_piece_dangerous_goods(piece_id):
    """ 
    Get all the DungerousGoods, 

    
    Parameters
    ----------
    piece_id: str
        id of the piece to get customs info for

    Returns:
    --------
    [obj]
        array of ORM objects (will be marshalled by the controller)

    Raises
    ------
    EonError:   
        500: manipulation goes wrong, db errors, etc..
        400: when there is something strange in the args (e.g. cannot cast to int)
    """
    return DangerousGood.query.filter_by(piece_id=piece_id).all()

def upsert_a_dangerous_good(data):
    #Update a DG if you find it, create it from scratch if missing
    dg = DangerousGood.query.filter_by(public_id = data['public_id']).first()
    if not dg:
        return save_new_dangerous_good(data, data['public_id'])
    else:
        _check_piece(data)
        dg.class_code=data.get('class_code', None)
        dg.net_quantity_value=data.get('net_quantity_value', None)
        dg.net_quantity_um=data.get('net_quantity_um', None)
        dg.packing_group=data.get('packing_group', None)
        dg.packing_instruction=data.get('packing_instruction', None)
        dg.un_number=data.get('un_number', None)
        dg.piece_id=data['piece_id'] #must be there
        _save_changes(dg)
        return _generate_update_ok_message(dg)

def _save_changes(data):
    db.session.add(data)
    db.session.commit()

def _check_piece(data):
    """ 
    check if the related piece exists, if not, raise error
    """
    if(not data.get("piece_id", None)):
            raise EonError('Missing Piece Id', 404) 
    else:
        piece = Piece.query.filter_by(public_id=data['piece_id']).first()
        if not piece:
            raise EonError('Connected Piece does not exist in this node.', 404) 

def _generate_creation_ok_message(dg):
    try:
        response_object = {
            'status': 'success',
            'message': 'New dangerous good created.',
            'public_id': dg.public_id
        }
        return response_object, 201
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401

def _generate_update_ok_message(dg):
    try:
        response_object = {
            'status': 'success',
            'message': 'Dungerous Good udpated.',
            'public_id': dg.public_id
        }
        return response_object, 200
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401