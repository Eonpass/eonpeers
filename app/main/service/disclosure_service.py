import datetime
import uuid

from app.main.model.user import User
from app.main.model.shipment import Shipment
from app.main.model.disclosure import Disclosure
from app.main.services import db
from app.main.util.eonerror import EonError

from app.main.service.user_service import get_a_user
from app.main.service.shipment_service  import get_a_shipment

def save_new_disclosure(data):
    """
    Save a new Disclosure Request

    The disclosure is creted by an active user and later on approved by the node admin

    Parameters
    ----------
    data: dict
        data, JSON payload received from the controller

    Response
    --------
    dict, int
        per our convention services return the success JSON and the http code to return (200 in case of success, etc..)

    Raises
    ------
    EonError
        Error that contains a message and http code to be returned by the controller to the client

    """

    #DTO makes sure we have both user_id and shipment_id in the payload
    requesting_user = get_a_user(data.get('user_id'))
    if not requesting_user:
        raise EonError('User not found.', 404)

    shipment = get_a_shipment(data.get('shipment_id'))
    if not shipment:
        raise EonError('Shipment not found.', 404)  

    disclosure = Disclosure(
        user_id = data.get('user_id'),
        shipment_id = data.get('shipment_id'),
        created_on=datetime.datetime.utcnow()
    )
    try:
        _save_changes(disclosure)
        return _creation_success()
    except Exception as e:
        db.session.rollback()  
        print(e)
        raise EonError('Something went wrong creating requested disclosure.', 409) 

def get_a_disclosure(shipment_id, user_id):
    """
    Get the disclosre for a given shipment and user

    TODO: make sure current logged user is either admin or the requesting user
    
    Parameters
    ----------
    shipment_id: str
        id of the shipment
    user_id: str
        id of the user

    Response
    --------
    obj
        ORM object, will get marshalled by controller

    Raises
    ------
    EonError
        Error that contains a message and http code to be returned by the controller to the client
    """
    requesting_user = get_a_user(user_id)
    if not requesting_user:
        raise EonError('User not found.', 404)

    shipment = get_a_shipment(shipment_id)
    if not shipment:
        raise EonError('Shipment not found.', 404)  

    disclosure = Disclosure.query.filter_by(user_id=user_id, shipment_id=shipment_id).first()
    if not disclosure:
        raise EonError('Disclosure request not found.', 404)  

    return disclosure

def get_active_shipments_for_user(user_id):
    """
    get all active shipment ids for the user

    TODO: make sure current logged user is either admin or the requesting user
    
    Parameters
    ----------
    user_id: str
        id of the user

    Response
    --------
    [str]
        array of shipment ids disclosed to this user

    Raises
    ------
    EonError
        Error that contains a message and http code to be returned by the controller to the client
    """
    requesting_user = get_a_user(user_id)
    if not requesting_user:
        raise EonError('User not found.', 404)

    disclosures = Disclosure.query.filter_by(user_id=user_id, approved=True).all()
    shipment_ids = []
    for disc in disclosures:
        shipment_ids.append(disc.shipment_id)

    return shipment_ids

def update_disclosure(data):
    """
    Update a Disclosure Request

    The disclosure is updated by the node admin, when active the user can see the connected shipment

    Parameters
    ----------
    data: dict
        data, JSON payload received from the controller

    Response
    --------
    dict, int
        per our convention services return the success JSON and the http code to return (200 in case of success, etc..)

    Raises
    ------
    EonError
        Error that contains a message and http code to be returned by the controller to the client

    """

    #DTO makes sure we have both user_id and shipment_id in the payload
    requesting_user = get_a_user(data.get('user_id'))
    if not requesting_user:
        raise EonError('User not found.', 404)

    shipment = get_a_shipment(data.get('shipment_id'))
    if not shipment:
        raise EonError('Shipment not found.', 404)  

    disclosure = Disclosure.query.filter_by(user_id=data.get('user_id'), shipment_id=data.get('shipment_id')).first()
    if not disclosure:
        raise EonError('Disclosure request not found.', 404)  

    disclosure.approved = data.get('approved') if data.get('approved') else False
    try:
        _save_changes(disclosure)
        return _update_success()
    except Exception as e:
        db.session.rollback()  
        print(e)
        raise EonError('Something went wrong creating requested disclosure.', 409) 

def _save_changes(data):
    db.session.add(data)
    db.session.commit()

def _creation_success():
    response_object = {
        'status': 'success',
        'message': 'Successfully registered.'
    }
    return response_object, 201

def _update_success():
    response_object = {
        'status': 'success',
        'message': 'Successfully updated.'
    }
    return response_object, 201

