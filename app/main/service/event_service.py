import datetime
import uuid
import math

from app.main.model.event import Event
from app.main.model.shipment import Shipment
from app.main.services import db
from app.main.util.eonerror import EonError
from app.main.util.keymanagementutils import kmc


def save_new_event(*args, **kwargs):
    """
    Save a new Event

    The Event id is created by the creator node, other nodes will inherit it 

    Parameters
    ----------
    args[0]: dict
        data, JSON payload received from the controller

    args[1]: str
        public_id, if this new piece comes with an ID, it's inherited (i.e. "creator lead ID")

    Response
    --------

    Raises
    ------
    EonError
        Error that contains a message and http code to be returned by the controller to the client

    """
    data = args[0]
    public_id = args[1] if (len(args)>1) else None
    _check_shipment(data)
    _check_type_indicator(data)
    _check_code_or_name(data)

    try:
        #datetime is a string when coming via JSON
        recording_datetime = None
        if(data.get('recording_datetime')):
            if(type(data.get('recording_datetime')) is str ):
                try:
                    recording_datetime =  datetime.datetime.strptime(data.get('recording_datetime'), '%Y-%m-%d %H:%M:%S.%f')
                except Exception as e:
                    raise EonError('Wrong datetime format, please use UTC %Y-%m-%d %H:%M:%S.%f', 409)  

        new_event = Event(
            public_id=public_id if public_id else str(uuid.uuid4()),
            created_on=datetime.datetime.utcnow(),
            code= data.get('code', None),
            name= data.get('name', None),
            latitude= data.get('latitude', None),
            longitude= data.get('longitude', None),
            recording_datetime = recording_datetime,
            sensor_reading_um=data.get('sensor_reading_um', None),
            sensor_reading_value=data.get('sensor_reading_value', None),
            type_indicator=data.get('type_indicator', None),
            shipment_id=data.get('shipment_id', None)
        )

        #event has no Unique Contraints, there is no risk of 409 here:
        _save_changes(new_event) 

        return _generate_creation_ok_message(new_event)
    except Exception as e:
        print(e)
        if (e.__class__.__name__ == "EonError"):
            raise e
        raise EonError('Another event already exists with the given data.', 409)

def get_an_event(public_id):
    return Event.query.filter_by(public_id=public_id).first()

def get_all_shipment_events(shipment_id, args):
    """ 
    Get all the txos, cached return the latest status from local db, false queries the node 

    note that some txo in the db may not be coming from the connected node, 
    so we cannot assueme if a txo is not in the fresh retrieved utxos list
    then it must be spent.
    Therefore we just cycle through the utxos retrieved from the node and try to save them a new,
    the save operations takes care of checking if we already had it.
    
    Parameters
    ----------
    shipment_id: str
        id of the shipment to get events for

    args is requet.args passed from the API controller

    args.get("page"): str
        page number, will be cast to int
    args.get("page_size"): str
        records per page, will be cast to int

    Returns:
    --------
    dict
        a dict with the pagianted resources:
        'total_record_count', the total number of resources on the lcoal db
        'page_size', how many records per page
        'page', pg number
        'records', [obj], array of ORM objects

    Raises
    ------
    EonError:   
        500: manipulation goes wrong, db errors, etc..
        400: when there is something strange in the args (e.g. cannot cast to int)
    """
    try:
        local_events = Event.query.filter_by(shipment_id=shipment_id).all() #roder by creation date
        payload = {}
        payload['total_record_count']=len(local_events)
        payload['page_size']= 20 if not args.get("page_size") else int(args.get("page_size"))
        payload['page']= 1 if not args.get("page") else int(args.get("page"))
        payload['records']=[]
        payload['page_size'] = payload['page_size'] if not payload['page_size'] > 200 else 200
        if(payload['page']<=0):
            payload['page']=1
        if(payload['page_size']*payload['page'] > payload['total_record_count']):
            #if indexes are out of bound, just return the last page
            payload['page'] = math.ceil(payload['total_record_count']/payload['page_size'])
        start_index = payload['page_size']*(payload['page']-1)
        end_index = (payload['page_size']*payload['page'])-1
        for eve in local_events[start_index:end_index]:
            payload['records'].append(eve)
        return payload
    except ValueError as verr:
        # casting args to int fails
        raise EonError(verr, 400)
    except Exception as e:
        print(e)
        raise EonError(e, 500)


"""
#Events should be write only, cannot take it back/delete/change...
def upsert_an_event(data):
    #Update an event if you find it, create it from scratch if missing
    ev = Event.query.filter_by(public_id = data['public_id']).first()
    if not ev:
        return save_new_event(data, data['public_id'])
    else:

        _check_shipment(data)
        _check_type_indicator(data)
        _check_code_or_name(data)

        recording_datetime = None
        if(data.get('recording_datetime')):
            if(type(data.get('recording_datetime')) is str ):
                try:
                    recording_datetime =  datetime.datetime.strptime(data.get('recording_datetime'), '%Y-%m-%d %H:%M:%S.%f')
                except Exception as e:
                    raise EonError('Wrong datetime format, please use UTC %Y-%m-%d %H:%M:%S.%f', 409)  

        ev.code=data.get('code', None)
        ev.name=data.get('name', None)
        ev.latitude=data.get('latitude', None)
        ev.longitude=data.get('longitude', None)
        ev.recording_datetime=recording_datetime
        ev.sensor_reading_um=data.get('sensor_reading_um', None)
        ev.sensor_reading_value=data.get('sensor_reading_value', None)
        ev.type_indicator=data.get('type_indicator', None)
        ev.shipment_id=data['shipment_id'] #must be there

        _save_changes(ev)
        #events should be pulled on request, for a shipment and for a time window
        return _generate_update_ok_message(ev)
"""

def _save_changes(data):
    db.session.add(data)
    db.session.commit()

def _check_shipment(data):
    """ 
    check is the related shipment exists, if not, raise error
    """
    if(not data.get("shipment_id", None)):
            raise EonError('Missing Shipment Id', 404) 
    else:
        shipment = Shipment.query.filter_by(public_id=data['shipment_id']).first()
        if not shipment:
            raise EonError('Shipment does not exist in this node.', 404) 

def _check_type_indicator(data):
    """
    check the type_indicator and makes sure it uses standard values, could be done ad DB level but different engines requries different codes..
    """
    if(not data.get("type_indicator", None)):
        data["type_indicator"]="Actual"

    types = ["Actual", "Estimated", "Scheduled", "Sensor"]
    if(data["type_indicator"] not in types):
        raise EonError('Event type must be: Actual, Estimated, Scheduled or Sensor.', 409) 

def _check_code_or_name(data):
    """
    at least one of the two must be present
    """
    if(not data.get("name", None) and not data.get("code", None)):
        raise EonError('Event must have at least name or code.', 409) 


def _generate_creation_ok_message(event):
    try:
        response_object = {
            'status': 'success',
            'message': 'New event created.',
            'public_id': event.public_id
        }
        return response_object, 201
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401

def _generate_update_ok_message(event):
    try:
        response_object = {
            'status': 'success',
            'message': 'Event updated.',
            'public_id': event.public_id
        }
        return response_object, 200
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401