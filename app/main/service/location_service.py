import datetime
import uuid

from sqlalchemy import or_

from app.main.model.location import Location
from app.main.model.company import Company
from app.main.services import db
from app.main.util.eonerror import EonError
from app.main.util.keymanagementutils import kmc
from app.main.util.hashutils import hu


def save_new_location(data):
    """
    Save a new location

    The unique key is location_code, location_type, street, postal_code, city_name, country.
    A "name" is requried via API controller, it has to be unique, but is not part of the uniquity constraint.
    """
    location = Location.query.filter_by(location_code=data['location_code'], location_type=data['location_type'], street=data['street'], postal_code=data['postal_code'], city_name=data['city_name'], country=data['country']).first()
    if not location:
        unique_hash = _generate_unique_hash_location(data)
        new_location = Location(
            public_id=str(uuid.uuid4()),
            name=data['name'],
            location_code=data['location_code'],
            location_type=data['location_type'],
            city_code = data.get('city_code'),
            city_name=data['city_name'],
            country=data['country'],
            po_box=data.get("po_box"),
            postal_code=data['postal_code'],
            region_code = data.get("region_code"),
            region_name = data.get("region_name"),
            street = data["street"],
            unique_hash = unique_hash,
            created_on=datetime.datetime.utcnow(),
        )
        try:
            _save_changes(new_location)
        except Exception as e:
            print(e)
            db.session.rollback() 
            raise EonError('Another Location already exists with the given data.', 409) 

        return _generate_creation_ok_message(new_location)
    else:
       raise EonError('Another location already exists with the given data.', 409)

def get_a_location(public_id):
    return Location.query.filter(or_((Location.public_id==public_id),(Location.unique_hash==public_id))).first()


def get_all_locations():
    return Location.query.filter_by().order_by(Location.created_on.desc()).all()

def upsert_a_location(data):
    """
    Update a location if you find it, create it from scratch if missing

    if a field is missing, the previous value is saved

    """
    location = Location.query.filter_by(location_code=data['location_code'], location_type=data['location_type'], street=data['street'], postal_code=data['postal_code'], city_name=data['city_name'], country=data['country']).first()
    if not location:
        return save_new_location(data)
    else:
        unique_hash = _generate_unique_hash_location(data)
        location.name=data['name']
        location.location_code=data['location_code']
        location.location_type=data['location_type']
        location.city_code = data.get('city_code', location.city_code)
        location.city_name=data['city_name']
        location.country=data['country']
        location.po_box=data.get("po_box", location.po_box)
        location.postal_code=data['postal_code']
        location.region_code = data.get("region_code", location.region_code)
        location.region_name = data.get("region_name", location.region_name)
        location.street = data["street"]
        location.unique_hash = unique_hash

        try:
            _save_changes(location)
        except Exception as e:
            print(e)
            db.session.rollback() 
            raise EonError('Another Location already exists with the given data.', 409) 
            
        return _generate_update_ok_message(location)


def _save_changes(data):
    db.session.add(data)
    db.session.commit()

def _generate_unique_hash_location(data):
    payload = data['city_name']+data['country']+data['location_code']+data['location_type']+data['postal_code']+data['street']
    return hu.digest(payload).hex()

def _generate_creation_ok_message(location):
    try:
        response_object = {
            'status': 'success',
            'message': 'New location created and currently being checked.',
            'public_id': location.public_id
        }
        return response_object, 201
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401

def _generate_update_ok_message(location):
    try:
        response_object = {
            'status': 'success',
            'message': 'Location updated.',
            'public_id': location.public_id
        }
        return response_object, 201
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401
