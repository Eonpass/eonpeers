import datetime
import uuid
import json

from flask_restplus import marshal

from app.main.model.offer import Offer
from app.main.model.quoterequest import QuoteRequest
from app.main.model.transportsegment import TransportSegment
from app.main.model.shipment import Shipment
from app.main.service.quoterequest_service import get_a_quote_request
from app.main.service.company_service import get_a_company, get_node_owner
from app.main.service.booking_service import save_new_booking
from app.main.services import db
from app.main.util.eonerror import EonError
from app.main.util.keymanagementutils import kmc
from app.main.util.verifierutils import verifier
from app.main.util.hashutils import hu
from app.main.util.dto import OfferDto
from app.main.util.tasks import send_offer, send_offer_acceptance
from app.main.util.eonerror import EonError



def save_new_offer(*args, **kwargs):
    """
    Save a new Offer

    The Offer id is created by the creator node, other nodes will inherit it. This method is used by the creator node,
    a node which inherits the offer will use the import method (and the sender the export method)

    Parameters
    ----------
    args[0]: dict
        data, JSON payload received from the controller

    args[1]: str
        public_id, if this new Offer comes with an ID, it's inherited (i.e. "creator lead ID")

    Response
    --------

    Raises
    ------
    EonError
        Error that contains a message and http code to be returned by the controller to the client

    """
    data = args[0]
    public_id = args[1] if (len(args)>1) else None

    if(data.get('transport_segment_id', None)):
        ts = TransportSegment.query.filter_by(public_id=data['transport_segment_id']).first()
        if not ts:
            raise EonError('Transport Segment does not exist in this node.', 404)
    
    if(data.get('shipment_id', None)):
        shipment = Shipment.query.filter_by(public_id=data['shipment_id']).first()
        if not shipment:
            raise EonError('Shipment does not exist in this node.', 404)  

    if(data.get('quote_request_id', None)):
        qr = QuoteRequest.query.filter_by(public_id=data['quote_request_id']).first()
        if not qr:
            raise EonError('Quote Request does not exist in this node.', 404)  
    else:
        #this should never happen due to DTO but still..
        raise EonError('Quote Request is mandatory.', 404)  

    #TODO: check if the shipment and TS can be different from the quote ones.. doesn't look like so..
    #if they must be the same, then probably they can be filled from the quote?

    try:
        now = datetime.datetime.utcnow()    
        new_offer = Offer(
            public_id=public_id if public_id else str(uuid.uuid4()),
            created_on=now,
            allotment=data.get('allotment', None),
            dangerous_goods=data.get('dangerous_goods', None),
            price_value=data.get('price_value', None),
            price_um=data.get('price_um', None),
            quote_request_id=data['quote_request_id'],
            rating=data.get('rating', None),
            routing=data.get('routing', None),
            security_status=data.get('security_status', None),
            transport_segment_id=data.get('transport_segment_id', None),
            shipment_id=data.get('shipment_id', None),
        )
        _save_changes(new_offer) 
        return _generate_creation_ok_message(new_offer)

    except Exception as e:
        print(e)
        db.session.rollback() 
        raise EonError('Another offer already exists with the given data (same quote request, price and rating).', 409)

def update_offer(data):
    """
    update a offer given its local identifier

    we expect to receive at least the mandatory fields public_id, if relation fields are empty relations are removed, if other fields are empty old values are preserved.
    This function is designed to be called by the node owner, not for P2P use, since it refers only to the local_id and doesn't match the unique_hash

    """
    offer = Offer.query.filter_by(public_id=data['public_id']).first()
    if offer:
        if(data.get('shipment_id', None)):
            shipment = get_a_shipment(data['shipment_id'])
            if not shipment:
                raise EonError('Unkown Shipment.', 404)  
        if(data.get('transport_segment_id', None)):
            ts = get_a_transport_segment(data['transport_segment_id'])
            if not ts:
                raise EonError('Unkown Transport Segment.', 404)  

        if(data.get('quote_request_id', None)):
            qr = QuoteRequest.query.filter_by(public_id=data['quote_request_id']).first()
            #NOTE: offers should be updated only by the supplier of the quote!
            if not qr:
                raise EonError('Quote Requestn does not exist in this node.', 404)  
            else:
                #get node owner and check that it's the supplier
                if qr.supplier != get_node_owner():
                    raise EonError('Only suppliers can update offers, ask them to modify the offer and export it again.', 404) 
        else:
            #this should never happen due to DTO but still..
            raise EonError('Quote Request is mandatory.', 404)  

        offer.allotment = data.get('allotment', None) if data.get('allotment', None) else offer.allotment
        offer.dangerous_goods = data.get('dangerous_goods', None) if data.get('dangerous_goods', None) else offer.dangerous_goods
        offer.price_value = data.get('price_value', None) if data.get('price_value', None) else offer.price_value
        offer.price_um = data.get('price_um', None) if data.get('price_um', None) else offer.price_um
        offer.rating = data.get('rating', None) if data.get('rating') else offer.rating
        offer.routing = data.get('routing', None) if data.get('routing') else offer.routing
        offer.security_status = data.get('security_status', None) if data.get('security_status', None) else offer.security_status
        offer.shipment_id = data.get('shipment_id', None) 
        offer.transport_segment_id = data.get('transport_segment_id', None)

        try:
            _save_changes(offer)
        except Exception as e:
            print(e)
            db.session.rollback() 
            raise EonError('Internal Server Error', 500) 

        return _generate_update_ok_message(quote)
    else:
        db.session.rollback() 
        raise EonError('Offer not found.', 404)

def upsert_offer(data):
    """
    upsert offer

    this is used during the import step, so that an offer can be updated with new information. An offer update is rejected if:
    - the public key is different from the one known for the supplier company (checked in the import step)
    - there is an accepted offer for this quote (TODO)
    """
    if(data["public_id"]):
        offer = Offer.query.filter_by(public_id=data['public_id']).first()
        if offer:
            return update_offer(data)
        else:
            return save_new_offer(data, data["public_id"])
    else:
        return save_new_offer(data)

def get_all_offers():
    """
    TODO: only the node admin or the requester/supplier of an offer should be able to query for it
    """
    return Offer.query.filter_by().all()

def get_an_offer(public_id):
    """
    TODO: only the node admin or the requester/supplier of an offer should be able to query for it
    """
    return Offer.query.filter_by(public_id=public_id).first()

def export_offer(public_id):
    """
    check the offer, then get the node details of the requester company and send call its /offer/rpc/import method

    This method is called from the controller: offer/id/rpc/export and uses the restplus marshal method to prepare the json payload from the offer.
    
    Parameters
    ----------
    public_id: str
        the id of the offer to export, the suppllier_id is the target node (the sender is the current node and requester_id)

    Response
    --------
    dict
        message and status_code for the controller, the task is pending within Celery

    Raises
    ------
    EonError
        Error that contains a specific message and http code to be returned by the controller to the client

    """
    offer = _check_offer(public_id)
    #prepare the data for the export:
    target_url = offer.quote_request.requester.base_url
    export_payload = _prepare_offer_export_payload(offer)
    #call the celery task to send the payload
    _task = send_offer.delay(target_url, export_payload) #delay returns an asynch task that we let resolve on its own
    return export_payload

def _check_offer(public_id):
    """
    Check that an offer has the minimum related objects before sending it out
    
    An offer should have at least the quote request and the price

    Parameters
    ----------
    public_id: str
        the id of the offer to check

    Response
    --------
    Object
        Offer, the offer object if it passes every check

    Raises
    ------
    EonError
        Error that contains a specific message and http code to be returned by the controller to the client

    """
    offer = Offer.query.filter_by(public_id=public_id).first()  
    if not offer:
        raise EonError('Offer does not exist.', 404)

    if(offer.quote_request.supplier ==  None):
        raise EonError('Connected Quote Request missing supplier company.', 409)        
    if(offer.quote_request.requester ==  None):
        raise EonError('Connected Quote Request missing requester company.', 409)
    if(offer.quote_request.shipment ==  None):
        raise EonError('Connected Quote Request missing shipment.', 409)
    if(offer.quote_request.transport_segment ==  None):
        raise EonError('Connected Quote Request missing transport segment.', 409)
    if(offer.quote_request.transport_segment.arrival_location == None or offer.quote_request.transport_segment.departure_location == None):
        raise EonError('Connected Transport segment is missing information.', 409)        
    if(len(offer.quote_request.shipment.pieces)==0):
        raise EonError('Connected Shipment is missing attached pieces.', 409)     
    
    return offer

def _prepare_offer_export_payload(offer):
    #the offer has a simpler body as it expects all other objects to have been created by a previous quote request import
    #get the payload and sign it
    payload = marshal(offer,OfferDto.offer) 

    payload_to_sign = bytes.fromhex(hu.digest(json.dumps(payload, sort_keys=True)).hex())
    signed_payload_hash = kmc.sign_message(payload_to_sign)
    signed_payload_hash_hex = signed_payload_hash['signed'].hex()
    export_payload = {
        "payload":payload,
        "signed_hash":signed_payload_hash_hex,
        "supplier_company_seralised_public_key": kmc.get_serialized_pub_key().decode('utf-8')
    }
    return export_payload   

def import_offer(import_payload):
    """
    get the related quote, check that the supplier public key matches the payload public key, then create or update the offer
    """
    #find the quote and the supplier:
    qr =  get_a_quote_request(import_payload["payload"]["quote_request_id"])
    if(not qr):
        raise EonError("Unrecognized quote request", 400)
    supplier = get_a_company(qr.supplier_company_id)
    if(supplier.public_key != import_payload["supplier_company_seralised_public_key"]):
        raise EonError("Public keys do no match, please align the keys first", 400)

    #TODO: check if this offer id doesn't exist already (could be someone trying to override the same id with a different payload)
    #if it exists it must be updated from the same key!


    #check signatures and payloads:
    #signed = bytes from hex (signed_message)
    signed_bytes = bytes.fromhex(import_payload["signed_hash"])
    #message = compute the hash of payload
    message = bytes.fromhex(hu.digest(json.dumps(import_payload["payload"], sort_keys=True)).hex())
    #serialized public should already be fine
    try:
        #in the future different verifier types may require different args..
        #verifier_type = current_app.config['VERIFIER_TYPE']
        #if verifier_type == 'STD'
        signature_checks_out = verifier.verify_payload(signed_bytes, message, import_payload["supplier_company_seralised_public_key"])
        if(not signature_checks_out):
            raise EonError("Signed message is corrupt", 400)        
    except Exception as e:
        print(e)
        raise EonError("Signed message is corrupt", 400) 
    return upsert_offer(import_payload["payload"])

def accept_offer(public_id):
    """
    a received offer can be accepted, this will trigger the provider node to prepare a booking object.

    Note that the body of the accepted offer should be reported, since the offer could have been changed on the provider node in the meanwhile. 
    If that is the case, notify the requester and the provider of the incongruency.

    This method responds to a simil-RPC call made by the admin, therefore it returns a DTO.MethodResult
    """
    offer = _check_offer(public_id)
    if not offer:
        raise EonError("Offer not found", 404)

    payload = _prepare_offer_export_payload(offer)    
    signing_key = payload["supplier_company_seralised_public_key"]
    payload.pop("supplier_company_seralised_public_key", None)
    payload["accepting_company_seralised_public_key"] = signing_key

    #send the payload to the remote node:
    target_url = offer.quote_request.supplier.base_url
    #call the celery task to send the payload
    _task = send_offer_acceptance.delay(target_url, payload) #this is asynch, we return a simple ACK that the offer was sent
    #in the task we have the result..
    
    rpc_response = {
        "method":"offer.accept",
        "result":"Success, acceptance sent to the provider node"
    }
    return rpc_response, 200 #convention for services is to return the payload and the status code in a simple array


def register_offer_acceptance(acceptance_payload):
    """
    the provider node registers that the requester accepted the offer, checks consitency and prepares the booking object.

    Note that final price on the booking may be different from the offer, but it's create at the same price.
    """
    qr =  get_a_quote_request(acceptance_payload["payload"]["quote_request_id"])
    if(qr.requester.public_key != acceptance_payload["accepting_company_seralised_public_key"]):
        raise EonError("Public keys do no match, please align the keys first", 400)

    #check signatures and payloads:
    #signed = bytes from hex (signed_message)
    signed_bytes = bytes.fromhex(acceptance_payload["signed_hash"])
    #message = compute the hash of payload
    message = bytes.fromhex(hu.digest(json.dumps(acceptance_payload["payload"], sort_keys=True)).hex())
    #serialized public should already be fine
    try:
        #in the future different verifier types may require different args..
        #verifier_type = current_app.config['VERIFIER_TYPE']
        #if verifier_type == 'STD'
        signature_checks_out = verifier.verify_payload(signed_bytes, message, acceptance_payload["accepting_company_seralised_public_key"])
        if(not signature_checks_out):
            raise EonError("Signed message is corrupt", 400)        
    except Exception as e:
        print(e)
        raise EonError("Signed message is corrupt", 400)     

    #check that the accepted offer is still coherent with the sent offer:
    offer = Offer.query.filter_by(public_id=acceptance_payload["payload"]["public_id"]).first()  
    if not offer:
        raise EonError('Offer does not exist.', 404)
    local_payload = marshal(offer,OfferDto.offer) 
    local_message = hu.digest(json.dumps(local_payload, sort_keys=True)).hex()
    remote_messate = message.hex()
    if(local_message!=remote_message):
        raise EonError('Offer does not match, an updated version should be sent.', 409)

    #if they are still the same: create the booking object
    #booking = save_new_booking(json_payload, [public_id]), when public_id is empty a new one is generated "creator-lead id"
    booking_payload = {
        "price_value": offer.prive_value,
        "price_um": offer.price_um,
        "offer_id": offer.public_id,
        "routing": offer.routing,
        "security_status": offer.security_status
    }
    booking_res = save_new_booking(booking_payload)

    #change offer status? TODO: do we have an offer status?
    rpc_response = {
        'method':'offer.registeracceptance',
        'message':'Success, acceptance registered, please wait until we send the booking confirmation'
    }
    return rpc_response, 200


def _save_changes(data):
    db.session.add(data)
    db.session.commit()

def _generate_creation_ok_message(offer):
    try:
        response_object = {
            'status': 'success',
            'message': 'New offer created and currently being checked.',
            'public_id': offer.public_id
        }
        return response_object, 201
    except Exception as e:
        print(e)
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401

def _generate_update_ok_message(offer):
    try:
        response_object = {
            'status': 'success',
            'message': 'Offer locally updated.',
            'public_id': offer.public_id
        }
        return response_object, 200
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401