import datetime
import uuid

from app.main.model.product import Product
from app.main.model.piece import Piece
from app.main.model.shipment import Shipment
from app.main.services import db
from app.main.util.eonerror import EonError
from app.main.util.keymanagementutils import kmc

from app.main.service.product_service import (generate_unique_hash_product, save_new_product, get_a_product)
from app.main.service.customsinfo_service import get_all_piece_customs_info
from app.main.service.dangerousgood_service import get_all_piece_dangerous_goods


def save_new_piece(*args, **kwargs):
    """
    Save a new Piece

    The Piece id is created by the creator node, other nodes will inherit it with the link of the "creator company".
    When a node sends out a Piace as part of a quote request, receiving nodes that respond are added to the "subscribed nodes"

    Parameters
    ----------
    args[0]: dict
        data, JSON payload received from the controller

    args[1]: str
        public_id, if this new piece comes with an ID, it's inherited (i.e. "creator lead ID")

    Response
    --------

    Raises
    ------
    EonError
        Error that contains a message and http code to be returned by the controller to the client

    """
    data = args[0]
    public_id = args[1] if (len(args)>1) else None

    _check_for_loops(data, public_id)


    if(data.get('shipment_id')):
        if(data.get('parent_piece_id', None)):
            raise EonError('Shipment can be connected only to the top Piece, not to a nested Piece', 409)
        shipment = Shipment.query.filter_by(public_id=data['shipment_id']).first()
        if not shipment:
            raise EonError('Shipment does not exist in this node.', 404)  

    try:
        #coload, stackable and turnable could be Strings if they come via API
        boolean_vars = ["coload","stackable","turnable"]
        for bool_var in boolean_vars:
            if(data.get(bool_var, None)):
                data[bool_var]=(data[bool_var]=="True" or data[bool_var]=="true")

        new_piece = Piece(
            public_id=public_id if public_id else str(uuid.uuid4()),
            created_on=datetime.datetime.utcnow(),
            additional_security_info=data.get('additional_security_info', None),
            coload=data.get('coload', None),
            parent_piece_id=data.get('parent_piece_id', None),
            goods_description=data['goods_description'],
            gross_weight_value=data['gross_weight_value'],
            gross_weight_um=data['gross_weight_um'],
            load_type=data.get('load_type', None),
            shipment_id = data.get('shipment_id', None),
            slac=data.get('slac', None),
            stackable=data.get('stackable', None),
            turnable=data.get('turnable', None),
            upid=data.get('upid', None),
            volumetric_weight_conversion_factor_um=data['volumetric_weight_conversion_factor_um'],
            volumetric_weight_conversion_factor_value=data['volumetric_weight_conversion_factor_value'],
            volumetric_weight_chargeable_weight_um=data['volumetric_weight_chargeable_weight_um'],
            volumetric_weight_chargeable_weight_value=data['volumetric_weight_chargeable_weight_value'],
            height_um=data.get('height_um', None),
            height_value=data.get('height_value', None),
            width_um=data.get('width_um', None),
            width_value=data.get('width_value', None),
            length_um=data.get('length_um', None),
            length_value=data.get('length_value', None),
        )

        #TODO: make 1 query only
        if(data.get("products", None)):
            for prod in data.get("products"):
                local_prod=None
                if(prod.get("unique_hash", None)):
                    local_prod = Product.query.filter_by(unique_hash=prod["unique_hash"]).first()
                else:
                    unique_hash = generate_unique_hash_product(prod)
                    local_prod = Product.query.filter_by(unique_hash=prod["unique_hash"]).first()
                if not local_prod:
                    #try to create the product
                    prod_res = save_new_product(prod)
                    local_prod = get_a_product(prod_res[0]["public_id"]) 

                new_piece.products.append(local_prod)

        #piece has no Unique Contraints, there is no risk of 409 here:
        _save_changes(new_piece) 

        return _generate_creation_ok_message(new_piece)
    except Exception as e:
        print(e)
        raise EonError('Another piece already exists with the given data.', 409)


def upsert_piece(data):
    piece = Piece.query.filter_by(public_id=data['public_id']).first()

    if(data.get('shipment_id')):
        if(data.get('parent_piece_id', None) or piece and piece.parent_piece_id):
            raise EonError('Shipment can be connected only to the top Piece, not to a nested Piece', 409)
        shipment = Shipment.query.filter_by(public_id=data['shipment_id']).first()
        if not shipment:
            raise EonError('Shipment does not exist in this node.', 404)  

    _check_for_loops(data, data['public_id'])

    if piece:
        boolean_vars = ["coload","stackable","turnable"]
        for bool_var in boolean_vars:
            if(data.get(bool_var, None)):
                data[bool_var]=data[bool_var]=="True"
        
        piece.additional_security_info = data.get('additional_security_info', None) if data.get('additional_security_info', None) else piece.additional_security_info
        piece.coload = data.get('coload', None) if data.get('coload') else piece.coload
        piece.parent_piece_id = data.get('parent_piece_id', None)
        piece.goods_description = data.get('goods_description', None) if data.get('goods_description') else piece.goods_description
        piece.gross_weight_value = data.get('gross_weight_value', None) if data.get('gross_weight_value', None) else piece.gross_weight_value
        piece.gross_weight_um = data.get('gross_weight_um', None) if data.get('gross_weight_um', None) else piece.gross_weight_um
        piece.load_type = data.get('load_type', None) if data.get('load_type', None) else piece.load_type
        piece.shipment_id = data.get('shipment_id', None)
        piece.slac = data.get('slac', None) if data.get('slac', None) else piece.slac
        piece.stackable = data.get('stackable', None) if data.get('stackable', None) else piece.stackable
        piece.turnable = data.get('turnable', None) if data.get('turnable', None) else piece.turnable
        piece.volumetric_weight_conversion_factor_um = data.get('volumetric_weight_conversion_factor_um', None) if data.get('volumetric_weight_conversion_factor_um', None) else piece.volumetric_weight_conversion_factor_um
        piece.volumetric_weight_conversion_factor_value = data.get('volumetric_weight_conversion_factor_value', None) if data.get('volumetric_weight_conversion_factor_value', None) else piece.volumetric_weight_conversion_factor_value
        piece.volumetric_weight_chargeable_weight_um = data.get('volumetric_weight_chargeable_weight_um', None) if data.get('volumetric_weight_chargeable_weight_um', None) else piece.volumetric_weight_chargeable_weight_um
        piece.volumetric_weight_chargeable_weight_value = data.get('volumetric_weight_chargeable_weight_value', None) if data.get('volumetric_weight_chargeable_weight_value', None) else piece.volumetric_weight_chargeable_weight_value
        piece.height_um=data.get('height_um', None)
        piece.height_value=data.get('height_value', None)
        piece.width_um=data.get('width_um', None)
        piece.width_value=data.get('width_value', None)
        piece.length_um=data.get('length_um', None)
        piece.length_value=data.get('length_value', None)

        #TODO: if you have prods here, update the links between piece and products
        if(data.get("products", None)):
            piece.products = []
            for prod in data.get("products"):
                unique_hash = generate_unique_hash_product(prod)
                prod = Product.query.filter_by(unique_hash=prod["unique_hash"]).first()
                if(prod):
                    piece.products.append(prod)
                else:
                    #create the product
                    prod_res = save_new_product(prod)
                    local_prod = get_a_product(prod_res[0]["public_id"]) 
                    piece.products.append(local_prod)
                
        _save_changes(piece) 

        return _generate_update_ok_message(piece)
    else:
        return save_new_piece(data, data["public_id"]) if data["public_id"] else save_new_piece(data)


def get_all_pieces():
    return Piece.query.filter_by().all()

def get_a_piece(public_id):
    return Piece.query.filter_by(public_id=public_id).first()

def get_pieces_for_shipment(shipment_id):
    return Piece.query.filter_by(shipment_id=shipment_id).all()

def get_piece_customs_info(public_id):
    return get_all_piece_customs_info(public_id)

def get_piece_dangerous_goods(public_id):
    return get_all_piece_dangerous_goods(public_id)

def _check_for_loops(data, public_id):
    if(data.get('parent_piece_id', None)):
        #self nesting?
        if data.get('parent_piece_id', None) == public_id:
            raise EonError('A piece cannot be the parent to itself', 409)

        parent_piece=None
        if(data.get('parent_piece_id', None) and data.get('parent_piece_id', None)!="None"): 
            #in the DTO payload, if parent_piece is empty the marshalling puts a "None" in the field
            parent_piece = Piece.query.filter_by(public_id=data.get('parent_piece_id', None)).first()
            if not parent_piece:
                raise EonError('Parent piece does not exist on this node yet.', 409)
        #check for nested pieces
        while(parent_piece):
            if parent_piece.parent_piece_id:
                if(parent_piece.parent_piece_id == public_id):
                    raise EonError('A parent of this piece, '+parent_piece.public_id+', is referring to this piece, no loops admitted', 409)
                parent_piece = Piece.query.filter_by(public_id=parent_piece.parent_piece_id).first()
            else:
                parent_piece=None
        return

def _save_changes(data):
    db.session.add(data)
    db.session.commit()

def _generate_creation_ok_message(product):
    try:
        response_object = {
            'status': 'success',
            'message': 'New piece created.',
            'public_id': product.public_id
        }
        return response_object, 201
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401

def _generate_update_ok_message(product):
    try:
        response_object = {
            'status': 'success',
            'message': 'Piece updated.',
            'public_id': product.public_id
        }
        return response_object, 200
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401
