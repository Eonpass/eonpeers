import datetime
import uuid

from sqlalchemy import or_

from app.main.model.product import Product
from app.main.model.piece import Piece
from app.main.model.company import Company
from app.main.service.company_service import get_a_company
from app.main.services import db
from app.main.util.eonerror import EonError
from app.main.util.hashutils import hu


def save_new_product(data):

    if( not data['product_identifier'] or not data['product_description']):
        raise EonError('Missing product id or product_description.', 404)

    product = Product.query.filter_by(product_identifier=data['product_identifier'], product_description=data['product_description']).first()
    if not product:
        manufacturer_company = None
        brand_owner = None
        if(data.get('manufacturer_company_id', None)):
            manufacturer_company = get_a_company(data['manufacturer_company_id'])
            if not manufacturer_company:
                raise EonError('Unkown manufacturer.', 404)
        if(data.get('brand_owner_company_id', None)):
            brand_owner = get_a_company(data['brand_owner_company_id'])
            if not brand_owner:
                raise EonError('Unkown brand owner.', 404)    

        unique_hash = generate_unique_hash_product(data)

        new_product = Product(
            public_id=str(uuid.uuid4()),
            brand_owner_company_id=brand_owner.public_id if brand_owner else None,
            created_on=datetime.datetime.utcnow(),
            commodity_code=data['commodity_code'],
            commodity_description=data['commodity_description'],
            commodity_name=data.get('commodity_name', None),
            commodity_type=data['commodity_type'],
            manufacturer_company_id=manufacturer_company.public_id if manufacturer_company else None,
            product_description=data.get('product_description', None),
            product_identifier=data.get('product_identifier', None),
            un_number=data.get('un_number', None),
            unique_hash=unique_hash
        )
        try:
            _save_changes(new_product) 
        except Exception as e:
            print(e)
            db.session.rollback()
            raise EonError('Another product already exists with the given data.', 409)

        return _generate_creation_ok_message(new_product)
    else:
       raise EonError('Another product already exists with the given data.', 409)


def update_product(data):
    """
    update a product given its local identifier

    we expect to receive at least the mandatory fields public_id, prod_description, prod_identifier, if not, an error will occur.
    This function is designed to be called by the node owner, not for P2P use, since it refers only to the local_id and doesn't match the unique_hash

    """
    product = Product.query.filter_by(public_id=data['public_id']).first()
    if product:
        if(data.get('manufacturer_company_id', None)):
            manufacturer_company = get_a_company(data['manufacturer_company_id'])
            if not manufacturer_company:
                raise EonError('Unkown manufacturer.', 404)
        if(data.get('brand_owner_company_id', None)):
            brand_owner = get_a_company(data['brand_owner_company_id'])
            if not brand_owner:
                raise EonError('Unkown brand owner.', 404)  

        unique_hash = generate_unique_hash_product(data)

        product.commodity_code = data.get('commodity_code', None) if data.get('commodity_code', None) else product.commodity_code
        product.commodity_description = data.get('commodity_description', None) if data.get('commodity_description') else product.commodity_description
        product.commodity_name = data.get('commodity_name', None) if data.get('commodity_name', None) else product.commodity_name
        product.commodity_type = data.get('commodity_type', None) if data.get('commodity_type') else product.commodity_type
        product.manufacturer_company_id = data.get('manufacturer_company_id', None) if data.get('manufacturer_company_id', None) else product.manufacturer_company_id
        product.product_description = data.get('product_description', None) if data.get('product_description', None) else product.product_description
        product.product_identifier = data.get('product_identifier', None) if data.get('product_identifier', None) else product.product_identifier
        product.un_number = data.get('un_number', None) if data.get('un_number', None) else product.un_number
        product.unique_hash = unique_hash
        if data.get('brand_owner_company_id', None):
            product.brand_owner_company_id = data.get('brand_owner_company_id', None)
        if data.get('manufacturer_company_id', None):
            product.manufacturer_company_id = data.get('manufacturer_company_id', None)

        try:
            _save_changes(product) 
        except Exception as e:
            print(e)
            db.session.rollback()
            raise EonError('Another product already exists with the given data.', 409)

        return _generate_update_ok_message(product)
    else:
       raise EonError('Product does not exist.', 404)

def get_all_products():
    return Product.query.filter_by().all()

def get_a_product(public_id):
    return Product.query.filter(or_((Product.public_id==public_id),(Product.unique_hash==public_id))).first()

def generate_unique_hash_product(data):
    payload = data['product_description']+data['product_identifier']
    return hu.digest(payload).hex()

def _save_changes(data):
    db.session.add(data)
    db.session.commit()


def _generate_creation_ok_message(product):
    try:
        response_object = {
            'status': 'success',
            'message': 'New product created.',
            'public_id': product.public_id
        }
        return response_object, 201
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401

def _generate_update_ok_message(product):
    try:
        response_object = {
            'status': 'success',
            'message': 'Product updated.',
            'public_id': product.public_id
        }
        return response_object, 200
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401
