import datetime
import uuid
import json
import copy
import requests

from flask_restplus import marshal
from sqlalchemy import desc

from app.main.model.company import Company
from app.main.model.shipment import Shipment
from app.main.model.transportsegment import TransportSegment
from app.main.service.company_service import get_a_company, get_node_owner
from app.main.service.location_service import upsert_a_location, get_a_location
from app.main.service.shipment_service import get_a_shipment, save_new_shipment, upsert_shipment, prepare_origin_shipment_payload
from app.main.service.transportsegment_service import get_a_transport_segment, upsert_a_transport_segment, save_new_transport_segment
from app.main.service.piece_service import save_new_piece, upsert_piece, get_pieces_for_shipment
from app.main.service.customsinfo_service import upsert_a_customs_info
from app.main.service.dangerousgood_service import upsert_a_dangerous_good
from app.main.model.quoterequest import QuoteRequest
from app.main.services import db
from app.main.util.eonerror import EonError
from app.main.util.keymanagementutils import kmc
from app.main.util.verifierutils import verifier
from app.main.util.hashutils import hu
from app.main.util.dto import QuoteRequestDto, PieceDto
from app.main.util.tasks import send_quote_request
from app.main.config import config_by_name


def save_new_quote_request(*args, **kwargs):
    """
    Save a new Quote Request

    The Quote id is created by the creator node, other nodes will inherit it with the link of the "creator company".
    When a node sends out a quote request, receiving nodes that respond are added to the "subscribed nodes"

    Parameters
    ----------
    args[0]: dict
        data, JSON payload received from the controller

    args[1]: str
        public_id, if this new Quote comes with an ID, it's inherited (i.e. "creator lead ID")

    Response
    --------

    Raises
    ------
    EonError
        Error that contains a message and http code to be returned by the controller to the client

    """
    data = args[0]
    public_id = args[1] if (len(args)>1) else None

    if(data.get('transport_segment_id', None)):
        ts = TransportSegment.query.filter_by(public_id=data['transport_segment_id']).first()
        if not ts:
            raise EonError('Transport Segment does not exist in this node.', 404)
    
    if(data.get('shipment_id', None)):
        shipment = Shipment.query.filter_by(public_id=data['shipment_id']).first()
        if not shipment:
            raise EonError('Shipment does not exist in this node.', 404)  
    requester=None
    if(data.get('requester_company_id', None)):
        requester = get_a_company(data['requester_company_id'])
        if not requester:
            raise EonError('Requester company does not exist in this node.', 404)  
    else:
        #if empty we assume this node is the requester:
        requester = get_node_owner()

    supplier=None
    if(data.get('supplier_company_id', None)):
        supplier = get_a_company(data['supplier_company_id'])
        if not supplier:
            raise EonError('Supplier company does not exist in this node.', 404)   

    if(data.get('requester_company_id', None) and data.get('supplier_company_id', None) and data.get('supplier_company_id', None)==data.get('requester_company_id', None) ):
        raise EonError('Supplier and Requester cannot be the same company.', 409)     

    try:
        now = datetime.datetime.utcnow()    
        new_quote_request = QuoteRequest(
            public_id=public_id if public_id else str(uuid.uuid4()),
            created_on=now,
            allotment=data.get('allotment', None),
            rating_preferences=data.get('rating_preferences', None),
            requester_company_id=requester.public_id if requester else None,
            routing_preference=data.get('routing_preference', None),
            security_status=data.get('security_status', None),
            transport_segment_id=data.get('transport_segment_id', None),
            shipment_id=data.get('shipment_id', None),
            supplier_company_id=supplier.public_id if supplier else None,
            units_preference=data.get('units_preference', None)
        )
        _save_changes(new_quote_request) 
        return _generate_creation_ok_message(new_quote_request)

    except Exception as e:
        print(e)
        db.session.rollback() 
        raise EonError('Another quote already exists with the given data (same requester, supplier, shipment and transport segment).', 409)

def update_quote_request(data):
    """
    update a quote request given its local identifier

    we expect to receive at least the mandatory fields public_id, if relation fields are empty relations are removed, if other fields are empty old values are preserved.
    This function is designed to be called by the node owner, not for P2P use, since it refers only to the local_id and doesn't match the unique_hash

    """
    quote = QuoteRequest.query.filter_by(public_id=data['public_id']).first()
    if quote:

        if(data.get('requester_company_id', None)):
            requester_company = get_a_company(data['requester_company_id'])
            if not requester_company:
                raise EonError('Unkown Requester Company.', 404)
            if quote.requester_company_id and quote.requester_company_id!=requester_company.public_id:
                raise EonError('Cannot change requester or suppliers once set.', 409)
        if(data.get('supplier_company_id', None)):
            supplier = get_a_company(data['supplier_company_id'])
            if not supplier:
                raise EonError('Unkown Supplier Company.', 404)  
            if quote.supplier_company_id and quote.supplier_company_id!=supplier.public_id:
                raise EonError('Cannot change requester or suppliers once set.', 409)
        if(data.get('shipment_id', None)):
            shipment = get_a_shipment(data['shipment_id'])
            if not shipment:
                raise EonError('Unkown Shipment.', 404)  
        if(data.get('transport_segment_id', None)):
            ts = get_a_transport_segment(data['transport_segment_id'])
            if not ts:
                raise EonError('Unkown Transport Segment.', 404)  

        quote.allotment = data.get('allotment', None) if data.get('allotment', None) else quote.allotment
        quote.rating_preferences = data.get('rating_preferences', None) if data.get('rating_preferences') else quote.rating_preferences
        quote.requester_company_id = data.get('requester_company_id', None) 
        quote.routing_preference = data.get('routing_preference', None) if data.get('routing_preference') else quote.routing_preference
        quote.security_status = data.get('security_status', None) if data.get('security_status', None) else quote.security_status
        quote.shipment_id = data.get('shipment_id', None) 
        quote.supplier_company_id = data.get('supplier_company_id', None) 
        quote.transport_segment_id = data.get('transport_segment_id', None)
        quote.units_preference = data.get('units_preference', None) if data.get('units_preference', None) else quote.units_preference

        try:
            _save_changes(quote)
        except Exception as e:
            print(e)
            db.session.rollback() 
            raise EonError('Another quote already exists with the given data (same requester, supplier, shipment and transport segment).', 409) 

        return _generate_update_ok_message(quote)
    else:
        db.session.rollback() 
        raise EonError('Quote Request does not exist.', 404)

def upsert_quote_request(data):
    """
    upsert quote request

    this is used during the import the import step, so that a quote can be updated with new information. A quote update is rejected if:
    - the public key is different from the one known for the requester company (checked in the import step)
    - there is an accepted offer for this quote (TODO)
    """
    if(data["public_id"]):
        qr = QuoteRequest.query.filter_by(public_id=data['public_id']).first()
        if qr:
            return update_quote_request(data)
        else:
            return save_new_quote_request(data, data["public_id"])
    else:
        return save_new_quote_request(data)



def get_all_quote_requests():
    return QuoteRequest.query.filter_by().order_by(QuoteRequest.created_on.desc()).all()

def get_a_quote_request(public_id):
    return QuoteRequest.query.filter_by(public_id=public_id).first()

def _check_quote_request(public_id):
    """
    Check that a quote request has the minimum related objects before sending it out
    
    A quote should have a shipment with at least 1 piece and 1 ts, the ts must have proper departure and arrival locations

    Parameters
    ----------
    public_id: str
        the id of the quote request to check

    Response
    --------
    Object
        Quote, the quote request object if it passes every check

    Raises
    ------
    EonError
        Error that contains a specific message and http code to be returned by the controller to the client

    """
    with db.session.no_autoflush:
        qr = QuoteRequest.query.filter_by(public_id=public_id).first()  
        if not qr:
            raise EonError('Quote Request does not exist.', 404)

        if(qr.supplier ==  None):
            raise EonError('Quote Request missing supplier company.', 409)        
        if(qr.requester ==  None):
            raise EonError('Quote Request missing requester company.', 409)
        if(qr.shipment ==  None):
            raise EonError('Quote Request missing shipment.', 409)
        if(qr.transport_segment ==  None):
            raise EonError('Quote Request missing transport segment.', 409)

        if(qr.transport_segment.arrival_location == None or qr.transport_segment.departure_location == None):
            raise EonError('Transport segment is missing information.', 409)        

        if(len(qr.shipment.pieces)==0):
            raise EonError('Shipment is missing attached pieces.', 409)     

        #shipment at this point is signed if this is the first requester (Brand Owner) sending out:
        node_owner = get_node_owner() # 
        if(not qr.shipment.origin_hash or node_owner.public_key==qr.shipment.originator_pubkey):
            signed_payload = prepare_origin_shipment_payload(qr.shipment.public_id)
            qr.shipment.origin_hash = signed_payload["origin_hash"]
            qr.shipment.signed_origin_hash = signed_payload["signed_origin_hash"]

        return qr     

def export_quote_request(public_id):
    """
    check the quote request, then get the node details of the supplier company and send call its /quoterequest/rpc/import method

    This method is called from the controller: quoterequest/id/rpc/export and uses the restplus marshal method to prepare the json payload fomr the quote request.
    
    Parameters
    ----------
    public_id: str
        the id of the quote request to export, the suppllier_id is the target node (the sender is the current node and requester_id)

    Response
    --------
    dict
        message and status_code for the controller, the task is pending within Celery

    Raises
    ------
    EonError
        Error that contains a specific message and http code to be returned by the controller to the client

    """
    qr = _check_quote_request(public_id)
    #prepare the data for the export:
    target_url = qr.supplier.base_url
    export_payload = _prepare_export_payload(qr)
    #call the celery task to send the payload
    _task = send_quote_request.delay(target_url, export_payload)
    db.session.flush()
    return export_payload

def _prepare_export_payload(qr):
    """
    The export payload has to be manipulated w.r.t. to the QR json, you need to add the signature nad use the unique hash identifiers of companies

    Parameters
    ----------
    obj
        quote request object as obtained from an SQLAlchemy query

    Response
    --------
    dict
        JSON payload ready for the export function

    """
    #qr supplier_id and requester_id should be the hash of unique contraints, so that remote nodes can find the matches

    payload = marshal(qr,QuoteRequestDto.full_quote_request) 

    #make sure you use unique ids for companies and products
    payload["requester_company_id"] = get_a_company(qr.requester_company_id).unique_hash
    payload["supplier_company_id"] = get_a_company(qr.supplier_company_id).unique_hash
    for piece in payload["shipment"]["pieces"]:
        for prod in piece["products"]:
            prod["brand_owner_company_id"] = get_a_company(prod["brand_owner_company_id"]).unique_hash if get_a_company(prod["brand_owner_company_id"]) else ""
            prod["manufacturer_company_id"] = get_a_company(prod["manufacturer_company_id"]).unique_hash if get_a_company(prod["manufacturer_company_id"]) else ""    

    #get the payload and sign it
    payload_to_sign = bytes.fromhex(hu.digest(json.dumps(payload, sort_keys=True)).hex())
    signed_payload_hash = kmc.sign_message(payload_to_sign)
    signed_payload_hash_hex = signed_payload_hash['signed'].hex()
    export_payload = {
        "payload":payload,
        "signed_hash":signed_payload_hash_hex,
        "requester_company_seralised_public_key": kmc.get_serialized_pub_key().decode('utf-8')
    }
    return export_payload


def import_quote_request(import_payload):
    """
    given a full quote request payload, check all the signatures and create the quote request locally

    There are two signed messages:
        - overall quote request, check that the public key matches the known one for the requester, check the signed message
        - shipment, check that the public key matches one of the approved ones (see verifier), check the message

    """
    #find company by hash - it would be nice to check also a signed hash of the compay unique details
    requester = get_a_company(import_payload["payload"]["requester_company_id"])
    supplier = get_a_company(import_payload["payload"]["supplier_company_id"])
    #match the requester public key with the received one
    if(requester.public_key != import_payload["requester_company_seralised_public_key"]):
        raise EonError("Public keys do no match, please align the keys first", 400)

    #check quote overall payload
    quote_verification_payload = {
        'payload':import_payload,
        'object_to_verify':"quote"
    }
    verifier.verify_payload(**quote_verification_payload) #this throws exception with error details

    #check shipment origin proof:
    shipment_api_object = copy.deepcopy(import_payload["payload"]["shipment"])
    shipment_verification_payload = {
        "payload":shipment_api_object,
        "object_to_verify":"shipment"
    }
    verifier.verify_payload(**shipment_verification_payload) #this throws exception with error details

    #check if this shipment id and quote id don't exist already (could be someone trying to override the same id with a different payload)
    quote_id = import_payload["payload"]["public_id"]
    shipment_id = import_payload["payload"]["shipment"]["public_id"]
    existing_qr = get_a_quote_request(quote_id)
    if(existing_qr):
        if existing_qr.requester_company_id != requester.public_id:
            raise EonError("Quote Id already registered, please chose a different one") #this would be very suspicious
    existing_shpmt = get_a_shipment(shipment_id)
    if(existing_shpmt):
        if existing_shpmt.originator_pubkey != import_payload["payload"]["shipment"]["originator_pubkey"]:
            raise EonError("Shipment Id already registered, please chose a different one") #this would be very suspicious

    #build the objects following the hierarchies, quote last since it checks the existence of all needed parts
    shipment_res = upsert_shipment(import_payload["payload"]["shipment"])
    #Locations and TS
    arrival_id = "" 
    departure_id = ""
    for piece in import_payload["payload"]["shipment"]["pieces"]:
        piece["shipment_id"]=shipment_res[0]["public_id"]
        upsert_piece(piece)
        #add dangerous goods and cusomts info
        if piece.get("customs_infos", None) is not None:
            for ci in piece["customs_infos"]:
                ci_res = upsert_a_customs_info(ci)
        if piece.get("dangerous_goods", None) is not None:
            for dg in piece["dangerous_goods"]:
                dg_res = upsert_a_dangerous_good(dg)
        #add transport segments
        for ts in piece["transport_segments"]:
            arrival_res = upsert_a_location(ts["arrival_location"])
            arrival_id = arrival_res[0]["public_id"]
            departure_res = upsert_a_location(ts["departure_location"])
            departure_id = departure_res[0]["public_id"]
            ts["arrival_location_id"]= arrival_id
            ts["departure_location_id"]= departure_id
            #TODO: double check that this way of binding pieces to transport segments make sense in complex scenarios
            ts["pieces"]=[piece]
            ts_res = upsert_a_transport_segment(ts)
    #quote with checks:
    import_payload["payload"]["shipment_id"]=import_payload["payload"]["shipment"]["public_id"]
    #fix the company ids in the local db:
    import_payload["payload"]["supplier_company_id"] = supplier.public_id
    import_payload["payload"]["requester_company_id"] = requester.public_id
    quote_request_res = upsert_quote_request(import_payload["payload"])

def prepare_quote_request_for_subcontractor_all_pieces(new_transport_segment_for_subcontractor, inbound_quote_request_id):
    """
    Create a new quote request for an existing quote request

    This is used by freight forwarders (FF) when they break down the itinerary and book on different sub contractors (e.g. carriers).
    All pieces refer to the fact that all the pieces will be passed through the subcontractor. In the future this may change.

    Parameters
    ----------
    new_transport_segment_for_subcontractor: dict
        new_transport_segment: dict
            new transport segment, if present this will create the new TS and link the pieces of the desired shipment to them 
            (typical when a FF breaks down the itineraru). (TODO: If empty, the original transport segments will be used)
        supplier_id: str
            id of the supplier which will receive the quote request (e.g. the FF is requesting a carrier a quote for the shipment)
    inbound_quote_request_id: str
        if of the quote request that the node received (e.g. from the brand owner) and for which it's creating this subcontracting quote request

    Response
    --------
    dict
        response with success/failre notes and the id of the new quote request

    Raises
    ------
    EonError
        Error with details and status code

    """
    if not new_transport_segment_for_subcontractor:
        raise EonError("missing payload", 409)
    if "new_transport_segment" not in new_transport_segment_for_subcontractor:
        raise EonError("missing new transport segment details", 409)
    supplier = get_a_company(new_transport_segment_for_subcontractor["supplier_id"])
    if not supplier:
        raise EonError("unknown supplier", 404)
    requester = get_node_owner()
    if not requester:
        raise EonError("node db missing configuration, make sure you have an owner", 500)
    inbound_quote_request = get_a_quote_request(inbound_quote_request_id)
    if not inbound_quote_request:
        raise EonError("unknown inbound quote request", 404)

    #prepare the payload for the new quote request
    new_quote_request = {
        'allotment': inbound_quote_request.allotment,
        'rating_preferences': inbound_quote_request.rating_preferences,
        'requester_company_id':requester.public_id,
        'routing_preference': inbound_quote_request.routing_preference,
        'security_status': inbound_quote_request.security_status,
        'transport_segment_id': inbound_quote_request.transport_segment_id,#this must be updated with the new TS, see below
        'shipment_id': inbound_quote_request.shipment_id,
        'supplier_company_id': supplier.public_id,
        'units_preference': inbound_quote_request.units_preference
    }

    #get the pieces from the original shipment since they will need to be connected to the new TS (pieces have shipment_id inside)
    pieces = get_pieces_for_shipment(inbound_quote_request.shipment_id)
    json_pieces = []
    for piece in pieces:
        json_piece = marshal(piece,PieceDto.piece)
        json_pieces.append(json_piece) 
    #set pieces (they should not be there anyway)
    new_transport_segment_for_subcontractor["new_transport_segment"]["pieces"]=json_pieces

    #insert new TS with the pieces
    res = save_new_transport_segment(new_transport_segment_for_subcontractor["new_transport_segment"])
    if(res[1]==201):
        new_quote_request["transport_segment_id"] = res[0]["public_id"]
        return save_new_quote_request(new_quote_request)
    else:
        raise EonError("new transport segment could not be saved", 500)


def _save_changes(data):
    db.session.add(data)
    db.session.commit()

def _generate_creation_ok_message(quote):
    try:
        response_object = {
            'status': 'success',
            'message': 'New quote request created.',
            'public_id': quote.public_id
        }
        return response_object, 201
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401

def _generate_update_ok_message(quote):
    try:
        response_object = {
            'status': 'success',
            'message': 'Quote request locally updated.',
            'public_id': quote.public_id
        }
        return response_object, 200
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401

def _generate_notarisation_request_message(quote):
    response_object = {
        'status': 'success',
        'message': 'Session created and quote request updated.',
        'session_id': quote.notarisation_session_id,
        'notarization_item_id':quote.notarisation_item_id
    }
    return response_object, 200

def _generate_notarisation_spend_message(quote):
    response_object = {
        'status': 'success',
        'message': 'Session locked on blockchain.',
        'session_id': quote.notarisation_session_id,
        'notarization_item_id':quote.notarisation_item_id
    }
    return response_object, 200