import datetime
import uuid
import requests, json

from flask_restplus import marshal
from sqlalchemy import desc

from flask import current_app
from app.main.model.shipment import Shipment
from app.main.model.company import Company
from app.main.model.disclosure import Disclosure
from app.main.model.quoterequest import QuoteRequest
from app.main.model.booking import Booking
from app.main.model.offer import Offer
from app.main.model.timestamp import Timestamp
from app.main.service.event_service import get_all_shipment_events
from app.main.service.company_service import get_a_company
from app.main.service.timestamp_service import save_new_timestamp, get_a_timestamp, get_all_shipment_timestamps, get_pending_timestamps_for_shipment
from app.main.services import db
from app.main.util.dto import ShipmentDto
from app.main.util.eonerror import EonError
from app.main.util.keymanagementutils import kmc
from app.main.util.hashutils import hu
from app.main.config import config_by_name

from app.main.service.user_service import get_public_id_from_id

def save_new_shipment(*args, **kwargs):
    """
    Save a new Shipment

    The Shipment id is created by the creator node, other nodes will inherit it with the link of the "creator company".
    When a node sends out a Shipment as part of a quote request, receiving nodes that respond are added to the "subscribed nodes"

    Parameters
    ----------
    args[0]: dict
        data, JSON payload received from the controller

    args[1]: str
        public_id, if this new Shipment comes with an ID, it's inherited (i.e. "creator lead ID")

    Response
    --------

    Raises
    ------
    EonError
        Error that contains a message and http code to be returned by the controller to the client

    """
    data = args[0]
    public_id = args[1] if (len(args)>1) else str(uuid.uuid4())
    originator_pubkey = None
    #to be sure we have as little collisions as possible on shipment ids, this id gets signed and hashed
    if(len(args)<=1):
        id_hash_bytes = hu.digest(public_id)
        signed_id = kmc.sign_message(id_hash_bytes)['signed']
        signed_hash = hu.digest(signed_id)
        public_id = signed_hash.hex()
        originator_pubkey = kmc.get_serialized_pub_key().decode('utf-8')
    else:
        originator_pubkey = data.get("originator_pubkey", None) #if id is provided, then also the originator pubkey MUST be there
        if not originator_pubkey:
            raise EonError('Must provide the originator pubkey along with the public_id.', 409)
        #TODO: check that signature is correct? This is already done during import though.. decide where to put the check, probably here it's the better place

    try:
        now = datetime.datetime.utcnow()    
        new_shipment = Shipment(
            public_id=public_id,
            created_on=now,
            height_um=data.get('height_um', None),
            height_value=data.get('height_value', None),
            length_um=data.get('length_um', None),
            length_value=data.get('length_value', None),
            volume_um=data.get('volume_um', None),
            volume_value=data.get('volume_value', None),
            width_um=data.get('width_um', None),
            width_value=data.get('width_value', None),
            external_reference=data.get('external_reference', None),
            goods_description=data.get('goods_description', None),
            insurance_details=data.get('insurance_details', None),
            total_gross_weight_um=data['total_gross_weight_um'],
            total_gross_weight_value=data['total_gross_weight_value'],
            total_slac=data.get('total_slac', None),
            volumetric_weight_conversion_factor_um=data['volumetric_weight_conversion_factor_um'],
            volumetric_weight_conversion_factor_value=data['volumetric_weight_conversion_factor_value'],
            volumetric_weight_chargeable_weight_um=data['volumetric_weight_chargeable_weight_um'],
            volumetric_weight_chargeable_weight_value=data['volumetric_weight_chargeable_weight_value'],
            originator_pubkey=originator_pubkey,
            origin_hash=data.get('origin_hash', None),
            signed_origin_hash=data.get('signed_origin_hash', None)
        )
        _save_changes(new_shipment) 
        return _generate_creation_ok_message(new_shipment)

    except Exception as e:
        print(e)
        db.session.rollback() 
        raise EonError('Another shipment already exists with the given data.', 409)

def update_shipment(data):
    """
    update a shipment given its local identifier

    we expect to receive at least the mandatory fields public_id, weight and volumetric conversion, if relation fields are empty relations are removed, if other fields are empty old values are preserved.
    This function is designed to be called by the node owner, not for P2P use, since it refers only to the local_id and doesn't execute proper checks

    """
    shipment = Shipment.query.filter_by(public_id=data['public_id']).first()
    if shipment:
        shipment.height_um = data.get('height_um', None) if data.get('height_um', None) else shipment.height_um
        shipment.height_value = data.get('height_value', None) if data.get('height_value', None) else shipment.height_value
        shipment.length_um = data.get('length_um', None) if data.get('length_um', None) else shipment.length_um
        shipment.length_value = data.get('length_value', None) if data.get('length_value', None) else shipment.length_value
        shipment.volume_um = data.get('volume_um', None) if data.get('volume_um', None) else shipment.volume_um
        shipment.volume_value = data.get('volume_value', None) if data.get('volume_value', None) else shipment.volume_value
        shipment.width_um = data.get('width_um', None) if data.get('width_um', None) else shipment.width_um
        shipment.width_value = data.get('width_value', None) if data.get('width_value', None) else shipment.width_value
        shipment.external_reference = data.get('external_reference', None) if data.get('external_reference', None) else shipment.external_reference
        shipment.goods_description = data.get('goods_description', None) if data.get('goods_description', None) else shipment.goods_description
        shipment.insurance_details = data.get('insurance_details', None) if data.get('insurance_details', None) else shipment.insurance_details
        shipment.total_gross_weight_um = data['total_gross_weight_um']
        shipment.total_gross_weight_value = data['total_gross_weight_value']
        shipment.total_slac = data.get('total_slac', None) if data.get('total_slac', None) else shipment.total_slac
        shipment.volumetric_weight_conversion_factor_um = data['volumetric_weight_conversion_factor_um']
        shipment.volumetric_weight_conversion_factor_value = data['volumetric_weight_conversion_factor_value']
        shipment.volumetric_weight_chargeable_weight_um = data['volumetric_weight_chargeable_weight_um']
        shipment.volumetric_weight_chargeable_weight_value = data['volumetric_weight_chargeable_weight_value']

        #shipment has no extra unique contraints on the fields that can be updated, there are no risks here:
        _save_changes(shipment) 

        return _generate_update_ok_message(shipment)
    else:
       raise EonError('Shipment does not exist.', 404)

def upsert_shipment(data):
    """
    During import the code should decide whether to create or update

    TODO: this is querying twice the same thing, once here, once inside update, possibly avoid querying twice
    """
    if(data["public_id"]):
        shipment = Shipment.query.filter_by(public_id=data['public_id']).first()
        if shipment:
            return update_shipment(data) #if this is called only here it can pass the "shipment" object along with the data and save a query later
        else:
            return save_new_shipment(data, data["public_id"])
    else:
        return save_new_shipment(data)


def prepare_origin_shipment_payload(public_id):
    """ 
    Before sending the quote request compute the hash of the shipment and of the connected pieces, this will be signed and used as originator proof.

    This function is done by Brand Owners when they start the shipment request process. It may be done by FF for small clients who are out of the network.

    Parameters
    ----------
    public_id: str
        id of the shipment to fix with signed data

    Response
    --------
    dict
        JSON with the two fields related to the signed shipment data

    Rasies
    ------
    500
        if somethign goes wrong with the db at _save_changes

    """
    shipment = Shipment.query.filter_by(public_id=public_id).first()
    if not shipment:
        raise EonError('Shipment does not exist.', 404)

    #TODO: check that you are signing with the expected originator_pubkey!

    payload = marshal(shipment,ShipmentDto.import_shipment) 
    for piece in payload["pieces"]:
        piece.pop('transport_segments', None)
        for prod in piece["products"]:
            prod["brand_owner_company_id"] = get_a_company(prod["brand_owner_company_id"]).unique_hash if get_a_company(prod["brand_owner_company_id"]) else ""
            prod["manufacturer_company_id"] = get_a_company(prod["manufacturer_company_id"]).unique_hash if get_a_company(prod["manufacturer_company_id"]) else ""

    #prepare the payload: which is the import_shipment from the DTO without the transport segments
    payload.pop('transport_segments', None)
    payload.pop('origin_hash', None)
    payload.pop('signed_origin_hash', None)
    #create JSON, hash and sign
    origin_hash = hu.digest(json.dumps(payload, sort_keys=True)).hex()
    payload_to_sign = bytes.fromhex(origin_hash)
    signed_payload_hash = kmc.sign_message(payload_to_sign)
    signed_payload_hash_hex = signed_payload_hash['signed'].hex()
    shipment.origin_hash = origin_hash
    shipment.signed_origin_hash = signed_payload_hash_hex
    #save the hash and the signed data
    _save_changes(shipment) 

    #this returns the payload with popped values, it's ok since it's not sent over to the p2p network
    sorted_payload = json.dumps(payload, sort_keys=True)
    sorted_payload_obj = json.loads(sorted_payload)

    signed_fields = {
        "origin_hash":origin_hash,
        "signed_origin_hash": signed_payload_hash_hex,
        "payload": sorted_payload
    }
    return signed_fields



def get_all_shipments(token):
    #get active user
    admin = token.get('admin')
    user_id = token.get('user_id') #this user id is "id" not "public_id" -> get the public_id
    user_id = get_public_id_from_id(user_id)
    #get all shipment id disclosed to the user
    if(admin):
        return Shipment.query.filter_by().order_by(Shipment.created_on.desc()).all()
    else:
        disclosures = Disclosure.query.filter_by(user_id=user_id, approved=True).all()
        shipment_ids = []
        for disc in disclosures:
            shipment_ids.append(disc.shipment_id)
        return Shipment.query.filter(Shipment.public_id.in_(shipment_ids)).all()

def get_a_shipment(public_id):
    shipment = Shipment.query.filter_by(public_id=public_id).first()
    if not shipment:
        raise EonError("Shipment not found.", 404)
    return shipment

def get_shipment_events(public_id, args):
    shipment = Shipment.query.filter_by(public_id=public_id).first()
    if not shipment:
        raise EonError("Shipment not found.", 404)
    return get_all_shipment_events(public_id, args)


def request_notarisation(public_id):
    """
    Create a timestamp for the shipment in the current state:
    connect to eonbasics, requests new session (i.e. check if there's an open one or create a new one)
    save the item and session_id in the timestamps and on the shipment.
    """
    shipment = Shipment.query.filter_by(public_id=public_id).first()
    if not shipment:
        raise EonError('Shipment does not exist.', 404)

    verify = True 
    try:
        verify = current_app.config['VERIFY_SSL_CERTS'] if current_app.config['VERIFY_SSL_CERTS'] else True
    except Exception as e:
        print(e) #but continue with default=True
    auth = _eonbasics_login(verify)

    #get the sessions:
    headers = {"Content-Type":"application/json", "Authorization": auth}
    r = requests.get(current_app.config.get("NOTARIZATION_URL")+"/notarizationsession/", headers=headers, verify=verify)
    open_session = None
    for s in r.json()["records"]:
        if(s["status"]=="new"):
            open_session=s
    session_id = None
    if not open_session:
        #check if there can be an origin session:
        new_session_payload={"stamper_type": "ebsi"}
        """
        Concatenated sessions are no longer useful with EBSI, but they don't harm either, it's just a relationships between sessions
        """
        #if there is no reserved previous session, get the available tips (closed sessions which have no children sessions yet)
        r = requests.get(current_app.config.get("NOTARIZATION_URL")+"/notarizationsession/availabletips", headers=headers, verify=verify)
        for s in r.json():
            new_session_payload["previous_session_id"]=s["public_id"]
            break
        #create new session:
        r = requests.post(current_app.config.get("NOTARIZATION_URL")+"/notarizationsession/",data=json.dumps(new_session_payload),headers=headers, verify=verify)
        session_id = r.json()["public_id"]
    else:
        session_id = open_session["public_id"]

    #now create the item for this notarization, which is the full jsonstring of the shipment
    export_payload = prepare_origin_shipment_payload(public_id)
    clear_text_shipment = json.dumps(export_payload)
    #hash
    final_digest = hu.digest(clear_text_shipment).hex()
    ebsi_notes = {"shipment_id": public_id, "auth_dids":[]} #get the pubkey from the provider and add it here
    #get offers and find an accepted one:
    did_keys = _get_didkeys_of_providers(public_id)
    ebsi_notes["auth_dids"]=did_keys

    new_item_payload = {
        "notarization_session_id":session_id,
        "message_hash": final_digest,
        "notes": "Shipment: "+public_id,
        "ebsi_notes": ""+json.dumps(ebsi_notes, separators=(',', ':'))
    }
    r = requests.post(current_app.config.get("NOTARIZATION_URL")+"/notarizationitem/",data=json.dumps(new_item_payload),headers=headers, verify=verify)
    item_id = r.json()["public_id"]

    shipment.notarisation_session_id = session_id
    shipment.notarisation_item_id = item_id
    _save_changes(shipment)

    #get an open timestamp for this shipment and update it, otherwise create a new one

    #save the session details in the timestamp table:
    #body is the clear-text that is hashed
    new_timestamp = {
        "notarisation_session_id":session_id,
        "notarisation_item_id":item_id,
        "body":clear_text_shipment,
        "shipment_id":public_id
    }
    res = save_new_timestamp(new_timestamp)

    return _generate_notarisation_request_message(shipment)

def _get_didkeys_of_providers(public_id):
    """
    Get all the public keys of providers for this shipment

    Get the offers with a booking (accepted offers), then get the related quote requests and form those get the public keys
    """
    did_keys = []
    offers_with_booking = Offer.query.\
    join(Booking, Offer.public_id == Booking.offer_id).\
    join(Shipment, Offer.shipment_id == Shipment.public_id).\
    filter(Shipment.public_id == public_id).\
    all()
    quote_request_ids = [offer.quote_request_id for offer in offers_with_booking]
    quote_requests = QuoteRequest.query.filter(QuoteRequest.public_id.in_(quote_request_ids)).all()
    #for each quoterequest, get the pub key of the supplier:
    for qr in quote_requests:
        didkey = kmc.convert_pubpemstring_to_didkey(qr.supplier.public_key)
        did_keys.append(didkey)
    return did_keys

def notarise(public_id):
    """
    connect to eonbasics and notarise
    """
    shipment = Shipment.query.filter_by(public_id=public_id).first()
    if not shipment:
        raise EonError('Shipment does not exist.', 404)

    if(not shipment.notarisation_session_id or not shipment.notarisation_item_id):
        #if only one of the two is set, for whatever reason, it will be overwritten with fresh data
        raise EonError('Notarization setup uncomplete for this Shipment', 409)

    verify = True 
    try:
        verify = current_app.config['VERIFY_SSL_CERTS'] if current_app.config['VERIFY_SSL_CERTS'] else True
    except Exception as e:
        print(e) #but continue with default=True
    auth = _eonbasics_login(verify)
    headers = {"Content-Type":"application/json", "Authorization": auth}
    #get the open timestamp sessions
    open_timestamps = get_pending_timestamps_for_shipment(public_id)

    for timestamp in open_timestamps:
        #lock the session
        r = requests.post(current_app.config.get("NOTARIZATION_URL")+"/notarizationsession/"+timestamp.notarisation_session_id+"/rpc/lock", data="{}", headers=headers, verify=verify)
        r = requests.post(current_app.config.get("NOTARIZATION_URL")+"/notarizationsession/"+timestamp.notarisation_session_id+"/rpc/send", data="{}", headers=headers, verify=verify)
    
    shipment.notarised_on = datetime.datetime.utcnow() 
    _save_changes(shipment)
    return _generate_notarisation_spend_message(shipment)

def get_proof(public_id):
    """
    get the ntoarisation proof:
        - body of the shipment
        - origin hash
        - signed_origin_hash
        - item proof
    """
    shipment = Shipment.query.filter_by(public_id=public_id).first()
    if not shipment:
        raise EonError('Shipment does not exist.', 404)

    if(not shipment.notarisation_session_id or not shipment.notarisation_item_id):
        #if only one of the two is set, for whatever reason, it will be overwritten with fresh data
        raise EonError('Notarization setup incomplete for this shipment', 409)

    verify = True 
    try:
        verify = current_app.config['VERIFY_SSL_CERTS'] if current_app.config['VERIFY_SSL_CERTS'] else True
    except Exception as e:
        print(e) #but continue with default=True
    auth = _eonbasics_login(verify)
    headers = {"Content-Type":"application/json", "Authorization": auth}

    #get the proof: notarizationitem/ITEM_ID/proof
    r = requests.get(current_app.config.get("NOTARIZATION_URL")+"/notarizationitem/"+shipment.notarisation_item_id+"/proof", headers=headers, verify=verify)
    proof = r.json()
    #add shipment payload
    export_payload = prepare_origin_shipment_payload(public_id)
    proof["shipment_payload"]=export_payload["payload"]
    return proof, 200

def get_timestamps(public_id):
    """
    get the timestamps

    for each timnestamp ping eonbasics for the proof
    """
    shipment = Shipment.query.filter_by(public_id=public_id).first()
    if not shipment:
        raise EonError('Shipment does not exist.', 404)

    verify = True 
    try:
        verify = current_app.config['VERIFY_SSL_CERTS'] if current_app.config['VERIFY_SSL_CERTS'] else True
    except Exception as e:
        print(e) #but continue with default=True
    auth = _eonbasics_login(verify)
    headers = {"Content-Type":"application/json", "Authorization": auth}

    timestamps = Timestamp.query.filter_by(shipment_id=public_id).all()
    timestamps_payload = []
    for timestamp in timestamps:
        r = requests.get(current_app.config.get("NOTARIZATION_URL")+"/notarizationitem/"+shipment.notarisation_item_id+"/proof", headers=headers, verify=verify)
        proof = r.json()
        payload = {
            "body": timestamp.body,
            "notarisation_session_id": timestamp.notarisation_session_id,
            "notarisation_item_id": timestamp.notarisation_item_id,
            "proof": proof
        }
        timestamps_payload.append(payload)
    return timestamps_payload

def _eonbasics_login(verify):
    """
    Login into the eonbasics server
    """
    eonbasics_url =  current_app.config.get("NOTARIZATION_URL")
    eonbasics_email = current_app.config.get("NOTARIZATION_USER")
    eonbasics_pwd = current_app.config.get("NOTARIZATION_PWD")
    #auth
    payload = {"email":eonbasics_email, "password": eonbasics_pwd} 
    headers = {"Content-Type":"application/json"}
    r = requests.post(eonbasics_url+"/auth/login",data=json.dumps(payload),headers=headers, verify=verify)
    return r.json()["Authorization"]

def delete_shipment(public_id):
    """
    destroy a document, this of course doesn't change the state of txos on the blockchain
    """
    shipment = Shipment.query.filter_by(public_id=public_id).first()

    if(shipment):
        #check if this shipment had QR connected, if so, deny delete
        qrs = QuoteRequest.query.filter_by(shipment_id=public_id).all()
        if(qrs and len(qrs)>0):
            raise EonError("Quote Requests still attached to this shipment, delete them first or request a clear delete for the quote request", 409)
        _delete_object(shipment)
        return _confirm_delete()  # returns a 200
    else:
        raise EonError('Shipment not found.', 404)


def _save_changes(data):
    db.session.add(data)
    db.session.commit()

def _delete_object(data):
    """
    commit the delete of the object
    """
    db.session.delete(data)
    db.session.commit()

def _generate_creation_ok_message(shipment):
    try:
        response_object = {
            'status': 'success',
            'message': 'New shipment created and currently being checked.',
            'public_id': shipment.public_id
        }
        return response_object, 201
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401

def _generate_update_ok_message(shipment):
    try:
        response_object = {
            'status': 'success',
            'message': 'Shipment locally updated.',
            'public_id': shipment.public_id
        }
        return response_object, 200
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401

def _confirm_delete():
    response_object = {
        'status': 'success',
        'message': 'Shipment deleted.',
    }
    return response_object, 200

def _generate_notarisation_request_message(shipment):
    response_object = {
        'status': 'success',
        'message': 'Notarization session created for shipment.',
        'session_id': shipment.notarisation_session_id,
        'notarization_item_id':shipment.notarisation_item_id
    }
    return response_object, 200

def _generate_notarisation_spend_message(shipment):
    response_object = {
        'status': 'success',
        'message': 'Notarization session locked on blockchain.',
        'session_id': shipment.notarisation_session_id,
        'notarization_item_id':shipment.notarisation_item_id
    }
    return response_object, 200