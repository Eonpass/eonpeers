import datetime
import uuid
import math

from app.main.model.timestamp import Timestamp
from app.main.model.shipment import Shipment
from app.main.services import db
from app.main.util.eonerror import EonError
from app.main.util.keymanagementutils import kmc


def save_new_timestamp(*args, **kwargs):
    """
    Save a new Timestamp

    The Event id is created by the creator node, other nodes will inherit it 

    Parameters
    ----------
    args[0]: dict
        data, JSON payload received from the controller

    args[1]: str
        public_id, if this new piece comes with an ID, it's inherited (i.e. "creator lead ID")

    Response
    --------

    Raises
    ------
    EonError
        Error that contains a message and http code to be returned by the controller to the client

    """
    data = args[0]
    public_id = args[1] if (len(args)>1) else None
    _check_shipment(data)

    try:
        new_timestamp = Timestamp(
            public_id=public_id if public_id else str(uuid.uuid4()),
            created_on=datetime.datetime.utcnow(),
            body=data.get('body', None),
            notarisation_session_id=data.get('notarisation_session_id', None),
            notarisation_item_id=data.get('notarisation_item_id', None),
            shipment_id=data.get('shipment_id', None)
        )
        #timetamp has no Unique Contraints, there is no risk of 409 here:
        _save_changes(new_timestamp) 

        return _generate_creation_ok_message(new_timestamp)
    except Exception as e:
        print(e)
        if (e.__class__.__name__ == "EonError"):
            raise e
        raise EonError('Another timestamp already exists with the given data.', 409)

def get_a_timestamp(public_id):
    return Timestamp.query.filter_by(public_id=public_id).first()

def get_pending_timestamps():
    """ get not-yet-notarised timestamps, to be closed all at once"""
    return Timestamp.query.filter_by(status="new").all()

def get_pending_timestamps_for_shipment(shipment_id):
    """ get not-yet-notarised timestamps for a shipment, to be closed all at once"""
    return Timestamp.query.filter_by(status="new", shipment_id=shipment_id).all()


def get_all_shipment_timestamps(shipment_id, args):
    """ 
    Get all the timestamps for a shipment

    
    Parameters
    ----------
    shipment_id: str
        id of the shipment to get events for

    args is requet.args passed from the API controller

    args.get("page"): str
        page number, will be cast to int
    args.get("page_size"): str
        records per page, will be cast to int

    Returns:
    --------
    dict
        a dict with the pagianted resources:
        'total_record_count', the total number of resources on the lcoal db
        'page_size', how many records per page
        'page', pg number
        'records', [obj], array of ORM objects

    Raises
    ------
    EonError:   
        500: manipulation goes wrong, db errors, etc..
        400: when there is something strange in the args (e.g. cannot cast to int)
    """
    try:
        local_timestamps = Timestamp.query.filter_by(shipment_id=shipment_id).all() #roder by creation date
        payload = {}
        payload['total_record_count']=len(local_timestamps)
        payload['page_size']= 20 if not args.get("page_size") else int(args.get("page_size"))
        payload['page']= 1 if not args.get("page") else int(args.get("page"))
        payload['records']=[]
        payload['page_size'] = payload['page_size'] if not payload['page_size'] > 200 else 200
        if(payload['page']<=0):
            payload['page']=1 #TODO
        if(payload['page_size']*payload['page'] > payload['total_record_count']):
            #if indexes are out of bound, just return the last page
            payload['page'] = math.ceil(payload['total_record_count']/payload['page_size'])
        start_index = payload['page_size']*(payload['page']-1)
        end_index = (payload['page_size']*payload['page'])-1
        for eve in local_events[start_index:end_index]:
            payload['records'].append(eve)
        return payload
    except ValueError as verr:
        # casting args to int fails
        raise EonError(verr, 400)
    except Exception as e:
        print(e)
        raise EonError(e, 500)

def _save_changes(data):
    db.session.add(data)
    db.session.commit()

def _check_shipment(data):
    """ 
    check is the related shipment exists, if not, raise error
    """
    if(not data.get("shipment_id", None)):
            raise EonError('Missing Shipment Id', 404) 
    else:
        shipment = Shipment.query.filter_by(public_id=data['shipment_id']).first()
        if not shipment:
            raise EonError('Shipment does not exist in this node.', 404) 

def _generate_creation_ok_message(event):
    try:
        response_object = {
            'status': 'success',
            'message': 'New timestamp created.',
            'public_id': event.public_id
        }
        return response_object, 201
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401
