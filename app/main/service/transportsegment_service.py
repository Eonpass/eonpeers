import datetime
import uuid

from app.main.model.piece import Piece
from app.main.model.location import Location
from app.main.model.transportsegment import TransportSegment
from app.main.services import db
from app.main.util.eonerror import EonError

from app.main.service.piece_service import (upsert_piece, get_a_piece)


def _fix_date_import_from_api(data):
    """
    When marshalling, the timexone info is lost (!!!!!) for now just add it
    """
    transport_date = None
    if(data.get('transport_date')):
        if(type(data.get('transport_date')) is str ):
            try:
                transport_date =  datetime.datetime.strptime(data.get('transport_date'), '%Y-%m-%d %H:%M:%S.%f')
            except Exception as e:
                try:
                    transport_date =  datetime.datetime.strptime(data.get('transport_date'), '%Y-%m-%d %H:%M:%S')
                except Exception as e:
                    raise EonError('Wrong datetime format, please use UTC %Y-%m-%d %H:%M:%S.%f', 409)  
    return transport_date

def save_new_transport_segment(*args, **kwargs):
    """
    Save a new Transport Segment

    The TS id is created by the creator node, other nodes will inherit it with the link of the "creator company".
    When a node sends out a TS as part of a quote request, receiving nodes that respond are added to the "subscribed nodes"

     Parameters
    ----------
    args[0]: dict
        data, JSON payload received from the controller

    args[1]: str
        public_id, if this new transport segment comes with an ID, it's inherited (i.e. "creator lead ID")

    Response
    --------

    Raises
    ------
    EonError
        Error that contains a message and http code to be returned by the controller to the client

    """
    data = args[0]
    public_id = args[1] if (len(args)>1) else None

    if(data.get('arrival_location_id')):
        arrival_location = Location.query.filter_by(public_id=data['arrival_location_id']).first()
        if not arrival_location:
            raise EonError('Arrival Location does not exist in this node.', 404)
    
    if(data.get('departure_location_id')):
        departure_location = Location.query.filter_by(public_id=data['departure_location_id']).first()
        if not departure_location:
            raise EonError('Departure Location does not exist in this node.', 404)  
    
    transport_date = _fix_date_import_from_api(data)

    try:
        new_transport_segment = TransportSegment(
            public_id=public_id if public_id else str(uuid.uuid4()),
            created_on=datetime.datetime.utcnow(),
            arrival_location_id=data.get('arrival_location_id', None),
            co2_calculation_method=data.get('co2_calculation_method', None),
            co2_emission_value=data.get('co2_emission_value', None),
            co2_emission_unit=data.get('co2_emission_unit', None),
            departure_location_id=data.get('departure_location_id', None),
            distance_calculated_value=data.get('distance_calculated_value', None),
            distance_calculated_unit=data.get('distance_calculated_unit', None),
            distance_measured_value=data.get('distance_measured_value', None),
            distance_measured_unit=data.get('distance_measured_unit', None),
            fuel_amount_calculated_value=data.get('fuel_amount_calculated_value', None),
            fuel_amount_calculated_unit=data.get('fuel_amount_calculated_unit', None),
            fuel_amount_measured_value=data.get('fuel_amount_measured_value', None),
            fuel_amount_measured_unit=data.get('fuel_amount_measured_unit', None),
            fuel_type=data.get('fuel_type', None),
            mode_code=data.get('mode_code', None),
            movement_type=data.get('movement_type', None),
            seal=data.get('seal', None),
            segment_level=data.get('segment_level', None),
            transport_identifier=data.get('transport_identifier', None)
        )
        if(transport_date):
            new_transport_segment.transport_date = transport_date
        #TODO: make 1 query only
        if(data.get("pieces", None)):
            for piece in data.get("pieces"):
                p = Piece.query.filter_by(public_id=piece["public_id"]).first()
                if(p):
                    new_transport_segment.pieces.append(p)
                else:
                    #create the piece
                    piece_res = upsert_piece(piece)
                    local_piece = get_a_piece(piece_res[0]["public_id"]) 
                    new_transport_segment.pieces.append(local_piece)

        _save_changes(new_transport_segment) 

        return _generate_creation_ok_message(new_transport_segment)
    except Exception as e:
        print(e)
        raise EonError('Something went wrong creating the transport segment.', 409)  

def get_a_transport_segment(public_id):
    return TransportSegment.query.filter_by(public_id=public_id).first()

def get_all_transport_segments():
    return TransportSegment.query.filter_by().all()

def upsert_a_transport_segment(data):
    """
    Update a transport segment if you find it, create it from scratch if missing

    if a field is missing, the previous value is saved

    """
    ts = TransportSegment.query.filter_by(public_id = data['public_id']).first()
    if not ts:
        return save_new_transport_segment(data, data['public_id'])
    else:
        transport_date = transport_date = _fix_date_import_from_api(data)

        ts.arrival_location_id=data.get('arrival_location_id', None)
        ts.co2_calculation_method=data.get('co2_calculation_method', None)
        ts.co2_emission_value=data.get('co2_emission_value', None)
        ts.co2_emission_unit=data.get('co2_emission_unit', None)
        ts.departure_location_id=data.get('departure_location_id', None)
        ts.distance_calculated_value=data.get('distance_calculated_value', None)
        ts.distance_calculated_unit=data.get('distance_calculated_unit', None)
        ts.distance_measured_value=data.get('distance_measured_value', None)
        ts.distance_measured_unit=data.get('distance_measured_unit', None)
        ts.fuel_amount_calculated_value=data.get('fuel_amount_calculated_value', None)
        ts.fuel_amount_calculated_unit=data.get('fuel_amount_calculated_unit', None)
        ts.fuel_amount_measured_value=data.get('fuel_amount_measured_value', None)
        ts.fuel_amount_measured_unit=data.get('fuel_amount_measured_unit', None)
        ts.fuel_type=data.get('fuel_type', None)
        ts.mode_code=data.get('mode_code', None)
        ts.movement_type=data.get('movement_type', None)
        ts.seal=data.get('seal', None)
        ts.segment_level=data.get('segment_level', None)
        if(transport_date):
            ts.transport_date = transport_date
        ts.transport_identifier=data.get('transport_identifier', None)

        if(data.get("pieces", None)):
            #TODO: check if pieces are to be removed
            for piece in data.get("pieces"):
                p = Piece.query.filter_by(public_id=piece["public_id"]).first()
                if(p):
                    ts.pieces.append(p)
                else:
                    #create the piece
                    piece_res = upsert_piece(piece)
                    local_piece = get_a_piece(piece_res[0]["public_id"]) 
                    ts.pieces.append(local_piece)


        _save_changes(ts)
        #if this TS is shared with other nodes, other nodes should be notified too of the changes!
        #i.e. there needs to be a list of subscribed companies waiting for updates on this node.
        return _generate_update_ok_message(ts)


def _save_changes(data):
    db.session.add(data)
    db.session.commit()

def _generate_creation_ok_message(ts):
    try:
        response_object = {
            'status': 'success',
            'message': 'New transprot segment created and currently being checked.',
            'public_id': ts.public_id
        }
        return response_object, 201
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401

def _generate_update_ok_message(ts):
    try:
        response_object = {
            'status': 'success',
            'message': 'Transport Segment updated.',
            'public_id': ts.public_id
        }
        return response_object, 201
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401
