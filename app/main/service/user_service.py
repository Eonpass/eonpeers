import datetime
import uuid

from flask import current_app
from app.main.model.user import User
from app.main.services import db


def save_new_user(data):
    user = User.query.filter_by(email=data['email']).first()
    if not user:
        new_user = User(
            public_id=str(uuid.uuid4()),
            email=data['email'],
            username=data['username'],
            password=data['password'],
            registered_on=datetime.datetime.utcnow()
        )
        save_changes(new_user)
        return generate_token(new_user)
    else:
        response_object = {
            'status': 'fail',
            'message': 'User already exists. Please Log in.',
        }
        return response_object, 409


def get_all_users():
    return User.query.all()


def get_a_user(public_id):
    return User.query.filter_by(public_id=public_id).first()

def get_public_id_from_id(id):
    user = User.query.filter_by(id=id).first()
    return user.public_id if user else ''

def create_user_for_company(data):
    """ 
    remote nodes use this method to register users on the local node
    """
    user = User.query.filter_by(email=data['email']).first()
    if not user:
        new_user = User(
            email=data['email'],
            username=data['username'],
            password_hash=data['password_hash'],
            company_id=data['company_id'],
            registered_on=datetime.datetime.utcnow()
        )
        db.session.add(new_user)
        db.session.commit()
        save_changes(new_user)
        return generate_token(new_user)
    else:
        response_object = {
            'status': 'fail',
            'message': 'User already exists. Please Log in.',
        }
        return response_object, 409

def create_admin_first_startup():
    """
    at the first startup, if there's no admin, try to create it from the config defaults
    """
    user = User.query.filter_by(email=current_app.config['ADMIN_EMAIL']).first()
    if not user:
        new_user = User(
            public_id=str(uuid.uuid4()),
            email=current_app.config['ADMIN_EMAIL'],
            username=current_app.config['ADMIN_USERNAME'],
            password=current_app.config['ADMIN_PASSWORD'],
            admin=True,
            registered_on=datetime.datetime.utcnow()
        )
        try:
            save_changes(new_user)
        except:
            db.session.rollback()   

def save_changes(data):
    db.session.add(data)
    db.session.commit()


def generate_token(user):
    try:
        # generate the auth token
        auth_token = user.encode_auth_token(user.id)
        response_object = {
            'status': 'success',
            'message': 'Successfully registered.',
            'Authorization': auth_token.decode()
        }
        return response_object, 201
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401

