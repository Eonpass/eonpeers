import datetime
import uuid

from sqlalchemy import desc

from app.main.model.company import Company
from app.main.model.shipment import Shipment
from app.main.model.waybill import Waybill
from app.main.model.offer import Offer
from app.main.services import db
from app.main.util.eonerror import EonError
from app.main.util.keymanagementutils import kmc
from app.main.util.hashutils import hu
from app.main.util.dto import WaybillDto
from app.main.util.verifierutils import verifier


def save_new_waybill(*args, **kwargs):
    """
    Save a new Waybill

    The waybill id is created by the creator node, other nodes will inherit it. This method is used by the creator node,
    a node which inherits the offer will use the import method (and the sender the export method).
    Waybill has a unique constraint: name and shipment_id, cannot have the same waybill for the same shipment twice.

    Parameters
    ----------
    args[0]: dict
        data, JSON payload received from the controller

    args[1]: str
        public_id, if this new Waybill comes with an ID, it's inherited (i.e. "creator lead ID")

    Response
    --------
    [dict, int]
        object with the message, int is the http status code to be returned

    Raises
    ------
    EonError
        Error that contains a message and http code to be returned by the controller to the client

    """
    data = args[0]
    public_id = args[1] if (len(args)>1) else None

    #precheck unique constraint:
    waybill = Waybill.query.filter_by(shipment_id=data['shipment_id'], name=data['name']).first()
    if not waybill:

        connected_shipment = Shipment.query.filter_by(public_id=data['shipment_id']).first()
        if not connected_shipment:
            raise EonError('Unknown shipment, create the shipment first.', 400)

        new_waybill = Waybill(
            public_id=public_id if public_id else str(uuid.uuid4()),
            name=data['name'],
            created_on=datetime.datetime.utcnow(),
            export_reference=data.get("export_reference", None),
            shipment_id=data['shipment_id'],
            waybill_type=data['waybill_type'],
            waybill_file_type=data.get('waybill_file_type', None),
            base64encodedfilebody=data.get('base64encodedfilebody',None)
        )
        _save_changes(new_waybill) 

        return _generate_creation_ok_message(new_waybill)
    else:
       raise EonError('The shipment already has an entry for this waybill.', 409)       

def update_waybill(data):
    """
    update a waybill given its local identifier

    we expect to receive at least the mandatory fields public_id, if relation fields are empty relations are removed, if other fields are empty old values are preserved.
    This function is designed to be called by the node owner

    """
    waybill = Waybill.query.filter_by(public_id=data['public_id']).first()
    if waybill:
        if(data.get('shipment_id', None)):
            connected_shipment = Shipment.query.filter_by(public_id=data['shipment_id']).first()
            if not connected_shipment:
                raise EonError('Connected shipment does not exist in this node.', 404)  
        else:
            #this should never happen due to DTO but still..
            raise EonError('Shipment is mandatory.', 404)  

        if(waybill.shipment_id != data.get('shipment_id', None) and waybill.name != data.get('name', None)):
            raise EonError('Cannot change shipment and name of a waybill', 409)

        waybill.export_reference = data.get("export_reference", None)
        waybill.waybill_type= data.get("waybill_type", None)
        waybill.waybill_file_type=data.get("waybill_file_type", None)
        waybill.base64econdedfilebody=data.get("base64encodedfilebody",None)
        
        try:
            _save_changes(waybill)
        except Exception as e:
            print(e)
            db.session.rollback() 
            raise EonError('Internal Server Error', 500) 

        return _generate_update_ok_message(waybill)
    else:
        raise EonError('Waybill not found.', 404)

def upsert_waybill(data):
    """
    upsert waybill

    this is used during the import step, so that a booking can be updated with new information. A booking update is rejected if:
    - the public key is different from the one known for the supplier company (checked in the import step)
    """
    if(data["public_id"]):
        waybill = Waybill.query.filter_by(public_id=data['public_id']).first()
        if waybill:
            return update_waybill(data)
        else:
            return save_new_waybill(data, data["public_id"])
    else:
        return save_new_waybill(data)

def delete_waybill(public_id):
    """
    delete waybill


    """
    waybill = Waybill.query.filter_by(public_id=public_id).first()

    if(waybill):
        _delete_object(waybill)
        return _confirm_delete()  # returns a 200
    else:
        raise EonError('Waybill not found.', 404)

def get_a_waybill(public_id):
    return Waybill.query.filter_by(public_id=public_id).first()

def get_all_waybills():
    return Waybill.query.filter_by().all()

def get_waybills_of_a_shipment(shipment_id):
    return Waybill.query.filter_by(shipment_id=shipment_id).order_by(Waybill.created_on.desc()).all()

def export_waybill(public_id):
    """
    check the waybill, then get the node details of the requester company and send call its /waybill/rpc/import method

    This method is called after receiving the "accept offer".
    
    Parameters
    ----------
    public_id: str
        the id of the booking to export, the suppllier_id is the target node (the sender is the current node and requester_id)

    Response
    --------
    dict
        body for the response message, a JSON with the payload and the signed payload

    Raises
    ------
    EonError
        Error that contains a specific message and http code to be returned by the controller to the client

    """
    waybill = _check_waybill(public_id)
    #prepare the data for the export:
    offer = Offer.query.filter_by(shipment_id=waybill.shipment_id).first()
    target_url = offer.quote_request.requester.base_url
    export_payload = _prepare_waybill_export_payload(waybill)
    _task = send_waybill.delay(target_url, export_payload)
    return export_payload

def _check_waybill(public_id):
    """
    Check that a waybill has the minimum related objects before sending it out
    
    An offer should have at least the quote request and the price

    Parameters
    ----------
    public_id: str
        the id of the booking to check

    Response
    --------
    Object
        Booking, the booking object if it passes every check

    Raises
    ------
    EonError
        Error that contains a specific message and http code to be returned by the controller to the client

    """
    waybill = Waybill.query.filter_by(public_id=public_id).first()  
    if not waybill:
        raise EonError('Waybill does not exist.', 404)

    if(waybill.shipment == None):
        raise EonError('Waybill data is corrupt, missing shipment', 409)  
    
    #check that shipment has an offer
    offer = Offer.query.filter_by(shipment_id=waybill.shipment_id).first()
    if not offer:
        raise EonError('Create and send an offer to the receiver node first.', 404)

    #TODO: is the current node the supplier for this offer? <-- this should be a receiver verification step, we can add it here just for completeness

    return waybill

def _prepare_waybill_export_payload(waybill):
    #the booking has a simpler body as it expects all other objects to have been created by a previous quote request import
    #get the payload and sign it
    payload = marshal(waybill,WaybillDto.waybill) 
    payload_to_sign = bytes.fromhex(hu.digest(json.dumps(payload, sort_keys=True)).hex())
    signed_payload_hash = kmc.sign_message(payload_to_sign)
    signed_payload_hash_hex = signed_payload_hash['signed'].hex()
    export_payload = {
        "payload":payload,
        "signed_hash":signed_payload_hash_hex,
        "supplier_company_seralised_public_key": kmc.get_serialized_pub_key().decode('utf-8')
    }
    return export_payload  

def import_waybill(import_payload):
    """
    get the related quote, check that the supplier public key matches the payload public key, then create or update the waybill
    """
    #find the quote and the supplier:
    if(not import_payload["payload"]["shipment_id"]):
        raise EonError("Payload is missing shipment id", 400)  
    
    #this shipment_id may have more than one offer from more than 1 provider
    offers = Offer.query.filter_by(shipment_id=import_payload["payload"]["shipment_id"]).all()
    #TODO: only one should have a booking, for now just check that it's coming from one of the possible suppliers
    found_matching_supplier = False
    for offer in offers:
        found_matching_supplier = offer.quote_request.supplier.public_key == import_payload["supplier_company_seralised_public_key"]
        if(found_matching_supplier):
            break

    if(not found_matching_supplier):
        raise EonError("Public keys do no match, please align the keys first", 400)

    #check signatures and payloads:
    #signed = bytes from hex (signed_message)
    signed_bytes = bytes.fromhex(import_payload["signed_hash"])
    #message = compute the hash of payload
    message = bytes.fromhex(hu.digest(json.dumps(import_payload["payload"], sort_keys=True)).hex())
    #serialized public should already be fine
    try:
        #in the future different verifier types may require different args..
        #verifier_type = current_app.config['VERIFIER_TYPE']
        #if verifier_type == 'STD'
        signature_checks_out = verifier.verify_payload(signed_bytes, message, import_payload["supplier_company_seralised_public_key"])
        if(not signature_checks_out):
            raise EonError("Signed message is corrupt", 400)        
    except Exception as e:
        print(e)
        raise EonError("Signed message is corrupt", 400) 

    waybill_res = upsert_waybill(import_payload["payload"])

def _save_changes(data):
    db.session.add(data)
    db.session.commit()

def _delete_object(data):
    """
    commit the delete of the object
    """
    db.session.delete(data)
    db.session.commit()

def _generate_creation_ok_message(step):
    try:
        response_object = {
            'status': 'success',
            'message': 'New waybill created.',
            'public_id': step.public_id
        }
        return response_object, 201
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401

def _generate_update_ok_message(step):
    try:
        response_object = {
            'status': 'success',
            'message': 'Waybill updated.',
            'public_id': step.public_id
        }
        return response_object, 201
    except Exception:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401

def _confirm_delete():
    response_object = {
        'status': 'success',
        'message': 'Waybill deleted.',
    }
    return response_object, 200
