from functools import wraps

from flask import request

from app.main.service.auth_helper import Auth
from app.main.service.disclosure_service import get_a_disclosure
from app.main.service.user_service import get_public_id_from_id


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        data, status = Auth.get_logged_in_user(request)
        token = data.get('data')
        if not token:
            return data, status
        return f(*args, **kwargs)
    return decorated


def admin_token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        data, status = Auth.get_logged_in_user(request)
        token = data.get('data')
        if not token:
            return data, status
        admin = token.get('admin')
        if not admin:
            response_object = {
                'status': 'fail',
                'message': 'admin token required'
            }
            return response_object, 401
        return f(*args, **kwargs)
    return decorated

def shipment_disclosure_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):

        data, status = Auth.get_logged_in_user(request)
        shipment_id = kwargs.get("public_id", None)
        if not shipment_id:
            response_object = {
                'status': 'fail',
                'message': 'wrong shipment id'
            }
            return response_object, 401

        token = data.get('data')
        if not token:
            return data, status

        admin = token.get('admin')
        if not admin:
            #check if the logged user has an entry for shipment:
            #TODO: this should chekc a REDIS entry, not ping the DB
            user_id = token.get('user_id') #this user id is "id" not "public_id" -> get the public_id
            user_id = get_public_id_from_id(user_id)
            try:
                discl = get_a_disclosure(shipment_id, user_id) #this raises EonError if disclosure is not there
                #TODO: alternative, if the current loggedin user is from a company which has quote requests or offers for this shipment
                if not(discl.approved):
                    response_object = {
                        'status': 'fail',
                        'message': 'Unauthorized'
                    }
                    return response_object, 401
            except Exception as e:
                print(e)
                response_object = {
                    'status': 'fail',
                    'message': 'Unauthorized'
                }
                return response_object, 401
        
        return f(*args, **kwargs)
    return decorated

# THIS IS DONE WITH THE DTO OBJECTS! --->
# check if the json has the field(s) described in the arguments
# must be decorate the function which handles the JSON
# (which has request.get_json())
# e.g. check the post json has the field "id":
# @app.route("/someroute", methods=["POST"])
# @validate_json("id")
# def someFunction():
#     json_data= request.get_json()
#     ...
def validate_json(*expected_args):
    def decorator_validate_json(f):
        @wraps(f)
        def wrapper_validate_json(*args, **kwargs):
            json_object = request.get_json()
            for expected_arg in expected_args:
                if expected_arg not in json_object:
                    response_object = {
                        'status': 'fail',
                        'message': 'requried field missing',
                        'errors': [
                            {
                                'status': '400',
                                "title":  "Attribute Missing",
                                "detail": expected_arg+" missing from JSON"
                            }
                        ]
                    }
                    return response_object, 400
            return f(*args, **kwargs)
        return wrapper_validate_json
    return decorator_validate_json




def log_error(logger):
    """ Decorator that logs the exception and re-rasies it """
    def decorated(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            try:
                return f(*args, **kwargs)
            except Exception as e:
                if logger:
                    logger.exception(e)
                raise
        return wrapped
    return decorated
