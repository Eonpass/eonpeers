from flask_restplus import Namespace, fields

class UserDto:
    api = Namespace('user', description='user related operations')
    user = api.model('user', {
        'email': fields.String(required=True,
                               description='user email address'),
        'username': fields.String(required=True, description='user username'),
        'public_id': fields.String(description='user Identifier')
    })
    new_user = api.model('new_user', {
        'email': fields.String(required=True,
                               description='user email address'),
        'username': fields.String(required=True, description='user username'),
        'password': fields.String(required=True, description='user password')
    })

class AuthDto:
    api = Namespace('auth', description='authentication related operations')
    user_auth = api.model('auth_details', {
        'email': fields.String(required=True, description='The email address'),
        'password': fields.String(required=True,
                                  description='The user password'),
    })

class CompanyDto:
    api = Namespace('company', description='company related operations')
    company = api.model('company', {
        'public_id': fields.String(description='company local Identifier'),
        'name': fields.String(required=True,
                              description='company name'),
        'vat_number': fields.String(required=True,
                                description='company identificaiton number'),
        'base_url': fields.String(required=True,
                                description='base url of the node controlled by the company'),
        'public_key': fields.String(required=False,
                                description='utf-8 pem representaion of the public key'),
        'eori_number': fields.String(required=False,
                                description='EORI number for the company'),      
        'aeo_status': fields.String(required=False,
                                description='AEO status declared by the company'),
        'fiscal_address': fields.String(required=False,
                                description='Address of the fiscal residency of the company'),
        'fiscal_city': fields.String(required=False,
                                description='City of the fiscal residency of the company'),
        'fiscal_province': fields.String(required=False,
                                description='Province of the fiscal residency of the company'),
        'fiscal_country': fields.String(required=True,
                                description='Country of the fiscal residency of the company'),
        'fiscal_zip': fields.String(required=False,
                                description='ZIP of the fiscal residency of the company'),
        'unique_hash': fields.String(required=False,
                                description='Hash identifying the company current data set')
    })
    new_company = api.model('new_company', {
        'name': fields.String(required=True,
                              description='company name'),
        'vat_number': fields.String(required=True,
                                description='company identificaiton number'),
        'base_url': fields.String(required=True,
                                description='base url of the node controlled by the company'),
        'public_key': fields.String(required=False,
                                description='utf-8 pem representaion of the public key'),
        'eori_number': fields.String(required=False,
                                description='EORI number for the company'),      
        'aeo_status': fields.String(required=False,
                                description='AEO status declared by the company'),
        'fiscal_address': fields.String(required=False,
                                description='Address of the fiscal residency of the company'),
        'fiscal_city': fields.String(required=False,
                                description='City of the fiscal residency of the company'),
        'fiscal_province': fields.String(required=False,
                                description='Province of the fiscal residency of the company'),
        'fiscal_country': fields.String(required=True,
                                description='Country of the fiscal residency of the company'),
        'fiscal_zip': fields.String(required=False,
                                description='ZIP of the fiscal residency of the company')
    })
    node_owner_company = api.model('node_owner_company', {
        'name': fields.String(required=True,
                              description='company name'),
        'vat_number': fields.String(required=True,
                                description='company identificaiton number'),
        'base_url': fields.String(required=True,
                                description='base url of the node controlled by the company'),
        'public_key': fields.String(required=True,
                                description='utf-8 pem representaion of the public key'),
        'eori_number': fields.String(required=False,
                                description='EORI number for the company'),      
        'aeo_status': fields.String(required=False,
                                description='AEO status declared by the company'),
        'fiscal_address': fields.String(required=False,
                                description='Address of the fiscal residency of the company'),
        'fiscal_city': fields.String(required=False,
                                description='City of the fiscal residency of the company'),
        'fiscal_province': fields.String(required=False,
                                description='Province of the fiscal residency of the company'),
        'fiscal_country': fields.String(required=True,
                                description='Country of the fiscal residency of the company'),
        'fiscal_zip': fields.String(required=False,
                                description='ZIP of the fiscal residency of the company'),
        'unique_hash': fields.String(required=False,
                                description='Hash identifying the company current data set')
    })
    new_key = api.model('new_key', {
        'kty': fields.String(required=True,
                              description='Type of cryptography JWK parameter, must be "EC"'),
        'crv': fields.String(required=True,
                                description='Selected curve, must be "secp256k1"'),
        'x': fields.String(required=True,
                                description='Public key X coordinate - base64 url encoded as per JWK standard'),
        'y': fields.String(required=True,
                                description='Public key Y coordinate - base64 url encoded as per JWK standard'),
        'd': fields.String(required=True,
                                description='Secret number - based64 url encoded as per JWK standard')
    })

class LocationDto:
    api = Namespace('location', description='location related operations')
    location = api.model('location', {
        'public_id': fields.String(description='location local identifier'),
        'name': fields.String(required=True,
                              description='location name'),
        'location_code': fields.String(required=True,
                                description='Location code of airport, freight terminal, seaport, rail station. UN/LOCODE city code (5 letter) or IATA airport code (3 letter)'),
        'location_type': fields.String(required=True,
                                description='Location type - e.g. Airport, Freight terminal, Rail station, Seaport, etc'),
        'postal_code': fields.String(required=True,
                                description='Postal / ZIP code'),
        'city_name': fields.String(required=True,
                                description='Full name of the city '),
        'country': fields.String(required=True,
                                description='Country ISO code. Refer ISO 3166-2'),
        'street': fields.String(required=True,
                                description='Street address including street name, street number, building number, apartment etc'),
        'city_code': fields.String(required=False,
                                description='UN/LOCODE city code (5 letter) or IATA city code (3 letter)'),
        'po_box': fields.String(required=False,
                                description='Post Office box number / code'),
        'region_code': fields.String(required=False,
                                description='Region/ State / Department. Refer ISO 3166-2 '),
        'region_name' : fields.String(required=False,
                                description='If no StateCode provided, full name of the region / state / province / canton ')
    })
    new_location = api.model('new_location', {
        'name': fields.String(required=True,
                              description='location name'),
        'location_code': fields.String(required=True,
                                description='Location code of airport, freight terminal, seaport, rail station. UN/LOCODE city code (5 letter) or IATA airport code (3 letter)'),
        'location_type': fields.String(required=False,
                                description='Location type - e.g. Airport, Freight terminal, Rail station, Seaport, etc'),
        'postal_code': fields.String(required=True,
                                description='Postal / ZIP code'),
        'city_name': fields.String(required=True,
                                description='Full name of the city '),
        'country': fields.String(required=True,
                                description='Country ISO code. Refer ISO 3166-2'),
        'street': fields.String(required=True,
                                description='Street address including street name, street number, building number, apartment etc'),
        'city_code': fields.String(required=False,
                                description='UN/LOCODE city code (5 letter) or IATA city code (3 letter)'),
        'po_box': fields.String(required=False,
                                description='Post Office box number / code'),
        'region_code': fields.String(required=False,
                                description='Region/ State / Department. Refer ISO 3166-2 '),
        'region_name' : fields.String(required=False,
                                description='If no StateCode provided, full name of the region / state / province / canton ')
    })

class ProductDto:
    api = api = Namespace('product', description='product related operations')
    product = api.model('product', {
        'public_id': fields.String(required=True, description='public identifier of this object on this node'),
        'brand_owner_company_id': fields.String(required=False, description='the local public id of the company who own the brand of the product'),
        'commodity_code': fields.String(required=True, description='Unique Commodity Code e.g. 391721 - Tubes, Pipes And Hoses, Rigid, Of Polymers Of Ethylene'),
        'commodity_description': fields.String(required=True, description='Commodity description'),
        'commodity_name':fields.String(required=False, description='Name of commodity'),
        'commodity_type':fields.String(required=True, description='Issuer of the Commodity Code - e.g. Brussels Tariff Nomenclature, EU Harmonized System Code, UN Standard International Trade…'),
        'manufacturer_company_id':fields.String(required=False, description='Manufacturing company'),
        'product_description':fields.String(required=True, description='Product description'),
        'product_identifier':fields.String(required=True, description='Manufacturer\'s unique product identifier'),
        'un_number':fields.String(required=False, description='Reference identifying the United Nations Dangerous Goods serial number assigned within the UN to substances and articles contained in a list of the dangerous goods most commonly carried. e.g. 1189 - Ethylene glycol monomethyl ether acetate'),
        'unique_hash':fields.String(required=False, description='Hash of the unique contraints that identifies the same product across different nodes')
    })
    new_product = api.model('new_product', {
        'brand_owner_company_id': fields.String(required=False, description='the local public id of the company who own the brand of the product'),
        'commodity_code': fields.String(required=True, description='Unique Commodity Code e.g. 391721 - Tubes, Pipes And Hoses, Rigid, Of Polymers Of Ethylene'),
        'commodity_description': fields.String(required=True, description='Commodity description'),
        'commodity_name':fields.String(required=False, description='Name of commodity'),
        'commodity_type':fields.String(required=True, description='Issuer of the Commodity Code - e.g. Brussels Tariff Nomenclature, EU Harmonized System Code, UN Standard International Trade…'),
        'manufacturer_company_id':fields.String(required=False, description='Manufacturing company'),
        'product_description':fields.String(required=True, description='Product description'),
        'product_identifier':fields.String(required=True, description='Manufacturer\'s unique product identifier'),
        'un_number':fields.String(required=False, description='Reference identifying the United Nations Dangerous Goods serial number assigned within the UN to substances and articles contained in a list of the dangerous goods most commonly carried. e.g. 1189 - Ethylene glycol monomethyl ether acetate'),
    })
    update_product = api.model('update_product', {
        'public_id': fields.String(required=True, description='public identifier of this object on this node'),
        'brand_owner_company_id': fields.String(required=False, description='the local public id of the company who own the brand of the product'),
        'commodity_code': fields.String(required=True, description='Unique Commodity Code e.g. 391721 - Tubes, Pipes And Hoses, Rigid, Of Polymers Of Ethylene'),
        'commodity_description': fields.String(required=True, description='Commodity description'),
        'commodity_name':fields.String(required=False, description='Name of commodity'),
        'commodity_type':fields.String(required=True, description='Issuer of the Commodity Code - e.g. Brussels Tariff Nomenclature, EU Harmonized System Code, UN Standard International Trade…'),
        'manufacturer_company_id':fields.String(required=False, description='Manufacturing company'),
        'product_description':fields.String(required=True, description='Product description'),
        'product_identifier':fields.String(required=True, description='Manufacturer\'s unique product identifier'),
        'un_number':fields.String(required=False, description='Reference identifying the United Nations Dangerous Goods serial number assigned within the UN to substances and articles contained in a list of the dangerous goods most commonly carried. e.g. 1189 - Ethylene glycol monomethyl ether acetate')
    })

class CustomsInfoDto:
    api = Namespace('customs_info', description='Customs info operations')
    customs_info = api.model('customs_info', {
        'public_id': fields.String(required=True, description='public identifier of this object on this node'),
        'content_code': fields.String(required=False, description='Customs content code. Refer CXML Code List 1.100, e.g. IST - Security Textual StatementNumber, M - Movement Reference Number'),
        'country_code': fields.String(required=False, description='Country code'),
        'information': fields.String(required=False, description='Information for customs submission'),
        'subject_code': fields.String(required=False, description='Customs subject code. Refer CXML Code List 1.19, e.g. IMP for import, EXP for export, AGT for Agent, ISS for The Regulated Agent Issuing the Security Status for a Consignment etc. At least one of the three elements (Country Code, Information Identifier or Customs, Security and Regulatory Control Information Identifier) must be completed'),
        'note': fields.String(required=False, description='Free text for customs remarks'),
        'piece_id':fields.String(required=True, description='Id of the related Piece'),
    }) 
    new_customs_info = api.model('new_customs_info', {
        'content_code': fields.String(required=False, description='Customs content code. Refer CXML Code List 1.100, e.g. IST - Security Textual StatementNumber, M - Movement Reference Number'),
        'country_code': fields.String(required=False, description='Country code'),
        'information': fields.String(required=False, description='Information for customs submission'),
        'subject_code': fields.String(required=False, description='Customs subject code. Refer CXML Code List 1.19, e.g. IMP for import, EXP for export, AGT for Agent, ISS for The Regulated Agent Issuing the Security Status for a Consignment etc. At least one of the three elements (Country Code, Information Identifier or Customs, Security and Regulatory Control Information Identifier) must be completed'),
        'note': fields.String(required=False, description='Free text for customs remarks'),
        'piece_id':fields.String(required=True, description='Id of the related Piece'),
    }) 

class DangerousGoodDto:
    api = Namespace('dangerous_good', description='Dangerous Goods operations')
    dangerous_good = api.model('dangerous_good', {
        'public_id': fields.String(required=True, description='public identifier of this object on this node'),
        'class_code': fields.String(required=False, description='Dangerous goods class code of the pieces in the shipment'),
        'net_quantity_value': fields.Arbitrary(required=False, description='net quantity'),
        'net_quantity_um': fields.String(required=False, description='Quantity unit of measure'),
        'packing_group': fields.String(required=False, description='Packing group'),
        'packing_instruction': fields.String(required=False, description='Packing instruction'),
        'un_number': fields.String(required=False, description='UN Number associated to the DG'),
        'piece_id':fields.String(required=True, description='Id of the related Piece'),
    }) 
    new_dangerous_good = api.model('new_dangerous_good', {
        'class_code': fields.String(required=False, description='Dangerous goods class code of the pieces in the shipment'),
        'net_quantity_value': fields.Arbitrary(required=False, description='net quantity'),
        'net_quantity_um': fields.String(required=False, description='Quantity unit of measure'),
        'packing_group': fields.String(required=False, description='Packing group'),
        'packing_instruction': fields.String(required=False, description='Packing instruction'),
        'un_number': fields.String(required=False, description='UN Number associated to the DG'),
        'piece_id':fields.String(required=True, description='Id of the related Piece'),
    }) 

class PieceDto:
    api = api = Namespace('piece', description='Piece related operations')
    piece = api.model('piece', {
        'public_id': fields.String(required=True, description='public identifier of this object on this node'),
        'additional_security_info': fields.String(required=False, description='Ad hoc security statement required by state regulators '),
        'coload': fields.Boolean(required=False, description='Coload indicator for the pieces (boolean as text: true / false)'),
        'parent_piece_id': fields.String(required=False, description='Id of the parent (i.e. containing) piece for composed pieces'),
        'goods_description':fields.String(required=False, description='General goods description'),
        'gross_weight_value':fields.Arbitrary(required=True, description='Weight details, decimal value - max 4 decimal places suggested'),
        'gross_weight_um':fields.String(required=True, description='Weight details, unit of measure'),
        'load_type':fields.String(required=False, description='Specify how the piece will be delivered (bulk or ULD)'),
        'shipment_id':fields.String(required=False, description='Id of the containing Shipment'),
        'slac':fields.Integer(required=False, description='Shipper\'s Load And Count (total contained piece count as provided by shipper)'),
        'stackable':fields.Boolean(required=False, description='Stackable indicator for the pieces (boolean as text: true / false)'),
        'turnable':fields.Boolean(required=False, description='Turnable indicator for the pieces (boolean as text: true / false)'),
        'upid':fields.String(required=False, description='Unique Piece Identifier (UPID) of the piece (Refer IATA Recommended Practice 1689) or URI if virtual grouping of pieces, if left empy this is the same as public_id'),
        'volumetric_weight_conversion_factor_value':fields.Arbitrary(required=True, description='Volumetric Weight details, decimal value - max 4 decimal places suggested'),
        'volumetric_weight_conversion_factor_um':fields.String(required=True, description='Volumetric Weight details, unit of measure'),
        'volumetric_weight_chargeable_weight_value':fields.Arbitrary(required=True, description='Volumetric Chargeable Weight details, decimal value - max 4 decimal places suggested'),
        'volumetric_weight_chargeable_weight_um':fields.String(required=True, description='Volumetric Chargeable Weight details, unit of measure'),
        'height_value':fields.Arbitrary(required=False, description='Height details, decimal value - max 4 decimal places suggested'),
        'height_um':fields.String(required=False, description='Height details, unit of measure'),
        'length_value':fields.Arbitrary(required=False, description='Length details, decimal value - max 4 decimal places suggested'),
        'length_um':fields.String(required=False, description='Length details, unit of measure'),
        'width_value':fields.Arbitrary(required=False, description='Width details, decimal value - max 4 decimal places suggested'),
        'width_um':fields.String(required=False, description='Width details, unit of measure')
    })
    new_product_piece = api.model('new_product_piece', {
        'brand_owner_company_id': fields.String(required=False, description='the local public id of the company who own the brand of the product. '),
        'commodity_code': fields.String(required=False, description='Unique Commodity Code e.g. 391721 - Tubes, Pipes And Hoses, Rigid, Of Polymers Of Ethylene. Must be present to create a new product.'),
        'commodity_description': fields.String(required=False, description='Commodity description. Must be present to create a new product.'),
        'commodity_name':fields.String(required=False, description='Name of commodity'),
        'commodity_type':fields.String(required=False, description='Issuer of the Commodity Code - e.g. Brussels Tariff Nomenclature, EU Harmonized System Code, UN Standard International Trade.. Must be present to create a new product.'),
        'manufacturer_company_id':fields.String(required=False, description='Manufacturing company'),
        'product_description':fields.String(required=False, description='Product description. Must be present to create a new product.'),
        'product_identifier':fields.String(required=False, description='Manufacturer\'s unique product identifier. Must be present to create a new product.'),
        'un_number':fields.String(required=False, description='Reference identifying the United Nations Dangerous Goods serial number assigned within the UN to substances and articles contained in a list of the dangerous goods most commonly carried. e.g. 1189 - Ethylene glycol monomethyl ether acetate'),
        'unique_hash':fields.String(required=False, description='Hash of the unique contraints that identifies the same product across different nodes, must be present if referencing an existing product')
    })
    new_transport_segment_piece = api.model('new_transport_segment_piece', {
        'public_id': fields.String(required=True, description='public identifier of this object'),
        'arrival_location': fields.Nested(LocationDto.new_location, requried="False", description="Arrival location object"),
        'co2_calculation_method': fields.String(required=False, description='Method of calculation of the CO2 emissions'),
        'co2_emission_value': fields.Arbitrary(required=False, description='Amount of CO2 emitted, numeric value'),
        'co2_emission_unit': fields.String(required=False, description='Amount of CO2 emitted, unit of measure e.g. kg/km' ),
        'departure_location': fields.Nested(LocationDto.new_location, requried="False", description="Departure location object"),
        'distance_calculated_value': fields.Arbitrary(required=False, description='Distance calculated if distance measured is not available, numeric value'),
        'distance_calculated_unit': fields.String(required=False, description='Distance calculated if distance measured is not available, unit of measure'),
        'distance_measured_value': fields.Arbitrary(required=False, description='Distance based on actually measured distance, numeric value'),
        'distance_measured_unit': fields.String(required=False, description='Distance based on actually measured distance, unit of measure'),
        'fuel_amount_calculated_value': fields.Arbitrary(required=False, description='Calculated fuel consumption, if measured not available, numeric value'),
        'fuel_amount_calculated_unit': fields.String(required=False, description='Calculated fuel consumption, if measured not available, unit of measure' ),
        'fuel_amount_measured_value': fields.Arbitrary(required=False, description='Actually measured fuel consumption, numeric value'),
        'fuel_amount_measured_unit': fields.String(required=False, description='Actually measured fuel consumption, unit of measure' ),
        'fuel_type': fields.String(required=False, description='e.g. Kerosene, Diesel, SAF, Electricity [renewable], Electricity [non-renewable]' ),
        'mode_code':  fields.String(required=False, description='Mode of transport code. Refer UNECE Recommendation N. 19 - Code for Modes of Transport,  e.g. 4 - Air transport' ),
        'movement_type': fields.String(required=False, description='Refers to the type of movement: Actual or Planned' ),
        'seal': fields.String(required=False, description='Seal identifier'),
        'segment_level': fields.String(required=False, description='Identification of the segment level in the movement of the pieces: contractual, flight leg, truck movement, etc.'),
        'transport_date': fields.String(required=False, description='Date associated with TransportIdentifier to uniquely identify the transport line , UTC format, e.g. "2030-03-20 01:31:12.467113"'),
        'transport_identifier': fields.String(required=False, description='Airline flight number, or rail /  truck / maritime line id'),
    })
    new_piece = api.model('new_piece', {
        'additional_security_info': fields.String(required=False, description='Ad hoc security statement required by state regulators '),
        'coload': fields.Boolean(required=False, description='Coload indicator for the pieces (boolean as text: true / false)'),
        'parent_piece_id': fields.String(required=False, description='Id of the parent (i.e. containing) piece for composed pieces'),
        'goods_description':fields.String(required=False, description='General goods description'),
        'gross_weight_value':fields.Arbitrary(required=True, description='Weight details, decimal value - max 4 decimal places suggested'),
        'gross_weight_um':fields.String(required=True, description='Weight details, unit of measure'),
        'load_type':fields.String(required=False, description='Specify how the piece will be delivered (bulk or ULD)'),
        'shipment_id':fields.String(required=False, description='Id of the containing Shipment'),
        'slac':fields.Integer(required=False, description='Shipper\'s Load And Count (total contained piece count as provided by shipper)'),
        'stackable':fields.Boolean(required=False, description='Stackable indicator for the pieces (boolean as text: true / false)'),
        'turnable':fields.Boolean(required=False, description='Turnable indicator for the pieces (boolean as text: true / false)'),
        'upid':fields.String(required=False, description='Unique Piece Identifier (UPID) of the piece (Refer IATA Recommended Practice 1689) or URI if virtual grouping of pieces, if left empy this is the same as public_id'),
        'volumetric_weight_conversion_factor_value':fields.Arbitrary(required=True, description='Volumetric Weight details, decimal value - max 4 decimal places suggested'),
        'volumetric_weight_conversion_factor_um':fields.String(required=True, description='Volumetric Weight details, unit of measure'),
        'volumetric_weight_chargeable_weight_value':fields.Arbitrary(required=True, description='Volumetric Chargeable Weight details, decimal value - max 4 decimal places suggested'),
        'volumetric_weight_chargeable_weight_um':fields.String(required=True, description='Volumetric Chargeable Weight details, unit of measure'),
        'height_value':fields.Arbitrary(required=False, description='Height details, decimal value - max 4 decimal places suggested'),
        'height_um':fields.String(required=False, description='Height details, unit of measure'),
        'length_value':fields.Arbitrary(required=False, description='Length details, decimal value - max 4 decimal places suggested'),
        'length_um':fields.String(required=False, description='Length details, unit of measure'),
        'width_value':fields.Arbitrary(required=False, description='Width details, decimal value - max 4 decimal places suggested'),
        'width_um':fields.String(required=False, description='Width details, unit of measure'),
        'products':fields.List(fields.Nested(new_product_piece))
    })
    upsert_piece = api.model('update_piece', {
        'public_id': fields.String(required=True, description='public identifier of this object on this node'),
        'additional_security_info': fields.String(required=False, description='Ad hoc security statement required by state regulators '),
        'coload': fields.String(required=False, description='Coload indicator for the pieces (boolean as text: true / false)'),
        'parent_piece_id': fields.String(required=False, description='Id of the parent (i.e. containing) piece for composed pieces, if left empy, it removes the existing connection'),
        'goods_description':fields.String(required=False, description='General goods description'),
        'gross_weight_value':fields.Arbitrary(required=True, description='Weight details, decimal value - max 4 decimal places suggested'),
        'gross_weight_um':fields.String(required=True, description='Weight details, unit of measure'),
        'load_type':fields.String(required=False, description='Specify how the piece will be delivered (bulk or ULD)'),
        'shipment_id':fields.String(required=False, description='Id of the containing Shipment, if left empy, it removes the existing connection'),
        'slac':fields.Integer(required=False, description='Shipper\'s Load And Count (total contained piece count as provided by shipper)'),
        'stackable':fields.String(required=False, description='Stackable indicator for the pieces (boolean as text: true / false)'),
        'turnable':fields.String(required=False, description='Turnable indicator for the pieces (boolean as text: true / false)'),
        'upid':fields.String(required=False, description='Unique Piece Identifier (UPID) of the piece (Refer IATA Recommended Practice 1689) or URI if virtual grouping of pieces, if left empy this is the same as public_id'),
        'volumetric_weight_conversion_factor_value':fields.Arbitrary(required=True, description='Volumetric Weight details, decimal value - max 4 decimal places suggested'),
        'volumetric_weight_conversion_factor_um':fields.String(required=True, description='Volumetric Weight details, unit of measure'),
        'volumetric_weight_chargeable_weight_value':fields.Arbitrary(required=True, description='Volumetric Chargeable Weight details, decimal value - max 4 decimal places suggested'),
        'volumetric_weight_chargeable_weight_um':fields.String(required=True, description='Volumetric Chargeable Weight details, unit of measure'),
        'height_value':fields.Arbitrary(required=False, description='Height details, decimal value - max 4 decimal places suggested'),
        'height_um':fields.String(required=False, description='Height details, unit of measure'),
        'length_value':fields.Arbitrary(required=False, description='Length details, decimal value - max 4 decimal places suggested'),
        'length_um':fields.String(required=False, description='Length details, unit of measure'),
        'width_value':fields.Arbitrary(required=False, description='Width details, decimal value - max 4 decimal places suggested'),
        'width_um':fields.String(required=False, description='Width details, unit of measure'),
        'products':fields.List(fields.Nested(new_product_piece), required=False, description='Optional list of related products, if empty no changes will be made, if filled the exact list will be imported and previous products removed. RPC actions have to be used to add or remove product by product from the piece')
    })
    nested_piece = api.model('nested_piece', {
        'public_id': fields.String(required=True, description='public identifier of this object on this node'),
        'additional_security_info': fields.String(required=False, description='Ad hoc security statement required by state regulators '),
        'coload': fields.String(required=False, description='Coload indicator for the pieces (boolean as text: true / false)'),
        'parent_piece_id': fields.String(required=False, description='Id of the parent (i.e. containing) piece for composed pieces, during import the piece pointed by the shipment must have no parent piece (i.e. must be the top piece).'),
        'goods_description':fields.String(required=False, description='General goods description'),
        'gross_weight_value':fields.Arbitrary(required=False, description='Weight details, decimal value - max 4 decimal places suggested. NOT REQUESTED if the piece already exists'),
        'gross_weight_um':fields.String(required=False, description='Weight details, unit of measure. NOT REQUESTED if the piece already exists'),
        'load_type':fields.String(required=False, description='Specify how the piece will be delivered (bulk or ULD)'),
        'slac':fields.Integer(required=False, description='Shipper\'s Load And Count (total contained piece count as provided by shipper)'),
        'stackable':fields.String(required=False, description='Stackable indicator for the pieces (boolean as text: true / false)'),
        'turnable':fields.String(required=False, description='Turnable indicator for the pieces (boolean as text: true / false)'),
        'upid':fields.String(required=False, description='Unique Piece Identifier (UPID) of the piece (Refer IATA Recommended Practice 1689) or URI if virtual grouping of pieces, if left empy this is the same as public_id'),
        'volumetric_weight_conversion_factor_value':fields.Arbitrary(required=False, description='Volumetric Weight details, decimal value - max 4 decimal places suggested. NOT REQUESTED if the piece already exists'),
        'volumetric_weight_conversion_factor_um':fields.String(required=False, description='Volumetric Weight details, unit of measure. NOT REQUESTED if the piece already exists'),
        'volumetric_weight_chargeable_weight_value':fields.Arbitrary(required=False, description='Volumetric Chargeable Weight details, decimal value - max 4 decimal places suggested. NOT REQUESTED if the piece already exists'),
        'volumetric_weight_chargeable_weight_um':fields.String(required=False, description='Volumetric Chargeable Weight details, unit of measure. NOT REQUESTED if the piece already exists'),
        'height_value':fields.Arbitrary(required=False, description='Height details, decimal value - max 4 decimal places suggested'),
        'height_um':fields.String(required=False, description='Height details, unit of measure'),
        'length_value':fields.Arbitrary(required=False, description='Length details, decimal value - max 4 decimal places suggested'),
        'length_um':fields.String(required=False, description='Length details, unit of measure'),
        'width_value':fields.Arbitrary(required=False, description='Width details, decimal value - max 4 decimal places suggested'),
        'width_um':fields.String(required=False, description='Width details, unit of measure'),
        'products':fields.List(fields.Nested(new_product_piece), required=False, description='Optional list of related products. RPC actions have to be used to add or remove product by product from the piece'),     
        'transport_segments': fields.List(fields.Nested(new_transport_segment_piece), requried=True, description="List of related transport_segments, during import at least one of those must be the one referenced by id in the quote_request object"),
        'dangerous_goods': fields.List(fields.Nested(DangerousGoodDto.dangerous_good), required=False, description="List of related Dangerous Goods declarations for this piece"),
        'customs_infos': fields.List(fields.Nested(CustomsInfoDto.customs_info), required=False, description="List of related Customs Info declarations for this piece"),
    })

class TransportSegmentDto:
    api = Namespace('transportsegment', description='transport segment related operations')
    transport_segment = api.model('transportsegment', {
        'public_id': fields.String(required=True, description='public identifier of this object on this node'),
        'arrival_location_id': fields.String(required=False, description='Id of the arrival location, can be the local id or the hash of the unique constraints of the location'),
        'co2_calculation_method': fields.String(required=False, description='Method of calculation of the CO2 emissions'),
        'co2_emission_value': fields.Arbitrary(required=False, description='Amount of CO2 emitted, numeric value'),
        'co2_emission_unit': fields.String(required=False, description='Amount of CO2 emitted, unit of measure e.g. kg/km' ),
        'departure_location_id': fields.String(required=False, description='Id of the departure location, can be the local id or the hash of the unique constraints of the location'),
        'distance_calculated_value': fields.Arbitrary(required=False, description='Distance calculated if distance measured is not available, numeric value'),
        'distance_calculated_unit': fields.String(required=False, description='Distance calculated if distance measured is not available, unit of measure'),
        'distance_measured_value': fields.Arbitrary(required=False, description='Distance based on actually measured distance, numeric value'),
        'distance_measured_unit': fields.String(required=False, description='Distance based on actually measured distance, unit of measure'),
        'fuel_amount_calculated_value': fields.Arbitrary(required=False, description='Calculated fuel consumption, if measured not available, numeric value'),
        'fuel_amount_calculated_unit': fields.String(required=False, description='Calculated fuel consumption, if measured not available, unit of measure' ),
        'fuel_amount_measured_value': fields.Arbitrary(required=False, description='Actually measured fuel consumption, numeric value'),
        'fuel_amount_measured_unit': fields.String(required=False, description='Actually measured fuel consumption, unit of measure' ),
        'fuel_type': fields.String(required=False, description='e.g. Kerosene, Diesel, SAF, Electricity [renewable], Electricity [non-renewable]' ),
        'mode_code':  fields.String(required=False, description='Mode of transport code. Refer UNECE Recommendation N. 19 - Code for Modes of Transport,  e.g. 4 - Air transport' ),
        'movement_type': fields.String(required=False, description='Refers to the type of movement: Actual or Planned' ),
        'seal': fields.String(required=False, description='Seal identifier'),
        'segment_level': fields.String(required=False, description='Identification of the segment level in the movement of the pieces: contractual, flight leg, truck movement, etc.'),
        'transport_date': fields.String(required=False, description='Date associated with TransportIdentifier to uniquely identify the transport line , UTC format, e.g. "2030-03-20 01:31:12.467113"'),
        'transport_identifier': fields.String(required=False, description='Airline flight number, or rail /  truck / maritime line id'),
        'pieces':fields.List(fields.Nested(PieceDto.nested_piece), required=False, description='Optional list of related pieces, if empty no changes will be made, if filled the exact list will be imported and previous pieces removed. RPC actions have to be used to add or remove piece by piece')
    })
    new_transport_segment = api.model('newtransportsegment', {
        'arrival_location_id': fields.String(required=False, description='Id of the arrival location, can be the local id or the hash of the unique constraints of the location'),
        'co2_calculation_method': fields.String(required=False, description='Method of calculation of the CO2 emissions'),
        'co2_emission_value': fields.Arbitrary(required=False, description='Amount of CO2 emitted, numeric value'),
        'co2_emission_unit': fields.String(required=False, description='Amount of CO2 emitted, unit of measure e.g. kg/km' ),
        'departure_location_id': fields.String(required=False, description='Id of the departure location, can be the local id or the hash of the unique constraints of the location'),
        'distance_calculated_value': fields.Arbitrary(required=False, description='Distance calculated if distance measured is not available, numeric value'),
        'distance_calculated_unit': fields.String(required=False, description='Distance calculated if distance measured is not available, unit of measure'),
        'distance_measured_value': fields.Arbitrary(required=False, description='Distance based on actually measured distance, numeric value'),
        'distance_measured_unit': fields.String(required=False, description='Distance based on actually measured distance, unit of measure'),
        'fuel_amount_calculated_value': fields.Arbitrary(required=False, description='Calculated fuel consumption, if measured not available, numeric value'),
        'fuel_amount_calculated_unit': fields.String(required=False, description='Calculated fuel consumption, if measured not available, unit of measure' ),
        'fuel_amount_measured_value': fields.Arbitrary(required=False, description='Actually measured fuel consumption, numeric value'),
        'fuel_amount_measured_unit': fields.String(required=False, description='Actually measured fuel consumption, unit of measure' ),
        'fuel_type': fields.String(required=False, description='e.g. Kerosene, Diesel, SAF, Electricity [renewable], Electricity [non-renewable]' ),
        'mode_code':  fields.String(required=False, description='Mode of transport code. Refer UNECE Recommendation N. 19 - Code for Modes of Transport,  e.g. 4 - Air transport' ),
        'movement_type': fields.String(required=False, description='Refers to the type of movement: Actual or Planned' ),
        'seal': fields.String(required=False, description='Seal identifier'),
        'segment_level': fields.String(required=False, description='Identification of the segment level in the movement of the pieces: contractual, flight leg, truck movement, etc.'),
        'transport_date': fields.String(required=False, description='Date associated with TransportIdentifier to uniquely identify the transport line , UTC format, e.g. "2030-03-20 01:31:12.467113"'),
        'transport_identifier': fields.String(required=False, description='Airline flight number, or rail /  truck / maritime line id'),
        'pieces':fields.List(fields.Nested(PieceDto.nested_piece), required=False, description='Optional list of related pieces, if empty no changes will be made, if filled the exact list will be imported and previous pieces removed. RPC actions have to be used to add or remove piece by piece')
    })
    new_transport_segment_no_pieces = api.model('newtransportsegment_nopieces', {
        'arrival_location_id': fields.String(required=False, description='Id of the arrival location, can be the local id or the hash of the unique constraints of the location'),
        'co2_calculation_method': fields.String(required=False, description='Method of calculation of the CO2 emissions'),
        'co2_emission_value': fields.Arbitrary(required=False, description='Amount of CO2 emitted, numeric value'),
        'co2_emission_unit': fields.String(required=False, description='Amount of CO2 emitted, unit of measure e.g. kg/km' ),
        'departure_location_id': fields.String(required=False, description='Id of the departure location, can be the local id or the hash of the unique constraints of the location'),
        'distance_calculated_value': fields.Arbitrary(required=False, description='Distance calculated if distance measured is not available, numeric value'),
        'distance_calculated_unit': fields.String(required=False, description='Distance calculated if distance measured is not available, unit of measure'),
        'distance_measured_value': fields.Arbitrary(required=False, description='Distance based on actually measured distance, numeric value'),
        'distance_measured_unit': fields.String(required=False, description='Distance based on actually measured distance, unit of measure'),
        'fuel_amount_calculated_value': fields.Arbitrary(required=False, description='Calculated fuel consumption, if measured not available, numeric value'),
        'fuel_amount_calculated_unit': fields.String(required=False, description='Calculated fuel consumption, if measured not available, unit of measure' ),
        'fuel_amount_measured_value': fields.Arbitrary(required=False, description='Actually measured fuel consumption, numeric value'),
        'fuel_amount_measured_unit': fields.String(required=False, description='Actually measured fuel consumption, unit of measure' ),
        'fuel_type': fields.String(required=False, description='e.g. Kerosene, Diesel, SAF, Electricity [renewable], Electricity [non-renewable]' ),
        'mode_code':  fields.String(required=False, description='Mode of transport code. Refer UNECE Recommendation N. 19 - Code for Modes of Transport,  e.g. 4 - Air transport' ),
        'movement_type': fields.String(required=False, description='Refers to the type of movement: Actual or Planned' ),
        'seal': fields.String(required=False, description='Seal identifier'),
        'segment_level': fields.String(required=False, description='Identification of the segment level in the movement of the pieces: contractual, flight leg, truck movement, etc.'),
        'transport_date': fields.String(required=False, description='Date associated with TransportIdentifier to uniquely identify the transport line , UTC format, e.g. "2030-03-20 01:31:12.467113"'),
        'transport_identifier': fields.String(required=False, description='Airline flight number, or rail /  truck / maritime line id')
    })
    new_transport_segment_for_subcontractor = api.model('newtransportsegmentforsubcontractor', {
        'supplier_id': fields.String(required=True, description="Id of the company which will supply this leg of the journey"),
        'new_transport_segment': fields.Nested(new_transport_segment_no_pieces, required=False, description='new transport segment for the subcontractor, this should not have pieces, which will be copied from the inbound quote request')
    })

class ShipmentDto:
    api = Namespace('shipment', description='shipment related operations')
    shipment = api.model('shipment', {
        'public_id': fields.String(required=True, description='public identifier of this object on this node'),
        'height_um': fields.String(required=False, description='Total height, unit of measure, e.g. m'),
        'height_value': fields.Arbitrary(required=False, description='Total height, numeric value, e.g. 1.1'),
        'length_um': fields.String(required=False, description='Total length, unit of measure, e.g. m'),
        'length_value': fields.Arbitrary(required=False, description='Total length, numeric value, e.g. 1.1'),
        'volume_um': fields.String(required=False, description='Total volume, unit of measure, e.g. m3'),
        'volume_value': fields.Arbitrary(required=False, description='Total volume, numeric value, e.g. 1.1'),
        'width_um': fields.String(required=False, description='Total height, unit of measure, e.g. m'),
        'width_value': fields.Arbitrary(required=False, description='Total height, numeric value, e.g. 1.1'),
        'external_reference':fields.String(required=False, description='Reference documents details' ),
        'goods_description':fields.String(required=False, description='General goods description' ),
        'insurance_details':fields.String(required=False, description='Insurance details' ),
        'total_gross_weight_um': fields.String(required=True, description='Total weight, unit of measure, e.g. Kg'),
        'total_gross_weight_value':  fields.Arbitrary(required=True, description='Total weight, numeric value, e.g. 150.45'),
        'total_slac': fields.Integer(required=False, description="Total SLAC of all piece groups " ),
        'volumetric_weight_conversion_factor_um':fields.String(required=True, description='Volumetric weight details, volume to weight conversion factor , unit of measure, e.g. Kg/m3'),
        'volumetric_weight_conversion_factor_value':fields.Arbitrary(required=True, description='Volumetric weight details, volume to weight conversion factor, value, e.g. 10.4'),
        'volumetric_weight_chargeable_weight_um':fields.String(required=True, description='Volumetric weight details, chargeable weight, unit of measure, e.g. Kg'),
        'volumetric_weight_chargeable_weight_value': fields.Arbitrary(required=True, description='Volumetric weight details, chargeable weight, value, e.g. 101.5'),
        'originator_pubkey': fields.String(required=True, description='PEM encoding of the pubkey of the originator - usually Brand Owner initiating the shipment'),
        'origin_hash': fields.String(required=False, description='Hash of the shipment and connected pieces and products at the time the first quote request was sent by the Brand Owner'),
        'signed_origin_hash': fields.String(required=False, description='signed origin hash by the key coresponding to the originator pubkey - used for validating the origin of this shipment'),
        'pieces': fields.List(fields.Nested(PieceDto.nested_piece), required=False, description='List of related pieces. RPC actions have to be used to add or remove piece by piece')
    })
    upd_shipment = api.model('upd_shipment', {
        'public_id': fields.String(required=True, description='public identifier of this object on this node'),
        'height_um': fields.String(required=False, description='Total height, unit of measure, e.g. m'),
        'height_value': fields.Arbitrary(required=False, description='Total height, numeric value, e.g. 1.1'),
        'length_um': fields.String(required=False, description='Total length, unit of measure, e.g. m'),
        'length_value': fields.Arbitrary(required=False, description='Total length, numeric value, e.g. 1.1'),
        'volume_um': fields.String(required=False, description='Total volume, unit of measure, e.g. m3'),
        'volume_value': fields.Arbitrary(required=False, description='Total volume, numeric value, e.g. 1.1'),
        'width_um': fields.String(required=False, description='Total height, unit of measure, e.g. m'),
        'width_value': fields.Arbitrary(required=False, description='Total height, numeric value, e.g. 1.1'),
        'external_reference':fields.String(required=False, description='Reference documents details' ),
        'goods_description':fields.String(required=False, description='General goods description' ),
        'insurance_details':fields.String(required=False, description='Insurance details' ),
        'total_gross_weight_um': fields.String(required=True, description='Total weight, unit of measure, e.g. Kg'),
        'total_gross_weight_value':  fields.Arbitrary(required=True, description='Total weight, numeric value, e.g. 150.45'),
        'total_slac': fields.Integer(required=False, description="Total SLAC of all piece groups " ),
        'volumetric_weight_conversion_factor_um':fields.String(required=True, description='Volumetric weight details, volume to weight conversion factor , unit of measure, e.g. Kg/m3'),
        'volumetric_weight_conversion_factor_value':fields.Arbitrary(required=True, description='Volumetric weight details, volume to weight conversion factor, value, e.g. 10.4'),
        'volumetric_weight_chargeable_weight_um':fields.String(required=True, description='Volumetric weight details, chargeable weight, unit of measure, e.g. Kg'),
        'volumetric_weight_chargeable_weight_value': fields.Arbitrary(required=True, description='Volumetric weight details, chargeable weight, value, e.g. 101.5'),
        'originator_pubkey': fields.String(required=True, description='PEM encoding of the pubkey of the originator - usually Brand Owner initiating the shipment'),
        'origin_hash': fields.String(required=False, description='Hash of the shipment and connected pieces and products at the time the first quote request was sent by the Brand Owner'),
        'signed_origin_hash': fields.String(required=False, description='signed origin hash by the key coresponding to the originator pubkey - used for validating the origin of this shipment')
    })
    new_shipment = api.model('new_shipment', {
        'height_um': fields.String(required=False, description='Total height, unit of measure, e.g. m'),
        'height_value': fields.Arbitrary(required=False, description='Total height, numeric value, e.g. 1.1'),
        'length_um': fields.String(required=False, description='Total length, unit of measure, e.g. m'),
        'length_value': fields.Arbitrary(required=False, description='Total length, numeric value, e.g. 1.1'),
        'volume_um': fields.String(required=False, description='Total volume, unit of measure, e.g. m3'),
        'volume_value': fields.Arbitrary(required=False, description='Total volume, numeric value, e.g. 1.1'),
        'width_um': fields.String(required=False, description='Total height, unit of measure, e.g. m'),
        'width_value': fields.Arbitrary(required=False, description='Total height, numeric value, e.g. 1.1'),
        'external_reference':fields.String(required=False, description='Reference documents details' ),
        'goods_description':fields.String(required=False, description='General goods description' ),
        'insurance_details':fields.String(required=False, description='Insurance details' ),
        'total_gross_weight_um': fields.String(required=True, description='Total weight, unit of measure, e.g. Kg'),
        'total_gross_weight_value':  fields.Arbitrary(required=True, description='Total weight, numeric value, e.g. 150.45'),
        'total_slac': fields.Integer(required=False, description="Total SLAC of all piece groups " ),
        'volumetric_weight_conversion_factor_um':fields.String(required=True, description='Volumetric weight details, volume to weight conversion factor , unit of measure, e.g. Kg/m3'),
        'volumetric_weight_conversion_factor_value':fields.Arbitrary(required=True, description='Volumetric weight details, volume to weight conversion factor, value, e.g. 10.4'),
        'volumetric_weight_chargeable_weight_um':fields.String(required=True, description='Volumetric weight details, chargeable weight, unit of measure, e.g. Kg'),
        'volumetric_weight_chargeable_weight_value': fields.Arbitrary(required=True, description='Volumetric weight details, chargeable weight, value, e.g. 101.5'),
        'originator_pubkey': fields.String(required=False, description='PEM encoding of the pubkey of the originator - usually Brand Owner initiating the shipment. When left empty the current node key is used.')
    })
    import_shipment = api.model('import_shipment', {
        'public_id': fields.String(required=True, description='public identifier of this object on the network'),
        'height_um': fields.String(required=False, description='Total height, unit of measure, e.g. m'),
        'height_value': fields.Arbitrary(required=False, description='Total height, numeric value, e.g. 1.1'),
        'length_um': fields.String(required=False, description='Total length, unit of measure, e.g. m'),
        'length_value': fields.Arbitrary(required=False, description='Total length, numeric value, e.g. 1.1'),
        'volume_um': fields.String(required=False, description='Total volume, unit of measure, e.g. m3'),
        'volume_value': fields.Arbitrary(required=False, description='Total volume, numeric value, e.g. 1.1'),
        'width_um': fields.String(required=False, description='Total height, unit of measure, e.g. m'),
        'width_value': fields.Arbitrary(required=False, description='Total height, numeric value, e.g. 1.1'),
        'external_reference':fields.String(required=False, description='Reference documents details' ),
        'goods_description':fields.String(required=False, description='General goods description' ),
        'insurance_details':fields.String(required=False, description='Insurance details' ),
        'total_gross_weight_um': fields.String(required=True, description='Total weight, unit of measure, e.g. Kg'),
        'total_gross_weight_value':  fields.Arbitrary(required=True, description='Total weight, numeric value, e.g. 150.45'),
        'total_slac': fields.Integer(required=False, description="Total SLAC of all piece groups " ),
        'volumetric_weight_conversion_factor_um':fields.String(required=True, description='Volumetric weight details, volume to weight conversion factor , unit of measure, e.g. Kg/m3'),
        'volumetric_weight_conversion_factor_value':fields.Arbitrary(required=True, description='Volumetric weight details, volume to weight conversion factor, value, e.g. 10.4'),
        'volumetric_weight_chargeable_weight_um':fields.String(required=True, description='Volumetric weight details, chargeable weight, unit of measure, e.g. Kg'),
        'volumetric_weight_chargeable_weight_value': fields.Arbitrary(required=True, description='Volumetric weight details, chargeable weight, value, e.g. 101.5'),
        #'hash': fields.String(required=True, description='the hash of the current data state for this shipment'),
        'originator_pubkey': fields.String(required=True, description='PEM encoding of the pubkey of the originator - usually Brand Owner initiating the shipment.'),
        'origin_hash': fields.String(required=False, description='Hash of the shipment and connected pieces and products at the time the first quote request was sent by the Brand Owner'),
        'signed_origin_hash': fields.String(required=False, description='signed origin hash by the key coresponding to the originator pubkey - used for validating the origin of this shipment'),
        'pieces':fields.List(fields.Nested(PieceDto.nested_piece), required=True, description='List of related pieces, during import there must be at least 1 piece. RPC actions have to be used to add or remove piece by piece')
    })

class EventDto:
    api = Namespace('event', description='event related operations')
    event = api.model('event', {
        'public_id': fields.String(required=True, description='public identifier of this object on this node'),
        'code': fields.String(required=False, description='Movement or milestone code. Refer CXML Code List 1.18, e.g. DEP, ARR, FOH, RCS, at least one between name and code must be present'),
        'name': fields.String(required=False, description='Name for the event, at least one between name and code must be present'),
        'latitude': fields.Arbitrary(required=False, description='Latitude, max 8 decimals, e.g. 13.1456455'),
        'longitude': fields.Arbitrary(required=False, description='Longitude, max 8 decimals,  e.g. 34.094562'),
        'recording_datetime': fields.String(required=True, description='Datetime when the event was recorded , UTC format, e.g. "2030-03-20 01:31:12.467113" - %Y-%m-%d %H:%M:%S.%f'),
        'sensor_reading_um': fields.String(required=False, description='Sensor reading, unit of measure, e.g. "C" for temperatures'),
        'sensor_reading_value': fields.Arbitrary(required=False, description='Sensor reading, numeric value, e.g. 150.45'),
        'type_indicator': fields.String(required=True, description='Tyep of event, must be one of: Actual, Expected, Estimated, Sensor'),
        'shipment_id':fields.String(required=True, description='Id of the related Shipment'),
    }) 
    new_event = api.model('new_event', {
        'code': fields.String(required=False, description='Movement or milestone code. Refer CXML Code List 1.18, e.g. DEP, ARR, FOH, RCS, at least one between name and code must be present'),
        'name': fields.String(required=False, description='Name for the event, at least one between name and code must be present'),
        'latitude': fields.Arbitrary(required=False, description='Latitude, max 8 decimals, e.g. 13.1456455'),
        'longitude': fields.Arbitrary(required=False, description='Longitude, max 8 decimals,  e.g. 34.094562'),
        'recording_datetime': fields.String(required=True, description='Datetime when the event was recorded , UTC format, e.g. "2030-03-20 01:31:12.467113" - %Y-%m-%d %H:%M:%S.%f'),
        'sensor_reading_um': fields.String(required=False, description='Sensor reading, unit of measure, e.g. "C" for temperatures'),
        'sensor_reading_value': fields.Arbitrary(required=False, description='Sensor reading, numeric value, e.g. 150.45'),
        'type_indicator': fields.String(required=True, description='Type of event, must be one of: Actual, Expected, Estimated, Sensor'),
        'shipment_id':fields.String(required=True, description='Id of the related Shipment'),
    }) 
    paged_event_list = api.model("paged_event_list",{
        'page': fields.Integer(description='Page number in the paged list'),
        'page_size': fields.Integer(description='Records per page'),
        'total_record_count': fields.Integer(description='Total records in the list'),
        'records':fields.List(fields.Nested(event)),
    })

class QuoteRequestDto:
    api = Namespace('quoterequest', description='quote request related operations')
    quote_request = api.model('quote_request', {
        'public_id': fields.String(required=True, description='public identifier of this object on the network'),
        'allotment': fields.String(required=False, description='Reference to the Allotment as per the contracts between forwarders and carriers'),
        'rating_preferences': fields.String(required=False, description='Ratings preferences of the request'),
        'requester_company_id': fields.String(required=False, description='Public id of the company requesting the quote'),
        'routing_preference': fields.String(required=False, description='Routing details that are part of the request, these details will be used to determine if the offer is a perfect match'),
        'security_status': fields.String(required=False, description='Indicate the secruty state of the shipment, screened or not'),
        'shipment_id': fields.String(required=True, description='Public id of the shipement that is to be shipped'),
        'supplier_company_id': fields.String(required=False, description='Public id of the company providing the quote'),
        'transport_segment_id': fields.String(required=True, description='Transport segment linked to the request, including the Departure and Arrival locations requested'),
        'units_preference': fields.String(required=False, description='Unit preferences of the request (e.g. kg or cm) '),
        'latest_log': fields.String(required=False, description='Result of the last p2p operation'),
    })
    new_quote_request = api.model('new_quote_request', {
        'allotment': fields.String(required=False, description='Reference to the Allotment as per the contracts between forwarders and carriers'),
        'rating_preferences': fields.String(required=False, description='Ratings preferences of the request'),
        'requester_company_id': fields.String(required=False, description='Public id of the company requesting the quote. IMPORTANT: if empty it defaults to the current node!'),
        'routing_preference': fields.String(required=False, description='Routing details that are part of the request, these details will be used to determine if the offer is a perfect match'),
        'security_status': fields.String(required=False, description='Indicate the secruty state of the shipment, screened or not'),
        'shipment_id': fields.String(required=True, description='Public id of the shipement that is to be shipped'),
        'supplier_company_id': fields.String(required=False, description='Public id of the company providing the quote'),
        'transport_segment_id': fields.String(required=True, description='Transport segment linked to the request, including the Departure and Arrival locations requested'),
        'units_preference': fields.String(required=False, description='Unit preferences of the request (e.g. kg or cm) ')
    })
    full_quote_request = api.model('full_quote_request', {
        'public_id': fields.String(required=True, description='public identifier of this object on the network'),
        'allotment': fields.String(required=False, description='Reference to the Allotment as per the contracts between forwarders and carriers'),
        'rating_preferences': fields.String(required=False, description='Ratings preferences of the request'),
        'requester_company_id': fields.String(required=False, description='Public id of the company requesting the quote'),
        'routing_preference': fields.String(required=False, description='Routing details that are part of the request, these details will be used to determine if the offer is a perfect match'),
        'security_status': fields.String(required=False, description='Indicate the secruty state of the shipment, screened or not'),
        'shipment': fields.Nested(ShipmentDto.import_shipment, required=True, description='Shipement object that contains all the information of the shipment to be quoted'),
        'supplier_company_id': fields.String(required=False, description='Public id of the company providing the quote'),
        'transport_segment_id': fields.String(required=True, description='Transport segment linked to the request, the id indicates the segment with departure and arrival locations which is included in the shipment object.'),
        'units_preference': fields.String(required=False, description='Unit preferences of the request (e.g. kg or cm) ')
    })
    signed_full_quote_request = api.model('signed_quote_request', {
        'payload':fields.Nested(full_quote_request, required=True, description="full quote request object"),
        "signed_hash": fields.String(required=True, description="Hex representation of the signed bytes: hash the payload (the full quote request object) and sign it with the key of the sender (aka requester) node. "),
        "requester_company_seralised_public_key": fields.String(required="True", description="serialized public key used for the signature, decoded in utf-8")
    })
    method_result = api.model('method_result', {
        'method': fields.String(required=True, description='The called method'),
        'result': fields.String(required=True, description='The result'),
    })

class OfferDto:
    api = Namespace('offer', description='offer related operations')
    offer = api.model('offer', {
        'public_id': fields.String(required=True, description='public identifier of this object on the network'),
        'allotment': fields.String(required=False, description='Reference to the Allotment as per the contracts between forwarders and carriers'),
        'price_value': fields.Arbitrary(required=True, description='Total price of this offer, numeric value, e.g. 1999.99'),
        'price_um': fields.String(required=True, description="Currency ISO code: EUR, USD, BTC, ..."),
        'quote_request_id': fields.String(required=True, description='Public id of the quote request addressed by this offer'),
        'rating': fields.String(required=False, description='Ratings preferences of the request'),
        'routing': fields.String(required=False, description='Routing details that are part of the request, these details will be used to determine if the offer is a perfect match'),
        'security_status': fields.String(required=False, description='Indicate the secruty state of the shipment, screened or not'),
        'units_preference': fields.String(required=False, description='Unit preferences of the request (e.g. kg or cm) ')
    })
    new_offer = api.model('new_offer', {
        'allotment': fields.String(required=False, description='Reference to the Allotment as per the contracts between forwarders and carriers'),
        'dangerous_goods': fields.String(required=False, description='Description of dangerous goods, will become a separate object'),
        'price_value': fields.Arbitrary(required=True, description='Total price of this offer, numeric value, e.g. 1999.99'),
        'price_um': fields.String(required=True, description="Currency ISO code: EUR, USD, BTC, ..."),
        'quote_request_id': fields.String(required=True, description='Public id of the quote request addressed by this offer'),
        'rating': fields.String(required=False, description='Ratings preferences of the request'),
        'routing': fields.String(required=False, description='Routing details that are part of the request, these details will be used to determine if the offer is a perfect match'),
        'security_status': fields.String(required=False, description='Indicate the secruty state of the shipment, screened or not'),
        'units_preference': fields.String(required=False, description='Unit preferences of the request (e.g. kg or cm) ')
    })
    signed_full_offer = api.model('signed_offer', {
        'payload':fields.Nested(offer, required=True, description="offer object"),
        "signed_hash": fields.String(required=True, description="Hex representation of the signed bytes: hash the payload (the offer object) and sign it with the key of the sender (aka supplier) node. "),
        "supplier_company_seralised_public_key": fields.String(required="True", description="serialized public key used for the signature, decoded in utf-8")
    })
    accepted_full_offer = api.model('accepted_offer', {
        'payload':fields.Nested(offer, required=True, description="offer object"),
        "signed_hash": fields.String(required=True, description="Hex representation of the signed bytes: hash the payload (the offer object) and sign it with the key of the sender (aka supplier) node. "),
        "accepting_company_seralised_public_key": fields.String(required="True", description="serialized public key used for the signature, decoded in utf-8. This represents the company which requested and accepts the offer")
    })
    method_result = api.model('method_result', {
        'method': fields.String(required=True, description='The called method'),
        'result': fields.String(required=True, description='The result'),
    })

class BookingDto:
    api = Namespace('booking', description='booking related operations')
    booking = api.model('booking', {
        'public_id': fields.String(required=True, description='public identifier of this object on the network'),
        'price_value': fields.Arbitrary(required=True, description='Total price of this booking, numeric value, e.g. 1999.99'),
        'price_um': fields.String(required=True, description="Currency ISO code: EUR, USD, BTC, ..."),
        'offer_id': fields.String(required=True, description='Public id of the offer addressed by this booking'),
        'routing': fields.String(required=False, description='Routing details that are part of the request, these details will be used to determine if the offer is a perfect match'),
        'security_status': fields.String(required=False, description='Indicate the secruty state of the shipment, screened or not'),
    })
    new_booking = api.model('new_booking', {
        'price_value': fields.Arbitrary(required=True, description='Total price of this booking, numeric value, e.g. 1999.99'),
        'price_um': fields.String(required=True, description="Currency ISO code: EUR, USD, BTC, ..."),
        'offer_id': fields.String(required=True, description='Public id of the offer addressed by this booking'),
        'routing': fields.String(required=False, description='Routing details that are part of the request, these details will be used to determine if the offer is a perfect match'),
        'security_status': fields.String(required=False, description='Indicate the secruty state of the shipment, screened or not'),
    })
    signed_booking = api.model('signed_booking', {
        'payload':fields.Nested(booking, required=True, description="booking object"),
        "signed_hash": fields.String(required=True, description="Hex representation of the signed bytes: hash the payload (the booking object) and sign it with the key of the sender (aka supplier) node. "),
        "supplier_company_seralised_public_key": fields.String(required="True", description="serialized public key used for the signature, decoded in utf-8")
    })
    method_result = api.model('method_result', {
        'method': fields.String(required=True, description='The called method'),
        'result': fields.String(required=True, description='The result'),
    })

class WaybillDto:
    api = Namespace('waybill', description='waybill related operations')
    waybill = api.model('waybill', {
        'public_id': fields.String(required=True, description='public identifier of this object on this node'),
        'name': fields.String(required=True, description='Waybill document number'),
        'export_reference': fields.String(required=True, description='other reference for this waybill, usually invoice number'),
        'created_on': fields.Date(required=True, description='creation date of the waybill within this node'),
        'shipment_id': fields.String(required=True, description='the local public id of the shipment the waybill refers to'),
        'waybill_type': fields.String(required=True, description='the type of waybill, eAWB FWD, house waybill, bill of lading, etc..'),
        'waybill_file_type': fields.String(required=False, description='the type of file, xml, pdf, etc.., use: none if there is no file yet, just the name and reference'),
        'base64encodedfilebody': fields.String(required=False, description='base64 string of the file, for now are not accepting file chunks, so the file should be roughly no more than 1Mb')        
    })
    new_waybill = api.model('new_waybill', {
        'name': fields.String(required=True, description='Waybill document number'),
        'export_reference': fields.String(required=False, description='other reference for this waybill, usually invoice number'),
        'waybill_type': fields.String(required=True, description='the type of waybill, eAWB FWD, house waybill, bill of lading, etc.., use: none if there is no file yet, just the name and reference'),
        'waybill_file_type': fields.String(required=False, description='the type of file, xml, pdf, etc..'),
        'base64encodedfilebody': fields.String(required=False, description='base64 string of the file, for now are not accepting file chunks, so the file should be roughly no more than 1Mb'),
        'shipment_id': fields.String(required=True, description='the local public id of the shipment the waybill refers to'),
    })
    

class DisclosureDto:
    api = Namespace('disclosure', description='Shipment disclosure related operations')
    disclosure = api.model('disclosure', {
        'created_on': fields.Date(required=False, description='creation date of the waybill within this node'),
        'user_id': fields.String(required=True, description='the local public id of the company who submitted this waybill'),
        'shipment_id': fields.String(required=True, description='the local public id of the shipment the waybill refers to'),
        'approved': fields.Boolean(required=False, description='Stackable indicator for the pieces (boolean as text: true / false)'),
    })
    new_disclosure = api.model('new_disclosure', {
        'user_id': fields.String(required=True, description='the local public id of the company who submitted this waybill'),
        'shipment_id': fields.String(required=True, description='the local public id of the shipment the waybill refers to'),
    })



class MethodResultDto:
    api = Namespace('method_result', description='special methods rpc-like')
    method_result = api.model('method_result', {
        'method': fields.String(required=True, description='The called method'),
        'result': fields.String(required=True, description='The result'),
    })
