import sys
import time

from flask import current_app
from app.main.celery import celery
from celery.utils.log import get_task_logger
from app.main.services import db
import requests, json
from app.main.model.company import Company
from app.main.model.offer import Offer
#since tasks are imported into services, tasks cannot import from services, we have a separate file "jobs" for working the callbacks
from app.main.job.offer_job import work_offer_acceptance_response, work_offer_acceptance_excpetion, work_send_offer_excpetion, work_send_offer_response
from app.main.job.booking_job import work_booking_import_response, work_booking_import_exception
from app.main.job.waybill_job import work_waybill_import_response, work_waybill_import_exception
from app.main.job.company_job import work_validate_company_response, work_validate_company_excpetion
from app.main.job.quote_request_job import work_send_quoterequest_response, work_send_quoterequest_excpetion

logger = get_task_logger(__name__)

def _make_gossip_call(*args):
    """ 
    Exectue the desired HTTP method on the url 

    the headers are optional, the response is returned
    although it may be ignored if when calling the task
    the ignore_result is set

    Parameters
    ----------
    args[0]: str
        the method, can be 'get','post','put'

    args[1]: str
        base_url, must have the complete url from http... 

    args[2]: dict
        the JSON to be sent as payload for put and post requests

    args[3]: dict
        the headers to be added 

    Response
    --------
    Reponse() object
        python Response() object with status_code and json() methods available for instance

    Raises
    ------
    HTTPError or Exception 
        if something wrong happens during the call

    """
    method = args[0]
    url = args[1]
    payload = {} if (len(args)<=2) else args[2]
    headers = {} if (len(args)<=3) else args[3]
    verify = True 
    try:
        verify = current_app.config['VERIFY_SSL_CERTS'] if current_app.config['VERIFY_SSL_CERTS'] else False
    except Exception as e:
        print(e) #but continue with default=False
    try:
        if(method=='get'):
            return requests.get(url,headers=headers, verify=verify)
        elif(method=='post'):
            r = requests.post(url,data=payload,headers=headers, verify=verify)
            return r
        elif(method=='put'):
            return requests.put(url,data=payload,headers=headers, verify=verify)
        else:
            return
    except Exception as general_exception:
        raise general_exception

@celery.task()
def validate_new_company(base_url, public_id):
    """ 
    GET the owner data and compare it against the local copy, if all checks out ACK, else destroy local company

    the method calls the remote system for its owner data, then compares it against the
    local public_id of the company that has been created with the initial POST.
    Empty data is filled, but if an incongruence is found the company is destroyed

    Parameters
    ----------
    base_url: str
        the endpoint to query must have the complete url from http... to /owner
    public_id: str
        the local id of the company 

    Response
    --------
    str
        operation log, a string describing what happened, it's useful only for debug purposes and for making clarity in the logs when celery runs within docker

    Raises
    ------

    """

    #we turn this down during test since company registration is a common task in all tests and waiting for the timeouts everytime is long and useless
    #is_test_running = 'unittest' in sys.modules;
    #if(is_test_running):
    #    return;

    url = _fix_base_url(base_url)
    url+='company/node-owner'
    try:
        response = _make_gossip_call("get", url) #this is a Response object: status_code json(), if it blows up it raises exception
        #with the response we can do the update of the local db
        return work_validate_company_response(response, public_id)
    except Exception as e:
        #report that somethign went wrong with this call
        print(e)
        return work_validate_company_excpetion(e, public_id)

@celery.task()
def create_user_for_company(base_url, payload, company_id):
    """ 
    POST the data to create a user for local company_id on its respective node

    the method calls the remote system with the data to create a user, the remote node will
    use the public key to udnerstand which company is requesting access and to validate it's authentic

    Parameters
    ----------
    base_url: str
        the endpoint to query must have the complete url from http... to /
    payload: dict
        the payload to use as body of the request
    public_id: str
        the local id of the company 

    Response
    --------
    str
        operation log, a string describing what happened, it's useful only for debug purposes and for making clarity in the logs when celery runs within docker

    Raises
    ------

    """

    #we turn this down during test since company registration is a common task in all tests and waiting for the timeouts everytime is long and useless
    #is_test_running = 'unittest' in sys.modules;
    #if(is_test_running):
    #    return;

    url = _fix_base_url(base_url)
    url+='company/node-owner/rpc/createuser'
    headers = {"Content-Type":"application/json", "accept":"application/json"}
    try:
        response = _make_gossip_call("post", url, payload, headers) #this is a Response object: status_code json(), if it blows up it raises exception
        #with the response we can do the update of the local db
        #return work_create_user_for_company_response(response, public_id)
    except Exception as e:
        #report that somethign went wrong with this call
        print(e)
        #return work_create_user_for_company_excpetion(e, public_id)


@celery.task()
def send_quote_request(base_url, payload):
    """
    POST the full quote payload to the remote import endpoint


    Parameters
    ----------
    base_url: str
        the endpoint to call, must start with https://...
    payload: dict
        the payload to use as body of the request

    Response
    --------
    str
        operation log, a string describing what happened, it's useful only for debug purposes and for making clarity in the logs when celery runs within docker

    Raises
    ------

    """
    #is_test_running = 'unittest' in sys.modules;
    #if(is_test_running):
    #    return;

    url=_fix_base_url(base_url)
    url+='quoterequest/rpc/import'
    headers = {"Content-Type":"application/json", "accept":"application/json", "Authorization":"demo"}
    public_id = payload["payload"]["public_id"]
    payload = json.dumps(payload)
    try:
        response = _make_gossip_call('post', url, payload, headers)  #this is a Response object: status_code json(), if it blows up it raises exception
        #with the response we can do the update of the local db
        return work_send_quoterequest_response(response, public_id)
    except Exception as e:
        #report that somethign went wrong with this call
        print(e)
        return work_send_quoterequest_excpetion(e, public_id)

    


@celery.task()
def send_offer(base_url, payload):
    """
    POST the full quote payload to the remote import endpoint


    Parameters
    ----------
    base_url: str
        the endpoint to call, must start with https://...
    payload: dict
        the payload to use as body of the request

    Response
    --------
    str
        operation log, a string describing what happened, it's useful only for debug purposes and for making clarity in the logs when celery runs within docker

    Raises
    ------

    """
    url=_fix_base_url(base_url)
    url+='offer/rpc/import'
    headers = {"Content-Type":"application/json", "accept":"application/json"}
    public_id = payload["payload"]["public_id"]
    payload = json.dumps(payload)
    try:
        response = _make_gossip_call('post', url, payload, headers)  #this is a Response object: status_code json(), if it blows up it raises exception
        #with the response we can do the update of the local db
        return work_send_offer_response(response, public_id)
    except Exception as e:
        #report that somethign went wrong with this call
        print(e)
        return work_send_offer_excpetion(e, public_id)


@celery.task()
def send_offer_acceptance(target_url, acceptance_payload):
    """
    POST the accepted offer the the node of the offer provider

    Parameters
    ----------
    base_url: str
        the endpoint to call, must start with https://...
    acceptance_payload: dict
        the payload to use as body of the request

    Response
    --------
    str
        operation log, a string describing what happened, it's useful only for debug purposes and for making clarity in the logs when celery runs within docker

    Raises
    ------
    Exception catched here and worked with a job
    """
    url = _fix_base_url(target_url)
    url+='offer/'+acceptance_payload["payload"]["public_id"]+'/rpc/registeracceptance' 
    headers = {"Content-Type":"application/json", "accept":"application/json"}
    payload = json.dumps(acceptance_payload)
    try:
        response = _make_gossip_call('post', url, payload, headers) #this is a Response object: status_code json(), if it blows up it raises exception
        #with the response we can do the update of the local db
        return work_offer_acceptance_response(response, acceptance_payload["payload"]["public_id"])
    except Exception as e:
        #report that somethign went wrong with this call
        print(e)
        return work_offer_acceptance_excpetion(e, acceptance_payload["payload"]["public_id"])


@celery.task()
def send_booking(base_url, payload):
    """
    POST the booking payload to the remote import endpoint

    Parameters
    ----------
    base_url: str
        the endpoint to call, must start with https://...
    payload: dict
        the payload to use as body of the request

    Response
    --------
    str
        operation log, a string describing what happened, it's useful only for debug purposes and for making clarity in the logs when celery runs within docker

    Raises
    ------
    Exception catched here and worked with a job

    """
    url=_fix_base_url(base_url)
    url+='booking/rpc/import'
    headers = {"Content-Type":"application/json", "accept":"application/json"}
    import_payload = json.dumps(payload)
    try:
        response = _make_gossip_call('post', url, import_payload, headers) 
        return work_booking_import_response(response, payload["payload"]["public_id"])
    except Exception as e:
        print(e)
        return work_booking_import_exception(e, payload["payload"]["public_id"])

@celery.task()
def send_waybill(base_url, payload):
    """
    POST the waybill payload to the remote import endpoint

    Parameters
    ----------
    base_url: str
        the endpoint to call, must start with https://...
    payload: dict
        the payload to use as body of the request

    Response
    --------
    str
        operation log, a string describing what happened, it's useful only for debug purposes and for making clarity in the logs when celery runs within docker

    Raises
    ------
    Exception catched here and worked with a job

    """
    url=_fix_base_url(base_url)
    url+='waybill/rpc/import'
    headers = {"Content-Type":"application/json", "accept":"application/json"}
    import_payload = json.dumps(payload)
    try:
        response = _make_gossip_call('post', url, import_payload, headers) 
        return work_waybill_import_response(response, payload["payload"]["public_id"])
    except Exception as e:
        print(e)
        return work_waybill_import_exception(e, payload["payload"]["public_id"])

def _fix_base_url(base_url):
    """
    be sure you have the trailing slash for adding new tokens to the URL
    """
    return base_url if base_url[-1]=='/' else base_url+'/'

