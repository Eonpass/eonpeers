from os import path

import json

from app.main.util.keymanagementutils import kmc
from app.main.util.hashutils import hu
from cryptography.exceptions import InvalidSignature 
from app.main.util.eonerror import EonError

advanced_libs = False
try:
    from app.main.util.ebsi.elsaverifier import ElsaVerifier, ElsaVerifierBuilder
    advanced_libs = True
except ImportError as e:
    #this just means there's no special folder and no advanced modules
    advanced_libs = False


"""
This class manages the creation of the requested verifier

A verifier checks payloads, the STD type just inspects signature, advanced types can do custom functions like 
resolving DIDs and checking VCs.

"""
class VerifierFactory:
    def __init__(self):
        self._builders = {}

    def register_builder(self, key, builder):
        self._builders[key] = builder

    def create(self, key, **kwargs):
        builder = self._builders.get(key)
        if not builder:
            if key == 'STD':
                builder = StandardVerifierBuilder(**kwargs)
            elif key == 'ELSA' and advanced_libs:
                builder = ElsaVerifierBuilder(**kwargs)
            else:
                raise Exception('Wrong or unsupported verifier type')
            #lazy build of the builder registry
            self.register_builder(key, builder)
        return builder(**kwargs)


class StandardVerifierBuilder:
    def __init__(self, **_ignored):
        #TODO: **ignored params will be useful to decide which builder to build, standard is default, "euipo" for the advanced VC verification, etc..
        self._instance = None

    def __call__(self, **ignored):
        if not self._instance:
            self._instance = StandardVerifier()
        return self._instance


class StandardVerifier:
    #basic verifier, will just check signatures against public keys, doesn't check external VC, etc..
    _type = 'STD'
    def __init__(self):
        try:
            #test KMC is working as expected
            kmc.get_serialized_pub_key()
        except Exception as e:
            print(e)
            raise Exception('Something wrong with Std Verifier Init')


    def verify_payload(self, *args, **kwargs):
        """
        Entry point for verification

        Check what object_to_verify is requested and then call the function accordingly

        """
        if kwargs and 'object_to_verify' in kwargs:
            if kwargs.get('object_to_verify') == "quote":
                #this is a quote, extract fields
                import_payload = kwargs.get("payload")
                signed_bytes = bytes.fromhex(import_payload["signed_hash"])
                #message = compute the hash of payload
                stringyfied_json = json.dumps(import_payload["payload"], sort_keys=True)
                hexed = hu.digest(stringyfied_json).hex()
                message = bytes.fromhex(hexed)
                #serialized public should already be fine
                try:
                    #in the future different verifier types may require different args..
                    #verifier_type = current_app.config['VERIFIER_TYPE']
                    #if verifier_type == 'STD'
                    signature_checks_out = self.verify_signed_message(signed_bytes, message, import_payload["requester_company_seralised_public_key"])
                    if(not signature_checks_out):
                        raise EonError("Overall payload signed message is corrupt", 409)        
                except Exception as e:
                    print(e)
                    raise EonError("Overall payload signed message is corrupt", 409) 
            elif kwargs.get('object_to_verify') == "shipment":
                #this is a shipment
                shipment_api_object = kwargs.get("payload")
                signed_bytes = bytes.fromhex(shipment_api_object["signed_origin_hash"])
                original_message = shipment_api_object["origin_hash"]
                shipment_api_object.pop("transport_segments", None)
                shipment_api_object.pop("origin_hash", None)
                shipment_api_object.pop("signed_origin_hash", None)
                for piece in shipment_api_object["pieces"]:
                    piece.pop('transport_segments', None)
                message = (hu.digest(json.dumps(shipment_api_object, sort_keys=True)).hex())
                #check the origin_hash is actually describing the shiupment
                if(not message == original_message):
                    raise EonError("Shipment Payload is corrupt", 409) 
                #check the origin signature
                try:
                    #in the future different verifier types may require different args..
                    #verifier_type = current_app.config['VERIFIER_TYPE']
                    #if verifier_type == 'STD'
                    #TODO: special verifiers may match the originator_pubkey against a list of known Brand Owners keys..
                    signature_checks_out = self.verify_signed_message(signed_bytes, bytes.fromhex(message), shipment_api_object["originator_pubkey"])
                    if(not signature_checks_out):
                        raise EonError("Shipment origin signed message is corrupt", 409)       
                except Exception as e:
                    print(e)
                    raise EonError("Shipment Payload is corrupt, cannot verify signature", 409) 
            else:
                self.verify_signed_message(*args, **kwargs)
        else:
            return self.verify_signed_message(*args, **kwargs)


    def verify_signed_message(self, *args, **kwargs):
        """
        verify an imported payload from another node

        payloads in general are very similar to signed message verification, they have a signed signed, a message and a key.
        The standard verifier just checks integrity, other verifiers may do other things (connect to external services, download VCs, etc..)
        Other verifiers may use more than 3 arguments

        Parameters
        ----------
        args[0] signed: bytes
            the signed version

        args[1] message: str or bytes
            the original message that was signed

        args[2] serialized_public: str (utf-8 decoded) or bytes
            the public key in PEM encoding

        Returns
        --------
        bool
            True if the signed message check out, False otherwise.
            Note that anything going wrong is catched and 
            the method just returns False
        """
        try:
            signed_bytes = args[0]
            message = args[1]
            serialized_public = args[2]
            return kmc.verify_signed_message(signed_bytes, message, serialized_public)
        except ValueError as ve:
            print("value error", ve)
            return False
        except InvalidSignature as invalid:
            print("invalid", invalid)
            return False
        except Exception as e:
            print("general exc", e)
            return False

    def resolve_did(self, *args, **kwargs):
        """
        get details from a did, in particular we should retrieve the public key

        Parameters (STD)
        ----------
        args[0] did: str
            the did in the form did:ebsi:.....

        Returns
        -------
        str 
            The corresponding public key
        """
        return 'unknown'

