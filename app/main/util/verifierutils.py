
from flask import current_app

from app.main.util.verifierfactory import VerifierFactory

class Singleton(object):
    _instance = None  # Keep instance reference

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = object.__new__(cls, *args, **kwargs)
        return cls._instance


class Verifier(Singleton):
    #on creation, get the enviornment type and call the factory accordingly
    _verifier = None

    def __init__(self):
        return

    def _get_verifier(self):
        """
        Get the verifier depending on the app configuration
        default is "STD", standard verifier which just checks the signatures

        TODO: future example could be "EUIPO" for using the EUIPO specific VC system
        """
        if not self._verifier:
            factory = VerifierFactory()
            #force default if missing
            verifier_type = current_app.config['VERIFIER_TYPE'] if current_app.config.get('VERIFIER_TYPE', None) else 'STD'
            self._verifier = factory.create(verifier_type)
        return self._verifier

    def verify_payload(self, *args, **kwargs):
        """
        check if a payload is authentic, depending on the verifier type different checks are done
        STD: just check the coherence of signatures
        EUIPO: use the VC system 

        Parameters (STD)
        ----------
        args[0] signed: bytes
            the signed version

        args[1] message: str or bytes
            the original message that was signed, it will be encoded if string

        args[2] serialized_public: str
            the public kley

        Returns
        --------
        bool
            True if the signed message check out, False otherwise.
            Note that anything going wrong is catched and 
            the method just returns False
        """
        return self._get_verifier().verify_payload(*args, **kwargs)

    def resolve_did(self, *args, **kwargs):
        """
        get details from a did, in particular we should retrieve the public key

        Parameters (STD)
        ----------
        args[0] did: str
            the did in the form did:ebsi:.....

        Returns
        -------
        str 
            The corresponding public key
        """
        return self._get_verifier().resolve_did(*args, **kwargs)

verifier = Verifier()
