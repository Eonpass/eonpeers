import datetime
import json
import unittest
import uuid
import copy

from flask_restplus import marshal

from app.main.services import db
from app.main.model.quoterequest import QuoteRequest
from app.main.model.offer import Offer
from app.main.model.piece import Piece
from app.main.model.company import Company
from app.main.model.booking import Booking
from app.main.model.shipment import Shipment
from app.main.model.transportsegment import TransportSegment
from app.main.model.dangerousgood import DangerousGood
from app.main.model.customsinfo import CustomsInfo
from app.main.service.company_service import mark_company_as_node_owner, get_a_company, save_new_company
from app.main.service.shipment_service import save_new_shipment
from app.main.service.piece_service import save_new_piece
from app.main.service.location_service import save_new_location
from app.main.service.offer_service import save_new_offer
from app.main.service.transportsegment_service import save_new_transport_segment
from app.main.service.quoterequest_service import (save_new_quote_request, get_a_quote_request, _check_quote_request, export_quote_request, _prepare_export_payload, import_quote_request)
from app.main.util.keymanagementutils import KeyManagementClient
from app.main.util.hashutils import HashUtils
from app.test.controller_test_utils import (register_company, register_location, register_shipment, register_piece_with_shipment, update_piece_with_shipment, register_planned_transport_segment, register_quote_request, update_quote_request, get_a_quote_request_via_api, register_dangerousgood, register_customsinfo)
from app.test.base import BaseTestCase
from app.test.data.eonpeers_payloads import qr_payload, qr_payload_2, qr_payload_3, qr_payload_error
from app.main.job.quote_request_job import work_send_quoterequest_response, work_send_quoterequest_excpetion
from app.main.util.eonerror import EonError
from app.main.util.dto import QuoteRequestDto, PieceDto, OfferDto, ShipmentDto, BookingDto
from app.main.util.hashutils import hu
from app.main.util.keymanagementutils import kmc


from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.serialization import load_pem_private_key
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.exceptions import InvalidSignature 

def create_qr_from_localKey_to_keyB(key_b):
    ##SETUP COMPANIES:
    company_a_res = save_new_company({"name":"AAA","fiscal_country":"ES","vat_number":"012943852348","base_url":"http://localhost:5400"})
    company_id = company_a_res[0]['public_id']
    mark_company_as_node_owner(company_id)
    pub_key_b = key_b.public_key()
    serialized_pub_key_b = pub_key_b.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    ).decode('utf-8')
    company_b_res = save_new_company({"name":"BBB","fiscal_country":"DE","vat_number":"835476934934","public_key":serialized_pub_key_b, "base_url":"http://localhost:5401"})
    company_b_id = company_b_res[0]['public_id']

    shipment = dict(
        height_um="m",
        height_value=1,
        volume_um="m3",
        volume_value=1,
        goods_description="some goods",
        total_gross_weight_um="Kg",
        total_gross_weight_value=100.5,
        total_slac=2,
        volumetric_weight_conversion_factor_um="Kg/Kg",
        volumetric_weight_conversion_factor_value=1.1,
        volumetric_weight_chargeable_weight_um="Kg",
        volumetric_weight_chargeable_weight_value=110.55
    )    
    shipment_res = save_new_shipment(shipment)
    shipment_id = shipment_res[0]['public_id']

    piece = dict(
        shipment_id=shipment_id,
        additional_security_info="ASD",
        coload=False,
        goods_description="goods with NFTs",
        gross_weight_value=123.12,
        commodity_type="123",
        gross_weight_um="Kg",
        load_type= "X",
        slac = 2,
        stackable = True,
        turnable = True,
        volumetric_weight_conversion_factor_um = "Kgm3",
        volumetric_weight_conversion_factor_value = 123.3,
        volumetric_weight_chargeable_weight_um = "Kg",
        volumetric_weight_chargeable_weight_value = 11.4
    )
    piece_res = save_new_piece(piece)
    piece_id = piece_res[0]["public_id"]


    #setup transport segment
    loc_a = dict(
            name="JFK A",
            location_code="JFK A",
            location_type="Airport",
            city_code = 'JFK',
            city_name= 'NY',
            country= 'USA',
            postal_code='21010',
            street = 'JSK Int Airport',
        )
    loc_b = dict(
            name="MXP A",
            location_code="MXP A",
            location_type="Airport",
            city_code = 'MXP',
            city_name= 'Ferno',
            country= 'IT',
            postal_code='21010',
            street = 'Aeroporto di Milano Malpensa',
        )
    loc_a_res = save_new_location(loc_a)
    loc_a_id = loc_a_res[0]["public_id"]
    loc_b_res = save_new_location(loc_b)
    loc_b_id = loc_b_res[0]["public_id"]
    now = datetime.datetime.utcnow()
    date_string = now.strftime("%Y-%m-%d %H:%M:%S.%f")

    ts = dict(
        co2_calculation_method="ballpark",
        co2_emission_value=11.34,
        co2_emission_unit="Kg/m3",
        fuel_type="Kerosene",
        mode_code="Air Transport",
        movement_type="planned",
        seal="1234",
        segment_level="flight leg",
        arrival_location_id=loc_a_id,
        departure_location_id=loc_b_id,
        pieces=[{"public_id":piece_id}],
        transport_date=date_string
    )
    ts_res = save_new_transport_segment(ts)
    ts_id = ts_res[0]["public_id"]

    #def register_quote_request(self, shipment_id, transport_segment_id, supplier_id, requester_id):
    qr = dict(
        allotment="m",
        rating_preferences="1e per km",
        routing_preference="via Istanbul",
        secutiry_status="n.a.",
        units_preference="Kg",
        shipment_id=shipment_id,
        transport_segment_id=ts_id,
        supplier_company_id=company_b_id,
        requester_company_id=company_id
    )   

    quote_request_res = save_new_quote_request(qr)
    quote_request_id = quote_request_res[0]["public_id"]
    qr = QuoteRequest.query.filter_by(public_id=quote_request_id).first()  
    #use unique hash for referincing companies:
    #prodcuts also have company ids in them, use hashes;
    payload = marshal(qr,QuoteRequestDto.full_quote_request) 
    #fix payload, not qr to avoid strange DB behavior (auto-commit?)
    payload["requester_company_id"] = get_a_company(qr.requester_company_id).unique_hash
    payload["supplier_company_id"] = get_a_company(qr.supplier_company_id).unique_hash
    for piece in payload["shipment"]["pieces"]:
        for prod in piece["products"]:
            prod["brand_owner_company_id"] = get_a_company(prod["brand_owner_company_id"]).unique_hash if get_a_company(prod["brand_owner_company_id"]) else ""
            prod["manufacturer_company_id"] = get_a_company(prod["manufacturer_company_id"]).unique_hash if get_a_company(prod["manufacturer_company_id"]) else ""    

    #get the payload and sign it
    db.session.rollback() #if we don't do this, qr is persisted and the test goes on thinking qr has a weird supplier id (which is normal if we import from another node, but not here where we are on the same node)
    payload_to_sign = bytes.fromhex(hu.digest(json.dumps(payload, sort_keys=True)).hex()) ##sign with the given key
    signed_payload_hash = kmc.sign_message(payload_to_sign)
    signed_payload_hash_hex = signed_payload_hash['signed'].hex()
    export_payload = {
        "payload":payload,
        "signed_hash":signed_payload_hash_hex,
        "requester_company_seralised_public_key": kmc.get_serialized_pub_key().decode('utf-8') #requester is local node, ok to use KMC
    }
    return export_payload

def create_qr_from_someKey_to_localKey(key_b, rating_preferences="1e per km"):
    ##SETUP COMPANIES:
    company_a_res = save_new_company({"name":"AAA","fiscal_country":"ES","vat_number":"012943852348","base_url":"http://localhost:5400"})
    company_id = company_a_res[0]['public_id']
    mark_company_as_node_owner(company_id) #acting as SUPPLIER - e.g. freight forwarder
    pub_key_b = key_b.public_key()
    serialized_pub_key_b = pub_key_b.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    ).decode('utf-8')
    company_b_res = save_new_company({"name":"BBB","fiscal_country":"DE","vat_number":"835476934934","public_key":serialized_pub_key_b, "base_url":"http://localhost:5401"})
    company_b_id = company_b_res[0]['public_id']

    shipment = dict(
        height_um="m",
        height_value=1,
        volume_um="m3",
        volume_value=1,
        goods_description="some goods",
        total_gross_weight_um="Kg",
        total_gross_weight_value=100.5,
        total_slac=2,
        volumetric_weight_conversion_factor_um="Kg/Kg",
        volumetric_weight_conversion_factor_value=1.1,
        volumetric_weight_chargeable_weight_um="Kg",
        volumetric_weight_chargeable_weight_value=110.55
    )    
    shipment_res = save_new_shipment(shipment)
    shipment_id = shipment_res[0]['public_id']

    piece = dict(
        shipment_id=shipment_id,
        additional_security_info="ASD",
        coload=False,
        goods_description="goods with NFTs",
        gross_weight_value=123.12,
        commodity_type="123",
        gross_weight_um="Kg",
        load_type= "X",
        slac = 2,
        stackable = True,
        turnable = True,
        volumetric_weight_conversion_factor_um = "Kgm3",
        volumetric_weight_conversion_factor_value = 123.3,
        volumetric_weight_chargeable_weight_um = "Kg",
        volumetric_weight_chargeable_weight_value = 11.4
    )
    piece_res = save_new_piece(piece)
    piece_id = piece_res[0]["public_id"]


    #setup transport segment
    loc_a = dict(
            name="JFK A",
            location_code="JFK A",
            location_type="Airport",
            city_code = 'JFK',
            city_name= 'NY',
            country= 'USA',
            postal_code='21010',
            street = 'JSK Int Airport',
        )
    loc_b = dict(
            name="MXP A",
            location_code="MXP A",
            location_type="Airport",
            city_code = 'MXP',
            city_name= 'Ferno',
            country= 'IT',
            postal_code='21010',
            street = 'Aeroporto di Milano Malpensa',
        )
    loc_a_res = save_new_location(loc_a)
    loc_a_id = loc_a_res[0]["public_id"]
    loc_b_res = save_new_location(loc_b)
    loc_b_id = loc_b_res[0]["public_id"]
    now = datetime.datetime.utcnow()
    date_string = now.strftime("%Y-%m-%d %H:%M:%S.%f")

    ts = dict(
        co2_calculation_method="ballpark",
        co2_emission_value=11.34,
        co2_emission_unit="Kg/m3",
        fuel_type="Kerosene",
        mode_code="Air Transport",
        movement_type="planned",
        seal="1234",
        segment_level="flight leg",
        arrival_location_id=loc_a_id,
        departure_location_id=loc_b_id,
        pieces=[{"public_id":piece_id}],
        transport_date=date_string
    )
    ts_res = save_new_transport_segment(ts)
    ts_id = ts_res[0]["public_id"]

    #def register_quote_request(self, shipment_id, transport_segment_id, supplier_id, requester_id):
    qr = dict(
        allotment="m",
        rating_preferences=rating_preferences,
        routing_preference="via Istanbul",
        secutiry_status="n.a.",
        units_preference="Kg",
        shipment_id=shipment_id,
        transport_segment_id=ts_id,
        supplier_company_id=company_id,
        requester_company_id=company_b_id
    )   

    quote_request_res = save_new_quote_request(qr)
    quote_request_id = quote_request_res[0]["public_id"]
   

    #prepare the shipment payload and sign it!!!!
    #TODO
    shipment = Shipment.query.filter_by(public_id=shipment_id).first()
    #make sure there are unique hashes only

    #prepare the payload: which is the import_shipment from the DTO without the transport segments
    payload = marshal(shipment,ShipmentDto.import_shipment) 
    for piece in payload["pieces"]:
        piece.pop('transport_segments', None)
        for prod in piece["products"]:
            prod["brand_owner_company_id"] = get_a_company(prod["brand_owner_company_id"]).unique_hash if get_a_company(prod["brand_owner_company_id"]) else ""
            prod["manufacturer_company_id"] = get_a_company(prod["manufacturer_company_id"]).unique_hash if get_a_company(prod["manufacturer_company_id"]) else ""

    payload.pop('transport_segments', None)
    payload.pop('origin_hash', None)
    payload.pop('signed_origin_hash', None)
    payload["originator_pubkey"]=serialized_pub_key_b #shipment service populates the pubkey on its own with the local one (correct default behaviour)
    #create JSON, hash and sign
    origin_hash = hu.digest(json.dumps(payload, sort_keys=True)).hex()
    ship_payload_to_sign = bytes.fromhex(origin_hash)
    ship_signed_payload_hash = key_b.sign(ship_payload_to_sign, ec.ECDSA(hashes.SHA256()))
    ship_signed_payload_hash_hex = ship_signed_payload_hash.hex()
    shipment.origin_hash = origin_hash
    shipment.signed_origin_hash = ship_signed_payload_hash_hex
    shipment.originator_pubkey = serialized_pub_key_b
    #save the hash and the signed data
    db.session.add(shipment) 
    db.session.commit() #make sure shipment has origin hash and signed hash filled before querying QR

    qr = QuoteRequest.query.filter_by(public_id=quote_request_id).first()  
    #use unique hash for referincing companies:
    #prodcuts also have company ids in them, use hashes;
    payload1 = marshal(qr,QuoteRequestDto.full_quote_request) 
    payload1["requester_company_id"] = get_a_company(qr.requester_company_id).unique_hash
    payload1["supplier_company_id"] = get_a_company(qr.supplier_company_id).unique_hash
    for piece in payload1["shipment"]["pieces"]:
        for prod in piece["products"]:
            prod["brand_owner_company_id"] = get_a_company(prod["brand_owner_company_id"]).unique_hash if get_a_company(prod["brand_owner_company_id"]) else ""
            prod["manufacturer_company_id"] = get_a_company(prod["manufacturer_company_id"]).unique_hash if get_a_company(prod["manufacturer_company_id"]) else ""

    #get the payload and sign it
    payload2 = copy.deepcopy(payload1)
    payload2["shipment"]["originator_pubkey"]=serialized_pub_key_b
    db.session.rollback() #if we don't do this, qr is persisted and the test goes on thinking qr has a weird supplier id (which is normal if we import from another node, but not here where we are on the same node)
    payload_to_sign2 = bytes.fromhex(hu.digest(json.dumps(payload2, sort_keys=True)).hex()) ##sign with the given key
    signed_payload_hash2 = key_b.sign(payload_to_sign2, ec.ECDSA(hashes.SHA256()))
    signed_payload_hash_hex2 = signed_payload_hash2.hex()
    
    export_payload = {
        "payload":payload2,
        "signed_hash":signed_payload_hash_hex2,
        "requester_company_seralised_public_key": serialized_pub_key_b #requester is remote node, sign with his key
    }

    #destroy all local objects:

    db.session.delete(qr)
    """
    tr = TransportSegment.query.filter_by(public_id=ts_id).first()  
    if(tr):
        db.session.delete(tr)
    piece = Piece.query.filter_by(public_id=piece_id).first()
    if(piece):
        db.session.delete(piece)
    """
    db.session.commit()
    db.session.flush()
    
    return export_payload

def apply_qr_change_on_payload_from_someKey(payload, key_b, rating_preferences="0.5e per km", goods_desc="some goods"):
    """
    modify the qr and sign again the payload - used to simulate an update of the QR
    """
    pub_key_b = key_b.public_key()
    serialized_pub_key_b = pub_key_b.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    ).decode('utf-8')

    if(goods_desc!="some goods"):
        #change goods_description
        payload["payload"]["shipment"]["goods_description"]=goods_desc
        payload["payload"]["shipment"]["originator_pubkey"]=serialized_pub_key_b
        for piece in payload["payload"]["shipment"]["pieces"]:
            piece["goods_description"]=goods_desc
        #create the signature
        shipment_payload = copy.deepcopy(payload["payload"]["shipment"])
        for piece in shipment_payload["pieces"]:
            piece.pop('transport_segments', None)
        shipment_payload.pop('transport_segments', None)
        shipment_payload.pop('origin_hash', None)
        shipment_payload.pop('signed_origin_hash', None)
        #create JSON, hash and sign again shipment payload
        origin_hash = hu.digest(json.dumps(shipment_payload, sort_keys=True)).hex()
        ship_payload_to_sign = bytes.fromhex(origin_hash)
        ship_signed_payload_hash = key_b.sign(ship_payload_to_sign, ec.ECDSA(hashes.SHA256()))
        ship_signed_payload_hash_hex = ship_signed_payload_hash.hex()
        payload["payload"]["shipment"]["origin_hash"] = origin_hash
        payload["payload"]["shipment"]["signed_origin_hash"] = ship_signed_payload_hash_hex

    #sign again with key_b the overall payload
    payload["payload"]["rating_preferences"]=rating_preferences
    payload_to_sign = bytes.fromhex(hu.digest(json.dumps(payload["payload"], sort_keys=True)).hex()) ##sign with the given key
    signed_payload_hash = key_b.sign(payload_to_sign, ec.ECDSA(hashes.SHA256()))
    signed_payload_hash_hex = signed_payload_hash.hex()
    
    export_payload = {
        "payload":payload["payload"],
        "signed_hash":signed_payload_hash_hex,
        "requester_company_seralised_public_key": serialized_pub_key_b
    }
    return export_payload
    
def create_offer_from_b_to_local(qr, key_b):

    #don't save to DB, this may create strange conflicts, since B is NOT local node
    now = datetime.datetime.utcnow()    
    new_offer = Offer(
        public_id=str(uuid.uuid4()),
        created_on=now,
        allotment="some contract",
        price_value=100.45,
        price_um="EUR",
        quote_request_id=qr["public_id"],
        rating="STD",
        routing="Via Dubai",
        transport_segment_id=qr["transport_segment_id"],
        shipment_id=qr["shipment"]["public_id"],
    )  

    payload = marshal(new_offer,OfferDto.offer) 

    payload_to_sign = bytes.fromhex(hu.digest(json.dumps(payload, sort_keys=True)).hex())
    signed_payload_hash = signed_payload_hash = key_b.sign(payload_to_sign, ec.ECDSA(hashes.SHA256()))
    signed_payload_hash_hex = signed_payload_hash.hex()
    pub_key = key_b.public_key()
    serialized_key = pub_key.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    )
    export_payload = {
        "payload":payload,
        "signed_hash":signed_payload_hash_hex,
        "supplier_company_seralised_public_key": serialized_key.decode('utf-8')
    }
    return export_payload   

def create_booking_from_b_to_local(offer_id, key_b):

    #don't save to DB, this may create strange conflicts, since B is NOT local node
    now = datetime.datetime.utcnow()    
    booking = Booking(
        public_id=str(uuid.uuid4()),
        created_on=now,
        price_value=100,
        price_um="EUR",
        offer_id=offer_id,
        routing="via AMS",
        security_status="pre-cleared"
    )

    payload = marshal(booking,BookingDto.booking) 

    payload_to_sign = bytes.fromhex(hu.digest(json.dumps(payload, sort_keys=True)).hex())
    signed_payload_hash = signed_payload_hash = key_b.sign(payload_to_sign, ec.ECDSA(hashes.SHA256()))
    signed_payload_hash_hex = signed_payload_hash.hex()
    pub_key = key_b.public_key()
    serialized_key = pub_key.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    )
    export_payload = {
        "payload":payload,
        "signed_hash":signed_payload_hash_hex,
        "supplier_company_seralised_public_key": serialized_key.decode('utf-8')
    }
    return export_payload   

