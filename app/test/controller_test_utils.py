import datetime
import json
import unittest

from app.main.services import db
from app.main.model.user import User
from flask import current_app

#Setup User;
def setup_user():
    user = User(
            public_id='123456789',
            email='test2test@test.com',
            password='test2test',
            registered_on=datetime.datetime.utcnow()
        )
    db.session.add(user)
    db.session.commit()
    auth_token = user.encode_auth_token(user.id)
    return auth_token.decode("utf-8"), user.id

def get_admin_token():
    admin = User.query.filter_by(email=current_app.config['ADMIN_EMAIL']).first()
    admin_token = admin.encode_auth_token(admin.id)
    return admin_token.decode("utf-8")

#Company
def register_company(self, name, vat, auth_token=""):
    return self.client.post(
        '/company/',
        data=json.dumps(dict(
            name=name,
            vat_number=vat,
            base_url='http://localhost:5400',
            public_key='123456787',
            fiscal_country='IT'
        )),
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )

def register_company_no_vat(self, auth_token=""):
    return self.client.post(
        '/company/',
        data=json.dumps(dict(
            name='test_company',
            base_url='http://localhost:5400',
            public_key='123456787',
            fiscal_country='IT'
        )),
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )

def update_company(self, company, auth_token=""):
    return self.client.post(
        '/company/'+company.public_id,
        data=json.dumps(dict(
            public_id=company.public_id,
            name='test_company',
            vat_number='12345678',
            base_url='http://localhost:5400',
            public_key='123456787',
            fiscal_address='Piazza D.Chiesa 13',
            fiscal_city = 'Rovereto',
            fiscal_province = 'TN',
            fiscal_country = 'IT'
        )),
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )

#Location
def register_location(self, name, location_code, location_type, auth_token=""):
    return self.client.post(
        '/location/',
        data=json.dumps(dict(
            name=name,
            location_code=location_code,
            location_type=location_type,
            city_code = 'MXP',
            city_name= 'Ferno',
            country= 'IT',
            postal_code='21010',
            street = 'Aeroporto di Milano Malpensa',
        )),
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )

#Product
def register_product(self, brand_owner_company_id, manufacturer_company_id, product_description, product_identifier, auth_token=""):
    payload = dict(
        commodity_code='ASD',
        commodity_description='TEST COMMODITY',
        commodity_type='123',
        product_description=product_description,
        product_identifier= product_identifier
    )
    if manufacturer_company_id:
        payload['manufacturer_company_id'] =  manufacturer_company_id
    if brand_owner_company_id:
        payload['brand_owner_company_id'] = brand_owner_company_id
    return self.client.post(
        '/product/',
        data=json.dumps(payload),
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )

def update_product(self, public_id, brand_owner_company_id, manufacturer_company_id, product_description, product_identifier, auth_token=""):
    payload = dict(
        public_id = public_id,
        commodity_code='ASD',
        commodity_description='TEST COMMODITY',
        commodity_type='123',
        product_description=product_description,
        product_identifier= product_identifier
    )
    if manufacturer_company_id:
        payload['manufacturer_company_id'] =  manufacturer_company_id
    if brand_owner_company_id:
        payload['brand_owner_company_id'] = brand_owner_company_id
    return self.client.post(
        '/product/'+public_id,
        data=json.dumps(payload),
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )

#Piece
def register_piece(self, goods_description, products, parent_piece_id, auth_token=""):
    payload = dict(
        additional_security_info="ASD",
        coload=False,
        goods_description=goods_description,
        gross_weight_value=123.12,
        commodity_type="123",
        gross_weight_um="Kg",
        load_type= "X",
        slac = 2,
        stackable = True,
        turnable = True,
        volumetric_weight_conversion_factor_um = "Kgm3",
        volumetric_weight_conversion_factor_value = 123.3,
        volumetric_weight_chargeable_weight_um = "Kg",
        volumetric_weight_chargeable_weight_value = 11.4
    )
    if parent_piece_id:
        payload["parent_piece_id"] =  parent_piece_id
    if products:
        payload["products"] = products
    return self.client.post(
        '/piece/',
        data=json.dumps(payload),
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )

def register_piece_with_shipment(self, goods_description, products, shipment_id, auth_token=""):
    payload = dict(
        additional_security_info="ASD",
        coload=False,
        goods_description=goods_description,
        gross_weight_value=123.12,
        commodity_type="123",
        gross_weight_um="Kg",
        load_type= "X",
        slac = 2,
        stackable = True,
        turnable = True,
        volumetric_weight_conversion_factor_um = "Kgm3",
        volumetric_weight_conversion_factor_value = 123.3,
        volumetric_weight_chargeable_weight_um = "Kg",
        volumetric_weight_chargeable_weight_value = 11.4
    )
    if shipment_id:
        payload["shipment_id"] =  shipment_id
    if products:
        payload["products"] = products
    return self.client.post(
        '/piece/',
        data=json.dumps(payload),
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )

def update_piece(self, public_id, goods_description, products, parent_piece_id, auth_token=""):
    payload = dict(
        public_id = public_id,
        additional_security_info="ASD",
        coload=False,
        goods_description=goods_description,
        gross_weight_value=123.12,
        commodity_type="123",
        gross_weight_um="Kg",
        load_type= "X",
        slac = 2,
        stackable = True,
        turnable = True,
        volumetric_weight_conversion_factor_um = "Kgm3",
        volumetric_weight_conversion_factor_value = 123.3,
        volumetric_weight_chargeable_weight_um = "Kg",
        volumetric_weight_chargeable_weight_value = 11.4
    )
    if parent_piece_id:
        payload["parent_piece_id"] =  parent_piece_id
    if products:
        payload["products"] = products
    return self.client.post(
        '/piece/'+public_id,
        data=json.dumps(payload),
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )

def update_piece_with_shipment(self, public_id, goods_description, products, shipment_id, auth_token=""):
    payload = dict(
        public_id = public_id,
        additional_security_info="ASD",
        coload=False,
        goods_description=goods_description,
        gross_weight_value=123.12,
        commodity_type="123",
        gross_weight_um="Kg",
        load_type= "X",
        slac = 2,
        stackable = True,
        turnable = True,
        volumetric_weight_conversion_factor_um = "Kgm3",
        volumetric_weight_conversion_factor_value = 123.3,
        volumetric_weight_chargeable_weight_um = "Kg",
        volumetric_weight_chargeable_weight_value = 11.4
    )
    if shipment_id:
        payload["shipment_id"] =  shipment_id
    if products:
        payload["products"] = products
    return self.client.post(
        '/piece/'+public_id,
        data=json.dumps(payload),
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )

def get_a_piece_via_api(self, public_id, auth_token=""):
    return self.client.get(
        '/piece/'+public_id,
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )

#Dangerous Good
def register_dangerousgood(self, un_number, piece_id,  auth_token=""):
    payload = dict(
        class_code= "radioactive",
        net_quantity_value= 10.34,
        net_quantity_um= "Kg",
        packing_group= "group AAA",
        packing_instruction="handle with care",
        un_number=un_number,
        piece_id=piece_id
    )
    return self.client.post(
        '/dangerousgood/',
        data=json.dumps(payload),
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )

#Customs Info
def register_customsinfo(self, content_code, piece_id,  auth_token=""):
    payload = dict(
        content_code= content_code,
        country_code= "IT",
        information= "Import details",
        subject_code= "12e3",
        note="Some notes",
        piece_id=piece_id
    )
    return self.client.post(
        '/customsinfo/',
        data=json.dumps(payload),
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )

#Transport Segment
def register_planned_transport_segment(self, arrival_location_id, departure_location_id, piece_id, transport_date, auth_token=""):
    """
    with a single piece
    """
    payload = dict(
        co2_calculation_method="ballpark",
        co2_emission_value=11.34,
        co2_emission_unit="Kg/m3",
        fuel_type="Kerosene",
        mode_code="Air Transport",
        movement_type="planned",
        seal="1234",
        segment_level="flight leg"
    )
    if arrival_location_id:
        payload["arrival_location_id"] =  arrival_location_id
    if departure_location_id:
        payload["departure_location_id"] =  departure_location_id
    if piece_id:
        payload["pieces"] = [{"public_id":piece_id}]
    if transport_date:
        payload["transport_date"] = transport_date

    return self.client.post(
        '/transportsegment/',
        data=json.dumps(payload),
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )

def get_a_transport_segment_via_api(self, public_id, auth_token=""):
    return self.client.get(
        '/transportsegment/'+public_id,
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )

#Shipment
def register_shipment(self, goods_description, auth_token=""):
    payload = dict(
        height_um="m",
        height_value=1,
        volume_um="m3",
        volume_value=1,
        goods_description=goods_description,
        total_gross_weight_um="Kg",
        total_gross_weight_value=100.5,
        total_slac=2,
        volumetric_weight_conversion_factor_um="Kg/Kg",
        volumetric_weight_conversion_factor_value=1.1,
        volumetric_weight_chargeable_weight_um="Kg",
        volumetric_weight_chargeable_weight_value=110.55
    )    
    return self.client.post(
        '/shipment/',
        data=json.dumps(payload),
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )

def update_shipment(self, public_id, goods_description, auth_token=""):
    payload = dict(
        public_id=public_id,
        height_um="m",
        height_value=1,
        volume_um="m3",
        volume_value=1,
        goods_description=goods_description,
        total_gross_weight_um="Kg",
        total_gross_weight_value=100.5,
        total_slac=2,
        volumetric_weight_conversion_factor_um="Kg/Kg",
        volumetric_weight_conversion_factor_value=1.1,
        volumetric_weight_chargeable_weight_um="Kg",
        volumetric_weight_chargeable_weight_value=110.55
    )    
    return self.client.post(
        '/shipment/'+public_id,
        data=json.dumps(payload),
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )

def get_a_shipment_via_api(self, public_id, auth_token):
    return self.client.get(
        '/shipment/'+public_id,
        headers={'Authorization':('Bearer ' + auth_token)}
    )

def delete_a_shipment_via_api(self, public_id, auth_token):
    return self.client.delete(
        '/shipment/'+public_id,
        headers={'Authorization':('Bearer ' + auth_token)}
    )

#Event
def register_event(self, type_indicator, shipment_id, auth_token=""):
    payload = dict(
        code="FOH",
        name="standard CXML event",
        latitude=  -12.1435034,
        longitude=  -34.40963296,
        recording_datetime= (datetime.datetime.utcnow()).strftime("%Y-%m-%d %H:%M:%S.%f"),
        type_indicator= type_indicator,
        shipment_id=  shipment_id
    )    
    return self.client.post(
        '/event/',
        data=json.dumps(payload),
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )

#Quote Request
def register_quote_request(self, shipment_id, transport_segment_id, supplier_id, requester_id, auth_token=""):
    payload = dict(
        allotment="m",
        rating_preferences="1e per km",
        routing_preference="via Istanbul",
        secutiry_status="n.a.",
        units_preference="Kg"
    )    
    if shipment_id:
        payload["shipment_id"] =  shipment_id
    if transport_segment_id:
        payload["transport_segment_id"] =  transport_segment_id
    if supplier_id:
        payload["supplier_company_id"] =  supplier_id
    if requester_id:
        payload["requester_company_id"] =  requester_id

    return self.client.post(
        '/quoterequest/',
        data=json.dumps(payload),
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )

def get_a_quote_request_via_api(self, public_id, auth_token=""):
    return self.client.get(
        '/quoterequest/'+public_id,
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )

def update_quote_request(self, quote_req_id, shipment_id, transport_segment_id, supplier_id, requester_id, auth_token=""):
    payload = dict(
        public_id=quote_req_id,
        allotment="m",
        rating_preferences="1e per km",
        routing_preference="via Istanbul",
        secutiry_status="n.a.",
        units_preference="Kg"
    )    
    if shipment_id:
        payload["shipment_id"] =  shipment_id
    if transport_segment_id:
        payload["transport_segment_id"] =  transport_segment_id
    if supplier_id:
        payload["supplier_company_id"] =  supplier_id
    if requester_id:
        payload["requester_company_id"] =  requester_id

    return self.client.post(
        '/quoterequest/'+quote_req_id,
        data=json.dumps(payload),
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )    

#Offer
def register_offer(self, shipment_id, transport_segment_id, quote_request_id, auth_token=""):
    payload = dict(
        price_value=100.45,
        price_um="EUR",
        routing="via Istanbul",
        rating="Std contract"
    )    
    if shipment_id:
        payload["shipment_id"] =  shipment_id
    if transport_segment_id:
        payload["transport_segment_id"] =  transport_segment_id
    if quote_request_id:
        payload["quote_request_id"] =  quote_request_id

    return self.client.post(
        '/offer/',
        data=json.dumps(payload),
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )

def post_update_offer(self, public_id, price_value, qr_id, auth_token=""):
    payload = dict(
        public_id=public_id,
        price_value=price_value,
        price_um="EUR",
        quote_request_id=qr_id
    )
    return self.client.post(
        '/offer/'+public_id,
        data=json.dumps(payload),
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )

def get_single_offer(self, public_id, auth_token=""):
    return self.client.get(
        '/offer/'+public_id,
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )

#Booking
def register_booking(self, offer_id, auth_token=""):
    payload = dict(
        offer_id=offer_id,
        price_value=100.45,
        price_um="EUR",
        routing="via Istanbul"
    )    

    return self.client.post(
        '/booking/',
        data=json.dumps(payload),
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )

def get_booking_via_api(self, booking_id, auth_token=""):
    return self.client.get(
        '/booking/'+booking_id,
        content_type='application/json',
        headers={'Authorization':('Bearer ' + auth_token)}
    )