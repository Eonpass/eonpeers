import datetime
import json
import unittest

from app.main.services import db
from app.main.model.quoterequest import QuoteRequest
from app.main.model.offer import Offer
from app.main.model.booking import Booking
from app.main.model.piece import Piece
from app.main.model.company import Company
from app.main.model.transportsegment import TransportSegment
from app.main.service.company_service import mark_company_as_node_owner, update_company
from app.main.service.shipment_service import _get_didkeys_of_providers
from app.main.service.quoterequest_service import (import_quote_request)
from app.main.service.offer_service import (_prepare_offer_export_payload, _check_offer, import_offer, accept_offer, register_offer_acceptance)
from app.main.service.booking_service import (_prepare_booking_export_payload, _check_booking, import_booking, export_booking)
from app.main.job.offer_job import work_offer_acceptance_response, work_offer_acceptance_excpetion
from app.main.util.keymanagementutils import KeyManagementClient
from app.main.util.hashutils import HashUtils
from app.test.controller_test_utils import (get_admin_token, register_company, register_location, register_shipment, register_piece_with_shipment, update_piece_with_shipment, register_planned_transport_segment, register_quote_request, update_quote_request, get_a_quote_request_via_api, register_offer, register_booking, get_booking_via_api)
from app.test.base import BaseTestCase
from app.test.data.eonpeers_payloads import qr_payload, qr_payload_2, offer_payload, booking_payload
from app.main.util.eonerror import EonError
from app.test.commercial_test_utils import (create_qr_from_localKey_to_keyB, create_offer_from_b_to_local, create_qr_from_someKey_to_localKey, create_booking_from_b_to_local)

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.serialization import load_pem_private_key
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.exceptions import InvalidSignature 

class TestBookingModel(BaseTestCase):

    DEBUG = False
    kmc = KeyManagementClient()


    def test_booking_lifecycle(self):
        """
        Receive a QR, create and export an offer, receive acceptance, build the booking, export the booking
        """

        #setup the inbound QR and companies
        admin_token = get_admin_token()
        key_b = ec.generate_private_key(ec.SECP256K1(), default_backend())   
        qr_from_some_key_to_local = create_qr_from_someKey_to_localKey(key_b)

        signature_failure=False
        try:
            import_quote_request(qr_from_some_key_to_local)
        except Exception as e:
            print(e)
            signature_failure=True
        self.assertTrue(signature_failure==False)

        qr = QuoteRequest.query.filter_by(public_id=qr_from_some_key_to_local["payload"]["public_id"]).first()
        company_b = Company.query.filter_by(name="BBB").first()  
        self.assertTrue(qr.requester.public_id == company_b.public_id)

        #create the offer
        offer_res = register_offer(self, qr.shipment_id, qr.transport_segment_id, qr.public_id, admin_token)
        offer_data = json.loads(offer_res.data.decode())
        self.assertTrue(offer_res.status_code==201)
        offer_id = offer_data['public_id']
        self.assertTrue(offer_id)
        shipment_id = qr.shipment_id

        #receive an acceptance for the offer
        rpc_response_body = {
            'method':'offer.registeracceptance',
            'message':'Success, acceptance registered, please wait until we send the booking confirmation'
        }
        res = TestResponse(json.dumps(rpc_response_body),200)
        work_offer_acceptance_response(res, offer_id)
        offer = Offer.query.filter_by(public_id=offer_id).first()  
        self.assertTrue("OK," in offer.latest_log)

        #create the booking:
        booking_res = register_booking(self, offer_id, admin_token)
        booking_id = json.loads(booking_res.data.decode())["public_id"]
        booking = Booking.query.filter_by(public_id=booking_id).first()
        self.assertTrue(booking)

        
        booking_export_payload = export_booking(booking_id)
        self.assertTrue(booking_export_payload["signed_hash"])
        hu = HashUtils()
        kmc = KeyManagementClient()
        #signed = bytes from hex (signed_message)
        signed_bytes = bytes.fromhex(booking_export_payload["signed_hash"])
        #message = compute the hash of payload
        message = bytes.fromhex(hu.digest(json.dumps(booking_export_payload["payload"], sort_keys=True)).hex())
        #serialized public should already be fine
        try:
            signature_checks_out = kmc.verify_signed_message(signed_bytes, message, booking_export_payload["supplier_company_seralised_public_key"])
            self.assertTrue(signature_checks_out)         
        except Exception as e:
            print(e)
            self.assertTrue(False)   

        #test the job methods that are called on receiving an offer acceptance (can't simulate the full REST call here)        
        e = EonError('Some excehption.', 400)
        work_offer_acceptance_excpetion(e, offer_id)  
        offer = Offer.query.filter_by(public_id=offer_id).first()  
        self.assertTrue("HTTPConnection error" in offer.latest_log)

        #this instead is the expected response if everything goes OK
        rpc_response_body = {
            'method':'offer.registeracceptance',
            'message':'Success, acceptance registered, please wait until we send the booking confirmation'
        }
        res = TestResponse(json.dumps(rpc_response_body),200)
        work_offer_acceptance_response(res, offer_id)
        offer = Offer.query.filter_by(public_id=offer_id).first()  
        self.assertTrue("OK," in offer.latest_log)

        #test get booking details:
        res = get_booking_via_api(self, booking_id, admin_token)
        self.assertTrue(json.loads(res.data.decode())["public_id"]==booking_id)
        #test missing admin token:
        res = get_booking_via_api(self, booking_id, "123")
        self.assertTrue(res.status_code==401)

        #try to get the did keys of providers for a booked shipment:
        didkeys = _get_didkeys_of_providers(shipment_id)
        self.assertTrue(len(didkeys)>0)


    def test_booking_import(self):
        """
        Create a QR, import and offer (accept it), import the booking
        """
        admin_token = get_admin_token()
        key_b = ec.generate_private_key(ec.SECP256K1(), default_backend())   
        qr_from_local_to_some_key = create_qr_from_localKey_to_keyB(key_b)

        #qr get exported to FF which replies with an offer:
        offer_from_b = create_offer_from_b_to_local(qr_from_local_to_some_key["payload"], key_b)

        signature_failure=False
        try:
            import_offer(offer_from_b) 
        except Exception as e:
            print(e)
            signature_failure=True
        self.assertTrue(signature_failure==False)

        #TODO: accept offer

        #import the booking
        try: 
            offer_id = offer_from_b["payload"]["public_id"]
            booking_payload = create_booking_from_b_to_local(offer_id, key_b)
            import_booking(booking_payload) #this has a wrong signature by design to test failure
        except Exception as e:
            print(e)
            signature_failure=True
        self.assertTrue(signature_failure==False)
        

class TestResponse(object):
    text = ""
    status_code = 0

    # The class "constructor" - It's actually an initializer 
    def __init__(self, text, status_code):
        self.text = text
        self.status_code = status_code


if __name__ == '__main__':
    unittest.main()
