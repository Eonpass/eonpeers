import datetime
import json
import unittest

from app.main.services import db
from app.main.model.company import Company
from app.main.model.user import User
from app.main.service.company_service import (save_new_company, get_a_company, get_node_owner, get_all_companies, mark_company_as_node_owner, create_user_on_remote_node, create_local_did)
from app.main.job.company_job import compare_new_company
from app.main.util.eonerror import EonError
from app.test.base import BaseTestCase
from app.test.controller_test_utils import (get_admin_token, register_company, register_company_no_vat, update_company)


class TestCompanyModel(BaseTestCase):

    DEBUG = False
    
    def test_create_and_delete_company(self):
        with self.client:
            admin_token = get_admin_token()
            company_res = register_company(self, 'test_company', '12345678', admin_token)
            data = json.loads(company_res.data.decode())
            self.assertTrue(data['public_id'])
            self.assertTrue(company_res.content_type == 'application/json')
            self.assertEqual(company_res.status_code, 201)

            company = get_a_company(data['public_id'])

            self.assertTrue(company.name == "test_company")

            #create a user, test relationship, delete company, see that user is deleted as well
            user = User(
                email='testcompany@test.com',
                password='test',
                registered_on=datetime.datetime.utcnow(),
                company_id=data['public_id']
            )
            db.session.add(user)
            db.session.commit()
            company = Company.query.filter_by(public_id=data['public_id']).first()
            self.assertTrue(len(company.users)==1)
            user = User.query.filter_by(company_id =data['public_id']).first()
            self.assertTrue(user)
            db.session.delete(company)
            db.session.commit()
            company = Company.query.filter_by(public_id=data['public_id']).first()
            self.assertTrue(not company)
            user = User.query.filter_by(company_id =data['public_id']).first()
            self.assertTrue(not user)

    def test_create_company_customkey(self):
        with self.client:
            serializaed_key = "-----BEGIN PUBLIC KEY-----\nMFYwEAYHKoZIzj0CAQYFK4EEAAoDQgAELI+8CAFfbJWcGUgrDtKy9w6GWcAwUI6H\nggEHb6P7DcveZfhHw94V1gCvE7fQcP6HNolj8XR3TehHX/0nisCY6Q==\n-----END PUBLIC KEY-----\n"
            company_b_res = save_new_company({"name":"BBB","fiscal_country":"DE","vat_number":"835476934934","public_key":serializaed_key, "base_url":"http://localhost:5401"})
            company_b_id = company_b_res[0]['public_id']
            company = Company.query.filter_by(public_id=company_b_id).first()
            self.assertTrue(company.public_key)

    def test_create_company_missing_data(self):
        with self.client:
            admin_token = get_admin_token()
            company_res = register_company_no_vat(self, admin_token)
            data = json.loads(company_res.data.decode())
            self.assertEqual(company_res.status_code, 400)
            self.assertTrue(data["errors"])
            self.assertTrue(data["message"]=="Input payload validation failed")

    def test_create_and_update_company(self):
        with self.client:
            admin_token = get_admin_token()
            company_res = register_company(self, 'test_company', '12345678', admin_token)
            data = json.loads(company_res.data.decode())
            self.assertTrue(data['public_id'])
            self.assertTrue(company_res.content_type == 'application/json')
            self.assertEqual(company_res.status_code, 201)

            company = get_a_company(data['public_id'])
            previous_hash = company.hash

            self.assertTrue(company.name == "test_company")
            self.assertTrue(company.hash)
            company_upd = update_company(self, company, admin_token)
            data = json.loads(company_res.data.decode())
            self.assertTrue(data['public_id'])

            company_after_update = get_a_company(data['public_id'])
            self.assertTrue(company_after_update.hash)
            self.assertTrue(company_after_update.hash!=previous_hash)
            self.assertTrue(company_after_update.fiscal_city=='Rovereto')

    def test_validate_new_company(self):
        with self.client:
            admin_token = get_admin_token()
            #create company A
            company_res = register_company(self, 'test_company', '12345678', admin_token)
            data = json.loads(company_res.data.decode())
            local_A_id = data['public_id']

            #Validation scenarios: 
            #A) the remote_company JSON is matching and has additional fields
            remote_A = {
                'public_id':'test1234567test',
                'name':'test_company',
                'vat_number':'12345678',
                'base_url':'http://localhost:5400',
                'public_key':'123456787',
                'fiscal_country':'IT',
                'fiscal_zip':'38852',
                'latest_log':'test error'
            }
            #in the current setup, local ids can be different, i.e. there will not be a company with id "test1234567test" on this node:
            compare_new_company(data['public_id'], remote_A)
            company_remote = get_a_company('test1234567test')
            self.assertTrue(company_remote == None)
            company = get_a_company(data['public_id'])
            self.assertTrue(company.fiscal_zip == '38852')

            #B) the remote_comapny JSON matches another company we already have, the local_company should be deleted and the other updated
            B_company_res = register_company(self, 'test_company_2', '1111111111', admin_token)
            data = json.loads(B_company_res.data.decode())
            #at this point I should have 2 companies
            self.assertTrue(len(get_all_companies())>1)
            #compare inbound resource with current db:
            compare_new_company(data['public_id'], remote_A)
            company_old_dup = get_a_company(data['public_id'])
            self.assertTrue(company_old_dup == None)
            #there should be only 1 company left now
            self.assertTrue(len(get_all_companies())==1)
            #test there's the latest_log field
            local_comp = Company.query.filter_by(public_id=local_A_id).first()
            self.assertTrue(local_comp.latest_log=='test error')

    def test_node_owner(self):
        with self.client:
            #create a node owner
            admin_token = get_admin_token()
            company_res = register_company(self, 'test_company', '12345678', admin_token)
            data = json.loads(company_res.data.decode())
            company_admin_id = data['public_id']
            mark_company_as_node_owner(data['public_id'])

            node_owner = get_node_owner()
            self.assertTrue(node_owner.name=='test_company')

            #try to create another node owner
            company_res = register_company(self, 'test_company2', '123456782', admin_token)
            data = json.loads(company_res.data.decode())
            try:
                mark_company_as_node_owner(data['public_id'])
            except EonError as e:
                self.assertTrue(e.code==409)

            #try to generate a new DID for the node owner company:
            diddata = {"did_type":"ebsi"}
            res = create_local_did(diddata)
            company = get_a_company(company_admin_id)
            self.assertTrue("did:ebsi" in company.did);

    def test_create_remote_user(self):
        with self.client:
            """
            create a node owner and another company, then try to request a user for the node of the other company
            """
            admin_token = get_admin_token()
            company_res = register_company(self, 'test_company', '12345678', admin_token)
            data = json.loads(company_res.data.decode())
            mark_company_as_node_owner(data['public_id'])

            node_owner = get_node_owner()
            self.assertTrue(node_owner.name=='test_company')

            company_res = register_company(self, 'test_company_remote', '0987654321', admin_token)
            data = json.loads(company_res.data.decode())

            company_before_update = get_a_company(data['public_id'])
            self.assertFalse(company_before_update.remote_username)

            payload = {
                'username':'remoteuser',
                'password':'password',
                'email':'test@localtest.co'
            }

            rpc_res = create_user_on_remote_node(payload, data['public_id'])
            self.assertTrue(rpc_res[1]==201)

            company_after_update = get_a_company(data['public_id'])
            self.assertTrue(company_after_update.remote_username == 'remoteuser')



if __name__ == '__main__':
    unittest.main()
