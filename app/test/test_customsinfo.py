import datetime
import json
import unittest

from app.main.services import db
from app.main.model.customsinfo import CustomsInfo
from app.main.service.customsinfo_service import save_new_customs_info, get_a_customs_info, upsert_a_customs_info
from app.main.service.product_service import get_a_product
from app.main.util.keymanagementutils import KeyManagementClient
from app.main.util.hashutils import HashUtils
from app.test.base import BaseTestCase
from app.test.controller_test_utils import (get_admin_token, register_company, register_product, update_product, register_piece, update_piece, register_shipment, register_event)
from app.main.util.eonerror import EonError

class TestCustomsInfoModel(BaseTestCase):

    DEBUG = False
    kmc = KeyManagementClient()

    def test_customsinfo_model(self):
        """
        basic test of the model
        """
        new_customs_info_missing_piece = {
            "content_code":"XY",
            "country_code":"IT",
            "information":"info per dogane",
            "subject_code":"123",
            "note":"note"
        }
        error=False
        try:
            res = save_new_customs_info(new_customs_info_missing_piece)
        except EonError as ee:
            error=True
            self.assertTrue(ee.code==404)
        self.assertTrue(error)

        

    def test_customsinfo_controller(self):
        """
        basic test of the model with errors
        """
        #setup a piece:
        admin_token = get_admin_token()
        prod_a_res = register_product(self, None, None, "test description", "id 10983450", admin_token)
        self.assertTrue(prod_a_res.status_code == 201)
        prod_a_id = json.loads(prod_a_res.data.decode("utf-8"))["public_id"]
        prod = get_a_product(prod_a_id)
        products = [
            {
            "commodity_code":"mandatory",
            "commodity_description":"mandatory",
            "commodity_type":"mandatory",
            "product_identifier":prod.product_identifier,
            "product_description":prod.product_description,
            "unique_hash":prod.unique_hash
            }
        ]
        piece_res = register_piece(self, "some test goods", products, None, admin_token)
        self.assertTrue(piece_res.status_code == 201)
        piece_id = json.loads(piece_res.data.decode("utf-8"))["public_id"]

        #test inserts
        new_customs_info_missing_codes = {
            "information":"info per dogane",
            "note":"note",
            "piece_id":piece_id
        }
        error=False
        try:
            res = save_new_customs_info(new_customs_info_missing_codes)
        except EonError as ee:
            error=True
            self.assertTrue(ee.code==409)
        self.assertTrue(error)

        new_customs_info = {
            "content_code":"XY",
            "country_code":"IT",
            "information":"info per dogane",
            "subject_code":"123",
            "note":"note",
            "piece_id":piece_id
        }
        error=False
        res = []
        try:
            res = save_new_customs_info(new_customs_info)
        except EonError as ee:
            error=True
        self.assertFalse(error)
        self.assertTrue(res[1]==201)
        self.assertTrue(res[0]["public_id"])
        ci = get_a_customs_info(res[0]["public_id"])
        self.assertTrue(ci.country_code == "IT")

        #test update
        upd_customs_info = {
            "public_id":res[0]["public_id"],
            "content_code":"XY",
            "country_code":"DE",
            "information":"info per dogane",
            "subject_code":"123",
            "note":"note",
            "piece_id":piece_id
        }
        res_upd = upsert_a_customs_info(upd_customs_info)
        self.assertTrue(res_upd[1]==200)
        ci = get_a_customs_info(res[0]["public_id"])
        self.assertTrue(ci.country_code == "DE")
        

if __name__ == '__main__':
    unittest.main()