import datetime
import json
import unittest
import math

from app.main.services import db
from app.main.model.dangerousgood import DangerousGood
from app.main.service.dangerousgood_service import save_new_dangerous_good, get_a_dangerous_good, upsert_a_dangerous_good
from app.main.service.product_service import get_a_product
from app.main.util.keymanagementutils import KeyManagementClient
from app.main.util.hashutils import HashUtils
from app.test.base import BaseTestCase
from app.test.controller_test_utils import (get_admin_token, register_company, register_product, update_product, register_piece, update_piece, register_shipment, register_event)
from app.main.util.eonerror import EonError

class TestCustomsInfoModel(BaseTestCase):

    DEBUG = False
    kmc = KeyManagementClient()

    def test_dangerousgood_model(self):
        """
        basic test of the model
        """
        dangerous_good_info_missing_piece = {
            "class_code":"XY",
            "net_quantity_value":1.3,
            "net_quantity_value":"um",
            "packing_group":"123",
            "packing_instruction":"prova prova",
            "un_number":"23"
        }
        error=False
        try:
            res = save_new_dangerous_good(dangerous_good_info_missing_piece)
        except EonError as ee:
            error=True
            self.assertTrue(ee.code==404)
        self.assertTrue(error)

    def test_dangerous_good_controller(self):
        """
        basic test of the services with errors
        """
        #setup a piece:
        admin_token = get_admin_token()
        prod_a_res = register_product(self, None, None, "test description", "id 10983451", admin_token)
        self.assertTrue(prod_a_res.status_code == 201)
        prod_a_id = json.loads(prod_a_res.data.decode("utf-8"))["public_id"]
        prod = get_a_product(prod_a_id)
        products = [
            {
            "commodity_code":"mandatory",
            "commodity_description":"mandatory",
            "commodity_type":"mandatory",
            "product_identifier":prod.product_identifier,
            "product_description":prod.product_description,
            "unique_hash":prod.unique_hash
            }
        ]
        piece_res = register_piece(self, "some test goods", products, None, admin_token)
        self.assertTrue(piece_res.status_code == 201)
        piece_id = json.loads(piece_res.data.decode("utf-8"))["public_id"]

        #test inserts
        new_dangerous_good = {
            "class_code":"XY",
            "net_quantity_value":1.3,
            "net_quantity_um":"m^3",
            "packing_group":"123",
            "packing_instruction":"prova prova",
            "un_number":"23",
            "piece_id":piece_id
        }
        error=False
        res = []
        try:
            res = save_new_dangerous_good(new_dangerous_good)
        except EonError as ee:
            error=True
        self.assertFalse(error)
        self.assertTrue(res[1]==201)
        self.assertTrue(res[0]["public_id"])
        dg = get_a_dangerous_good(res[0]["public_id"])
        self.assertTrue(dg.class_code == "XY")

        #test update
        upd_dangerous_good = {
            "public_id":res[0]["public_id"],
            "class_code":"XY",
            "net_quantity_value":1.42,
            "net_quantity_um":"m^3",
            "packing_group":"123",
            "packing_instruction":"prova prova",
            "un_number":"23",
            "piece_id":piece_id
        }
        res_upd = upsert_a_dangerous_good(upd_dangerous_good)
        self.assertTrue(res_upd[1]==200)
        dg = get_a_dangerous_good(res[0]["public_id"])
        self.assertTrue(math.isclose(dg.net_quantity_value, 1.42))
        

if __name__ == '__main__':
    unittest.main()