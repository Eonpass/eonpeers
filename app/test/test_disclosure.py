import datetime
import json
import unittest

from app.main.services import db
from app.main.model.shipment import Shipment
from app.main.model.disclosure import Disclosure
from app.main.model.user import User
from app.main.service.shipment_service import (save_new_shipment, get_a_shipment, get_all_shipments, prepare_origin_shipment_payload)
from app.main.service.disclosure_service import save_new_disclosure, get_a_disclosure, update_disclosure
from app.main.service.user_service import get_all_users, get_public_id_from_id
from app.main.util.keymanagementutils import KeyManagementClient
from app.main.util.hashutils import HashUtils
from app.test.controller_test_utils import (get_admin_token, setup_user, register_shipment, update_shipment, register_piece, update_piece, get_a_piece_via_api, register_piece_with_shipment, update_piece_with_shipment, register_planned_transport_segment, get_a_shipment_via_api)
from app.test.base import BaseTestCase
from app.test.test_auth import login_custom_user


class TestDisclosureModel(BaseTestCase):

    DEBUG = False
    kmc = KeyManagementClient()
    
    def test_disclosure_relationships(self):
        """
        create a shipment and a connected piece, from the shipment get the pieces and test the relationship, then update the shipment and test update
        """
        admin_token = get_admin_token()
        #setup shipment
        shipment_res = register_shipment(self,"some goods", admin_token)
        shipment_data = json.loads(shipment_res.data.decode())
        self.assertTrue(shipment_data['public_id'])
        shipment_id = shipment_data['public_id']

        #setup user
        auth_token, user_id = setup_user()

        u_public_id = get_public_id_from_id(user_id)

        user = User.query.filter_by(id=user_id).first()
        users = User.query.all()
        for u in users:
            print(u.id, u.public_id)

        #test failure to view shipment without disclosure
        shipment_res = get_a_shipment_via_api(self, shipment_id, auth_token)
        self.assertTrue( shipment_res.status_code == 401)

        #add disclosure
        save_new_disclosure({'user_id':u_public_id, "shipment_id":shipment_id})
        disclosures = Disclosure.query.filter_by().all()
        self.assertTrue(len(disclosures)==1)

        try: 
            disc = get_a_disclosure(shipment_id, u_public_id)
            self.assertTrue(not disc.approved)
        except Exception as e:
            print("WRONG DISCLOSRE!") #TODO assert a failure

        upd_payload = {
            "user_id":u_public_id,
            "shipment_id":shipment_id,
            "approved":True
        }
        update_disclosure(upd_payload)

        try: 
            disc = get_a_disclosure(shipment_id, u_public_id)
            self.assertTrue(disc.approved)
        except Exception as e:
            print("WRONG DISCLOSRE!") #TODO assert a failure

        shipment_res = get_a_shipment_via_api(self, shipment_id, auth_token)
        shipment_api_object = json.loads(shipment_res.data.decode("utf-8"))
        self.assertTrue(shipment_api_object["originator_pubkey"])
        self.assertTrue( shipment_res.status_code == 200)

        #destroy shipment and check the cascade
        shipment = Shipment.query.filter_by(public_id=shipment_id).first()
        db.session.delete(shipment)
        db.session.commit()

        disclosures = Disclosure.query.filter_by().all()
        self.assertTrue(len(disclosures)==0)
        

