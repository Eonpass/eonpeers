import datetime
import json
import unittest

from app.main.services import db
from app.main.model.event import Event
from app.main.service.event_service import save_new_event
from app.main.service.shipment_service import get_shipment_events
from app.main.util.keymanagementutils import KeyManagementClient
from app.main.util.hashutils import HashUtils
from app.test.base import BaseTestCase
from app.test.controller_test_utils import (get_admin_token, register_company, register_product, update_product, register_piece, update_piece, register_shipment, register_event)
from app.main.util.eonerror import EonError

class TestEventModel(BaseTestCase):

    DEBUG = False
    kmc = KeyManagementClient()

    def test_event_model(self):
        """
        basic test of the model
        """
        new_event = Event(
            code="FOH",
            name="standard CXML event",
            latitude = 12.1435034,
            longitude = 34.40963296,
            recording_datetime = datetime.datetime.utcnow(),
            type_indicator = "Actual"
        )

        event_dict = new_event.as_dict()
        event_dict["recording_datetime"] = event_dict["recording_datetime"].strftime('%Y-%m-%d %H:%M:%S.%f')
        payload =  json.dumps(event_dict, sort_keys=True);
        hu = HashUtils()
        current_hash = hu.digest(payload).hex()
        hash_id = new_event.hash
        self.assertTrue(hash_id == current_hash)

    def test_event_service(self):
        """
        basic test of the model with errors
        """
        auth_token = get_admin_token()

        shipment_res = register_shipment(self,"some goods", auth_token)
        shipment_data = json.loads(shipment_res.data.decode())
        self.assertTrue(shipment_data['public_id'])
        shipment_id = shipment_data['public_id']
        self.assertTrue(shipment_res.content_type == 'application/json')
        self.assertEqual(shipment_res.status_code, 201)

        new_event = {
            "code":"FOH",
            "name":"standard CXML event",
            "latitude":  -12.1435034,
            "longitude":  -34.40963296,
            "recording_datetime": (datetime.datetime.utcnow()).strftime("%Y-%m-%d %H:%M:%S.%f"),
            "type_indicator":  "Actual",
            "shipment_id":  shipment_id
        }

        new_event_res = save_new_event(new_event)
        self.assertTrue(new_event_res[0]["public_id"])
        self.assertTrue(new_event_res[1]==201)

        events = get_shipment_events(shipment_id, {"page":"1", "page_size":"20"})
        self.assertTrue(events["records"][0].public_id==new_event_res[0]["public_id"])

        #wrong event type
        event_res = register_event(self, "some type", shipment_id, auth_token)
        event_data = json.loads(event_res.data.decode())
        self.assertEqual(event_res.status_code, 409)

        #missing shipment
        event_res = register_event(self, "Actual", None, auth_token)
        event_data = json.loads(event_res.data.decode())
        self.assertEqual(event_res.status_code, 400)

        #wrong shipment
        event_res = register_event(self, "Actual", "1123123", auth_token)
        event_data = json.loads(event_res.data.decode())
        self.assertEqual(event_res.status_code, 404)        

        


if __name__ == '__main__':
    unittest.main()