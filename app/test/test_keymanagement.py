import unittest
import os.path
import base64
from flask import current_app

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.serialization import load_pem_private_key

from app.main.util.keymanagementutils import KeyManagementClient
from app.test.base import BaseTestCase


class TestKeyManagmentClientFactory(BaseTestCase):

    def test_kmc_factory(self):
        """ Get the current test key, in test env we use the developer KMI which creates a file in the project folder """
        kmc = KeyManagementClient()
        #test the loaded type of KMC according to config
        self.assertTrue(kmc._get_kmic()._type == current_app.config['KMI_TYPE'])
        #check that the privatekey file has been created or was already there:
        filename = os.path.join(os.path.dirname(__file__), '../main/util/privkey.pem') #path relative to test file
        exception_raised = False
        try:
            with open(filename, 'rb') as pem_in:
                pemlines = pem_in.read()
        except OSError as e:
            print(e)
            exception_raised = True
        finally:
            self.assertFalse(exception_raised)     

    def test_kmc_signature(self):
        """ Test the signature and verification """
        message = '0145eaea'
        kmc = KeyManagementClient()
        result = kmc.sign_message(bytes.fromhex(message))
        priv_key = kmc._get_kmic()._get_key()
        pub_key = priv_key.public_key()
        serialized_pub_key = pub_key.public_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PublicFormat.SubjectPublicKeyInfo
        )
        #check that the public key is the one originating from the file
        self.assertTrue(result['serialized_public'] == serialized_pub_key)
        #check that the signature is correct
        verify_outcome = kmc.verify_signed_message(result['signed'], bytes.fromhex(message), serialized_pub_key)
        self.assertTrue(verify_outcome)
        #check that the signature is non corrupt
        message2 = '32323232'
        verify_outcome = kmc.verify_signed_message(result['signed'], bytes.fromhex(message2), serialized_pub_key)
        self.assertFalse(verify_outcome)
        #try to decode and encode the public key and the signed message, which travels as hex over the API
        decoded_pub = kmc.get_serialized_pub_key().decode('utf-8')
        hex_signed_message = result['signed'].hex()
        #test default decode:
        verify_outcome = kmc.verify_signed_message(hex_signed_message, bytes.fromhex(message), decoded_pub)
        self.assertFalse(verify_outcome)
        verify_outcome = kmc.verify_signed_message(bytes.fromhex(hex_signed_message), bytes.fromhex(message), decoded_pub)
        self.assertTrue(verify_outcome)

    def test_jwk_conversion(self):
        """ test the conversion from jwk to pem """
        jwk_negy = {"kty":"EC","crv":"secp256k1","x":"0XAQccZX0Pm7Rt14j6aIzWTX1O6HoqKSx0s0Z6I9at4","y":"-2GziWARxIGDmb5nHJ9TjcuB8GVvJ9trExmsxSJfBV4","d":"s0SwryLPzVDTgXXM-Uj9fwNH6Hb_fbB68hkvCezhzOY"}
        jwk_posy = {
            "kty": "EC",
            "d": "MXrxKTl_o9yIQlExYy9c1LcWZX_OwX3aw-oGP0flUdo",
            "use": "sig",
            "crv": "secp256k1",
            "kid": "Im53aoD8zJoHzOXmfIAUkncONCIeR1pgy_nhvQrwN3s",
            "x": "hHXNLbjBY_SFeP-tOPoyoGGYjISm-m3aVJLpc3suka0",
            "y": "yYIjrvo_lqrsdxq-oMQQxBG8eyIUKmF9XazdwdGTwSY",
            "alg": "ES256"
        }
        jwk = jwk_posy
        kmc = KeyManagementClient()
        pem = kmc.convert_jwk_to_pem(jwk)
        #ket pub key, convert to coordinate, check that x and y are as expected
        priv_key = load_pem_private_key(pem, None, default_backend())
        pub_key = priv_key.public_key()
        #---- X
        x = (pub_key.public_numbers().x)
        number_bytes = x.to_bytes(32, byteorder="big")
        encoded = base64.urlsafe_b64encode(number_bytes)
        print(jwk["x"],encoded.decode() )
        self.assertTrue(jwk["x"] == encoded.decode().replace("=",""))
        #---- Y
        y = (pub_key.public_numbers().y)
        number_bytes = y.to_bytes(32, byteorder="big")
        encoded = base64.urlsafe_b64encode(number_bytes)
        print(jwk["y"],encoded.decode() )
        self.assertTrue(jwk["y"] == encoded.decode().replace("=",""))


if __name__ == '__main__':
    unittest.main()
