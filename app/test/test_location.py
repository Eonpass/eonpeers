import datetime
import json
import unittest

from app.main.services import db
from app.main.model.company import Company
from app.main.model.location import Location
from app.main.service.location_service import (save_new_location, upsert_a_location, get_a_location, get_all_locations )
from app.main.util.keymanagementutils import KeyManagementClient
from app.main.util.hashutils import HashUtils
from app.test.base import BaseTestCase
from app.main.util.eonerror import EonError
from app.test.controller_test_utils import (register_location)


#TODO: add controller tests! the one here are testing only the service and the model

class TestLocationModel(BaseTestCase):

    DEBUG = False
    kmc = KeyManagementClient()
    
    def test_create_location(self):
        data = {
            "name":'Malpensa Aeroporto',
            "location_code":'MXP',
            "location_type":'Airport',
            "city_code" : 'MXP',
            "city_name": 'Ferno',
            "country": 'IT',
            "postal_code":'21010',
            "street" : 'Aeroporto di Milano Malpensa'
        }
        res = save_new_location(data)
        self.assertTrue(res[1]==201)
        self.assertTrue(res[0] and res[0]["public_id"])

    def test_update_location(self):
        data = {
            "name":'Malpensa Aeroporto',
            "location_code":'MXP',
            "location_type":'Airport',
            "city_code" : 'MXP',
            "city_name": 'Ferno',
            "country": 'IT',
            "postal_code":'21010',
            "street" : 'Aeroporto di Milano Malpensa'
        }
        res = save_new_location(data)
        self.assertTrue(res[1]==201)
        self.assertTrue(res[0] and res[0]["public_id"])

        data = {
            "name":'Malpensa Aeroporto',
            "location_code":'MXP',
            "location_type":'Airport',
            "city_code" : 'MXP',
            "city_name": 'Ferno',
            "country": 'IT',
            "region_code": "VA",
            "region_name": "Varese",
            "postal_code":'21010',
            "street" : 'Aeroporto di Milano Malpensa'
        }
        res_upd = upsert_a_location(data)
        self.assertTrue(res_upd[1]==201)
        self.assertTrue(res_upd[0] and res_upd[0]["public_id"])
        self.assertTrue(res_upd[0]["public_id"] == res[0]["public_id"])

        data = {
            "name":'Malpensa Aeroporto',
            "location_code":'MXP',
            "location_type":'Airport',
            "city_code" : 'MXP T2',
            "city_name": 'Ferno',
            "country": 'IT',
            "postal_code":'21010',
            "street" : 'Aeroporto di Milano Malpensa'
        }
        res_upd = upsert_a_location(data)
        self.assertTrue(res_upd[1]==201)
        self.assertTrue(res_upd[0]["public_id"] == res[0]["public_id"])

        location = get_a_location(res_upd[0]["public_id"])
        self.assertTrue(location.region_code=="VA") #check that the field is not empied
        #test also the hash as a public_id for GET
        location =  get_a_location(location.unique_hash)
        self.assertTrue(location.public_id == res_upd[0]["public_id"])


    def test_duplicate_location(self):
        data = {
            "name":'Malpensa Aeroporto',
            "location_code":'MXP',
            "location_type":'Airport',
            "city_code" : 'MXP',
            "city_name": 'Ferno',
            "country": 'IT',
            "postal_code":'21010',
            "street" : 'Aeroporto di Milano Malpensa'
        }
        res = save_new_location(data)

        try:
            res_dup = save_new_location(data)
        except EonError as e:
            self.assertTrue(e.code==409)
            self.assertTrue(e.message == "Another location already exists with the given data.")
        


if __name__ == '__main__':
    unittest.main()
