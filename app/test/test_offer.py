import datetime
import json
import unittest

from app.main.services import db
from app.main.model.quoterequest import QuoteRequest
from app.main.model.offer import Offer
from app.main.model.piece import Piece
from app.main.model.company import Company
from app.main.model.transportsegment import TransportSegment
from app.main.service.company_service import mark_company_as_node_owner, update_company
from app.main.service.quoterequest_service import (import_quote_request)
from app.main.service.offer_service import (_prepare_offer_export_payload, _check_offer, import_offer, accept_offer, register_offer_acceptance, update_offer)
from app.main.job.offer_job import work_offer_acceptance_response, work_offer_acceptance_excpetion
from app.main.util.keymanagementutils import KeyManagementClient
from app.main.util.hashutils import HashUtils
from app.test.controller_test_utils import (get_admin_token, register_company, register_location, register_shipment, register_piece_with_shipment, update_piece_with_shipment, register_planned_transport_segment, register_quote_request, update_quote_request, get_a_quote_request_via_api, register_offer, post_update_offer, get_single_offer)
from app.test.base import BaseTestCase
from app.test.data.eonpeers_payloads import offer_payload, qr_payload, qr_payload_2
from app.main.job.offer_job import work_send_offer_response, work_send_offer_excpetion
from app.main.util.eonerror import EonError
from app.test.commercial_test_utils import (create_qr_from_localKey_to_keyB, create_offer_from_b_to_local, create_qr_from_someKey_to_localKey)

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.serialization import load_pem_private_key
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.exceptions import InvalidSignature 

class TestOfferModel(BaseTestCase):

    DEBUG = False

    def test_offer_creation(self):
        """ 
        import a quote request, then create a matching offer, export the offer, then receive an offer acceptance

        Company B requests a quote to Company A. Company A replies by creating an Offer.

        Company A is the current node, Company B has a public key taken from the payloads.
        """
        #create the same companies

        admin_token = get_admin_token()
        key_b = ec.generate_private_key(ec.SECP256K1(), default_backend())   
        qr_from_some_key_to_local = create_qr_from_someKey_to_localKey(key_b)

        signature_failure=False
        try:
            import_quote_request(qr_from_some_key_to_local)
        except Exception as e:
            print(e)
            signature_failure=True
        self.assertTrue(signature_failure==False)

        #finally check quote and companies
        qr = QuoteRequest.query.filter_by(public_id=qr_from_some_key_to_local["payload"]["public_id"]).first()
        company_b = Company.query.filter_by(name="BBB").first()  
        self.assertTrue(qr.requester.public_id == company_b.public_id)

        #insert offer as the supplier company - controller test:
        offer_res = register_offer(self, qr.shipment_id, qr.transport_segment_id, qr.public_id, admin_token)
        offer_data = json.loads(offer_res.data.decode())
        self.assertTrue(offer_res.status_code==201)
        offer_id = offer_data['public_id']
        self.assertTrue(offer_id)

        offer = Offer.query.filter_by(public_id=offer_id).first()

        #test payload creation:
        offer = _check_offer(offer_id)
        offer_export_payload = _prepare_offer_export_payload(offer)
        kmc = KeyManagementClient()
        self.assertTrue(offer_export_payload["supplier_company_seralised_public_key"] == kmc.get_serialized_pub_key().decode('utf-8')) 
        #ABOUT KEYS: we cannot assume that the test key in the current node is always the same! Therefore 
        #we need to check that all tests don't assume the local pubkey

        #test the send offer job methods:
        fake_response = TestResponse("test response", 200)
        work_send_offer_response(fake_response, offer_id)
        offer = Offer.query.filter_by(public_id=offer_id).first() 
        self.assertTrue(offer.latest_log)
        fake_excp = EonError('Some excpetion', 500)
        work_send_offer_excpetion(fake_excp, offer_id)
        offer = Offer.query.filter_by(public_id=offer_id).first() 
        self.assertTrue("HTTPConnection" in offer.latest_log) #exception doesn't save any particular detail for now

    def test_import_offer(self):
        """
        Local node A creates the QR for B (with random key), B creates the offer (signed with random key)

        create QR and Offer with customs keys
        """
        #create locally the QR
        admin_token = get_admin_token()
        key_b = ec.generate_private_key(ec.SECP256K1(), default_backend())
        qr_from_local_to_b = create_qr_from_localKey_to_keyB(key_b)
        #Export QR tested in the other tests

        #import offer - build an offer on node with key B
        offer_from_b_to_local = create_offer_from_b_to_local(qr_from_local_to_b["payload"], key_b)
        signature_failure=False
        try:
            res = import_offer(offer_from_b_to_local) 
        except Exception as e:
            print(e)
            signature_failure=True
        self.assertTrue(signature_failure==False)

        offer = _check_offer(offer_from_b_to_local["payload"]["public_id"])
        self.assertTrue(offer)
        
        #call the accept offer:
        accept_response = accept_offer(offer_from_b_to_local["payload"]["public_id"])
        self.assertTrue(accept_response[1]==200)

        e = EonError('Some excehption.', 400)
        work_offer_acceptance_excpetion(e, offer_from_b_to_local["payload"]["public_id"])  
        offer = Offer.query.filter_by(public_id=offer_from_b_to_local["payload"]["public_id"]).first()  
        self.assertTrue("HTTPConnection error" in offer.latest_log)

        #this instead is the expected response if everything goes OK
        rpc_response_body = {
            'method':'offer.registeracceptance',
            'message':'Success, acceptance registered, please wait until we send the booking confirmation'
        }
        res = TestResponse(json.dumps(rpc_response_body),200)
        work_offer_acceptance_response(res, offer_from_b_to_local["payload"]["public_id"])
        offer = Offer.query.filter_by(public_id=offer_from_b_to_local["payload"]["public_id"]).first()  
        self.assertTrue("OK," in offer.latest_log)

        ##--- other POST and GET controller tests:
        #try to update the offer not being the supplier:
        tweak_offer = offer_from_b_to_local["payload"]
        tweak_offer["price_value"]=50
        post_res = post_update_offer(self, offer_from_b_to_local["payload"]["public_id"], 50, qr_from_local_to_b["payload"]["public_id"], admin_token)
        self.assertTrue(post_res.status_code==404)
        #try to get offer without being admin:
        get_res = get_single_offer(self, offer_from_b_to_local["payload"]["public_id"], "")
        self.assertTrue(get_res.status_code==401)


class TestResponse(object):
    text = ""
    status_code = 0

    # The class "constructor" - It's actually an initializer 
    def __init__(self, text, status_code):
        self.text = text
        self.status_code = status_code


if __name__ == '__main__':
    unittest.main()
