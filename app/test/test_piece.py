import datetime
import json
import unittest

from app.main.services import db
from app.main.model.piece import Piece
from app.main.model.product import Product
from app.main.model.productpiececomposition import ProductPieceComposition
from app.main.service.product_service import (save_new_product, get_a_product, update_product)
from app.main.service.piece_service import (save_new_piece, get_a_piece, upsert_piece)
from app.main.util.keymanagementutils import KeyManagementClient
from app.main.util.hashutils import HashUtils
from app.test.base import BaseTestCase
from app.test.controller_test_utils import (get_admin_token, register_company, register_product, update_product, register_piece, update_piece)
from app.main.util.eonerror import EonError

class TestPieceModel(BaseTestCase):

    DEBUG = False
    kmc = KeyManagementClient()

    def test_post_piece_and_update_product(self):
        """
        simple test of the endpoints, create new product, create a piece with that product, create another product, update the piece and check
        """
        admin_token = get_admin_token()
        #create product
        prod_a_res = register_product(self, None, None, "test description", "id 10983450", admin_token)
        self.assertTrue(prod_a_res.status_code == 201)
        prod_a_id = json.loads(prod_a_res.data.decode("utf-8"))["public_id"]
        prod = get_a_product(prod_a_id)

        #create a piece
        products = [
            {
            "commodity_code":"mandatory",
            "commodity_description":"mandatory",
            "commodity_type":"mandatory",
            "product_identifier":prod.product_identifier,
            "product_description":prod.product_description,
            "unique_hash":prod.unique_hash
            }
        ]
        piece_res = register_piece(self, "some test goods", products, None, admin_token)
        self.assertTrue(piece_res.status_code == 201)
        piece_id = json.loads(piece_res.data.decode("utf-8"))["public_id"]
        piece = get_a_piece(piece_id)
        self.assertTrue(piece.products[0].unique_hash == prod.unique_hash)

        #change the attached product
        prod_b_res = register_product(self, None, None, "test B description", "id 10983451", admin_token)
        self.assertTrue(prod_a_res.status_code == 201)
        prod_b_id = json.loads(prod_b_res.data.decode("utf-8"))["public_id"]
        prod_b = get_a_product(prod_b_id)
        products = [
            {
            "commodity_code":"mandatory",
            "commodity_description":"mandatory",
            "commodity_type":"mandatory",
            "product_identifier":prod_b.product_identifier,
            "product_description":prod_b.product_description,
            "unique_hash":prod_b.unique_hash
            }
        ]
        piece_res = update_piece(self, piece_id, "some test goods", products, None, admin_token)
        self.assertTrue(piece_res.status_code == 200)
        piece_id = json.loads(piece_res.data.decode("utf-8"))["public_id"]
        piece = get_a_piece(piece_id)
        self.assertTrue(len(piece.products)==1)
        self.assertTrue(piece.products[0].unique_hash != prod.unique_hash)

    def test_against_basic_loops(self):
        """
        simple test of the endpoints, create new product, create a piece with that product, create another product, update the piece and check
        """
        #create product
        admin_token = get_admin_token()
        prod_a_res = register_product(self, None, None, "test description", "id 10983450", admin_token)
        self.assertTrue(prod_a_res.status_code == 201)
        prod_a_id = json.loads(prod_a_res.data.decode("utf-8"))["public_id"]
        prod = get_a_product(prod_a_id)

        #create a piece
        products = [
            {
            "commodity_code":"mandatory",
            "commodity_description":"mandatory",
            "commodity_type":"mandatory",
            "product_identifier":prod.product_identifier,
            "product_description":prod.product_description,
            "unique_hash":prod.unique_hash
            }
        ]
        piece_res = register_piece(self, "some test goods", products, None, admin_token)
        self.assertTrue(piece_res.status_code == 201)
        piece_id = json.loads(piece_res.data.decode("utf-8"))["public_id"]
        piece_res_2 = update_piece(self, piece_id, "some test goods 2", products, piece_id, admin_token)
        self.assertTrue(piece_res_2.status_code == 409)
        self.assertTrue(json.loads(piece_res_2.data.decode("utf-8"))["message"] == "A piece cannot be the parent to itself")


if __name__ == '__main__':
    unittest.main()