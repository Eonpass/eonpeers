import datetime
import json
import unittest

from app.main.services import db
from app.main.model.company import Company
from app.main.model.piece import Piece
from app.main.model.product import Product
from app.main.model.productpiececomposition import ProductPieceComposition
from app.main.service.company_service import (save_new_company, get_a_company, get_branded_products_of_a_company)
from app.main.service.product_service import (save_new_product, get_a_product, update_product)
from app.main.service.piece_service import (save_new_piece, get_a_piece)
from app.main.job.company_job import compare_new_company
from app.main.util.keymanagementutils import KeyManagementClient
from app.main.util.hashutils import HashUtils
from app.test.base import BaseTestCase
from app.test.controller_test_utils import (get_admin_token, register_company, register_product, update_product)
from app.main.util.eonerror import EonError

class TestTransportSegmentModel(BaseTestCase):

    DEBUG = False
    kmc = KeyManagementClient()

    def test_post_product_and_update(self):
        """
        simple test of the endpoints, create new product, try to insert duplicate, then update original product, test with a non existing brand owner first
        """
        admin_token = get_admin_token()
        #create product
        prod_a_res = register_product(self, None, None, 'test description', 'id 10983450', admin_token)
        self.assertTrue(prod_a_res.status_code == 201)
        prod_a_id = json.loads(prod_a_res.data.decode("utf-8"))["public_id"]

        #try to create duplicate
        prod_adup_res = register_product(self, None, None, 'test description', 'id 10983450', admin_token)
        self.assertTrue(prod_adup_res.status_code == 409)

        #update with a non existing company
        prod_a_upd_res = update_product(self, prod_a_id, '123companyid', None, 'test description', 'id 10983450', admin_token)
        self.assertTrue(prod_a_upd_res.status_code == 404)

        #create the company and update the product
        compRes_initial = register_company(self,'test','1234567890', admin_token)
        correct_company_id = json.loads(compRes_initial.data.decode("utf-8"))["public_id"]
        prod_a_upd_res = update_product(self, prod_a_id, correct_company_id, None, 'test description', 'id 10983450', admin_token)
        self.assertTrue(prod_a_upd_res.status_code == 200)
        prod = get_a_product(prod_a_id)
        #test also product query for a company:
        branded_products = get_branded_products_of_a_company(correct_company_id)
        self.assertTrue(len(branded_products)==1)

    def test_product_rels_with_company(self):
        """
        Create two companies for the same entity and attach the products to one. Then simulate that the entity comes online and align the database.

        This means that the local copy with the wrong VAT is destroyed when aligned with the remote data, therefore the product should move to the other company
        """
        admin_token = get_admin_token()
        #create a company with the right VAT 1234567890
        compRes_initial = register_company(self,'test','1234567890', admin_token)
        correct_company_id = json.loads(compRes_initial.data.decode("utf-8"))["public_id"]

        #create a company with the wrong VAT and attach the product
        compRes = register_company(self,'testCopy','12345678XX', admin_token)
        company_id = json.loads(compRes.data.decode("utf-8"))["public_id"]
        product_data = {
            "commodity_code":"AAA",
            "commodity_description":"prova",
            "commodity_name":"prova",
            "commodity_type":"prova",
            "manufacturer_company_id":company_id,
            "product_description":"descrizione",
            "product_identifier":"123",
            "un_number":"1A2UD"
        }
        prod_res = save_new_product(product_data)
        prod = get_a_product(prod_res[0]["public_id"])
        hu = HashUtils()
        payload = product_data['product_description']+product_data['product_identifier']
        unique_hash = hu.digest(payload).hex()
        self.assertTrue(prod.unique_hash==unique_hash)

        #assume some time passes and finally the node of the company comes up, so we can validate the database:
        #use compare_new_company from the tasks
        remote_A = {
            'public_id':'test1234567test',
            'name':'test_company',
            'vat_number':'1234567890',
            'base_url':'http://localhost:5400',
            'public_key':'123456787',
            'fiscal_country':'IT',
            'fiscal_zip':'38852'
        }
        compare_new_company(company_id, remote_A)
        company_wrong = get_a_company(company_id)
        self.assertTrue(company_wrong == None)

        company_fixed = get_a_company(correct_company_id)
        self.assertTrue(len(company_fixed.manufactured_products)>0)

if __name__ == '__main__':
    unittest.main()