import datetime
import json
import unittest
import copy

from app.main.services import db
from app.main.model.quoterequest import QuoteRequest
from app.main.model.piece import Piece
from app.main.model.transportsegment import TransportSegment
from app.main.model.dangerousgood import DangerousGood
from app.main.model.customsinfo import CustomsInfo
from app.main.service.company_service import mark_company_as_node_owner, save_new_company
from app.main.service.quoterequest_service import (save_new_quote_request, get_a_quote_request, _check_quote_request, export_quote_request, _prepare_export_payload, import_quote_request)
from app.main.util.keymanagementutils import KeyManagementClient
from app.main.util.hashutils import HashUtils
from app.test.controller_test_utils import (get_admin_token, register_company, register_location, register_shipment, update_shipment, register_piece_with_shipment, update_piece_with_shipment, register_planned_transport_segment, register_quote_request, update_quote_request, get_a_quote_request_via_api, register_dangerousgood, register_customsinfo)
from app.test.base import BaseTestCase
from app.test.data.eonpeers_payloads import qr_payload, qr_payload_2, qr_payload_3, qr_payload_error
from app.main.job.quote_request_job import work_send_quoterequest_response, work_send_quoterequest_excpetion
from app.main.util.eonerror import EonError
from app.test.commercial_test_utils import (create_qr_from_localKey_to_keyB, create_qr_from_someKey_to_localKey, create_offer_from_b_to_local, apply_qr_change_on_payload_from_someKey)

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.serialization import load_pem_private_key
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.exceptions import InvalidSignature 


class TestQuoteRequestModel(BaseTestCase):

    DEBUG = False
    kmc = KeyManagementClient()
    
    def test_create_quote_request(self):
        """
        create a shipment and a connected piece, create the related tranposrt segument. 
        then create the quote request and test the JSON body and the relationships
        """
        admin_token = get_admin_token()
        company_b_res = register_company(self, "BBB", "4321", admin_token)
        company_b_data = json.loads(company_b_res.data.decode())
        company_b_id = company_b_data['public_id']
        mark_company_as_node_owner(company_b_id)
        
        shipment_res = register_shipment(self,"some goods", admin_token)
        shipment_data = json.loads(shipment_res.data.decode())
        shipment_id = shipment_data['public_id']

        piece_res = register_piece_with_shipment(self, "some goods", None, shipment_id, admin_token)
        piece_id = json.loads(piece_res.data.decode("utf-8"))["public_id"]

        loc_a_res = register_location(self, "A", "AAA", "Warehouse", admin_token)
        loc_a_id = json.loads(loc_a_res.data.decode("utf-8"))["public_id"]

        loc_b_res = register_location(self, "B", "BBB", "Warehouse", admin_token)
        loc_b_id = json.loads(loc_b_res.data.decode("utf-8"))["public_id"]

        now = datetime.datetime.utcnow()
        date_string = now.strftime("%Y-%m-%d %H:%M:%S.%f")
        ts_res = register_planned_transport_segment(self, loc_a_id, loc_b_id, piece_id, date_string, admin_token)
        ts_id = json.loads(ts_res.data.decode("utf-8"))["public_id"]

        quote_request_res = register_quote_request(self, shipment_id, ts_id, None, None, admin_token)
        quote_request_id = json.loads(quote_request_res.data.decode("utf-8"))["public_id"]
        self.assertTrue(quote_request_res.status_code, 201)

        #test the JSON output
        quote_request_res = get_a_quote_request_via_api(self, quote_request_id, admin_token)
        qr_body = json.loads(quote_request_res.data.decode("utf-8"))
        self.assertTrue(qr_body["shipment_id"]==shipment_id)

        #test the inner relationships:
        qr = get_a_quote_request(quote_request_id)
        self.assertTrue(qr.shipment.public_id == shipment_id)
        self.assertTrue(qr.transport_segment.public_id == ts_id)

        #remove shipment relationships: no shipment, no companies, etc..
        qr_updated_res = update_quote_request(self, quote_request_id, None, None, None, None, admin_token)
        self.assertTrue(qr_updated_res.status_code, 200)
        qr = get_a_quote_request(quote_request_id)
        self.assertTrue(qr.shipment == None)

        try:
            qr_status = _check_quote_request(quote_request_id)
        except Exception as e:
            print(e)
            self.assertTrue(e.message == "Quote Request missing supplier company.")


    def test_create_quotes_with_companies(self):
        """
        create a company and try to insert and trigger errors: same supplier-requester, double quote on same supplier, etc..
        """
        admin_token = get_admin_token()
        company_a_res = register_company(self, "AAA", "1234", admin_token)
        company_data = json.loads(company_a_res.data.decode())
        company_id = company_data['public_id']

        company_b_res = register_company(self, "BBB", "4321", admin_token)
        company_b_data = json.loads(company_b_res.data.decode())
        company_b_id = company_b_data['public_id']
        mark_company_as_node_owner(company_b_id)

        shipment_res = register_shipment(self,"some goods", admin_token)
        shipment_data = json.loads(shipment_res.data.decode())
        shipment_id = shipment_data['public_id']

        piece_res = register_piece_with_shipment(self, "some goods", None, shipment_id, admin_token)
        piece_id = json.loads(piece_res.data.decode("utf-8"))["public_id"]

        loc_a_res = register_location(self, "A", "AAA", "Warehouse", admin_token)
        loc_a_id = json.loads(loc_a_res.data.decode("utf-8"))["public_id"]

        loc_b_res = register_location(self, "B", "BBB", "Warehouse", admin_token)
        loc_b_id = json.loads(loc_b_res.data.decode("utf-8"))["public_id"]

        now = datetime.datetime.utcnow()
        date_string = now.strftime("%Y-%m-%d %H:%M:%S.%f")
        ts_res = register_planned_transport_segment(self, loc_a_id, loc_b_id, piece_id, date_string, admin_token)
        ts_id = json.loads(ts_res.data.decode("utf-8"))["public_id"]

        #test requester cannot be supplier
        quote_request_res = register_quote_request(self, shipment_id, ts_id, company_id, company_id, admin_token)
        self.assertTrue(quote_request_res.status_code, 409)

        #test duplicate quote
        quote_request_res = register_quote_request(self, shipment_id, ts_id, company_id, company_b_id, admin_token)
        quote_request_id = json.loads(quote_request_res.data.decode("utf-8"))["public_id"]
        self.assertTrue(quote_request_res.status_code, 201)
        quote_request_res = register_quote_request(self, shipment_id, ts_id, company_id, company_b_id, admin_token)
        self.assertTrue(quote_request_res.status_code, 409)
        #test check status
        qr_status = _check_quote_request(quote_request_id)
        self.assertTrue(qr_status)

        #test duplicate empty supplier and requester
        quote_request_res = register_quote_request(self, shipment_id, ts_id, None, None, admin_token)
        self.assertTrue(quote_request_res.status_code, 201)
        quote_request_res = register_quote_request(self, shipment_id, ts_id, None, None, admin_token)
        self.assertTrue(quote_request_res.status_code, 409)


    def test_check_status(self):
        """
        Test various scenarios of failure for check_status
        """
        admin_token = get_admin_token()
        company_a_res = register_company(self, "AAA", "1234", admin_token)
        company_data = json.loads(company_a_res.data.decode())
        company_id = company_data['public_id']

        company_b_res = register_company(self, "BBB", "4321", admin_token)
        company_b_data = json.loads(company_b_res.data.decode())
        company_b_id = company_b_data['public_id']
        #make sure this is the node owner
        mark_company_as_node_owner(company_b_id)

        shipment_res = register_shipment(self,"some goods", admin_token)
        shipment_data = json.loads(shipment_res.data.decode())
        shipment_id = shipment_data['public_id']

        piece_res = register_piece_with_shipment(self, "some goods", None, shipment_id, admin_token)
        piece_id = json.loads(piece_res.data.decode("utf-8"))["public_id"]

        loc_a_res = register_location(self, "A", "AAA", "Warehouse", admin_token)
        loc_a_id = json.loads(loc_a_res.data.decode("utf-8"))["public_id"]

        loc_b_res = register_location(self, "B", "BBB", "Warehouse", admin_token)
        loc_b_id = json.loads(loc_b_res.data.decode("utf-8"))["public_id"]

        now = datetime.datetime.utcnow()
        date_string = now.strftime("%Y-%m-%d %H:%M:%S.%f")
        ts_res = register_planned_transport_segment(self, loc_a_id, loc_b_id, piece_id, date_string, admin_token)
        ts_id = json.loads(ts_res.data.decode("utf-8"))["public_id"]

        #test missing shipment id
        quote_request_res = register_quote_request(self, None, ts_id, company_id, company_b_id, admin_token)
        self.assertTrue(quote_request_res.status_code==400)
        self.assertTrue(json.loads(quote_request_res.data.decode("utf-8"))["message"]=="Input payload validation failed")

        #test missing requester
        quote_request_res = register_quote_request(self, shipment_id, ts_id, company_id, None, admin_token)
        quote_request_id = json.loads(quote_request_res.data.decode("utf-8"))["public_id"]
        try:
            qr_status = _check_quote_request(quote_request_id)
        except Exception as e:
            self.assertTrue(e.message == "Quote Request missing requester company.")

        #test shipment without piece
        quote_request_res = update_quote_request(self, quote_request_id, shipment_id, ts_id, company_id, company_b_id, admin_token)
        piece_res = update_piece_with_shipment(self, piece_id, "goods_description", None, None, admin_token)
        try:
            qr_status = _check_quote_request(quote_request_id)
        except Exception as e:
            self.assertTrue(e.message == "Shipment is missing attached pieces.")

    def test_export_quote_request(self):
        """
        test che export function of a Quote Request, this test can be used to create payloads for other tests 
        """
        admin_token = get_admin_token()
        company_a_res = register_company(self, "AAA", "1234", admin_token)
        company_data = json.loads(company_a_res.data.decode())
        company_id = company_data['public_id']

        company_b_res = register_company(self, "BBB", "4321", admin_token)
        company_b_data = json.loads(company_b_res.data.decode())
        company_b_id = company_b_data['public_id']
        #make sure this is the node owner
        mark_company_as_node_owner(company_b_id)


        shipment_res = register_shipment(self,"some goods", admin_token)
        shipment_data = json.loads(shipment_res.data.decode())
        shipment_id = shipment_data['public_id']

        piece_res = register_piece_with_shipment(self, "some goods", None, shipment_id, admin_token)
        piece_id = json.loads(piece_res.data.decode("utf-8"))["public_id"]

        #add customs info and dangerous goods
        dg_res = register_dangerousgood(self, "asd", piece_id, admin_token)
        dg_id = json.loads(dg_res.data.decode("utf-8"))["public_id"]
        ci_res = register_customsinfo(self, "asd", piece_id, admin_token)
        ci_id = json.loads(ci_res.data.decode("utf-8"))["public_id"]

        #setup transport segment
        loc_a_res = register_location(self, "A", "AAA", "Warehouse", admin_token)
        loc_a_id = json.loads(loc_a_res.data.decode("utf-8"))["public_id"]
        loc_b_res = register_location(self, "B", "BBB", "Warehouse", admin_token)
        loc_b_id = json.loads(loc_b_res.data.decode("utf-8"))["public_id"]
        now = datetime.datetime.utcnow()
        date_string = now.strftime("%Y-%m-%d %H:%M:%S.%f")
        ts_res = register_planned_transport_segment(self, loc_a_id, loc_b_id, piece_id, date_string, admin_token)
        ts_id = json.loads(ts_res.data.decode("utf-8"))["public_id"]

        quote_request_res = register_quote_request(self, shipment_id, ts_id, company_id, company_b_id, admin_token)
        quote_request_id = json.loads(quote_request_res.data.decode("utf-8"))["public_id"]
        qr = QuoteRequest.query.filter_by(public_id=quote_request_id).first()  
        qr_v1 = ""
        try:
            #since the target company has bogus endpoint, this should be  Failed to establish a new connection: [Errno 61] Connection refused?
            qr_v1 = export_quote_request(quote_request_id)
        except Exception as e:
            #during tests the actual callout is blocked
            db.session.rollback() 

        #update shipment description and test that the export payload for the shipment is different
        updatedshp = update_shipment(self, shipment_id, "some new description for shipment", admin_token)
        qr_v2 = ""
        try:
            #since the target company has bogus endpoint, this should be  Failed to establish a new connection: [Errno 61] Connection refused?
            qr_v2 = export_quote_request(quote_request_id)
        except Exception as e:
            #during tests the actual callout is blocked
            db.session.rollback() 

        self.assertTrue(qr_v1["payload"]["shipment"]["origin_hash"]!=qr_v2["payload"]["shipment"]["origin_hash"])

        #test job
        fake_response = MockResponse(200)
        work_send_quoterequest_response(fake_response, quote_request_id)
        qr = QuoteRequest.query.filter_by(public_id=quote_request_id).first()  
        self.assertTrue(qr.latest_log)
        fake_excp = EonError('Some excpetion', 500)
        work_send_quoterequest_excpetion(fake_excp, quote_request_id)
        qr = QuoteRequest.query.filter_by(public_id=quote_request_id).first() 
        self.assertTrue("HTTPConnection" in qr.latest_log) #exception doesn't save any particular detail for now


    def test_import(self):
        """
        Import quotes and test the imported DB structure and some error scenarios
        """
        admin_token = get_admin_token()
        key_b = ec.generate_private_key(ec.SECP256K1(), default_backend())   
        #import QR from another key made toward local key, i.e. local node acts as SUPPLIER
        signature_failure=False
        qr_from_some_key_to_local={}
        qr_buffer={}
        qr_buffer_2={}
        try:
            #create QR from local node (BBB) to target note AAA with a special pubKey
            #create 2, one with a small change that will be used later on to update
            qr_from_some_key_to_local = create_qr_from_someKey_to_localKey(key_b) #note: this function also clears the DB before returning the json           
            qr_buffer = copy.deepcopy(qr_from_some_key_to_local)
            qr_buffer_2 = copy.deepcopy(qr_from_some_key_to_local)
            import_quote_request(qr_from_some_key_to_local) #this is a json payload imported from file //TODO: use the special function to create QR and Offers
        except Exception as e:
            print(e)
            signature_failure=True
        self.assertTrue(signature_failure==False)

        #as long as the signature does not fail, I should have 1 piece here with its products (if any)
        piece = Piece.query.filter_by(public_id=qr_from_some_key_to_local["payload"]["shipment"]["pieces"][0]["public_id"]).first()
        self.assertTrue(piece.public_id==qr_from_some_key_to_local["payload"]["shipment"]["pieces"][0]["public_id"])
        self.assertTrue(piece.transport_segments[0].public_id == qr_from_some_key_to_local["payload"]["shipment"]["pieces"][0]["transport_segments"][0]["public_id"])
        self.assertTrue(piece.shipment.public_id == qr_from_some_key_to_local["payload"]["shipment"]["public_id"])
        #check the transport segment and its locations
        ts = TransportSegment.query.filter_by(public_id = qr_from_some_key_to_local["payload"]["shipment"]["pieces"][0]["transport_segments"][0]["public_id"]).first()
        self.assertTrue(ts)
        self.assertTrue(ts.arrival_location.name=="JFK A")
        #finally check quote and companies
        qr = QuoteRequest.query.filter_by(public_id=qr_from_some_key_to_local["payload"]["public_id"]).first()
        self.assertTrue(qr.supplier)
        self.assertTrue(qr.requester)

        #test an update on this quote:
        signature_failure=False
        updated_qr={}
        try:
            updated_qr = apply_qr_change_on_payload_from_someKey(qr_buffer, key_b, "2e per km", "goods 2")
            import_quote_request(updated_qr)
        except Exception as e:
            print(e)
            signature_failure=True
        self.assertTrue(signature_failure==False)
        qr = QuoteRequest.query.filter_by(public_id=qr_from_some_key_to_local["payload"]["public_id"]).first()
        self.assertTrue(qr.rating_preferences=="2e per km")

        #test a wrong overall signature
        signature_failure=False
        try:
            qr_buffer_2["signed_hash"]="304502207fd74a09d284f0b78ff99ca39a528ae5bb05172bf43499f6dc1b6d17d8eb1b8c021100be741dab97a80f80b740b9d27af38813a43fb0d7f69f5336bc39f70b369b66f3"
            import_quote_request(qr_buffer_2)
        except Exception as e:
            signature_failure=True
            self.assertTrue(e.message=="Overall payload signed message is corrupt")
        self.assertTrue(signature_failure)

        #TODO: test complex import (multiple customs info, dangerous goods, peices, etc..) .. update function in commercial_test_utils to create also additional items


class MockResponse:
    def __init__(self, status_code):
        self.status_code = status_code
        self.text = "test response"


if __name__ == '__main__':
    unittest.main()
