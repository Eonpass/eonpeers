import datetime
import json
import unittest
import copy

from app.main.services import db
from app.main.model.quoterequest import QuoteRequest
from app.main.model.piece import Piece
from app.main.model.company import Company
from app.main.model.transportsegment import TransportSegment
from app.main.model.dangerousgood import DangerousGood
from app.main.model.customsinfo import CustomsInfo
from app.main.service.company_service import mark_company_as_node_owner
from app.main.service.quoterequest_service import (save_new_quote_request, get_a_quote_request, _check_quote_request, export_quote_request, _prepare_export_payload, import_quote_request, prepare_quote_request_for_subcontractor_all_pieces)
from app.main.service.location_service import (save_new_location, upsert_a_location, get_a_location, get_all_locations )
from app.main.util.keymanagementutils import KeyManagementClient
from app.main.util.hashutils import hu
from app.test.controller_test_utils import (get_admin_token, register_company, register_location, register_shipment, register_piece_with_shipment, update_piece_with_shipment, register_planned_transport_segment, register_quote_request, update_quote_request, get_a_quote_request_via_api, register_dangerousgood, register_customsinfo)
from app.test.base import BaseTestCase
from app.test.data.eonpeers_payloads import qr_payload, qr_payload_2, qr_payload_3, qr_payload_error
from app.main.job.quote_request_job import work_send_quoterequest_response, work_send_quoterequest_excpetion
from app.test.commercial_test_utils import (create_qr_from_localKey_to_keyB, create_offer_from_b_to_local, create_qr_from_someKey_to_localKey)

from app.main.util.eonerror import EonError

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.serialization import load_pem_private_key
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.exceptions import InvalidSignature 

class TestSubcontractorsQuoteRequestModel(BaseTestCase):

    DEBUG = False
    kmc = KeyManagementClient()

    def test_subcontractors_generation(self):
        """
        Improt quotes and test the imported DB structure and some error scenarios

        Local node is the FF, company_C is the carrier. FF receives a quote, then creates a quote for the carrier
        """
        admin_token = get_admin_token()
        #create the companies, one with a given key B which will be the client sending us a quote request
        key_b = ec.generate_private_key(ec.SECP256K1(), default_backend())   
        qr_from_some_key_to_local = create_qr_from_someKey_to_localKey(key_b)
        #create the subcontractor:
        company_c_res = register_company(self, "SUB", "0001", admin_token)
        company_c_data = json.loads(company_c_res.data.decode())
        company_c_id = company_c_data['public_id']

        #import quote request
        signature_failure=False
        try:
            import_quote_request(qr_from_some_key_to_local) #this is a json payload
        except Exception as e:
            signature_failure=True
        self.assertTrue(signature_failure==False)

        qr = QuoteRequest.query.filter_by(public_id=qr_from_some_key_to_local["payload"]["public_id"]).first()

        #create the new locations for the new TS
        data = {
            "name":'Fiumicino Aeroporto',
            "location_code":'FCO',
            "location_type":'Airport',
            "city_code" : 'RM',
            "city_name": 'Roma',
            "country": 'IT',
            "postal_code":'00054',
            "street" : 'Aeroporto di Roma Fiumicino'
        }
        res_l_1 = save_new_location(data)
        data = {
            "name":'John F. Kennedy International Airport',
            "location_code":'JFK',
            "location_type":'Airport',
            "city_code" : 'NYC',
            "city_name": 'New York',
            "country": 'US',
            "postal_code":'11430',
            "street" : 'John F Kennedy Airport'
        }
        res_l_2 = save_new_location(data)
        new_transportsegment = {
            "arrival_location_id":res_l_1[0]["public_id"],
            "co2_calculation_method":"ballpark",
            "co2_emission_value":10.03,
            "co2_emission_unit":"Kg/m3",
            "departure_location_id":res_l_2[0]["public_id"],
            "fuel_type":"Kerosene",
            "mode_code":"Air Transport",
            "movement_type":"Planned",
            "seal":"22342345",
            "segment_level":"Flight Leg",
            "transport_date":datetime.datetime.utcnow()
        }
        new_ts_payload = {
            "new_transport_segment":new_transportsegment,
            "supplier_id": company_c_id
        }

        res_new_quote_sub = prepare_quote_request_for_subcontractor_all_pieces(new_ts_payload, qr.public_id)

        qr_sub = QuoteRequest.query.filter_by(public_id=res_new_quote_sub[0]["public_id"]).first()

        self.assertTrue(qr_sub)
        company_a = Company.query.filter_by(name="AAA").first()  
        self.assertTrue(qr_sub.requester_company_id == company_a.public_id) #the node owner is asking to the subcontractor C to quote this trip
        self.assertTrue(qr_sub.supplier_company_id == company_c_id)

        #check that the export payload of qr2 contains the shipment signed data
        qr_sub2 = _check_quote_request(res_new_quote_sub[0]["public_id"])
        export_payload_sub = _prepare_export_payload(qr_sub2)
        self.assertTrue(export_payload_sub["payload"]["shipment"]["origin_hash"]==qr_from_some_key_to_local["payload"]["shipment"]["origin_hash"])
        self.assertTrue(export_payload_sub["payload"]["shipment"]["originator_pubkey"]==qr_from_some_key_to_local["payload"]["shipment"]["originator_pubkey"])
        self.assertTrue(export_payload_sub["payload"]["shipment"]["signed_origin_hash"]==qr_from_some_key_to_local["payload"]["shipment"]["signed_origin_hash"])
        #check all other shipment fields?



        #run the validation on the carrier's node:
        #check shipment origin proof:
        shipment_api_object = copy.deepcopy(export_payload_sub["payload"]["shipment"])
        signed_bytes = bytes.fromhex(shipment_api_object["signed_origin_hash"])
        original_message = shipment_api_object["origin_hash"]
        for piece in shipment_api_object["pieces"]:
            piece.pop("transport_segments", None)
        shipment_api_object.pop("transport_segments", None)
        shipment_api_object.pop("origin_hash", None)
        shipment_api_object.pop("signed_origin_hash", None)
        message = (hu.digest(json.dumps(shipment_api_object, sort_keys=True)).hex())
        #check the origin_hash is actually describing the shipment exported to the carrier
        self.assertTrue(message==original_message) 

        #delete quote request, switch node owner to node C
        db.session.delete(qr_sub)
        current_admin = Company.query.filter_by(is_own=True).first()
        current_admin.is_own=False
        db.session.add(current_admin)
        new_admin = Company.query.filter_by(public_id=company_c_id).first()
        new_admin.is_own=True
        db.session.add(new_admin)
        db.session.commit()
        db.session.flush()
        #import quote request
        signature_failure=False
        try:
            import_quote_request(export_payload_sub) 
        except Exception as e:
            print(e)
            signature_failure=True
        self.assertTrue(signature_failure==False)

class MockResponse:
    def __init__(self, status_code):
        self.status_code = status_code


if __name__ == '__main__':
    unittest.main()
