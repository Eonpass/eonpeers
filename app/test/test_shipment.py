import datetime
import json
import unittest

from app.main.services import db
from app.main.model.shipment import Shipment
from app.main.model.disclosure import Disclosure
from app.main.service.shipment_service import (save_new_shipment, get_a_shipment, delete_shipment, get_all_shipments, prepare_origin_shipment_payload)
from app.main.service.disclosure_service import save_new_disclosure, update_disclosure, get_a_disclosure
from app.main.service.piece_service import get_a_piece
from app.main.service.user_service import get_all_users
from app.main.util.keymanagementutils import KeyManagementClient
from app.main.service.user_service import get_all_users, get_public_id_from_id
from app.main.util.hashutils import HashUtils
from app.test.controller_test_utils import (get_admin_token, setup_user, register_shipment, update_shipment, register_piece, update_piece, get_a_piece_via_api, register_piece_with_shipment, update_piece_with_shipment, register_planned_transport_segment, get_a_shipment_via_api, delete_a_shipment_via_api)
from app.test.base import BaseTestCase
from app.test.test_auth import login_custom_user



class TestShipmentModel(BaseTestCase):

    DEBUG = False
    kmc = KeyManagementClient()
    
    def test_create_shipment_with_piece(self):
        """
        create a shipment and a connected piece, from the shipment get the pieces and test the relationship, then update the shipment and test update
        """
        admin_token = get_admin_token()
        shipment_res = register_shipment(self,"some goods",admin_token)
        shipment_data = json.loads(shipment_res.data.decode())
        self.assertTrue(shipment_data['public_id'])
        shipment_id = shipment_data['public_id']
        self.assertTrue(shipment_res.content_type == 'application/json')
        self.assertEqual(shipment_res.status_code, 201)

        piece_res = register_piece_with_shipment(self, "some goods", None, shipment_id, admin_token)
        piece_id = json.loads(piece_res.data.decode("utf-8"))["public_id"]

        shipment = get_a_shipment(shipment_id)
        self.assertTrue(len(shipment.pieces)==1)

        auth_token, user_id = setup_user()
        u_public_id = get_public_id_from_id(user_id)
        save_new_disclosure({'user_id':u_public_id, "shipment_id":shipment_id})
        upd_payload = {
            "user_id":u_public_id,
            "shipment_id":shipment_id,
            "approved":True
        }
        update_disclosure(upd_payload)

        shipment_res = get_a_shipment_via_api(self, shipment_id, auth_token)
        shipment_api_object = json.loads(shipment_res.data.decode("utf-8"))
        self.assertTrue(shipment_api_object["pieces"][0]["public_id"] == piece_id)
        self.assertTrue(shipment_api_object["originator_pubkey"])

        shipment_res = update_shipment(self,shipment_id,"some goods edit", admin_token)
        self.assertTrue(shipment_res.status_code==200)
        shipment_res = get_a_shipment_via_api(self, shipment_id, auth_token)
        shipment_api_object = json.loads(shipment_res.data.decode("utf-8"))
        self.assertTrue(shipment_api_object["goods_description"] == "some goods edit")

        #delete the shipment and see if pieces, disclosures and products are there?
        del_service_res = delete_a_shipment_via_api(self, shipment_id, admin_token)
        self.assertTrue(del_service_res.status_code==200)
        #check that cascade-delete worked:
        disclosure = Disclosure.query.filter_by(user_id=u_public_id, shipment_id=shipment_id).first()
        self.assertFalse(disclosure)

        piece = get_a_piece(piece_id)
        #TODO: at the moment piece are not in cascade-delete, decide the strategy, maybe two calls - one to destroy only shipment, one to destroy also pieces


    def test_shipment_with_nested_pieces(self):
        """ 
        create 2 pieces, one nested into the other, then try to attach the nested piece to the shipment, then un-nest and attach both, test the relationship from shipment
        """
        admin_token = get_admin_token()
        auth_token, user_id = setup_user()
        u_public_id = get_public_id_from_id(user_id)
        piece_1_res = register_piece(self, "some goods", None, None, admin_token)
        piece_1_data = json.loads(piece_1_res.data.decode())
        piece_1_id = piece_1_data['public_id']

        piece_2_res = register_piece(self, "some goods with details", None, piece_1_id, admin_token)
        piece_2_data = json.loads(piece_2_res.data.decode())
        piece_2_id = piece_2_data['public_id']

        shipment_res = register_shipment(self,"some goods", admin_token)
        shipment_data = json.loads(shipment_res.data.decode())
        shipment_id = shipment_data['public_id']
        save_new_disclosure({'user_id':u_public_id, "shipment_id":shipment_id})
        upd_payload = {
            "user_id":u_public_id,
            "shipment_id":shipment_id,
            "approved":True
        }
        update_disclosure(upd_payload)

        #check the error
        piece_2_upd_res = update_piece_with_shipment(self, piece_2_id, "new detailed description", None, shipment_id, admin_token)
        self.assertEqual(piece_2_upd_res.status_code, 409)
        piece_2_upd_data = json.loads(piece_2_upd_res.data.decode())
        self.assertEqual(piece_2_upd_data["message"], 'Shipment can be connected only to the top Piece, not to a nested Piece')

        #remove parent_piece
        piece_2_upd_res = update_piece(self, piece_2_id, "new detailed description", None, None, admin_token)
        self.assertEqual(piece_2_upd_res.status_code, 200)
        
        #connect the pieces to the shipment
        piece_2_upd_res = update_piece_with_shipment(self, piece_2_id, "new detailed description", None, shipment_id, admin_token)
        piece_1_upd_res = update_piece_with_shipment(self, piece_1_id, "some new description", None, shipment_id, admin_token)

        #check
        piece_2_res = get_a_piece_via_api(self, piece_2_id, admin_token)
        piece_2_upd_data = json.loads(piece_2_res.data.decode())
        self.assertTrue(piece_2_upd_data["shipment_id"], shipment_id)
        shipment_res = get_a_shipment_via_api(self, shipment_id, auth_token)
        shipment_api_object = json.loads(shipment_res.data.decode("utf-8"))
        self.assertTrue(len(shipment_api_object["pieces"]), 2)

    def test_shipment_contraints(self):
        admin_token = get_admin_token()
        auth_token, user_id = setup_user()
        u_public_id = get_public_id_from_id(user_id)
        shipment_res = register_shipment(self,"some goods", admin_token)
        shipment_data = json.loads(shipment_res.data.decode())
        self.assertTrue(shipment_data['public_id'])
        shipment_id = shipment_data['public_id']
        self.assertTrue(shipment_res.content_type == 'application/json')
        self.assertEqual(shipment_res.status_code, 201)
        save_new_disclosure({'user_id':u_public_id, "shipment_id":shipment_id})

        shipment_res = register_shipment(self,"some goods 2", admin_token)
        shipment_data = json.loads(shipment_res.data.decode())
        self.assertTrue(shipment_data['public_id'])
        shipment_id2 = shipment_data['public_id']
        self.assertTrue(shipment_res.content_type == 'application/json')
        self.assertEqual(shipment_res.status_code, 201)
        self.assertTrue(shipment_id!=shipment_id2)
        #these two should both succeed even if they have empty origin_hash (two None in a Unique column)

    def test_shipment_origin(self):
        admin_token = get_admin_token()
        auth_token, user_id = setup_user()
        u_public_id = get_public_id_from_id(user_id)
        shipment_res = register_shipment(self,"some goods", admin_token)
        shipment_data = json.loads(shipment_res.data.decode())
        self.assertTrue(shipment_data['public_id'])
        shipment_id = shipment_data['public_id']
        self.assertTrue(shipment_res.content_type == 'application/json')
        self.assertEqual(shipment_res.status_code, 201)
        save_new_disclosure({'user_id':u_public_id, "shipment_id":shipment_id})
        upd_payload = {
            "user_id":u_public_id,
            "shipment_id":shipment_id,
            "approved":True
        }
        update_disclosure(upd_payload)

        prepare_origin_shipment_payload(shipment_id)
        shipment_res = get_a_shipment_via_api(self, shipment_id, auth_token)
        shipment_api_object = json.loads(shipment_res.data.decode("utf-8"))
        self.assertTrue(shipment_api_object["origin_hash"])
        self.assertTrue(shipment_api_object["originator_pubkey"])
        
        #verify the signed message:
        hu = HashUtils()
        kmc = KeyManagementClient()
        signed_bytes = bytes.fromhex(shipment_api_object["signed_origin_hash"])
        original_message = shipment_api_object["origin_hash"]
        shipment_api_object.pop("transport_segments", None)
        shipment_api_object.pop("origin_hash", None)
        shipment_api_object.pop("signed_origin_hash", None)
        message = (hu.digest(json.dumps(shipment_api_object, sort_keys=True)).hex())
        #check the origin_hash is actually describing the shiupment
        self.assertTrue(message == original_message)
        #check the signature
        exc = False
        try:
            signature_checks_out = kmc.verify_signed_message(signed_bytes, bytes.fromhex(message), shipment_api_object["originator_pubkey"])
            self.assertTrue(signature_checks_out)      
        except Exception as e:
            print(e)
            exc=True
        self.assertFalse(exc)


if __name__ == '__main__':
    unittest.main()
