import datetime
import json
import unittest
from unittest.mock import patch
from flask import current_app

from app.main.services import db
from app.main.model.shipment import Shipment
from app.main.model.disclosure import Disclosure
from app.main.service.shipment_service import (save_new_shipment, get_a_shipment, delete_shipment, get_all_shipments, prepare_origin_shipment_payload, request_notarisation, notarise, get_proof)
from app.main.service.disclosure_service import save_new_disclosure, update_disclosure, get_a_disclosure
from app.main.service.piece_service import get_a_piece
from app.main.service.user_service import get_all_users
from app.main.util.keymanagementutils import KeyManagementClient
from app.main.service.user_service import get_all_users, get_public_id_from_id
from app.main.util.hashutils import HashUtils
from app.test.controller_test_utils import (get_admin_token, setup_user, register_shipment, update_shipment, register_piece, update_piece, get_a_piece_via_api, register_piece_with_shipment, update_piece_with_shipment, register_planned_transport_segment, get_a_shipment_via_api, delete_a_shipment_via_api)
from app.test.base import BaseTestCase
from app.test.test_auth import login_custom_user
from app.main.config import config_by_name



class TestShipmentModel(BaseTestCase):

    DEBUG = False
    kmc = KeyManagementClient()
    
    @patch('app.main.service.shipment_service.requests.get')
    @patch('app.main.service.shipment_service.requests.post')
    def test_create_shipment_with_piece_and_notarize(self, mock_post, mock_get):
        """
        create a shipment and a connected piece, then start the notarisation process with mock service
        """
        admin_token = get_admin_token()
        shipment_res = register_shipment(self,"some goods",admin_token)
        shipment_data = json.loads(shipment_res.data.decode())
        self.assertTrue(shipment_data['public_id'])
        shipment_id = shipment_data['public_id']
        self.assertTrue(shipment_res.content_type == 'application/json')
        self.assertEqual(shipment_res.status_code, 201)
        piece_res = register_piece_with_shipment(self, "some goods", None, shipment_id, admin_token)
        piece_id = json.loads(piece_res.data.decode("utf-8"))["public_id"]
        shipment = get_a_shipment(shipment_id)
        self.assertTrue(len(shipment.pieces)==1)

        #setup mock services:
        def _proof_payload():
            proof = {
                "merkle_proof": [],
                "merkle_item": "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08",
                "notarised_item": "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08;99472451d5c25fe0cd4812ffe0a52d69f6eece9219b65a11714cc87e5533d833,0;abc3a43ac488fce7797a330e3c666564e643a9a74d72a8d01f40db7d2f915f59,1",
                "notariser_pubkey": "-----BEGIN PUBLIC KEY-----\nMFYwEAYHKoZIzj0CAQYFK4EEAAoDQgAEvTxyE9g7vTRHzaQVa6RYqloZjE3veGVN\nu2BDUhivAXguF2TlcCVcJgJsKgcevHUvbfMdL88BmoHJ6cBvq56j8A==\n-----END PUBLIC KEY-----\n",
                "next_notariser_pubkey": "-----BEGIN PUBLIC KEY-----\nMFYwEAYHKoZIzj0CAQYFK4EEAAoDQgAEvTxyE9g7vTRHzaQVa6RYqloZjE3veGVN\nu2BDUhivAXguF2TlcCVcJgJsKgcevHUvbfMdL88BmoHJ6cBvq56j8A==\n-----END PUBLIC KEY-----\n",
                "signed_notarised_item": "3045022100d93693703b5043850a866c6f20465e2c5a234f4d61ca8fea3fbf52eb2e664edd0220301f640d24737f3fccb71fb44ceda245be4bdf6a9c7472027142117be84c7d18",
                "notarisation_txid": "31933305338914adf51477892d3f94584b92ef9f51b80e3523f515f55be05740"
            }
            return proof
        eonbasics_url =  current_app.config.get("NOTARIZATION_URL")
        eonbasics_email = current_app.config.get("NOTARIZATION_USER")
        eonbasics_pwd = current_app.config.get("NOTARIZATION_PWD")
        proof = _proof_payload()
        get_mock_responses = {
            eonbasics_url+'/notarizationsession/': {'records': [{"status":"new", "public_id":"123opensession"}]},
            eonbasics_url+'/notarizationitem/123newitemid/proof': proof
        }
        post_mock_responses = {
            eonbasics_url+"/auth/login": {'Authorization': 'somelogintoken'},
            eonbasics_url+"/notarizationitem/": {"public_id":"123newitemid"},
            eonbasics_url+"/notarizationsession/123opensession/rpc/lock": {"public_id":"123opensession","merkle_root":"9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08","status":"locked and waiting notarization"},
            eonbasics_url+"/notarizationsession/123opensession/rpc/send": {"public_id":"123opensession","merkle_root":"9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08","status":"closed"}
        }
        def side_effect_GET(url, *args, **kwargs):
            mock_response = unittest.mock.Mock()
            mock_response.status_code = 200
            mock_response.json.return_value = get_mock_responses.get(url, {})
            return mock_response
        def side_effect_POST(url, *args, **kwargs):
            mock_response = unittest.mock.Mock()
            mock_response.status_code = 200
            mock_response.json.return_value = post_mock_responses.get(url, {})
            return mock_response
        mock_get.side_effect = side_effect_GET
        mock_post.side_effect = side_effect_POST
        
        #start notarization tests:
        request_notarisation(shipment_id)
        shipment = get_a_shipment(shipment_id)
        self.assertTrue(shipment.notarisation_session_id=='123opensession')
        self.assertTrue(shipment.notarised_on==None)

        notarise(shipment_id)
        shipment = get_a_shipment(shipment_id)
        self.assertTrue(shipment.notarised_on!=None)

        proof = get_proof(shipment_id)
        self.assertTrue(proof[0]["merkle_item"]=="9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08")
        self.assertTrue(proof[1]==200)

    
        
if __name__ == '__main__':
    unittest.main()
