import datetime
import json
import unittest

from app.main.services import db
from app.main.model.location import Location
from app.main.model.piece import Piece
from app.main.model.product import Product
from app.main.model.productpiececomposition import ProductPieceComposition
from app.main.model.transportsegment import TransportSegment
from app.main.service.location_service import (save_new_location, upsert_a_location, get_a_location, get_all_locations )
from app.main.service.transportsegment_service import (save_new_transport_segment, upsert_a_transport_segment, get_a_transport_segment, get_all_transport_segments )
from app.main.service.product_service import (save_new_product, get_a_product)
from app.main.service.piece_service import (save_new_piece, get_a_piece)
from app.main.util.keymanagementutils import KeyManagementClient
from app.main.util.hashutils import HashUtils
from app.test.base import BaseTestCase
from app.test.controller_test_utils import (get_admin_token, register_company, register_location, register_planned_transport_segment, register_piece, get_a_transport_segment_via_api)
from app.main.util.eonerror import EonError


class TestTransportSegmentModel(BaseTestCase):

    DEBUG = False
    kmc = KeyManagementClient()
    
    def test_create_ts_and_locations(self):
        """
        Setup 2 locations, create two TS between the two and test the relationshps, e.g. 2 arriving_segments in the arrival location
        TODO: this is not using the controller yet
        """
        data = {
            "name":'Malpensa Aeroporto',
            "location_code":'MXP',
            "location_type":'Airport',
            "city_code" : 'MXP',
            "city_name": 'Ferno',
            "country": 'IT',
            "postal_code":'21010',
            "street" : 'Aeroporto di Milano Malpensa'
        }
        resMxp = save_new_location(data)
        self.assertTrue(resMxp[1]==201)
        self.assertTrue(resMxp[0] and resMxp[0]["public_id"])

        data = {
            "name":'Schiphol Airport',
            "location_code":'SCH',
            "location_type":'Airport',
            "city_code" : 'AMS',
            "city_name": 'Amsterdam',
            "country": 'NL',
            "postal_code":'1118',
            "street" : 'Schiphol Airport'
        }
        resSch = save_new_location(data)
        self.assertTrue(resSch[1]==201)
        self.assertTrue(resSch[0] and resSch[0]["public_id"])

        data = {
            "arrival_location_id":resMxp[0]["public_id"],
            "co2_calculation_method":"ballpark",
            "co2_emission_value":10.03,
            "co2_emission_unit":"Kg/m3",
            "departure_location_id":resSch[0]["public_id"],
            "fuel_type":"Kerosene",
            "mode_code":"Air Transport",
            "movement_type":"Planned",
            "seal":"22342345",
            "segment_level":"Flight Leg",
            "transport_date":datetime.datetime.utcnow()
        }
        tsRes = save_new_transport_segment(data)
        self.assertTrue(tsRes[1]==201)
        self.assertTrue(tsRes[0] and tsRes[0]["public_id"])

        #Test relationship with location
        ts = get_a_transport_segment(tsRes[0]["public_id"])
        self.assertTrue(ts.arrival_location.name=='Malpensa Aeroporto')

        #insert another TS and test the relationship from location to TS
        data = {
            "arrival_location_id":resMxp[0]["public_id"],
            "co2_calculation_method":"ballpark",
            "co2_emission_value":11.03,
            "co2_emission_unit":"Kg/m3",
            "departure_location_id":resSch[0]["public_id"],
            "fuel_type":"Kerosene",
            "mode_code":"Air Transport",
            "movement_type":"Planned",
            "seal":"22342346",
            "segment_level":"Flight Leg",
            "transport_date":datetime.datetime.utcnow()
        }
        tsRes2 = save_new_transport_segment(data)

        mxp = get_a_location(resMxp[0]["public_id"])
        #there should be 2 segments with this arrival location:
        self.assertTrue(len(mxp.arriving_segments)==2)
        self.assertTrue(len(mxp.departing_segments)==0)


    def test_create_ts_with_product(self):
        admin_token = get_admin_token()
        compRes = register_company(self,'test', '1234982347569', admin_token)
        company_id = json.loads(compRes.data.decode("utf-8"))["public_id"]

        product_data = {
            "commodity_code":"AAA",
            "commodity_description":"prova",
            "commodity_name":"prova",
            "commodity_type":"prova",
            "manufacturer_company_id":company_id,
            "product_description":"descrizione",
            "product_identifier":"123",
            "un_number":"1A2UD"
        }
        prod_res = save_new_product(product_data)
        prod = get_a_product(prod_res[0]["public_id"])

        piece_data = {
            "additional_security_info":"TEST STRING",
            "coload":False,
            "goods_description":"asd test",
            "gross_weight_value":100.23,
            "gross_weight_um":"Kg",
            "load_type":"X",
            "slac":2,
            "stackable":True,
            "turnable":True,
            "volumetric_weight_conversion_factor_um":"Kgm3",
            "volumetric_weight_conversion_factor_value":2.34,
            "volumetric_weight_chargeable_weight_um":"Kg",
            "volumetric_weight_chargeable_weight_value":110.5,
            "products":[{
                "product_identifier":prod.product_identifier,
                "product_description":prod.product_description,
                "unique_hash":prod.unique_hash
            }]
        }
        piece_res = save_new_piece(piece_data)
        piece = get_a_piece(piece_res[0]["public_id"])
        self.assertTrue(len(piece.products)>0)
        self.assertTrue(piece.products[0].public_id == prod_res[0]["public_id"])
        
        #insert a TS and link it to the link
        loc_a_res = json.loads(register_location(self, 'A', 'MXP','Airport', admin_token).data.decode("utf-8"))
        loc_b_res = json.loads(register_location(self, 'B', 'CDG', 'Airport', admin_token).data.decode("utf-8"))
        loc_a = get_a_location(loc_a_res["public_id"])
        loc_b = get_a_location(loc_b_res["public_id"])

        #insert another TS and test the relationship from location to TS
        data = {
            "arrival_location_id":loc_a.public_id,
            "co2_calculation_method":"ballpark",
            "co2_emission_value":11.03,
            "co2_emission_unit":"Kg/m3",
            "departure_location_id":loc_b.public_id,
            "fuel_type":"Kerosene",
            "mode_code":"Air Transport",
            "movement_type":"Planned",
            "pieces":[{"public_id":piece_res[0]["public_id"]}],
            "seal":"22342346",
            "segment_level":"Flight Leg",
            "transport_date":datetime.datetime.utcnow()
        }
        ts_res = save_new_transport_segment(data)

        #check the relation sizes: product.pieces, piece.product, ts.piece, etc..
        ts = get_a_transport_segment(ts_res[0]["public_id"])
        self.assertTrue(len(ts.pieces)>0)
        self.assertTrue(ts.pieces[0].public_id == piece_res[0]["public_id"]) #test relationship
        self.assertTrue(len(piece.transport_segments)>0)

    def test_create_with_controller(self):
        """
        Create related objects and a transport segment, test time formats on the transport segment date
        """
        admin_token = get_admin_token()
        comp_res = register_company(self,'test', '1234982347569', admin_token)
        company_id = json.loads(comp_res.data.decode("utf-8"))["public_id"]

        loc_a_res = register_location(self, "A", "AAA", "Warehouse", admin_token)
        loc_a_id = json.loads(loc_a_res.data.decode("utf-8"))["public_id"]

        loc_b_res = register_location(self, "B", "BBB", "Warehouse", admin_token)
        loc_b_id = json.loads(loc_b_res.data.decode("utf-8"))["public_id"]

        piece_res = register_piece(self, "some goods", None, None, admin_token)
        piece_id = json.loads(piece_res.data.decode("utf-8"))["public_id"]

        now = datetime.datetime.utcnow()
        #test wrong inbound time format
        date_string = now.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        ts_res = register_planned_transport_segment(self, loc_a_id, loc_b_id, piece_id, date_string, admin_token)
        self.assertTrue(ts_res.status_code==409)
        self.assertTrue(json.loads(ts_res.data.decode("utf-8"))["message"] == "Wrong datetime format, please use UTC %Y-%m-%d %H:%M:%S.%f")

        #test standard python UTC inbound representaion inbound
        date_string = now.strftime("%Y-%m-%d %H:%M:%S.%f")
        ts_res = register_planned_transport_segment(self, loc_a_id, loc_b_id, piece_id, date_string, admin_token)
        ts_id = json.loads(ts_res.data.decode("utf-8"))["public_id"]
        ts = get_a_transport_segment(ts_id)
        self.assertTrue(ts.arrival_location.name=="A")
        self.assertTrue(ts.transport_date==now)

        #test the datetime outbound format
        ts_get_res = get_a_transport_segment_via_api(self, ts_id, admin_token)
        ts_body = json.loads(ts_get_res.data.decode("utf-8"))
        self.assertTrue(ts_body["transport_date"]==date_string)

if __name__ == '__main__':
    unittest.main()
