import datetime
import unittest

from app.main.services import db, flask_bcrypt
from app.main.model.user import User
from app.main.service.user_service import create_admin_first_startup
from app.test.base import BaseTestCase


class TestUserModel(BaseTestCase):

    DEBUG = False
    
    def test_encode_auth_token(self):
        user = User(
            email='test@test.com',
            password='test',
            registered_on=datetime.datetime.utcnow()
        )
        db.session.add(user)
        db.session.commit()
        auth_token = user.encode_auth_token(user.id)
        self.assertTrue(isinstance(auth_token, bytes))

    def test_decode_auth_token(self):
        user = User(
            email='test2@test.com',
            password='test2',
            registered_on=datetime.datetime.utcnow()
        )
        db.session.add(user)
        db.session.commit()
        auth_token = user.encode_auth_token(user.id)
        self.assertTrue(isinstance(auth_token, bytes))
        self.assertTrue(User.decode_auth_token(auth_token.decode("utf-8")) == 1)

    def test_startup_creates_admin(self):
        #the test environment deleted the tables test by test, so the startup admin is destroyed, create it again and test the creation works as intended
        admin = User.query.filter_by(admin=True).first()
        self.assertTrue(admin==None)

        create_admin_first_startup()
        admin = User.query.filter_by(admin=True).first()
        self.assertTrue(admin!=None)

        create_admin_first_startup()
        admins = User.query.filter_by(admin=True).all()
        self.assertTrue(len(admins)==1)

    def test_create_a_user_with_only_pwdhash(self):
        hashed_pass = flask_bcrypt.generate_password_hash("test3").decode('utf-8')
        user = User(
            email='test3@test.com',
            password_hash=hashed_pass,
            registered_on=datetime.datetime.utcnow()
        )
        db.session.add(user)
        db.session.commit()
        auth_token = user.encode_auth_token(user.id)
        self.assertTrue(isinstance(auth_token, bytes))



if __name__ == '__main__':
    unittest.main()
