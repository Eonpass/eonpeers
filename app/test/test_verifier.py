import unittest
import os.path
from flask import current_app

from cryptography.hazmat.primitives import serialization

from app.main.util.verifierutils import Verifier
from app.main.util.keymanagementutils import KeyManagementClient
from app.test.base import BaseTestCase


class TestVerifierFactory(BaseTestCase):

    def test_verifier_factory(self):
        """ Get the current test key, in test env we use the developer KMI which creates a file in the project folder """
        verifier = Verifier()
        #test the loaded type of KMC according to config
        self.assertTrue(verifier._get_verifier()._type == current_app.config['VERIFIER_TYPE'])
 

    def test_std_verifier(self):
        """ Test the signature and verification """
        verifier = Verifier() #defaults to STD verifier, only signature checking
        
        message = '0145eaea'
        kmc = KeyManagementClient()
        result = kmc.sign_message(bytes.fromhex(message))
        priv_key = kmc._get_kmic()._get_key()
        pub_key = priv_key.public_key()
        serialized_pub_key = pub_key.public_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PublicFormat.SubjectPublicKeyInfo
        )
        #check that the public key is the one originating from the file
        self.assertTrue(result['serialized_public'] == serialized_pub_key)
        #check that the signature is correct
        verify_outcome = verifier.verify_payload(result['signed'], bytes.fromhex(message), serialized_pub_key)
        self.assertTrue(verify_outcome)
        

if __name__ == '__main__':
    unittest.main()
