import datetime
import json
import unittest
import base64
import os
import xmlschema

from app.main.services import db
from app.main.model.company import Company
from app.main.model.shipment import Shipment
from app.main.service.company_service import (save_new_company, get_a_company, get_node_owner,mark_company_as_node_owner )
from app.main.service.shipment_service import (save_new_shipment, get_a_shipment, get_all_shipments )
from app.main.service.waybill_service import (save_new_waybill, get_a_waybill, get_waybills_of_a_shipment, upsert_waybill , delete_waybill, export_waybill)
from app.test.controller_test_utils import (register_company, register_shipment, get_admin_token)
from app.main.util.keymanagementutils import KeyManagementClient
from app.main.util.hashutils import HashUtils
from app.test.base import BaseTestCase
from flask import current_app

def register_waybill(self, shipment_id, admin_token):
    filename = os.path.join(os.path.dirname(__file__), 'data/cargowaybill.xml')
    with open(filename, 'rb') as file_in:
        filelines = file_in.read()

    stringfile =  base64.b64encode(filelines).decode('ascii')

    return self.client.post(
        '/waybill/',
        data=json.dumps(dict(
            shipment_id=shipment_id,
            name='074-39541983_P5PE4U7NfdqVKWjKzPb',
            export_reference='1230982340 invoice number',
            waybill_type='eAWB cargo element',
            waybill_file_type='xml',
            base64encodedfilebody=stringfile
        )),
        content_type='application/json',
        headers={'Authorization':('Bearer ' + admin_token)}
    )

def get_proxy_shipment_payload():
    payload = {'name': 'test_shipment', 'custom_reference_number': '467794e862cc26ea7c6536529cd2df3df766369d4e08f2f1a270631135103fc9', 'shipment_request_date': '2020-11-27T13:40:22.782724', 'origin': 'USA', 'destination': 'IT', 'hs_code': '12323345', 'description': 'test shipment a b c', 'current_company_vat': '12345678test_company_1', 'steps': [{'company_vat': '12345678test_company_1', 'position': 1, 'role': 'Production', 'hash_id': '98946994e34352143e62b763b50eb6ced5a0db1785d99836604fbe82de32f952', 'signed_hash': ''}, {'company_vat': '12345678test_company_2', 'position': 2, 'role': 'Logistics operator', 'hash_id': 'b80a55a135e3fbc9eae7f267b28814be7a6f00f1bb146c77dcb5eaf7472b972a', 'signed_hash': ''}]}
    return payload

class TestWaybillModel(BaseTestCase):

    DEBUG = False
    kmc = KeyManagementClient()
    
    def test_create_company_shipment_and_waybill(self):
        with self.client:
            admin_token = get_admin_token()
            company_b_res = register_company(self, "BBB", "4321", admin_token)
            company_b_data = json.loads(company_b_res.data.decode())
            company_b_id = company_b_data['public_id']
            mark_company_as_node_owner(company_b_id)
            shipment_res = register_shipment(self,"some goods", admin_token)
            shipment_data = json.loads(shipment_res.data.decode())
            shipment_id = shipment_data['public_id']

            self.assertTrue(shipment_id)
            waybill_res = register_waybill(self, shipment_id, admin_token)
            waybill_data = json.loads(waybill_res.data.decode())
            self.assertTrue(waybill_data['public_id'])

            waybill = get_a_waybill(waybill_data['public_id'])
            waybill_id = waybill.public_id

            base64_message = waybill.base64encodedfilebody   
            base64_bytes = base64_message.encode('ascii')
            message_bytes = base64.b64decode(base64_bytes) 
            xmlres = xmlschema.XMLResource(message_bytes.decode('ascii'))

            my_schema = xmlschema.XMLSchema('app/main/model/eawb/xsd/schema0.xsd')
            is_valid = my_schema.is_valid(xmlres)
            self.assertTrue(is_valid)

            #try to update the export_reference:
            data = {
                "public_id": waybill.public_id,
                "export_reference":"newreference",
                "shipment_id":shipment_id,
                "name":"074-39541983_P5PE4U7NfdqVKWjKzPb"
            }
            res = upsert_waybill(data)

            waybill = get_a_waybill(waybill_id)
            self.assertTrue(waybill.export_reference=="newreference")

            #delete waybill
            delete_waybill(waybill_id)
            waybill = get_a_waybill(waybill_id)
            self.assertFalse(waybill)


    def test_export_waybill(self):   
        with self.client:
            admin_token = get_admin_token()
            company_b_res = register_company(self, "BBB", "4321", admin_token)
            company_b_data = json.loads(company_b_res.data.decode())
            company_b_id = company_b_data['public_id']
            mark_company_as_node_owner(company_b_id)
            shipment_res = register_shipment(self,"some goods", admin_token)
            shipment_data = json.loads(shipment_res.data.decode())
            shipment_id = shipment_data['public_id']

            self.assertTrue(shipment_id)
            waybill_res = register_waybill(self, shipment_id, admin_token)
            waybill_data = json.loads(waybill_res.data.decode())
            self.assertTrue(waybill_data['public_id'])

            missing_offer=False
            try:
                export_payload = export_waybill(waybill_data['public_id'])
                print(export_payload)
            except Exception as e:
                missing_offer=True 

            self.assertTrue(missing_offer)

            

if __name__ == '__main__':
    unittest.main()
