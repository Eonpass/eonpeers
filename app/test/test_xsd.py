import datetime
import json
import unittest
import xmlschema

from app.main.services import db
from app.test.base import BaseTestCase


class TestCompanyModel(BaseTestCase):

    DEBUG = False
    
    def test_load_xsd(self):
        my_schema = xmlschema.XMLSchema('app/main/model/eawb/xsd/schema0.xsd')
        is_valid = my_schema.is_valid('app/test/data/cargowaybill.xml')
        self.assertTrue(is_valid)
        cargowaybill = my_schema.to_dict('app/test/data/cargowaybill.xml')
        self.assertTrue(cargowaybill.get('origin').get('country') == 'FR')
        

if __name__ == '__main__':
    unittest.main()
